package com.wozart.aura.aura.ui.createautomation

import android.annotation.TargetApi
import android.content.Context
import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wozart.aura.R
import com.wozart.aura.ui.selectdevices.SelectDevicesFragment
import com.wozart.aura.data.model.AutomationModel
import com.wozart.aura.ui.createautomation.AutomationScene
import com.wozart.aura.ui.createautomation.OnFragmentInteractionListener
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.fragment_set_time.*
import kotlinx.android.synthetic.main.layout_header.*
import java.util.*
import org.jetbrains.anko.support.v4.toast
import java.text.SimpleDateFormat


/**
 * Created by Saif on 11/18/2018.
 * Wozart Technology Pvt Ltd
 * mds71964@gmail.com
 */

class SetTimeAutomationFragment : Fragment() {

    private var mListener: OnFragmentInteractionListener? = null
    private var selectedWeekDays = HashMap<String, Boolean>()
    private var automationScene = AutomationScene()
    private var automationSceneType: String? = null
    private var globalStartTime: String? = null
    private var timeformat: String? = null
    private var timeId: String? = null


    companion object {
        private var scheduleName: String? = null
        private var scheduleType: String? = null
        private var automationSceneIcon: Int = 0
        private var automationNameOld: String? = null
        private var automationScheduleType: String? = null
        private var automationEnable: Boolean = false
        fun newInstance(automationName: String, type: String, icon: Int, automationSceneNameOld: String, automationSceneType: String, automationenable: Boolean): SetTimeAutomationFragment {
            scheduleName = automationName
            scheduleType = type
            automationSceneIcon = icon
            automationNameOld = automationSceneNameOld
            automationScheduleType = automationSceneType
            automationEnable = automationenable
            return SetTimeAutomationFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_set_time, container, false)
        var inputStart = view.findViewById<TextView>(R.id.inputStart)
        var inputEnd = view.findViewById<EditText>(R.id.inputEnd)

        if (context is CreateAutomationActivity) {
            automationScene = (context as CreateAutomationActivity).getAutomationScene()
            automationSceneType = (context as CreateAutomationActivity).getAutomationSceneType()

        }
        if (automationSceneType == "edit") {

            inputStart.setText(automationScene.endTime)

        }

        return view
    }


    @TargetApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun init() {
        val timeZone = TimeZone.getDefault()
        timeId = timeZone.id
        Log.d("TIME_ZOME", timeId)

        val calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault())
        val currentLocalTime = calendar.getTime()
        val date = SimpleDateFormat("Z")
        val localTime = date.format(currentLocalTime)
        var offsetTimetype = localTime[0]
        var offsetTimeHours = localTime.substring(1, 3).toInt()
        var offsetTimeMinutes = localTime.substring(3).toInt()

        var linear_time_layout = view!!.findViewById<RelativeLayout>(R.id.linear_time_layout)
        linear_time_layout.visibility = View.VISIBLE
        var schedule_time = view!!.findViewById<TextView>(R.id.schedule_time)
        var time_picker = view!!.findViewById<TimePicker>(R.id.time_picker)
        if (automationSceneType == "edit") {
            var times = automationScene.endTime!!.replace("A", "").replace("P", "").replace("M", "").replace(" ", "").split(":")
            var editHour = times[0].toInt()
            var editMinute = times[1].toInt()
            time_picker.minute = editMinute.toInt()
            if (automationScene.endTime!!.contains("P")) {
                time_picker.hour = editHour.toInt() + 12
            }
            var timeshow = automationScene.endTime!!
            schedule_time.text = "Scheduled time is" + "$timeshow"
        } else {
            var hour = time_picker.hour
            var minute = time_picker.minute

            if (hour > 12) {
                hour -= 12
                inputStart.text = "${hour} " + ": $minute PM"
            } else {
                inputStart.text = "${hour} " + ": $minute AM"
            }
        }

        time_picker.setOnTimeChangedListener { timePicker, hour, minutes ->
            val StrMin = String.format("%02d", minutes)
            val timeHours24 = hour
            val timeMinutes24 = minutes
            var totalTime = timeHours24 * 60 + timeMinutes24
            var totalOffset = offsetTimeHours * 60 + offsetTimeMinutes
            var gmtTime = 0

            if (offsetTimetype == '+') {
                if (totalTime > totalOffset) {
                    gmtTime = totalTime - totalOffset
                } else {
                    gmtTime = 1440 - totalOffset + totalTime
                }
            } else {
                gmtTime = totalTime + totalOffset
                if (gmtTime > 1440) {
                    gmtTime = gmtTime - 1440
                }
            }


            timeformat = String.format("%02d", (gmtTime / 60)) + ":" + String.format("%02d", (gmtTime % 60))
            val StrHr = String.format("%02d", getHourAMPM(hour))
            globalStartTime = "$StrHr:$StrMin:${getAMPM(hour)}"
            inputStart.text = "${getHourAMPM(hour)} " + ": $StrMin ${getAMPM(hour)}"

            Log.d("TIMEZONE", "SElected Time : " + timeformat)

            schedule_time.text = "Schedule time is " + "${inputStart.text}"
        }

        tvTitle.setTextColor(ContextCompat.getColor(activity!!.baseContext, R.color.black))
        tvNext.setTextColor(ContextCompat.getColor(activity!!.baseContext, R.color.black))
        home.setColorFilter(ContextCompat.getColor(activity!!.baseContext, R.color.black), PorterDuff.Mode.SRC_ATOP);
        tvTitle.text = getString(R.string.title_set_time)
        home.setOnClickListener { mListener?.onHomeBtnClicked() }
        tvNext.setOnClickListener {

            openNextScreen()
        }
        tvSunday.setOnClickListener {
            changeTvBg(tvSunday, Constant.SUNDAY)
        }
        tvMonday.setOnClickListener {
            changeTvBg(tvMonday, Constant.MONDAY)
        }
        tvTuesday.setOnClickListener {
            changeTvBg(tvTuesday, Constant.TUESDAY)
        }
        tvWednesday.setOnClickListener {
            changeTvBg(tvWednesday, Constant.WEDNESDAY)
        }
        tvThursday.setOnClickListener {
            changeTvBg(tvThursday, Constant.THURSDAY)
        }
        tvFriday.setOnClickListener {
            changeTvBg(tvFriday, Constant.FRIDAY)
        }
        tvSaturday.setOnClickListener {
            changeTvBg(tvSaturday, Constant.SATURDAY)
        }


        initMap() //Just for testing
    }

    private fun getAMPM(hour: Int): String {
        return if (hour > 11) "PM" else "AM"
    }

    private fun getHourAMPM(hour: Int): Int {
        var modifiedHour = if (hour > 11) hour - 12 else hour
        if (modifiedHour == 0) {
            modifiedHour = 12
        }
        return modifiedHour

    }


    private fun initMap() {
        if (automationSceneType == "edit") {
            val type = object : TypeToken<HashMap<String, Boolean>>() {}.type
            val gson = Gson()
            val routine: HashMap<String, Boolean>
            routine = gson.fromJson(automationScene.routine, type)
            selectedWeekDays[Constant.SUNDAY] = routine["Sunday"]!!
            selectedWeekDays[Constant.MONDAY] = routine["Monday"]!!
            selectedWeekDays[Constant.TUESDAY] = routine["Tuesday"]!!
            selectedWeekDays[Constant.WEDNESDAY] = routine["Wednesday"]!!
            selectedWeekDays[Constant.THURSDAY] = routine["Thursday"]!!
            selectedWeekDays[Constant.FRIDAY] = routine["Friday"]!!
            selectedWeekDays[Constant.SATURDAY] = routine["Saturday"]!!
            updateAll()
        } else {
            selectedWeekDays[Constant.SUNDAY] = true
            selectedWeekDays[Constant.MONDAY] = true
            selectedWeekDays[Constant.TUESDAY] = true
            selectedWeekDays[Constant.WEDNESDAY] = true
            selectedWeekDays[Constant.THURSDAY] = true
            selectedWeekDays[Constant.FRIDAY] = true
            selectedWeekDays[Constant.SATURDAY] = true
            updateAll()
        }

    }

    private fun updateAll() {
        updateView(tvSunday, Constant.SUNDAY)
        updateView(tvMonday, Constant.MONDAY)
        updateView(tvTuesday, Constant.TUESDAY)
        updateView(tvWednesday, Constant.WEDNESDAY)
        updateView(tvThursday, Constant.THURSDAY)
        updateView(tvFriday, Constant.FRIDAY)
        updateView(tvSaturday, Constant.SATURDAY)
    }


    private fun changeTvBg(textView: TextView, dayKey: String) {
        //This is a way to change bg but based on business logic check logic {selectWeekDays -> Hashmap) can change}
        activity?.let {
            val status = selectedWeekDays[dayKey]
            if (status!!) {
                selectedWeekDays[dayKey] = false
                textView.background = ContextCompat.getDrawable(activity!!, R.drawable.circle_out_bg)
                textView.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
            } else {
                selectedWeekDays[dayKey] = true
                textView.background = ContextCompat.getDrawable(activity!!, R.drawable.circle_in_bg)
                textView.setTextColor(ContextCompat.getColor(activity!!, R.color.white))
            }
        }
    }

    private fun updateView(textView: TextView, dayKey: String) {
        //This is a way to change bg but based on business logic check logic {selectWeekDays -> Hashmap) can change}
        activity?.let {
            val status = selectedWeekDays[dayKey]
            if (status!!) {
                textView.background = ContextCompat.getDrawable(activity!!, R.drawable.circle_in_bg)
                textView.setTextColor(ContextCompat.getColor(activity!!, R.color.white))
            } else {
                textView.background = ContextCompat.getDrawable(activity!!, R.drawable.circle_out_bg)
                textView.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
            }
        }
    }

    private fun openNextScreen() {
        var roomList = arrayListOf<RoomModel>()
        var time1 = inputStart.text.toString()
        if (timeformat == null) {
            toast("Plase select time")
        } else {
            var schedule = AutomationModel(scheduleName, automationSceneIcon, roomList, automationEnable, timeformat, scheduleType, time1, selectedWeekDays.toString(), "")

            mListener?.navigateToFragment(SelectDevicesFragment.newInstance(schedule, automationNameOld!!, automationScheduleType!!))
        }

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnAutomationListInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

}
