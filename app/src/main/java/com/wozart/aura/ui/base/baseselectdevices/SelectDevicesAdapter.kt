package com.wozart.aura.ui.base.baseselectdevices

import android.database.sqlite.SQLiteDatabase
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.animation.AnimationUtils
import com.wozart.aura.R
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.aura.utilities.Utils.getIconDrawable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.ui.createautomation.baseadapters.BaseDevicesAdapter
import com.wozart.aura.ui.dashboard.Device
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.item_select_device.view.*

class SelectDevicesAdapter(deviceList: ArrayList<Device>) : BaseDevicesAdapter(deviceList) {

    override fun customizeUI(itemView: View?, device: Device) {
        itemView?.let {
            device.isSelected = device.isTurnOn
            itemView.deviceStatus.visibility = View.GONE
            itemView.imgSelect?.background = ContextCompat.getDrawable(itemView.context, getDrawableId(device.isTurnOn))

           // itemView.deiceIcon.setImageResource(getIconDrawable(device, device.isSelected))


        }
    }

    override fun getLayoutType(): Int {
        return R.layout.item_select_device
    }

    override fun registerListeners(holder: DeviceHolder?, position: Int) {
        /*val clickAnimation = AnimationUtils.loadAnimation(holder?.itemView?.context,R.anim.bounce);
        val interpolator = BounceInterpolator(0.2, 20.0)
        clickAnimation.setInterpolator(interpolator)*/
        val zoomin = AnimationUtils.loadAnimation(holder?.itemView?.context, R.anim.zoomin)
        val zoomout = AnimationUtils.loadAnimation(holder?.itemView?.context, R.anim.zoomout)


        holder?.itemView?.deviceCard?.setOnClickListener {
            val res = holder.itemView.context.resources

            holder.itemView.deviceCard.startAnimation(zoomin)
            holder.itemView.deviceCard.startAnimation(zoomout)
            var device = deviceList[position]
            device.isSelected = !device.isSelected
            holder.itemView.imgSelect?.background = ContextCompat.getDrawable(holder.itemView.context, getDrawableId(device.isSelected))
            if (device.isSelected)
                holder.itemView.deviceCard.setCardBackgroundColor(res.getColor(R.color.white))
            else holder.itemView.deviceCard.setCardBackgroundColor(res.getColor(R.color.list_item_inactive))
            holder.itemView.deiceIcon.setImageResource(getIconDrawable(device.type, device.isSelected))
        }
    }

    private fun getDrawableId(selected: Boolean): Int {
        if (selected) {
            return R.drawable.filled_circle
        } else {
            return R.drawable.ic_round_stroke
        }
    }

}