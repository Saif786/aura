package com.wozart.aura.ui.adapter

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.sqlite.SQLiteDatabase
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import aura.wozart.com.aura.entity.amazonaws.models.nosql.ThingTableDO
import aura.wozart.com.aura.entity.network.Nsd
import com.google.gson.Gson
import com.wozart.aura.R
import com.wozart.aura.data.device.IpHandler
import com.wozart.aura.data.dynamoDb.DeviceTableHandler
import com.wozart.aura.data.dynamoDb.ThingTableHandler
import com.wozart.aura.data.model.CustomizationList
import com.wozart.aura.data.model.IpModel
import com.wozart.aura.data.model.ThingError
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.entity.network.ConnectTask
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.home.ConfigureLoadActivity
import com.wozart.aura.utilities.JsonHelper
import java.util.ArrayList
import kotlinx.android.synthetic.main.item_customization_list.view.*
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.longToast
import org.jetbrains.anko.startActivity
import kotlin.concurrent.thread


class CustomizationAdapter(private var context: Context, private val customizationArrayList: ArrayList<CustomizationList>, private var type: Boolean, onDeviceDeleted: onDeviceDeletedListener,private var auraSwitches: MutableList<AuraSwitch>) : RecyclerView.Adapter<CustomizationAdapter.CustomizationViewHolder>(),CustomizationItemAdapter.onItemDeletedListener,ConnectTask.TcpMessageReceiver  {


    private var deviceListener: onDeviceDeletedListener = onDeviceDeleted
    var thing: ThingTableDO? = null
    var mDbUtils: SQLiteDatabase? = null


    override fun onItemDelete(name:String,ip:String,uiud: String) {
        deviceListener.onDeviceDeleted(name,ip,uiud)
    }

    class CustomizationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomizationViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_customization_list, parent, false)
        val dbUtils = UtilsDbHelper(parent.context)
        mDbUtils = dbUtils.writableDatabase

        return CustomizationViewHolder(view)
    }

        override fun onTcpMessageReceived(message: String) {
          var data = message

         }

    override fun onBindViewHolder(holder: CustomizationViewHolder, position: Int) {
        val customizationModel = customizationArrayList[position]
        holder.itemView?.customization_label?.text = customizationModel.customizationLabel

        //recycler view for items
        holder.itemView?.item_recycler_view?.setHasFixedSize(true)
        holder.itemView?.item_recycler_view?.isNestedScrollingEnabled = false

        /* set layout manager on basis of recyclerview enum type */

        val linearLayoutManager = LinearLayoutManager(holder.itemView.context, LinearLayoutManager.VERTICAL, false)
        holder.itemView?.item_recycler_view?.layoutManager = linearLayoutManager
        val adapter = CustomizationItemAdapter(context,position, this, customizationModel.customizationLabel, customizationModel.itemArrayList,type,this,auraSwitches)
        holder.itemView?.item_recycler_view?.adapter = adapter
    }

    override fun getItemCount(): Int {
        return customizationArrayList.size
    }

    interface onDeviceDeletedListener {
        fun onDeviceDeleted(name:String,ip:String,uiud:String)
    }

     }