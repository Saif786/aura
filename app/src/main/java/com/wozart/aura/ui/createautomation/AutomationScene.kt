package com.wozart.aura.ui.createautomation

class AutomationScene{
    var name  : String? = null
    var icon : Int = 0
    var room : ArrayList<RoomModel> = ArrayList()
    var load : MutableList<RoomModel> = ArrayList()
    var property : MutableList<GeoModal> = ArrayList()
    var time : String? =null
    var type : String? =null
    var endTime : String? = null
    var routine : String? = null
    var home :String ?= null

}