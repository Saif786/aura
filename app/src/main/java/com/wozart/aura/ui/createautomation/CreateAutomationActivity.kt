package com.wozart.aura.aura.ui.createautomation

import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import com.wozart.aura.R
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.sqlLite.ScheduleTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.sql.scheduling.ScheduleDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.base.BaseAbstractActivity
import com.wozart.aura.ui.createautomation.AutomationScene
import com.wozart.aura.ui.createautomation.OnFragmentInteractionListener
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.activity_create_automation.*


/**
 * Created by Saif on 11/10/2018.
 * Wozart Technology Pvt Ltd
 * mds71964@gmail.com
 */

class CreateAutomationActivity : BaseAbstractActivity(), OnFragmentInteractionListener {

    private var automationSceneName: String? = null
    private var automstionSceneType: String? = null
    private var automationIconUrl: Int ?= 0
    private val localSqlDatabaseSchedule = ScheduleTable()
    private var mDBAutomation: SQLiteDatabase? =null
    private var automationScene = AutomationScene()
    private var mDbUtils: SQLiteDatabase? = null
    private val localSqlUtils = UtilsTable()

    override fun onRoomBtnClicked() {
        TODO("not implemented")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_automation)
        val dbUtils = UtilsDbHelper(this)
        mDbUtils = dbUtils.writableDatabase
        //Utils.setDrawable(this, containerAutomation)
        var listRoom: MutableList<RoomModelJson> = ArrayList()
        listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")

        for(x in listRoom){
            if(x.name == Constant.HOME) {

                Utils.setDrawable(this, containerAutomation, x.bgUrl.toInt())

            }

        }

        val intent = intent
        automstionSceneType =intent.getStringExtra("automationNameType")
        automationSceneName= intent.getStringExtra("input_name")
        automationIconUrl= intent.getIntExtra("automationIconUrl",0)


        val dbAutomation = ScheduleDbHelper(this)
        mDBAutomation = dbAutomation.writableDatabase

        if(automstionSceneType == "edit"){
            automationScene=localSqlDatabaseSchedule.getAutomationSceneByName(mDBAutomation!!,automationSceneName!!)
        }

        init()
    }

    private fun init() {

        navigateToFragment(CreateAutomationFragment(), getString(R.string.empty_tag), true, true)
    }

    override fun onHomeBtnClicked() {
        onBackPressed()
    }
    fun getSceneIconUrl() : Int?{
        return automationIconUrl
    }
    fun getAutomationSceneName() : String? {
        return automationSceneName
    }
    fun getAutomationSceneType() : String? {
        return automstionSceneType
    }
    fun getAutomationScene(): AutomationScene{
        return automationScene
    }


}
