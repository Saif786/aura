package com.wozart.aura.ui.auraswitchlist

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.sqlite.SQLiteDatabase
import android.net.nsd.NsdServiceInfo
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.RelativeLayout
import aura.wozart.com.aura.entity.amazonaws.models.nosql.ThingTableDO
import aura.wozart.com.aura.entity.network.Nsd
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.Encryption
import com.google.gson.Gson
import com.wozart.aura.R
import com.wozart.aura.aura.ui.auraswitchlist.OnAdapterInteractionListener
import com.wozart.aura.ui.home.ConfigureLoadActivity
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.device.IpHandler
import com.wozart.aura.data.dynamoDb.DeviceTableHandler
import com.wozart.aura.data.dynamoDb.ThingTableHandler
import com.wozart.aura.data.model.DeviceUiud
import com.wozart.aura.data.model.IpModel
import com.wozart.aura.data.model.ThingError
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.entity.model.aura.AuraSwitchLoad
import com.wozart.aura.entity.model.aura.AuraType
import com.wozart.aura.entity.network.ConnectTask
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.base.BaseAbstractActivity
import com.wozart.aura.ui.dashboard.GridAutoFitLayoutManager
import com.wozart.aura.utilities.JsonHelper
import kotlinx.android.synthetic.main.activity_aura_list.*
import kotlinx.android.synthetic.main.dialog_aura_device_code.*
import okhttp3.*
import org.jetbrains.anko.*
import org.jetbrains.anko.design.longSnackbar
import org.json.JSONObject
import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import kotlin.concurrent.thread
import kotlin.system.exitProcess


class AuraListActivity : BaseAbstractActivity(), OnAdapterInteractionListener, AnkoLogger, ConnectTask.TcpMessageReceiver {

    private var adapter: AuraListAdapter = AuraListAdapter(this)
    private var auraSwitches: MutableList<AuraSwitch> = ArrayList()
    var deviceSet = mutableSetOf<String>()
    var keysSetFlags = ArrayList<Int>()
    private lateinit var Nsd: Nsd
    private var jsonHelper: JsonHelper = JsonHelper()
    private val deviceDynamoDb = DeviceTableHandler()
    private val thingDynamoDb = ThingTableHandler()
    private val localSqlDatabase = DeviceTable()
    private var mDb: SQLiteDatabase? = null
    private var UIUD: String? = null
    private var CLOUD_CONNECT = false
    private var nsdServiceData: MutableList<NsdServiceInfo> = ArrayList()
    private var resetAllKeysFlag: Boolean = false
    var thing: ThingTableDO? = null
    var allAwsKeys: MutableList<String> = ArrayList()
    lateinit var configuredIpAddress: String
    var reconnectTries: Int = 0
    var UiudList: MutableList<DeviceUiud> = ArrayList()

    //utils table for ip list
    private var mDbUtils: SQLiteDatabase? = null
    private val localSqlUtils = UtilsTable()
    private var IP = IpHandler()
    var urlType : String ?= null


    var ERROR_TAG = "ERRORCONFIGURE"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aura_list)
        val dbHelper = DeviceDbHelper(this)
        mDb = dbHelper.writableDatabase
        init()
    }

    fun init() {

        //initilizing utlis table for getting ip list
        val dbUtils = UtilsDbHelper(this)
        mDbUtils = dbUtils.writableDatabase

        Nsd = Nsd(this, "WIFI")
        back.setOnClickListener {
            this.finish()
        }
        imgScan.setOnClickListener { scan() }

        adapter.init(auraSwitches)
        //listSwitches.layoutManager = LinearLayoutManager(this)
        //val cellSize = resources.getDimensionPixelSize(R.dimen.wifi_device_item_size)
        listSwitches.layoutManager = GridAutoFitLayoutManager(this, resources.getDimensionPixelSize(R.dimen.wifi_device_item_size))
        listSwitches.adapter = adapter

        if (!Nsd.getDiscoveryStatus()) {
            Nsd.setBroadcastType("WIFI")
            Nsd.initializeNsd()
            Nsd.discoverServices()
        } else {
            Nsd.setBroadcastType("WIFI")
        }

        LocalBroadcastManager.getInstance(this!!).registerReceiver(
                onNSDServiceResolved, IntentFilter("nsdDiscoverWifi"))

        imgScan.visibility = View.INVISIBLE
        val nsdDiscoveryHandler = Handler()
        nsdDiscoveryHandler.postDelayed({
            onCompletionOfScan()
        }, 4000)
    }

    private fun nsdDiscovery() {
        nsdServiceData.clear()
        for (service in Nsd.getServiceInfo()!!) {
            var newDeviceFlag = true
            for (data in nsdServiceData) {
                if (service == data) {
                    newDeviceFlag = false
                }
            }
            if (newDeviceFlag) {
                nsdServiceData.add(service)
                try {
                    if (UIUD == null) UIUD = Constant.UNPAIRED
                    ConnectTask(this, this, jsonHelper.initialData(UIUD!!), service.host.hostAddress, "XXXXXX").execute("")
                    info { "Initial data: " + jsonHelper.initialData(UIUD!!) + " to " + service.serviceName + " IP: " + service.host.hostAddress }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        val nsdDiscoveryHandler = Handler()
        nsdDiscoveryHandler.postDelayed({
            onCompletionOfScan()
        }, 2000)
    }

    /**
     * Data from the TcpServer : Asynchronous
     */
    private val onNSDServiceResolved = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d("NSD_DEBUG_WIFI", "nsd onReceive in WiFIFragment")
            val name = intent.getStringExtra("name")
            val ip = intent.getStringExtra("ip")
            var deviceuiud = DeviceUiud()
            deviceuiud!!.name = name
            deviceuiud!!.ip = ip
            deviceuiud!!.uiud = Constant.UNPAIRED
            UiudList.add(deviceuiud)
            try {
                if(name.endsWith("6010D4")){
                    urlType = "Scan Aura"
                    typeUrl(Constant.UNPAIRED,ip, urlType!!)
                }else{
                    sendTcpConnect(Constant.UNPAIRED, ip, name)
                }
            } catch (e: Exception) {
                Log.e("WIFI_LOG", "Error : " + e.stackTrace)
            }
        }
    }

    private fun sendTcpConnect(uiud: String, ip: String, name: String) {
        ConnectTask(this!!, this, jsonHelper.initialData(uiud), ip, name).execute("")
    }

    private fun typeUrl(uiudDevice: String,ip:String,name:String){
        thread {
            var encryptedData :String ?= null
            if(name == "Scan Aura"){
                var json = jsonHelper.initialData(uiudDevice)
                encryptedData = Encryption.encryptMessage(json)
            }else{
                if(name == "Pairing"){
                    encryptedData = Encryption.encryptMessage(uiudDevice)
                }
            }
            val url = URL("http://$ip/android")
            val connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "PUT"
            connection.connectTimeout = 300000
            connection.connectTimeout = 300000
            connection.doOutput = true

            connection.setRequestProperty("charset", "utf-8")
            connection.setRequestProperty("Content-lenght", encryptedData)
            connection.setRequestProperty("Content-Type", "application/json")
            connection.setRequestProperty("authorization", "Bearer teNa5F6AGgIKwxg54fOzJRjNoWyGZaCv")

            try {
                val outputStream: DataOutputStream = DataOutputStream(connection.outputStream)
                outputStream.write(encryptedData!!.toByteArray())
                outputStream.flush()
            } catch (exception: Exception) {

            }
            if (connection.responseCode == HttpURLConnection.HTTP_OK) {
                try {
                    val reader: BufferedReader = BufferedReader(InputStreamReader(connection.inputStream))
                    val output: String = reader.readLine()
                    val decryptedData = Encryption.decryptMessage(output)
                    updateMessage(decryptedData)
                } catch (exception: Exception) {
                    Log.d("ERROR", "Exception while push the Notification")
                }
            }else{
                Log.d("error", "check connection")
                exitProcess(0)
            }
        }
    }

    private fun scan() {
        auraSwitches.clear()
        onStartingScan()
        if (!Nsd.getDiscoveryStatus()) {
            Nsd.setBroadcastType("WIFI")
            Nsd.initializeNsd()
            Nsd.discoverServices()
        } else {
            Nsd.setBroadcastType("WIFI")
            Nsd.stopDiscovery()
            Nsd.initializeNsd()
            Nsd.discoverServices()
        }
        val nsdDiscoveryHandler = Handler()
        nsdDiscoveryHandler.postDelayed({
            onCompletionOfScan()
        }, 3000)
    }

    private fun onCompletionOfScan() {
        progress_bar.visibility = View.INVISIBLE
        imgScan.visibility = View.VISIBLE
    }

    private fun onStartingScan() {
        progress_bar.visibility = View.VISIBLE
        imgScan.visibility = View.INVISIBLE
        tvScanSwitch.visibility = View.INVISIBLE
    }

    private fun placeScanUi() {
        val params = imgScan.layoutParams as RelativeLayout.LayoutParams
        if (auraSwitches.size > 0) {
            tvScanSwitch.visibility = View.INVISIBLE
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)

            params.setMargins(0, 0, 0, 100)

        } else {
            tvScanSwitch.visibility = View.VISIBLE
            params.addRule(RelativeLayout.CENTER_IN_PARENT)
        }
        imgScan.layoutParams = params
    }

    override fun onSelectAuraDevice(switch: AuraSwitch) {
        openDialog(switch)
    }

    private fun openDialog(device: AuraSwitch) {

        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_aura_device_code)
        dialog.tvDialogTitle.text = device.name
        if (device.aws == 1) {
            dialog.switchConnect.visibility = View.INVISIBLE
            dialog.tvConnectAws.visibility = View.INVISIBLE
            dialog.tvMore.visibility = View.INVISIBLE
            CLOUD_CONNECT = false
        } else {
            dialog.switchConnect.visibility = View.VISIBLE
            dialog.tvConnectAws.visibility = View.VISIBLE
            dialog.tvMore.visibility = View.VISIBLE
        }

        dialog.btnSubmt.setOnClickListener {
            Utils.hideSoftKeyboard(this)
            val pindata = dialog.inputDeviceCode.value.toString()
            val encryptedPin = Encryption.SHA256(dialog.inputDeviceCode.value.toString())
            UIUD = Encryption.generateUIUD(device.id!!)
            val data = jsonHelper.pairingData(UIUD!!, encryptedPin)
            Log.d("NULLPOINTER", "$data")
            if(device.name.endsWith("6010D4")){
                urlType = "Pairing"
                typeUrl(data,device.ip!!, urlType!!)
            }else{
                sendTcpData(data, device.ip!!, device.name)
            }

            if (device.aws == 0) {
                if (dialog.switchConnect.isChecked) {
                    CLOUD_CONNECT = true
                } else {
                    CLOUD_CONNECT = false
                }
            }
            //ConnectTask(this, this, data, device.ip!!,device.name).execute("")
            dialog.cancel()
        }
        dialog.setOnCancelListener {
            hideSoftKeyBoard()
        }
        dialog.show()
    }

    fun sendTcpData(data: String, ip: String, name: String) {
        ConnectTask(this, this, data, ip, name).execute("")
    }

    /**
     * Transferring Keys to Device
     */
    private fun getKeys(device: AuraSwitch) {
        resetAllKeysFlag = true
        keysSetFlags.add(0)
        keysSetFlags.add(1)
        keysSetFlags.add(2)
        configuredIpAddress = device.ip!!
        reconnectTries = 0
        ConnectTask(this, this, "{\"type\":8, \"set\":1}", device.ip!!, device.name).execute("")

        thread {
            try {
                val thingAlreadyExists = deviceDynamoDb.checkThing(UIUD!!.substring(0, Math.min(UIUD!!.length, 12)))
                if (thingAlreadyExists!!.error == "error") {
                    if (thingAlreadyExists.version == "error") {
                        progress_bar.visibility = View.INVISIBLE
                        longToast("No internet Please check!!")
                        val gson = Gson()
                        startActivity<ConfigureLoadActivity>("DEVICE" to gson.toJson(device), "UIUD" to UIUD)
                        this.finish()
                    } else {
                        if (thingAlreadyExists.thing == null) {
                            thing = thingDynamoDb.searchAvailableDevices()!!

                        } else {
                            thing = thingDynamoDb.thingDetails(thingAlreadyExists.thing!!)
                        }
                        allAwsKeys.clear()
                        val data = jsonHelper.awsRegionThing(thing!!.region!!, thing!!.thing!!)
                        allAwsKeys.add(data)
                        val certs = jsonHelper.certificates(thing!!.certificate!!)
                        allAwsKeys.addAll(certs)
                        val keys = jsonHelper.privateKeys(thing!!.privateKey!!)
                        allAwsKeys.addAll(keys)
                        allAwsKeys.add(data)
                        longSnackbar(findViewById(android.R.id.content), "Communicating Cloud...")
                        sendDataAllKeys(allAwsKeys as java.util.ArrayList<String>, device)
                        //store to local sql
                        var localThing = ThingError()
                        localThing.region = thing!!.region
                        localThing.privateKey = thing!!.privateKey
                        localThing.certificate = thing!!.certificate
                        localThing.thing = thing!!.thing
                        localThing.version = "Version1"
                        localThing.error = "success"
                        localSqlUtils.replaceThing(mDbUtils!!, device.name, localThing)
                    }
                } else {
                    allAwsKeys.clear()
                    val data = jsonHelper.awsRegionThing(thingAlreadyExists.region!!, thingAlreadyExists.thing!!)
                    allAwsKeys.add(data)
                    val certs = jsonHelper.certificates(thingAlreadyExists.certificate!!)
                    allAwsKeys.addAll(certs)
                    val keys = jsonHelper.privateKeys(thingAlreadyExists!!.privateKey!!)
                    allAwsKeys.addAll(keys)
                    allAwsKeys.add(data)
                    sendDataAllKeys(allAwsKeys as java.util.ArrayList<String>, device)
                    //store to local sql
                    var localThing = ThingError()
                    localThing.region = thingAlreadyExists!!.region
                    localThing.privateKey = thingAlreadyExists!!.privateKey
                    localThing.certificate = thingAlreadyExists!!.certificate
                    localThing.thing = thingAlreadyExists!!.thing
                    localThing.version = "Version2"
                    localThing.error = "success"
                    localSqlUtils.replaceThing(mDbUtils!!, thingAlreadyExists!!.thing!!, localThing)
                }
            } catch (e: Exception) {
                progress_bar.visibility = View.INVISIBLE
                longSnackbar(findViewById(android.R.id.content), "Thing not matched $thing,Please Consult the developer $thing")
                Log.d(ERROR_TAG, "getKeys : " + e)
                //error { "Error: ${e.printStackTrace()}" }
            }
        }
    }


    private fun getKeysMissing(reg: Int, cert: Int, keys: Int) {
        val runnable = Runnable {
            try {

                var data = ArrayList<String>()
                //   allAwsKeys.add(data)   thing
                if (reg == 0) {
                    data.add(allAwsKeys[0])
                }

                for (i in 0..19) {
                    val d = (cert shr i) and 1
                    if (d % 2 == 0) {
                        data.add(allAwsKeys[i + 1])
                    }
                }
                for (i in 0..26) {
                    val d = (keys shr i) and 1
                    if (d % 2 == 0) {
                        data.add(allAwsKeys[i + 21])
                    }
                }

                sendDataAllKeysMissing(data)
            } catch (e: Exception) {
                progress_bar.visibility = View.INVISIBLE

                longSnackbar(findViewById(android.R.id.content), "Keys are missing,Please reset the device and try again")
                Log.d(ERROR_TAG, "getKeysMissing : " + e)
                //error { "Error: ${e.printStackTrace()}" }
            }
        }
        val getAvailableDevices = Thread(runnable)
        getAvailableDevices.start()
    }


    private fun sendCertificate(device: AuraSwitch, KeysAndCertificates: ThingTableDO) {
        var nameRegion: String
        val runnable = Runnable {
            try {
                nameRegion = jsonHelper.awsRegionThing(KeysAndCertificates.region!!, KeysAndCertificates.thing!!)
                ConnectTask(this, this, nameRegion, device.ip!!, device.name).execute("")
                val data = jsonHelper.certificates(KeysAndCertificates.certificate!!)
                sendData(data, "Certificate", device)
                info { "Send Certificate Keys$data" }
            } catch (e: Exception) {
                Log.d(ERROR_TAG, "sendCertificate : " + e)
                //error { "Error: $e" }
            }
        }
        val sendCertificates = Thread(runnable)
        sendCertificates.start()
    }

    private fun sendKeys(device: AuraSwitch, KeysAndCertificates: ThingTableDO) {
        val runnable = Runnable {
            try {
                val data = jsonHelper.privateKeys(KeysAndCertificates.privateKey!!)
                sendData(data, "PrivateKey", device)
                info { "Send Private Keys$data" }
            } catch (e: Exception) {
                Log.d(ERROR_TAG, "sendKeys : " + e)
                //error { "Error: ${e.printStackTrace()}" }
            }
        }
        val sendCertificates = Thread(runnable)
        sendCertificates.start()
    }

    private fun sendData(data: java.util.ArrayList<String>, whatData: String, device: AuraSwitch) {
        for (key in data) {
            try {
                ConnectTask(this, this, key, device.ip!!, device.name).execute("")
                Thread.sleep(100)
            } catch (e: InterruptedException) {
                //error { "Error: ${e.printStackTrace()}" }
                Log.d(ERROR_TAG, "sendData : " + e)
            }

        }

        if (whatData == "PrivateKey") {
            resetAllKeysFlag = false
            ConnectTask(this, this, "{\"type\":8, \"set\":0}", device.ip!!, "XXXXXX").execute("")
        }
    }

    private fun sendDataAllKeys(data: java.util.ArrayList<String>, device: AuraSwitch) {

        for (key in data) {
            try {
                ConnectTask(this, this, key, device.ip!!, device.name).execute("")
                Thread.sleep(20)
            } catch (e: InterruptedException) {
                Log.d(ERROR_TAG, "sendDataAllKeys : " + e)
                //error { "Error: ${e.printStackTrace()}" }
            }

        }
        resetAllKeysFlag = false
        ConnectTask(this, this, "{\"type\":8, \"set\":0}", device.ip!!, device.name).execute("")

    }

    private fun sendDataAllKeysMissing(data: java.util.ArrayList<String>) {

        for (key in data) {
            try {
                ConnectTask(this, this, key, configuredIpAddress, "XXXXXX").execute("")
                Thread.sleep(20)
            } catch (e: InterruptedException) {
                Log.d(ERROR_TAG, "sendDataAllKeysMissing : " + e)
                //error { "Error: ${e.printStackTrace()}" }
            }

        }
        resetAllKeysFlag = false
        ConnectTask(this, this, "{\"type\":8, \"set\":0}", configuredIpAddress, "XXXXXX").execute("")


    }


    override fun onTcpMessageReceived(message: String) {
        var data = message
        updateMessage(message)


    }

    fun updateMessage(message: String){
        if (message.contains("ERROR")) {
            Log.d("ERROR_MESSAGE", "ERROR : " + message)
        } else {
            val dummyDevice = jsonHelper.deserializeTcp(message)
            //Log.d(LOG_TAG,"Dummy Device Available" +dummyDevice)
            val gson = Gson()
            when (dummyDevice.type) {
                1 -> {
                    if (dummyDevice.dsy == 0) {
                        dummyDevice.ip = (Nsd.getIP(dummyDevice.name))
                        deviceSet.add(dummyDevice.name)
                        for (element in deviceSet) {
                            if (element == dummyDevice.name) {
                                val name = "Aura Switch-" + dummyDevice.name
                                dummyDevice.name = name
                                if (auraSwitches.size > 0) {
                                    var flag = true
                                    for (stc in auraSwitches) {
                                        if (stc.name == dummyDevice.name) {
                                            flag = false
                                        }
                                    }
                                    if (flag) {
                                        auraSwitches.add(dummyDevice)
                                    }
                                } else {
                                    auraSwitches.add(dummyDevice)
                                }


                            }
                        }
                        adapter.notifyDataSetChanged()
                        placeScanUi()
                        return
                    }
                }

                2 -> {
                    if (dummyDevice.error == 0) {
                        for (y in UiudList) {
                            if (y.name!!.contains(dummyDevice.name)) {
                                var IpListDevices = localSqlUtils.getIpList(mDbUtils!!, "device")
                                var flag = true
                                for (ip in IpListDevices) {
                                    if (ip.name == dummyDevice.name) {
                                        ip.uiud = UIUD
                                        ip.owned = 0
                                        flag = false
                                        break
                                    }
                                }
                                if (flag) {
                                    var ip = IpModel()
                                    ip.name = dummyDevice.name
                                    ip.owned = 0
                                    ip.uiud = UIUD
                                    ip.ip = y.ip
                                    ip.aws = false
                                    ip.local = false
                                    IpListDevices.add(ip)
                                    IP.registerIpDevice(ip)
                                }
                                val auraLoads = AuraSwitchLoad()
                                val loads = auraLoads.defaultLoadList()
                                Log.d("NULLPOINTER", "Constant.HOME : " + Constant.HOME + " UIUD : " + UIUD + " dummyDevice.name : " + dummyDevice.name + " gson.toJson(loads) : " + gson.toJson(loads))
                                try {

                                    if (dummyDevice.thing != null) {
                                        localSqlDatabase.insertDevice(mDb!!, Constant.HOME!!, "Living Room", UIUD!!, dummyDevice.name, gson.toJson(loads), dummyDevice.thing!!)
                                    } else {
                                        localSqlDatabase.insertDevice(mDb!!, Constant.HOME!!, "Living Room", UIUD!!, dummyDevice.name, gson.toJson(loads), "null")
                                    }
                                    localSqlUtils.replaceIpList(mDbUtils!!, "device", IpListDevices)
                                } catch (e: Exception) {
                                    Log.d("NULLPOINTER", e.toString())
                                }
                                dummyDevice.ip = y.ip
                            }
                        }
                        if (CLOUD_CONNECT) {
                            longSnackbar(findViewById(android.R.id.content), "Enabling Remote Access..")
                            getKeys(dummyDevice)
                            onStartingScan()

                        } else {
                            longSnackbar(findViewById(android.R.id.content), "Fetching thing from database..")

                            // getThing(dummyDevice)

                            longToast("Device Paired Successfully")
                            startActivity<ConfigureLoadActivity>("DEVICE" to gson.toJson(dummyDevice), "UIUD" to UIUD)
                            this.finish()
                        }
                        return
                    }

                    if (dummyDevice.error == 1) {
                        longSnackbar(findViewById(android.R.id.content), "Wrong Pin Entered")
                        onCompletionOfScan()
                        return
                    }
                    return
                }

                5 -> {
                    keysSetFlags[0] = dummyDevice.r
                    keysSetFlags[1] = dummyDevice.c
                    keysSetFlags[2] = dummyDevice.k
                    //val progressPercentage = ((Integer.bitCount(dummyDevice.c) + Integer.bitCount(dummyDevice.k)) * 100) / 47
                    //progress_bar.progress = progressPercentage
                }

                6 -> {
                    keysSetFlags[0] = dummyDevice.r
                    keysSetFlags[1] = dummyDevice.c
                    keysSetFlags[2] = dummyDevice.k
                    // val progressPercentage = ((Integer.bitCount(dummyDevice.c) + Integer.bitCount(dummyDevice.k)) * 100) / 47
                    // progress_bar.progress = progressPercentage
                }
                7 -> {
                    keysSetFlags[0] = dummyDevice.r
                    keysSetFlags[1] = dummyDevice.c
                    keysSetFlags[2] = dummyDevice.k
                }

                8 -> {
                    if (!resetAllKeysFlag) {
                        if (dummyDevice.error == 0) {
                            keysSetFlags.clear()
                            progress_bar.visibility = View.INVISIBLE
                            longToast("Device Paired Successfully")
                            startActivity<ConfigureLoadActivity>("DEVICE" to gson.toJson(dummyDevice), "UIUD" to UIUD)
                            this.finish()
                        } else {
                            val a = keysSetFlags[0]
                            val b = keysSetFlags[1]
                            val c = keysSetFlags[2]
                            if ((a == 1) and (b == 134217727) and (c == 1048575)) {
                                Log.d("KEYS_SUCCESS", "Wrong keys added Report to WOZART!! a=" + a + " b=" + b + " c=" + c)
                                progress_bar.visibility = View.INVISIBLE
                                longToast("Wrong keys added Report to WOZART!!")
                                startActivity<ConfigureLoadActivity>("DEVICE" to gson.toJson(dummyDevice), "UIUD" to UIUD)
                                this.finish()
                            } else {
                                if (reconnectTries > 2) {
                                    progress_bar.visibility = View.INVISIBLE
                                    longToast("Wrong keys added Report to WOZART!!")
                                    startActivity<ConfigureLoadActivity>("DEVICE" to gson.toJson(dummyDevice), "UIUD" to UIUD)
                                    this.finish()
                                } else {
                                    longToast("Getting Missing Keys")
                                    getKeysMissing(keysSetFlags[0], keysSetFlags[1], keysSetFlags[2])
                                    reconnectTries++
                                }
                            }
                        }
                    }

                }

                else -> {
                    onCompletionOfScan()
                }
            }

        }
    }
}

