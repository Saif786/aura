package com.wozart.aura.ui.adapter

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.sqlite.SQLiteDatabase
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.*
import aura.wozart.com.aura.entity.amazonaws.models.nosql.ThingTableDO
import aura.wozart.com.aura.entity.network.Nsd
import com.google.gson.Gson
import com.wozart.aura.R
import com.wozart.aura.aura.data.model.Customization
import com.wozart.aura.ui.home.ConfigureLoadActivity
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.dynamoDb.DeviceTableHandler
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.dynamoDb.ThingTableHandler
import com.wozart.aura.data.model.IpModel
import com.wozart.aura.data.model.ThingError
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.entity.model.scene.Scene
import com.wozart.aura.entity.network.ConnectTask
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.DialogListener
import com.wozart.aura.utilities.Encryption
import com.wozart.aura.utilities.JsonHelper
import kotlinx.android.synthetic.main.item_customization.view.*
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.longToast
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.startActivity
import java.lang.Exception
import kotlin.concurrent.thread

class CustomizationItemAdapter(private var context: Context, private val pos: Int, private val adapter: CustomizationAdapter, private val label: String, private val arrayList: MutableList<Customization>, val userType: Boolean, onItemDelete: onItemDeletedListener, private val auraSwitches: MutableList<AuraSwitch>) : RecyclerView.Adapter<CustomizationItemAdapter.ItemViewHolder>(), ConnectTask.TcpMessageReceiver {

    private lateinit var Nsd: Nsd
    private var selectedPosition = -1
    private val localSqlDatabase = DeviceTable()
    private val localSqlScene = SceneTable()
    private var mDb: SQLiteDatabase? = null
    private var mDbScene: SQLiteDatabase? = null
    var mDbUtils: SQLiteDatabase? = null
    var IpListDevices: MutableList<IpModel> = ArrayList()
    private var jsonHelper: JsonHelper = JsonHelper()
    val localSqlUtils = UtilsTable()
    var scenes = ArrayList<Scene>()

    var ERROR_TAG = "ERRORCONFIGURE"
    private var resetAllKeysFlag: Boolean = false
    var keysSetFlags = java.util.ArrayList<Int>()
    var thing: ThingTableDO? = null
    private var rulesTableDo = RulesTableHandler()
    private var itemListener: onItemDeletedListener = onItemDelete
    var reconnectTries: Int = 0
    var allAwsKeys: MutableList<String> = java.util.ArrayList()
    private val deviceDynamoDb = DeviceTableHandler()
    private val thingDynamoDb = ThingTableHandler()
    lateinit var configuredIpAddress: String
    var builder = AlertDialog.Builder(context)
    private var CLOUD_CONNECT = false
    var eroorTest: String? = null
    var errorCheck: Int = 0
    var upgrade_firmware_flag = false


    interface onItemDeletedListener {
        fun onItemDelete(name: String, ip: String,uiud: String)
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_customization, parent, false)
        val dbHelper = DeviceDbHelper(parent.context)
        mDb = dbHelper.writableDatabase
        val scenDbHelper = SceneDbHelper(parent.context)
        mDbScene = scenDbHelper.writableDatabase
        val dbUtils = UtilsDbHelper(parent.context)
        mDbUtils = dbUtils.writableDatabase
        if (auraSwitches.size != 0) {
            init()
        }


        return ItemViewHolder(view)
    }

    fun init() {
        var switch = AuraSwitch()
        for (data in auraSwitches) {
            switch.name = data.name
            switch.uiud = data.uiud
            switch.ip = data.ip
            try {
                sendTcpConnect(switch.uiud, switch.ip!!, switch.name)
            } catch (e: Exception) {
                Log.d("WIFI_LOG", "Error : " + e.stackTrace)
            }

//        Nsd = Nsd(context!!,"WIFI"
        }
//
//        nsdDiscovery()
//        LocalBroadcastManager.getInstance(context!!).registerReceiver(
//                onNSDServiceResolved, IntentFilter("nsdDiscoverWifi"))
    }

    private fun nsdresolvedForKey() {
        Nsd = Nsd(context!!, "WIFI")
        nsdDiscover()
        LocalBroadcastManager.getInstance(context).registerReceiver(
                onNSDServiceResolved, IntentFilter("nsdDiscoverWifi"))

    }

    fun nsdDiscover() {
        if (!Nsd.getDiscoveryStatus()) {
            Nsd.setBroadcastType("WIFI")
            Nsd.initializeNsd()
            Nsd.discoverServices()
        } else {
            Nsd.setBroadcastType("WIFI")
            Nsd.stopDiscovery()
            Nsd.initializeNsd()
            Nsd.discoverServices()
        }
    }

    private val onNSDServiceResolved = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d("NSD_DEBUG_WIFI", "nsd onReceive in WiFIFragment")
            val name = intent.getStringExtra("name")
            val ip = intent.getStringExtra("ip")
            val device = name.substring(name.length - 6, name.length)
            var switchData = AuraSwitch()
            for (switch in auraSwitches) {
                if (arrayList[pos].deviceName == switch.name) {
                    if (switch.name == device) {
                        CLOUD_CONNECT = true
                        sendTcpConnectTogetKey(switch.uiud, switch.ip!!, switch.name)
                    }
                    break
                }

            }
        }
    }


    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {

        holder.itemView?.layoutExtended?.visibility = if ((position == selectedPosition) and (userType)) View.VISIBLE else View.GONE
        holder.itemView?.layout_upgrade?.visibility = if ((position == selectedPosition) and (userType)) View.VISIBLE else View.GONE
        holder.itemView?.device_name?.text = arrayList[position].deviceName.toString()

        for (connect in auraSwitches) {
            if (arrayList[position].deviceName == connect.name) {
                if ((connect.CloudPresence == true) || (connect.aws == 1)) {
                    holder.itemView?.device_status?.visibility = View.VISIBLE
                    holder.itemView?.device_status?.text = "Connected"
                    holder.itemView?.layoutConnect?.visibility = View.INVISIBLE

                } else {
                    holder.itemView?.device_status?.visibility = View.VISIBLE
                    holder.itemView?.device_status?.text = "Not Connected"
                    holder.itemView?.layoutConnect?.visibility = View.VISIBLE
                }
                break
            }
        }
        holder.itemView?.layoutConnect?.setOnClickListener {
            Utils.showCustomDialog(holder.itemView.context, holder.itemView.context.getString(R.string.title_aws_connect), holder.itemView.context.getString(R.string.aws_connect_dialog_message), R.layout.dialog_layout_aws_connect, object : DialogListener {
                override fun onOkClicked() {
                    for (switcData in auraSwitches) {
                        if (arrayList[position].deviceName == switcData.name) {
                            if ((eroorTest == "true") || (errorCheck != 0)) {
                                //longSnackbar(it.findViewById(android.R.id.content), "Device no longer available")
                                Toast.makeText(context, "Device no longer available", Toast.LENGTH_SHORT).show()
                            } else if (errorCheck == 0) {
                                nsdresolvedForKey()
                                arrayList[position].isConnected = true
                                holder.itemView.device_status?.visibility = View.VISIBLE
                                holder.itemView.layoutConnect?.visibility = View.INVISIBLE
                                saveData(switcData)
                                notifyDataSetChanged()
                            }
                            break
                        }
                        notifyDataSetChanged()
                    }

                }

                override fun onCancelClicked() {
                    arrayList[position].isConnected = false
                    holder.itemView.device_status?.visibility = View.INVISIBLE
                    holder.itemView.layoutConnect?.visibility = View.VISIBLE
                }
            })
        }
        holder.itemView?.layoutDelete?.setOnClickListener {
            Utils.showCustomDialog(holder.itemView.context, holder.itemView.context.getString(R.string.title_delete_appliance), holder.itemView.context.getString(R.string.dialog_delete_appliance), R.layout.dialog_layout, object : DialogListener {
                override fun onOkClicked() {
                    scenes = localSqlScene.getAllScenes(mDbScene!!, Constant.HOME!!)
                    val uiud = localSqlDatabase.getUiud(mDb!!, arrayList[position].deviceName!!)
                    val device = arrayList[position].deviceName
                    if (uiud != null) {
                        thread {
                            if (Encryption.isInternetWorking()) {
                                val auraDevice = localSqlDatabase.getDevice(mDb!!, device!!)
                                val error = rulesTableDo.deleteDevice(auraDevice)
                                if (error == "SUCCESS") {
                                    localSqlDatabase.deleteDevice(mDb!!, device!!)
                                } else {
                                    Toast.makeText(it.context, "Failed to delete device try later.", Toast.LENGTH_SHORT).show()
                                }
                            } else {
                                context.runOnUiThread {
                                    Toast.makeText(it.context, "Check Internet connection", Toast.LENGTH_SHORT).show()
                                }
                            }
                        }
                    }
                    thread {
                        if (Encryption.isInternetWorking()) {
                            localSqlDatabase.deleteDevice(mDb!!, arrayList[position].deviceName!!)
                            IpListDevices = localSqlUtils.getIpList(mDbUtils!!, "device")
                            var rmv = IpModel()
                            var Nsd = Nsd(it.context, "IP")
                            for (l in IpListDevices) {
                                if (l.name == arrayList[position].deviceName) {
                                    Log.d("IP", "IP is ${l.ip},${arrayList[position].deviceName}")
                                    if (l.ip == null) {
                                        l.ip = (Nsd.getIP(l.name!!))
                                    } else {
                                        itemListener.onItemDelete(arrayList[position].deviceName!!, l.ip!!,uiud!!)
                                        rmv = l
                                    }
                                    // ConnectTask(holder.itemView.context,   dataReceived, "{\"type\":8,\"name\":\"95FF5C\",\"set\":1,\"pair\":1}", l.ip!!,arrayList[position].deviceName!!).execute()
                                }
                            }
                            IpListDevices.remove(rmv)
                            localSqlUtils.replaceIpList(mDbUtils!!, "device", IpListDevices)
                            arrayList.removeAt(position)
                            notifyItemRemoved(position)
                            notifyItemChanged(position)
                        } else {
                            context.runOnUiThread {
                                Toast.makeText(it.context, "Check Internet connection", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }

                override fun onCancelClicked() {
                }
            })
        }

        holder.itemView?.layout_upgrade?.setOnClickListener {
            Utils.showCustomDialog(holder.itemView.context, holder.itemView.context.getString(R.string.upgrade_firmware), holder.itemView.context.getString(R.string.details_upgradation), R.layout.dialog_layout, object : DialogListener {
                override fun onOkClicked() {
                    if (errorCheck == 0) {
                        var ip: String? = null
                        var wifi: Int = 0
                        var name: String? = null
                        var uiud: String? = null
                        var url = "http://s3.ap-south-1.amazonaws.com/wozart/ota_android.bin"
                        for (deviceUiud in auraSwitches) {
                            if (arrayList[position].deviceName == deviceUiud.name) {
                                name = deviceUiud.name
                                ip = deviceUiud.ip
                                uiud = deviceUiud.uiud
                                break
                            }
                        }

                        var type9 = "{\"type\":9,\"url\":\"$url\",\"uiud\":\"$uiud\",\"WIFI\":\"$wifi\"}"
                        sendTcpData(type9, ip, name)
                    } else {
                        Toast.makeText(context, "Device not found..", Toast.LENGTH_SHORT).show()
                        //longSnackbar(it.findViewById(android.R.id.content), "Device not available..")
                    }


                }

                override fun onCancelClicked() {

                }
            })
        }

        holder.itemView?.layoutDetails?.setOnClickListener {
            val device = localSqlDatabase.getDevice(mDb!!, arrayList[position].deviceName!!)
            val gson = Gson()
            val intent = Intent(holder.itemView.context, ConfigureLoadActivity::class.java)
            intent.putExtra("DEVICE", gson.toJson(device))
            intent.putExtra("UIUD", "FARGIUIUD")
            holder.itemView.context?.startActivity(intent)
        }

        holder.itemView?.setOnClickListener {
            onItemLongClick(position)
        }
        holder.itemView?.setOnLongClickListener {
            onItemLongClick(position)
            false
        }

    }

    fun saveData(device: AuraSwitch) {
        val gson = Gson()

        var thingDetails = localSqlUtils.getThingData(mDbUtils!!, device.name)
        if (device.thing == null) {
            localSqlDatabase.insertDevice(mDb!!, Constant.HOME!!, device.room, device.uiud, device.name, gson.toJson(device.loads), "null")
        } else {

            localSqlDatabase.insertDevice(mDb!!, Constant.HOME!!, device.room, device.uiud, device.name, gson.toJson(device.loads), device.thing!!)

        }
        thread {
            rulesTableDo.insertDeviceLoads(device)
        }
    }

//    fun awsEnableConnect(message: String) {
//        data = message
//        updateAwsDevice(message)
//        notifyDataSetChanged()
//    }

    fun updateAwsDevice(message: String): MutableList<AuraSwitch> {

        var auraSwitch: AuraSwitch = jsonHelper.deserializeTcp(message)
        if (auraSwitch.aws == 1) {
            for (awsDeviceConnect in auraSwitches) {
                if (auraSwitch.name == awsDeviceConnect.name) {
                    awsDeviceConnect.aws = auraSwitch.aws
                    awsDeviceConnect.error = auraSwitch.error
                    notifyDataSetChanged()
                    break
                }
            }
        }
        notifyDataSetChanged()
        return auraSwitches
    }

    fun sendTcpConnectTogetKey(uiud: String, ip: String, name: String) {
        ConnectTask(context, this, jsonHelper.initialData(uiud), ip, name).execute("")
    }

    fun sendTcpConnect(uiud: String, ip: String, name: String) {
        ConnectTask(context, this, jsonHelper.initialData(uiud), ip, name).execute("")
    }

    fun sendTcpData(type9: String, ip: String?, name: String?) {
        ConnectTask(context, this, type9, ip!!, name!!).execute("")
    }


    override fun onTcpMessageReceived(message: String) {
        if (message.contains("ERROR")) {
            eroorTest = message.contains("ERROR").toString()
            if (auraSwitches[0].CloudPresence == true) {

            } else {
                Toast.makeText(context, "Device is connecting to cloud.", Toast.LENGTH_SHORT).show()
            }

        } else {
            var auraSwitch: AuraSwitch = jsonHelper.deserializeTcp(message)

            errorCheck = auraSwitch.error

            if ((auraSwitch.aws == 1) and (auraSwitch.error == 0)) {

                updateAwsDevice(message)
            }

            if (CLOUD_CONNECT) {

                if (Encryption.isInternetWorking()) {
                    for (switcData in auraSwitches) {
                        if (auraSwitch.name == switcData.name) {
                            Toast.makeText(context, "Enabling Remote Access..", Toast.LENGTH_SHORT).show()
                            auraSwitch.thing = "null"
                            auraSwitch.ip = switcData.ip
                            auraSwitch.uiud = switcData.uiud
                            getKeys(auraSwitch)
                        }
                    }

                    if (!resetAllKeysFlag) {
                        if (auraSwitch.error == 0) {
                            keysSetFlags.clear()
                        } else {
                            val a = keysSetFlags[0]
                            val b = keysSetFlags[1]
                            val c = keysSetFlags[2]
                            if ((a == 1) and (b == 134217727) and (c == 1048575)) {
                                Log.d("KEYS_SUCCESS", "Wrong keys added Report to WOZART!! a=" + a + " b=" + b + " c=" + c)
                                context.longToast("Wrong keys added Report to WOZART!!")
                            } else {
                                if (reconnectTries > 2) {
                                    context.longToast("Wrong keys added Report to WOZART!!")
                                } else {
                                    configuredIpAddress = auraSwitch.ip!!
                                    getKeysMissing(keysSetFlags[0], keysSetFlags[1], keysSetFlags[2])
                                    reconnectTries++
                                }
                            }
                        }
                    }
                } else {
                    context.runOnUiThread {
                        Toast.makeText(context, "Check Internet connection", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private fun getKeysMissing(reg: Int, cert: Int, keys: Int) {
        val runnable = Runnable {
            try {

                var data = ArrayList<String>()
                //   allAwsKeys.add(data)   thing
                if (reg == 0) {
                    data.add(allAwsKeys[0])
                }

                for (i in 0..19) {
                    val d = (cert shr i) and 1
                    if (d % 2 == 0) {
                        data.add(allAwsKeys[i + 1])
                    }
                }
                for (i in 0..26) {
                    val d = (keys shr i) and 1
                    if (d % 2 == 0) {
                        data.add(allAwsKeys[i + 21])
                    }
                }

                sendDataAllKeysMissing(data)
            } catch (e: Exception) {

                context.longToast("Keys missing, please reset the device.")

                //error { "Error: ${e.printStackTrace()}" }
            }
        }
        val getAvailableDevices = Thread(runnable)
        getAvailableDevices.start()
    }


    private fun sendDataAllKeysMissing(data: java.util.ArrayList<String>) {

        for (key in data) {
            try {
                ConnectTask(context, this, key, configuredIpAddress, "XXXXXX").execute("")
                Thread.sleep(20)
            } catch (e: InterruptedException) {
                Log.d("ERROR_TAG", "sendDataAllKeysMissing : " + e)
                //error { "Error: ${e.printStackTrace()}" }
            }

        }
        resetAllKeysFlag = false
        ConnectTask(context, this, "{\"type\":8, \"set\":0}", configuredIpAddress, "XXXXXX").execute("")

    }

    private fun onItemLongClick(position: Int) {

        for (i in 0 until arrayList.size) {
            if (i != pos)
                adapter.notifyItemChanged(i)
        }
        selectedPosition = if (position == selectedPosition) {
            -1
        } else position

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }


    fun getKeys(device: AuraSwitch) {
        resetAllKeysFlag = true
        keysSetFlags.add(0)
        keysSetFlags.add(1)
        keysSetFlags.add(2)

        ConnectTask(context, this, "{\"type\":8, \"set\":1}", device.ip!!, device.name).execute("")

        thread {
            try {
                val thingAlreadyExists = deviceDynamoDb.checkThing(device.uiud!!.substring(0, Math.min(device.uiud!!.length, 12)))
                if (thingAlreadyExists!!.error == "error") {
                    if (thingAlreadyExists.version == "error") {
                        // progress_bar.visibility = View.INVISIBLE
                        context.longToast("Check Internet connection")
                        val gson = Gson()
                        context.startActivity<ConfigureLoadActivity>("DEVICE" to gson.toJson(device), "UIUD" to device.uiud)

                    } else {
                        if (thingAlreadyExists.thing == null) {
                            thing = thingDynamoDb.searchAvailableDevices()!!

                        } else {
                            thing = thingDynamoDb.thingDetails(thingAlreadyExists.thing!!)
                        }
                        allAwsKeys.clear()
                        val data = jsonHelper.awsRegionThing(thing!!.region!!, thing!!.thing!!)
                        allAwsKeys.add(data)
                        val certs = jsonHelper.certificates(thing!!.certificate!!)
                        allAwsKeys.addAll(certs)
                        val keys = jsonHelper.privateKeys(thing!!.privateKey!!)
                        allAwsKeys.addAll(keys)
                        allAwsKeys.add(data)
                        sendDataAllKeys(allAwsKeys as java.util.ArrayList<String>, device)
                        var localThing = ThingError()
                        localThing.region = thing!!.region
                        localThing.privateKey = thing!!.privateKey
                        localThing.certificate = thing!!.certificate
                        localThing.thing = thing!!.thing
                        localThing.version = "Version1"
                        localThing.error = "success"
                        localSqlUtils.replaceThing(mDbUtils!!, device.name, localThing)
                    }
                } else {
                    allAwsKeys.clear()
                    val data = jsonHelper.awsRegionThing(thingAlreadyExists.region!!, thingAlreadyExists.thing!!)
                    allAwsKeys.add(data)
                    val certs = jsonHelper.certificates(thingAlreadyExists.certificate!!)
                    allAwsKeys.addAll(certs)
                    val keys = jsonHelper.privateKeys(thingAlreadyExists!!.privateKey!!)
                    allAwsKeys.addAll(keys)
                    allAwsKeys.add(data)
                    sendDataAllKeys(allAwsKeys as java.util.ArrayList<String>, device)
                    var localThing = ThingError()
                    localThing.region = thingAlreadyExists!!.region
                    localThing.privateKey = thingAlreadyExists!!.privateKey
                    localThing.certificate = thingAlreadyExists!!.certificate
                    localThing.thing = thingAlreadyExists!!.thing
                    localThing.version = "Version2"
                    localThing.error = "success"
                    localSqlUtils.replaceThing(mDbUtils!!, device.name, localThing)
                }
            } catch (e: Exception) {
                Log.d("CustomizationAdapter", "getKeys : " + e)
                //error { "Error: ${e.printStackTrace()}" }
            }
        }

    }

    private fun sendDataAllKeys(data: java.util.ArrayList<String>, device: AuraSwitch) {

        for (key in data) {
            try {
                ConnectTask(context, this, key, device.ip!!, device.name).execute("")
                Thread.sleep(20)
            } catch (e: InterruptedException) {
                Log.d(ERROR_TAG, "sendDataAllKeys : " + e)
                //error { "Error: ${e.printStackTrace()}" }
            }

        }
        resetAllKeysFlag = false
        ConnectTask(context, this, "{\"type\":8, \"set\":0}", device.ip!!, device.name).execute("")

    }
}

