package com.wozart.aura.ui.selectdevices

import android.database.sqlite.SQLiteDatabase
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.wozart.aura.R
import com.wozart.aura.aura.ui.createautomation.CreateAutomationActivity
import com.wozart.aura.aura.ui.createautomation.SetAutomationActivity
import com.wozart.aura.aura.ui.setactions.SetActionsFragment
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.model.AutomationModel
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.ScheduleTable
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scheduling.ScheduleDbHelper
import com.wozart.aura.ui.base.baseselectdevices.BaseSelectAutomationDevices
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.fragment_select_devices.*
import kotlinx.android.synthetic.main.layout_header.*
import org.jetbrains.anko.support.v4.intentFor
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.toast
import kotlin.collections.ArrayList
import kotlin.concurrent.thread

/**
 * Created by Saif on 11/20/2018.
 * Wozart Technology Pvt Ltd
 * mds71964@gmail.com
 */

class SelectDevicesFragment : BaseSelectAutomationDevices() {

    private var localSqlSchedule = ScheduleTable()
    private val localSqlDatabase = DeviceTable()
    private var mDbSchedule: SQLiteDatabase? = null
    private var mDb: SQLiteDatabase? = null
    private var scheduleTypeCheck : String ?= null
    companion object {
        private  var automationScheduleType:String?=null
        private var automationNameOld: String?= null
        var room :ArrayList<RoomModel> = ArrayList()
        var rooms : MutableList<RoomModel> = ArrayList()
        var scheduleDetails = AutomationModel("title",0,room,false," "," "," "," ","")
        fun newInstance(scheduleData: AutomationModel,automationSceneNameOld:String,automationSceneType:String):SelectDevicesFragment{
            var scheduleDetail= scheduleData
            scheduleDetails = scheduleDetail
            automationNameOld = automationSceneNameOld
            automationScheduleType= automationSceneType
            return SelectDevicesFragment()
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_automation_select_device, container, false)
        var btnSceneDelete = view.findViewById<Button>(R.id.btnSceneDelete)
        var dbSchedule = ScheduleDbHelper(context!!)
        val scheduleDynamoDb = RulesTableHandler()

        mDbSchedule = dbSchedule.writableDatabase
        if(context is CreateAutomationActivity){
            automationScheduleType = (context as CreateAutomationActivity).getAutomationSceneType()
            automationNameOld = (context as CreateAutomationActivity).getAutomationSceneName()
        }


        if(context is SetAutomationActivity){
            automationNameOld = (context as SetAutomationActivity).getAutomationOldName()
            automationScheduleType = (context as SetAutomationActivity).getAutomationType()
            scheduleTypeCheck = (context as SetAutomationActivity).getScheduleType()
        }

        btnSceneDelete.setOnClickListener {
            if (automationScheduleType == "create") {
                btnSceneDelete.visibility = View.INVISIBLE
                longToast(" Automation not create to Delete")
            } else {
                btnSceneDelete.visibility =View.VISIBLE
                localSqlSchedule.deleteAutomationScene(mDbSchedule!!, automationNameOld!!,Constant.HOME!!)
                deleteAutomationScene(automationNameOld!!)
                thread{
                    scheduleDynamoDb.deleteUserSchedule(Constant.IDENTITY_ID!!,Constant.HOME!!,automationNameOld!!)
                }
                startActivity(intentFor<DashboardActivity>())
                toast("Automation Scene Deleted")
            }
        }
        return view

    }
    fun deleteAutomationScene( automationScene: String){

        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase
        val dummyRooms = localSqlDatabase.getRooms(mDb!!, Constant.HOME!!)
        for (x in dummyRooms) {
            val room = RoomModel()
            room.name = x.roomName
            roomsList.add(room)
        }
        if (!roomsList.isEmpty()) {
            for (x in roomsList) {
                val devices = localSqlDatabase.getDevicesForRoom(mDb!!,  Constant.HOME!!, x.name!!)
                //NEWSCENES delete all scenes
            }

        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initialize()
    }


    override fun openNextScreen() {
        var leng : Int = 0
        rooms = getSelectedRoomDeviceData()

        for(item in rooms){
            leng += item.deviceList.size
        }
        if(leng == 0){
            longToast("Please Select Loads..")
        }else{
           if(scheduleTypeCheck == "geo"){

               mListener?.navigateToFragment(SetActionsFragment())

           }else{

               mListener?.navigateToFragment(SetActionsFragment.newInstance(rooms, scheduleDetails, automationNameOld!!, automationScheduleType!!)) //pass selected devices with rooms data

           }

        }

    }
    override fun showSceneInputs(): Boolean {
        return false
    }

    override fun getTitle(): String {
        return getString(R.string.text_select_devices)
    }

    fun initialize()
    {
        //main_layout.setBackgroundColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
       // tvSelectLoadIcon.setTextColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
        tvTitle.setTextColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
        home.setColorFilter(ContextCompat.getColor(activity!!.baseContext,R.color.white), PorterDuff.Mode.SRC_ATOP)
        tvNext.setTextColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
    }


}
