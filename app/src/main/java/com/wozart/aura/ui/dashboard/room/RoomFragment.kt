package com.wozart.aura.ui.dashboard.room

import android.content.*
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.os.IBinder
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.wozart.aura.R
import com.wozart.aura.aura.ui.dashboard.rooms.AddRoomActivity
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.device.IpHandler
import com.wozart.aura.data.device.SceneHandler
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.model.IpModel
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraComplete
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.entity.network.ConnectTask
import com.wozart.aura.entity.service.AwsPubSub
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.ui.dashboard.Device
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.DialogListener
import com.wozart.aura.utilities.Encryption
import com.wozart.aura.utilities.JsonHelper
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.onUiThread
import org.jetbrains.anko.support.v4.toast
import kotlin.concurrent.thread


/***************************************************************************
 * File Name :
 * Author : Saif
 * Date of Creation : 09/08/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0.2
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class RoomFragment : Fragment(), RoomAdapter.OnAllDevice, RoomAdapter.OffAllDevice, ConnectTask.TcpMessageReceiver {


    private val localSqlDatabase = DeviceTable()
    private var mDb: SQLiteDatabase? = null
    var roomsList: MutableList<RoomModel> = ArrayList()
    val rooms = ArrayList<RoomModel>()
    private val localSqlUtils = UtilsTable()
    var IpListDevices: MutableList<IpModel> = ArrayList()
    var allDeviceList = ArrayList<AuraComplete>()
    var devicesList: ArrayList<Device> = ArrayList()
    private var IP = IpHandler()
    private var mDbUtils: SQLiteDatabase? = null
    var listRoom: MutableList<RoomModelJson> = ArrayList()
    var roomsListDetails: MutableList<RoomModel> = ArrayList()
    private var jsonHelper: JsonHelper = JsonHelper()
    private var sceneHandler: SceneHandler = SceneHandler()
    internal var mBounded: Boolean = false
    private var awsPubSub: AwsPubSub? = null
    private var serviceConnection = false
    private var rulesTableDo = RulesTableHandler()
    var deviceError: Int = -1

    companion object {
        fun newInstance(): RoomFragment {
            return RoomFragment()
        }
    }

    var roomIcon = arrayOf(R.drawable.ic_living_room_off)
    var userType = true
    private lateinit var adapter: RoomAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_room, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val roomNameList = view.findViewById(R.id.roomNameList) as RecyclerView
        var addRoomButtonplus = view!!.findViewById<ImageView>(R.id.addRoomButtonplus)
        val dbUtils = UtilsDbHelper(context!!)
        mDbUtils = dbUtils.writableDatabase
        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase
        IpListDevices = IP.getIpDevices()
        allDeviceList = localSqlDatabase.getAllDevicesScenes(mDb!!, Constant.HOME!!)
        listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")

        LocalBroadcastManager.getInstance(context!!).registerReceiver(
                onAwsMessageReceived, IntentFilter("AwsShadow"))

        Thread(Runnable {
            try {
                Thread.sleep(1000)
                for (l in IpListDevices) {
                    if (l.owned == 0) {
                        if (!l.local) {
                            if (!l.aws) {
                                if (l.thing != null) {
                                    if (serviceConnection) {
                                        if (context is DashboardActivity) (context as DashboardActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeLEDData())
                                    }
                                }
                            }

                        }

                    }
                }
            } catch (e: Exception) {
                Log.d("AWS_ERROR", "Error : " + e.stackTrace)
            }
        }).start()


        adapter = RoomAdapter(context!!, this, this, roomsListDetails as ArrayList<RoomModel>)

        init()
        updateRoomList()

        roomNameList.adapter = adapter
        roomNameList.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)


        for (x in listRoom) {
            if (x.name == Constant.HOME) {
                if (x.sharedHome == "guest") {
                    userType = false
                    break
                }
            }
        }
        if (userType) {
            addRoomButtonplus.visibility = View.VISIBLE
            addRoomButtonplus.setOnClickListener {
                showpopup(addRoomButtonplus)
            }
        } else {
            addRoomButtonplus.visibility = View.INVISIBLE
        }
    }

    fun updateRoomList() {

        for (listrooms in roomsList) {
            val room = RoomModel()
            var flagtest = false
            for (checkroom in roomsListDetails) {
                if (checkroom.name_room == listrooms.name_room) {
                    flagtest = true
                    break

                }
            }
            if (!flagtest) {
                var totalDevice = 0
                var onDevices = 0
                var offDevices = 0
                room.name_room = listrooms.name_room
                room.roomIcon = 0
                room.roombg = listrooms.roombg
                room.room_total_device_count = totalDevice.toString()
                room.room_deviceCount_off = offDevices.toString()
                room.room_deviceCount_on = onDevices.toString()
                roomsListDetails.add(room)
            }
            adapter.notifyDataSetChanged()

        }
    }


    fun init() {
        listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")
        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase


        val roomBg = localSqlDatabase.getRoomNews(mDb!!, Constant.HOME!!)

        for (x in roomBg) {
            val room = RoomModel()
            var isRoomExist = false
            for (y in roomsList) {
                if (y.name_room == x.name_room) {
                    isRoomExist = true
                    break
                }
            }
            if (!isRoomExist) {
                room.name_room = x.name_room
                var roomFound = false
                for (dummy in listRoom) {
                    if (dummy.name == x.name_room) {

//                        if((dummy.type == "room") and (dummy.sharedHome == "default")){
//                            room.roombg = dummy.bgUrl.toInt()
//                        }
                        room.roomIcon = roomIcon[0]
                        roomsList.add(room)
                        roomFound = true
                        break
                    }
                }
                if (!roomFound) {
                    room.roombg = 0
                    room.roomIcon = roomIcon[0]
                    roomsList.add(room)
                }

            }
        }

        for (l in listRoom) {
            val room = RoomModel()
            if ((l.type == "room") and (l.sharedHome == Constant.HOME)) {
                var flag = false
                for (r in roomsList) {
                    if (r.name_room == l.name) {
                        flag = true
                        break
                    }
                }
                if (!flag) {
                    room.name_room = l.name
                    room.roomIcon = roomIcon[l.roomIcon]
                    room.roombg = l.bgUrl.toInt()
                    roomsList.add(room)
                }
            }

        }

        for (l in IpListDevices) {
            for (rooms in roomsList) {
                if (rooms.name_room == l.room) {
                    if (l.owned == 0) {
                        try {
                            sendTcpConnect(l.uiud!!, l.ip!!, "XXXXXX")
                        } catch (e: Exception) {
                            Log.d("DATA_SEND", "Error : " + e.stackTrace)
                        }
                    }
                }
            }
        }

        updateDeviceCount(roomsList, false)
    }


    private fun sendTcpConnect(uiud: String, ip: String, name: String) {
        ConnectTask(context!!, this, jsonHelper.initialData(uiud), ip, name).execute("")

    }


    /**
     * Data from the AwsShadowUpdate
     */
    private val onAwsMessageReceived = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val shadow = intent.getStringExtra("data")
            val segments = shadow.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (shadow != "Connected") {
                val device = localSqlDatabase.getDeviceForThing(mDb!!, segments[1])
                val data = jsonHelper.deserializeAwsData(segments[0])
                if (data.led != 0) {
                    var flag = false
                    for (l in IpListDevices) {
                        if (l.name == device) {
                            if (data.uiud != null) {
                                if (data.uiud == l.uiud) {
                                    if (!l.local) {
                                        l.aws = true
                                        for (i in 0..3) {
                                            l.condition[i] = "ready"
                                        }
                                        l.dim[0] = data.dim["d0"]!!
                                        l.dim[1] = data.dim["d1"]!!
                                        l.dim[2] = data.dim["d2"]!!
                                        l.dim[3] = data.dim["d3"]!!
                                        for (i in 0..3) {
                                            l.state[i] = data.state["s$i"] == 1
                                            l.failure[i] = 0
                                            l.curn_load[i] = false
                                        }
                                        IP.registerIpDevice(l)

                                    }
                                }
                            }
                            break
                        }
                    }
                } else {
                    for (l in IpListDevices) {
                        if (l.name == device) {
                            if (!l.local) {
                                if (!l.aws) {
                                    for (i in 0..3) {
                                        if (!l.curn_load[i]) {
                                            l.failure[i] = l.failure[i] + 1
                                            if (l.failure[i] >= 2) {
                                                l.aws = false
                                                if (!l.local) {
                                                    for (i in 0..3) {
                                                        l.condition[i] = "fail"
                                                    }
                                                }
                                                for (j in 0..3) {
                                                    l.failure[j] = 0
                                                }
                                                IP.registerIpDevice(l)
                                            }
                                        } else {
                                            if (l.curn_load[i]) {
                                                l.failure[i] = l.failure[i] + 1
                                                if (l.failure[i] >= 2) {
                                                    l.aws = false
                                                    if (!l.local) {
                                                        for (i in 0..3) {
                                                            l.condition[i] = "fail"
                                                        }
                                                    }
                                                    for (j in 0..3) {
                                                        l.failure[j] = 0
                                                    }
                                                    IP.registerIpDevice(l)

                                                }
                                                l.curn_load[i] = false

                                            }
                                        }
                                    }
                                }

                            }
                            break
                        }
                    }
                }
                updateDeviceCount(roomsList, true)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val mIntent = Intent(context, AwsPubSub::class.java)
        context!!.bindService(mIntent, mConnection, Context.BIND_AUTO_CREATE)
    }

    private var mConnection: ServiceConnection = object : ServiceConnection {

        override fun onServiceDisconnected(name: ComponentName) {
            mBounded = false
            awsPubSub = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            mBounded = true
            val mLocalBinder = service as AwsPubSub.LocalAwsBinder
            awsPubSub = mLocalBinder.getServerInstance()
            serviceConnection = true
        }
    }


    fun updateDeviceCount(roomsList: MutableList<RoomModel>, b: Boolean) {
        if (!b) {
            var flag = false
            for (IplistDevice in IpListDevices) {
                for (device in allDeviceList) {
                    if ((IplistDevice.name == device.name) and (IplistDevice.owned == 0)) {
                        if (IplistDevice.home == Constant.HOME) {
                            for (room in roomsList) {
                                // val rooms = RoomModel()
                                if (IplistDevice.room == room.name_room) {
                                    for (presentRoom in roomsListDetails) {
                                        if (presentRoom.name_room == room.name_room) {
                                            flag = true
                                            break
                                        } else {
                                            flag = false
                                            break
                                        }
                                    }
                                    if (!flag) {
                                        var totalDevice = 0
                                        var onDevices = 0
                                        var offDevices = 0
                                        for (i in IplistDevice.state) {
                                            totalDevice++
                                            if (i) {
                                                onDevices++

                                            } else {
                                                offDevices++
                                            }
                                        }
                                        room.room_deviceCount_on = onDevices.toString()
                                        room.room_deviceCount_off = offDevices.toString()
                                        room.room_total_device_count = totalDevice.toString()
                                        room.name_room = room.name_room
                                        room.roombg = room.roombg
                                        room.roomIcon = room.roomIcon

                                        roomsListDetails.add(room)
                                    }
                                    if (flag) {
                                        var totalDevice = 0
                                        var onDevices = 0
                                        var offDevices = 0
                                        for (i in IplistDevice.state) {
                                            totalDevice++
                                            if (i) {
                                                onDevices++

                                            } else {
                                                offDevices++
                                            }
                                        }
                                        var deviceTotalCount = room.room_total_device_count!!.toInt()
                                        var deviceOnCount = room.room_deviceCount_on!!.toInt()
                                        var deviceOffCount = room.room_deviceCount_off!!.toInt()
                                        var totalCount = deviceTotalCount + totalDevice
                                        var totalOn = deviceOnCount + onDevices
                                        var totalOff = deviceOffCount + offDevices
                                        room.room_deviceCount_on = totalOn.toString()
                                        room.room_deviceCount_off = totalOff.toString()
                                        room.room_total_device_count = totalCount.toString()
                                    }

                                }

                            }

                        }
                    }
                }
            }
        } else {
            roomsListDetails.clear()
            var flag = false
            for (IplistDevice in IpListDevices) {
                for (device in allDeviceList) {
                    if ((IplistDevice.name == device.name) and (IplistDevice.owned == 0)) {
                        if (IplistDevice.home == Constant.HOME) {
                            for (room in roomsList) {
                                // val rooms = RoomModel()
                                if (IplistDevice.room == room.name_room) {
                                    for (presentRoom in roomsListDetails) {
                                        if (presentRoom.name_room == room.name_room) {
                                            flag = true
                                            break
                                        } else {
                                            flag = false
                                            break
                                        }
                                    }
                                    if (!flag) {
                                        var totalDevice = 0
                                        var onDevices = 0
                                        var offDevices = 0
                                        for (i in IplistDevice.state) {
                                            totalDevice++
                                            if (i) {
                                                onDevices++

                                            } else {
                                                offDevices++
                                            }
                                        }
                                        room.room_deviceCount_on = onDevices.toString()
                                        room.room_deviceCount_off = offDevices.toString()
                                        room.room_total_device_count = totalDevice.toString()
                                        room.name_room = room.name_room
                                        room.roombg = room.roombg
                                        room.roomIcon = room.roomIcon

                                        roomsListDetails.add(room)
                                    }
                                    if (flag) {
                                        var totalDevice = 0
                                        var onDevices = 0
                                        var offDevices = 0
                                        for (i in IplistDevice.state) {
                                            totalDevice++
                                            if (i) {
                                                onDevices++

                                            } else {
                                                offDevices++
                                            }
                                        }
                                        var deviceTotalCount = room.room_total_device_count!!.toInt()
                                        var deviceOnCount = room.room_deviceCount_on!!.toInt()
                                        var deviceOffCount = room.room_deviceCount_off!!.toInt()
                                        var totalCount = deviceTotalCount + totalDevice
                                        var totalOn = deviceOnCount + onDevices
                                        var totalOff = deviceOffCount + offDevices
                                        room.room_deviceCount_on = totalOn.toString()
                                        room.room_deviceCount_off = totalOff.toString()
                                        room.room_total_device_count = totalCount.toString()
                                    }

                                }

                            }

                        }
                    }
                }
            }
            updateRoomList()
            //adapter.notifyDataSetChanged()
        }

    }

    override fun onAlldevice(name_room: String?, b: Boolean) {
        val listOfDevices: ArrayList<AuraSwitch> = localSqlDatabase.getDevicesForRoom(mDb!!, Constant.HOME!!, name_room!!)
        devicesList.clear()
        var ipList = IP.getIpDevices()

        for (device in listOfDevices) {
            for (l in ipList) {
                if (device.name == l.name) {
                    for (i in 0..3) {
                        l.state[i] = b
                        val load = Device(device.loads[i].icon!!, l.state[i]!!, l.dim[i], device.loads[i].name!!, localSqlDatabase.getRoomForDevice(mDb!!, device.name), device.name, device.loads[i].index!!, device.loads[i].dimmable!!)
                        if (l.condition[i] == "ready") {
                            if (l.local) {
                                load.status = "on"
                            } else {
                                load.status = "cloud"
                            }
                        } else {
                            load.status = "update"
                        }
                        devicesList.add(load)
                    }
                }

            }
        }

        var deviceLoad = sceneHandler.convertToAllLoad(devicesList)
        for (device in deviceLoad) {
            for (l in ipList) {
                if (device.device == l.name) {
                    var data = jsonHelper.serializeOnAllDevice(device, l.uiud!!, true, device.device!!)

                    if (l.ip == null) {
                        if (l.thing != null) {
                            if (context is DashboardActivity) {
                                var flag = (context as DashboardActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeOnAllDeviceAws(device, b))
                                if (!flag) {


                                }
                            }
                        }
                    } else {
                        if (l.local) {
                            ConnectTask(context!!, this, data, l.ip!!, device.device!!).execute()
                        } else {
                            if (l.thing != null) {
                                if (context is DashboardActivity) {
                                    var flag = (context as DashboardActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeOnAllDeviceAws(device, b))
                                    if (!flag) {


                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun offAllDevice(name_room: String?, b: Boolean) {
        val listOfDevices: ArrayList<AuraSwitch> = localSqlDatabase.getDevicesForRoom(mDb!!, Constant.HOME!!, name_room!!)

        var ipList = IP.getIpDevices()
        devicesList.clear()

        for (device in listOfDevices) {
            for (l in ipList) {
                if (device.name == l.name) {
                    for (i in 0..3) {
                        l.state[i] = b
                        val load = Device(device.loads[i].icon!!, l.state[i]!!, l.dim[i], device.loads[i].name!!, localSqlDatabase.getRoomForDevice(mDb!!, device.name), device.name, device.loads[i].index!!, device.loads[i].dimmable!!)
                        if (l.condition[i] == "ready") {
                            if (l.local) {
                                load.status = "on"
                            } else {
                                load.status = "cloud"
                            }
                        } else {
                            load.status = "update"
                        }
                        devicesList.add(load)
                    }
                }

            }
        }

        var deviceLoad = sceneHandler.convertToAllLoad(devicesList)
        for (device in deviceLoad) {
            for (l in ipList) {
                if (device.device == l.name) {
                    var data = jsonHelper.serializeOnAllDevice(device, l.uiud!!,
                            false, device.device!!)

                    if (l.ip == null) {
                        if (l.thing != null) {
                            if (context is DashboardActivity) {
                                var flag = (context as DashboardActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeOnAllDeviceAws(device, b))
                                if (!flag) {
                                    //l.condition[device.index] = "fail"

                                }
                            }
                        }
                    } else {
                        if (l.local) {
                            ConnectTask(context!!, this, data, l.ip!!, device.device!!).execute()
                        } else {
                            if (l.thing != null) {
                                if (context is DashboardActivity) {
                                    var flag = (context as DashboardActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeOnAllDeviceAws(device, b))
                                    if (!flag) {
                                        //l.condition[device.index] = "fail"

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onTcpMessageReceived(message: String) {
        updateStates(message)
    }


    private fun showpopup(v: View) {
        val popup = context?.let { PopupMenu(it, v) }
        val inflater = popup?.menuInflater
        inflater?.inflate(R.menu.room_add_option, popup.menu)
        popup?.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.addRoom -> {
                    val intent = Intent(context, AddRoomActivity::class.java)
                    intent.putExtra("ROOM_NAME", "Room Name")
                    intent.putExtra("ROOM_EDIT_TYPE", "create")
                    startActivity(intent)
                    true
                }
                else -> false
            }
        }
        popup?.show()
    }


    fun updateStates(message: String) {

        if (message.contains("ERROR")) {
            deviceError = -1
            var data = message.split(":")
            if (deviceError != 0) {
                for (x in data) {
                    for (l in IpListDevices) {
                        if (l.aws) {
                            for (i in 0..3) {
                                l.condition[i] = "ready"
                            }
                        } else {
                            Thread(Runnable {
                                if (Encryption.isInternetWorking()) {
                                    try {
                                        for (l in IpListDevices) {
                                            if (l.owned == 0) {
                                                l.local = false
                                                for (i in 0..3) {
                                                    l.condition[i] = "update"
                                                }
                                                onUiThread {
                                                    if (l.thing != null) {
                                                        if (serviceConnection) {
                                                            if (context is DashboardActivity) (context as DashboardActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeLEDData())
                                                        }
                                                    }
                                                }
                                            }
                                            IP.registerIpDevice(l)
                                            break
                                        }

                                    } catch (e: Exception) {
                                        Log.d("Error", "Check Internet connection")
                                    }

                                } else {
                                    onUiThread {
                                        toast("Check internet connection.")
                                        for (l in IpListDevices) {
                                            l.local = false
                                            l.aws = false
                                            for (i in 0..3) {
                                                l.condition[i] = "fail"
                                            }
                                            IP.registerIpDevice(l)
                                            break
                                        }
                                    }
                                }
                            }).start()

                        }

                    }
                }
            }

        } else {
            val updatedDevice: AuraSwitch = jsonHelper.deserializeTcp(message)
            when (updatedDevice.type) {
                1 -> {
                    for (l in IpListDevices) {
                        if (l.name == updatedDevice.name) {
                            l.local = true
                            l.aws = false
                            l.state[0] = updatedDevice.state[0] == 1
                            l.state[1] = updatedDevice.state[1] == 1
                            l.state[2] = updatedDevice.state[2] == 1
                            l.state[3] = updatedDevice.state[3] == 1
                            l.dim[0] = updatedDevice.dim[0]
                            l.dim[1] = updatedDevice.dim[1]
                            l.dim[2] = updatedDevice.dim[2]
                            l.dim[3] = updatedDevice.dim[3]
                            for (i in 0..3) {
                                l.condition[i] = "ready"
                            }
                            IP.registerIpDevice(l)
                            updateDeviceCount(roomsList, true)
                            break
                        }
                    }

                }

                4 -> {
                    if (updatedDevice.error == 1) {
                        for (l in IpListDevices) {
                            if (l.name == updatedDevice.name) {
                                l.local = false
                                l.aws = false
                                for (i in 0..3) {
                                    l.condition[i] = "fail"
                                }
                                IP.registerIpDevice(l)
                                break
                            }
                        }
                        toast("Device used by someone,Unauthorized Access")
                    } else {
                        for (l in IpListDevices) {
                            if (l.name == updatedDevice.name) {
                                l.local = true
                                l.state[0] = updatedDevice.state[0] == 1
                                l.state[1] = updatedDevice.state[1] == 1
                                l.state[2] = updatedDevice.state[2] == 1
                                l.state[3] = updatedDevice.state[3] == 1
                                l.dim[0] = updatedDevice.dim[0]
                                l.dim[1] = updatedDevice.dim[1]
                                l.dim[2] = updatedDevice.dim[2]
                                l.dim[3] = updatedDevice.dim[3]
                                for (i in 0..3) {
                                    l.condition[i] = "ready"
                                }
                                l.aws = false
                                IP.registerIpDevice(l)
                                updateDeviceCount(roomsList, true)
                                break
                            }
                        }
                    }
                }
                else -> {
                    for (l in IpListDevices) {
                        if (l.name == updatedDevice.name) {
                            l.state[0] = updatedDevice.state[0] == 1
                            l.state[1] = updatedDevice.state[1] == 1
                            l.state[2] = updatedDevice.state[2] == 1
                            l.state[3] = updatedDevice.state[3] == 1
                            l.dim[0] = updatedDevice.dim[0]
                            l.dim[1] = updatedDevice.dim[1]
                            l.dim[2] = updatedDevice.dim[2]
                            l.dim[3] = updatedDevice.dim[3]
                            l.local = true
                            l.aws = false
                            for (i in 0..3) {
                                l.condition[i] = "ready"
                            }
                            IP.registerIpDevice(l)
                            updateDeviceCount(roomsList, true)
                            break
                        }
                    }
                }

            }
        }
    }

}
