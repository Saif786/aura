package com.wozart.aura.ui.createautomation

import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.preference.PreferenceManager
import android.support.v4.app.JobIntentService
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingEvent
import com.google.gson.Gson
import com.wozart.aura.utilities.Constant
import okhttp3.*
import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL



class GeofenceService : JobIntentService() {
    var geoData= GeoDetailsData()
    var geoDataDetail :MutableList<GeoDetailsData> = ArrayList()
    private lateinit var client: OkHttpClient
    private var JOB_ID = 573
    private var TAG = "GeoService"

     fun enqueueWork(context: Context, intent: Intent) {
          enqueueWork(context, GeofenceService::class.java, JOB_ID, intent)
     }

     override fun onHandleWork(intent: Intent) {


         var geofenceEvent = GeofencingEvent.fromIntent(intent)
         var geofencetriggring: MutableList<Geofence>
         if(geofenceEvent.hasError()){
             var geofenceError = GeofenceErrorMessages.getErrorString(this,geofenceEvent.errorCode)
             Log.i(TAG, "Found error$geofenceError")
         }
         val geofenceTransition = geofenceEvent.geofenceTransition
         if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER || geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT ){
             geofencetriggring = geofenceEvent.triggeringGeofences
             var geofenceDetails = getGeofenceDetails(this,geofenceTransition,geofencetriggring)
             Log.i(TAG,geofenceDetails)
         }else{
             Log.i(TAG,"Error in transition")
         }
     }

     fun getGeofenceDetails(context: Context,geofenceTransition:Int,triggring:MutableList<Geofence>):String {
         Log.d(TAG, "===============> getGeofenceTransitionDetails()")
         val geofenceTransitionString = getTransitionString(geofenceTransition)
         val triggeringGeofencesIdsList = triggring.map { geofence -> geofence.requestId }
         return geofenceTransitionString.toString() + ": " + TextUtils.join(", " , triggeringGeofencesIdsList)
     }

    private fun sendNotification(geoDetailsData: MutableList<GeoDetailsData>){

                val url = URL("https://bpesxehfze.execute-api.us-east-1.amazonaws.com/testGeo/")
                val connection = url.openConnection() as HttpURLConnection
                connection.requestMethod = "POST"
                connection.connectTimeout = 300000
                connection.connectTimeout = 300000
                connection.doOutput = true

                val json = Gson().toJson(geoDetailsData)

                connection.setRequestProperty("charset", "utf-8")
                connection.setRequestProperty("Content-lenght", json.toString())
                connection.setRequestProperty("Content-Type", "application/json")
                //connection.setRequestProperty("authorization", "Bearer teNa5F6AGgIKwxg54fOzJRjNoWyGZaCv")

                try {
                    val outputStream: DataOutputStream = DataOutputStream(connection.outputStream)
                    outputStream.write(json.toByteArray())
                    outputStream.flush()
                } catch (exception: Exception) {

                }
                if (connection.responseCode != HttpURLConnection.HTTP_OK && connection.responseCode != HttpURLConnection.HTTP_CREATED) {
                    try {
                        val reader: BufferedReader = BufferedReader(InputStreamReader(connection.inputStream))
                        val output: String = reader.readLine()
                        Toast.makeText(this,"There was error $output",Toast.LENGTH_SHORT).show()
                        System.exit(0)

                    } catch (exception: Exception) {
                        throw Exception("Exception while push the notification  $exception.message")
                    }
                }
    }


    fun getTransitionString(geofenceTransition:Int){
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val userId = prefs.getString("ID", "NULL")
        val automationName = prefs.getString("AUTOMATION_NAME","NULL")

         var am: AudioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
         Log.i(TAG, "geofenceTransition = " + geofenceTransition + " Enter : " +
                 Geofence.GEOFENCE_TRANSITION_ENTER + "Exit : " + Geofence.GEOFENCE_TRANSITION_EXIT)
         when (geofenceTransition) {
             Geofence.GEOFENCE_TRANSITION_ENTER  -> {
                 geoData.passage = "Arriving"
                 geoData.Geomessage ="Entered to Location"
                 geoData.geoUserId = userId
                 geoData.scheduleName = automationName
                 geoData.Geohome = Constant.HOME
                 geoDataDetail.add(geoData)
                 sendNotification(geoDataDetail)
                 //am.ringerMode = AudioManager.RINGER_MODE_VIBRATE
             }
             Geofence.GEOFENCE_TRANSITION_EXIT -> {
                 geoData.passage = "Leaving"
                 geoData.Geomessage = "Exited the Location"
                 geoData.geoUserId = userId
                 geoData.scheduleName = automationName
                 geoData.Geohome = Constant.HOME
                 geoDataDetail.add(geoData)
                 sendNotification(geoDataDetail)
                // am.ringerMode = AudioManager.RINGER_MODE_NORMAL
             }
             else -> {
                 Toast.makeText(this,"oh! cow eyes something bad happen!!",Toast.LENGTH_SHORT).show()
                 Log.e(TAG, "Error ")
             }
         }
    }
}