package com.wozart.aura.ui.login

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.Toast
import butterknife.ButterKnife
import com.auth0.android.Auth0
import com.auth0.android.authentication.AuthenticationException
import com.auth0.android.authentication.storage.SecureCredentialsManager
import com.auth0.android.provider.AuthCallback
import com.auth0.android.provider.WebAuthProvider
import com.auth0.android.result.Credentials
import com.bumptech.glide.Glide
import com.wozart.aura.R
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.dynamoDb.UserTableHandler
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.ui.base.BaseAbstractActivity
import com.wozart.aura.utilities.Encryption.isInternetWorking
import kotlinx.android.synthetic.main.activity_google_login.*
import okhttp3.*
import org.json.JSONObject
import java.io.IOException


/**
 * Activity to demonstrate basic retrieval of the Google user's ID, email address, and basic
 * profile.
 */
class GoogleLoginActivity : BaseAbstractActivity() {

    private var isLoginButtonClick: Boolean = false

    private lateinit var auth0: Auth0
    private lateinit var client: OkHttpClient

    private var credentialsManager: SecureCredentialsManager? = null
    private var loginType: String? = null
    var lastBackPressTime :Long = 0
    private var toast: Toast? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_google_login)
        val btn_submit = findViewById<Button>(R.id.btn_submit)
        ButterKnife.bind(this)
        isLoginButtonClick = false
        loginType = intent.getStringExtra("type")
        if (loginType == "Verify account") {
            btn_submit.text = loginType
        } else {
            btn_submit.text = loginType
        }
        init()

        auth0 = Auth0(this)
        auth0.isOIDCConformant = true
        client = OkHttpClient()

    }

    //login with email auth 0
    private fun loginWithEmail() {
        WebAuthProvider.init(auth0)
                .withScheme("demo")
                .withAudience(String.format("https://%s/userinfo", getString(R.string.com_auth0_domain)))
                .withScope("openid offline_access profile email")
                .start(this@GoogleLoginActivity, object : AuthCallback {
                    override fun onFailure(dialog: Dialog) {
                        // Show error Dialog to user
                    }

                    override fun onFailure(exception: AuthenticationException) {
                        runOnUiThread {
                            Log.d("ERROR :", "Error $exception")
                            val mainIntent = Intent(this@GoogleLoginActivity, GoogleLoginActivity::class.java)
                            mainIntent.putExtra("type", "Login")
                            startActivity(mainIntent)
                            Toast.makeText(this@GoogleLoginActivity, "Error: " + exception.message, Toast.LENGTH_SHORT).show()
                        }

                    }

                    override fun onSuccess(credentials: Credentials) {
                        runOnUiThread {
                            getWebservice(credentials.accessToken, "Login")
                        }
                    }
                })
    }

    private fun getWebservice(tokens: String?, type: String) {
        val request = Request.Builder().url("https://wozart.auth0.com/userinfo")
                .addHeader("authorization", "Bearer " + tokens!!)
                .build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                runOnUiThread {
                    //token.setText("Failure !");
                }
            }

            override fun onResponse(call: Call, response: Response) {
                runOnUiThread {
                    try {
                        updateUIIndipendent(response.body()!!.string(), tokens, type)
                    } catch (ioe: IOException) {
                        Log.d("LOGIN", "Login : " + "Error during get body")
                    }
                }
            }
        })
    }

    private fun updateUIIndipendent(account: String, tokens: String?, type: String) {
        Log.d("LOGIN", "Login account: $account")
        try {
            var personName = "User"
            val user = JSONObject(account)
            if (user.isNull("given_name")) {
                personName = user.getString("nickname")
                //email login
            } else {
                val personFirstName = user.getString("given_name")
                val personLastName = user.getString("family_name")
                personName = "$personFirstName $personLastName"
                //gmail login
            }

            val personEmail = user.getString("email")
            val personPhoto = user.getString("picture")
            val personId_string = user.getString("sub")
            val email_verified = user.getString("email_verified")
            val splitIndex = personId_string.indexOf("|")
            val personId = personId_string.substring(splitIndex + 1)


            val prefEditor = PreferenceManager.getDefaultSharedPreferences(this@GoogleLoginActivity).edit()
            prefEditor.putString("USERNAME", personName)
            prefEditor.putString("EMAIL", personEmail)
            prefEditor.putString("ID", "us-east-1:$personId")
            prefEditor.putString("VERIFIED_EMAIL", email_verified)
            prefEditor.putString("PROFILE_PICTURE", personPhoto)
            prefEditor.putString("HOME", "My Home")
            prefEditor.putString("TOKEN", tokens)
            Log.d("LOGIN", "Login account: $tokens")
            prefEditor.apply()

            registerUserInAws("us-east-1:$personId", personName, personEmail)


            if ((email_verified == "true") and (loginType =="Login")) {
                registerUserRulesTable("us-east-1:$personId", personName, personEmail, email_verified)
                runOnUiThread {
                    val mainIntent = Intent(this@GoogleLoginActivity, DashboardActivity::class.java)
                    this@GoogleLoginActivity.startActivity(mainIntent)
                    this@GoogleLoginActivity.finish()
                }
            } else if((email_verified == "true") and (loginType == "Verify account")){
                btn_submit.text = "Done"
                verify_account.visibility = View.VISIBLE
                verify_account.text = "Your mail has been verified. \n Click Done to process."
                btn_submit.visibility = View.VISIBLE
                loginType = "Login"

            }
            else {
//                Glide.with(this).load(R.drawable.login_background_city).into(background_login)
                if (type == "Login") {
                    btn_submit.text = "Verify Account"
                    btn_submit.visibility = View.VISIBLE
                    loginType = "Verify account"
                } else {
                    verifyEmail()
                }

            }


        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun verifyEmail() {
        val intent = packageManager.getLaunchIntentForPackage("com.google.android.gm")
        startActivity(intent)
    }

    private fun registerUserInAws(userId: String, userName: String, email: String) {
        val userDynamo = UserTableHandler()
        Thread(Runnable {
            if (isInternetWorking()) {
                if (!userDynamo.isUserAlreadyRegistered(userId)) {
                    userDynamo.insertUser(userId, userName, email)
                }
            }
        }).start()
    }

    private fun registerUserRulesTable(userId: String, userName: String, email: String, verified: String) {
        var rulesDynamoTable = RulesTableHandler()
        Thread(Runnable {
            if (isInternetWorking()) {
                var data = rulesDynamoTable.isUserAlreadyRegistered(userId)
                if (!data) {
                    rulesDynamoTable.insertUser(userId, userName, email, verified)
                }
            }
        }).start()
    }

    fun init() {
        Glide.with(this).load(R.drawable.login_screen_empty).into(background_login)
        btn_submit.setOnClickListener {
            if (loginType == "Verify account") {
                val prefs = PreferenceManager.getDefaultSharedPreferences(this)
                val tokens = prefs.getString("TOKEN", "NULL")
                getWebservice(tokens, "verify")
            } else {
                loginWithEmail()
               btn_submit.visibility = View.INVISIBLE
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.finish()

    }
}

