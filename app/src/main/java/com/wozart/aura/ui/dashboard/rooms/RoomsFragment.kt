package com.wozart.aura.ui.dashboard.rooms

import android.app.Dialog
import android.content.*
import android.database.sqlite.SQLiteDatabase
import android.net.nsd.NsdServiceInfo
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import aura.wozart.com.aura.entity.network.Nsd
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wozart.aura.R
import com.wozart.aura.data.model.Room
import com.wozart.aura.ui.dashboard.Device
import com.wozart.aura.aura.ui.dashboard.listener.OnDeviceOptionsListener
import com.wozart.aura.aura.ui.dashboard.listener.OnOptionsListener
import com.wozart.aura.aura.ui.dashboard.rooms.AddRoomActivity
import com.wozart.aura.aura.ui.dashboard.rooms.RoomsListAdapter
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.ui.home.HomeDetailsActivity
import com.wozart.aura.data.device.DeviceHandler
import com.wozart.aura.data.device.IpHandler
import com.wozart.aura.data.device.SceneHandler
import com.wozart.aura.data.model.DeviceUiud
import com.wozart.aura.data.model.IpModel
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraComplete
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.entity.network.ConnectTask
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.createautomation.OnFragmentInteractionListener
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.createscene.CreateSceneActivity
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.ui.dashboard.Scenes
import com.wozart.aura.ui.dashboard.ScenesAdapter
import com.wozart.aura.ui.dashboard.room.RoomActivity
import com.wozart.aura.ui.dashboard.room.RoomFragment
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.Encryption
import com.wozart.aura.utilities.JsonHelper
import kotlinx.android.synthetic.main.fragment_rooms.*
import org.jetbrains.anko.support.v4.longToast
import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import kotlin.concurrent.thread
import kotlin.system.exitProcess

/***
 * Created by Kiran on 14-03-2018.
 */

class RoomsFragment : Fragment(), OnOptionsListener, OnDeviceOptionsListener, RoomsAdapter.OnLoadPressedListiner, RoomsAdapter.OnScenePressedListener, ConnectTask.TcpMessageReceiver {


    private var mListener: OnFragmentInteractionListener? = null
    private val LOG_TAG = RoomsFragment::class.java.simpleName

    private var optionType: Int = 0
    private val localSqlDatabase = DeviceTable()
    private val localSqlUtils = UtilsTable()
    private var mDb: SQLiteDatabase? = null
    private var mDbUtils: SQLiteDatabase? = null
    private var sceneHandler: SceneHandler = SceneHandler()
    private lateinit var nsd: Nsd
    private var jsonHelper: JsonHelper = JsonHelper()
    private lateinit var roomsAdapter: RoomsAdapter
    var roomNameOld: String? = null
    var usertype = true
    var IpListDevices: MutableList<IpModel> = ArrayList()
    var sceneDeviceList = ArrayList<AuraComplete>()
    private var IP = IpHandler()
    var urlType: String? = null


    override fun onDeviceOptionsClicked(view: View) {
        optionType = 1
        showPopup(view)
    }

    override fun onOptionsClicked(view: View) {
        optionType = 0
        showPopup(view)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_rooms, container, false)
        return view
    }

    /*companion object {
        fun newInstance(): RoomsFragment {
            return RoomsFragment()
        }
    }*/

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var addRoomBtn = view.findViewById<ImageView>(R.id.addRoomBtn)
        addRoomBtn.visibility = View.VISIBLE
        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase

        nsd = Nsd(context!!, "ROOM")

        val dbUtils = UtilsDbHelper(context!!)
        mDbUtils = dbUtils.writableDatabase

        roomNameOld = (context as RoomActivity).getRoomName()
        IpListDevices = IP.getIpDevices()

        for (l in IpListDevices) {
            if (l.room == roomNameOld) {
                if (l.owned == 0) {
                    try {
                        if (l.full_device_name!!.endsWith("6010D4")) {
                            urlType = "Device Found"
                            typeUrl(l.uiud!!, l.ip!!, urlType!!)
                        } else {
                            sendTcpConnect(l.uiud!!, l.ip!!, "XXXXXX")
                        }
                    } catch (e: Exception) {
                        Log.d(LOG_TAG, "Error : " + e.stackTrace)
                    }
                }
            }
        }


        var listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")
        for (x in listRoom) {
            if ((x.type == "room") and (x.sharedHome == Constant.HOME)) {
                if (x.name == roomNameOld) {
                    val contentView = activity!!.findViewById(R.id.rooms_main_activity) as RelativeLayout
                    Utils.setRoomDrawable(context!!, contentView, x.bgUrl.toInt())
                }
            } else if ((x.type == "room") and (x.sharedHome == "default")) {
                if (x.name == roomNameOld) {
                    val contentView = activity!!.findViewById(R.id.rooms_main_activity) as RelativeLayout
                    Utils.setRoomDrawable(context!!, contentView, x.bgUrl.toInt())
                }
            } else if (x.name == Constant.HOME) {
                if (x.sharedHome == "guest") {
                    addRoomBtn.visibility = View.INVISIBLE
                    val contentView = activity!!.findViewById(R.id.rooms_main_activity) as RelativeLayout
                    Utils.setDrawable(context!!, contentView, x.bgUrl.toInt())
                    usertype = false
                }
            }
        }

        nsdDiscovery()
        LocalBroadcastManager.getInstance(context!!).registerReceiver(
                onNSDServiceResolved, IntentFilter("nsdDiscoverRoom"))
        LocalBroadcastManager.getInstance(context!!).registerReceiver(
                onTcpServerMessageReceived, IntentFilter("intentKey"))
        LocalBroadcastManager.getInstance(context!!).registerReceiver(
                onAwsMessageReceived, IntentFilter("AwsShadow"))


        Thread(Runnable {
            try {
                Thread.sleep(1000)
                for (l in IpListDevices) {
                    if (l.owned == 0) {
                        if (!l.local) {
                            if (!l.aws) {
                                if (l.thing != null) {
                                    var flag = (context as RoomActivity).isServiceConnected()
                                    if ((context is RoomActivity) and (flag)) {
                                        (context as RoomActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeLEDData())

                                    }
                                }
                            }

                        }
                    }

                }

            } catch (e: Exception) {
                Log.d(LOG_TAG, "Error : " + e.stackTrace)
            }
        }).start()

        sceneDeviceList = localSqlDatabase.getAllDevicesScenes(mDb!!, Constant.HOME!!)
        var dummyRoom = Room()
        dummyRoom.roomName = roomNameOld
        val roomsList: ArrayList<Room> = ArrayList()
        roomsList.add(dummyRoom)
        roomsRv.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        roomsAdapter = RoomsAdapter(context!!, this, this, roomsList, this, this) { prevPosition: Int, position: Int ->
            if (prevPosition != -1 && prevPosition != position)
                roomsRv.adapter!!.notifyItemChanged(prevPosition)
            (roomsRv.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(position, 0)
        }
        roomsRv.adapter = roomsAdapter
        addRoomBtn.setOnClickListener {
            showPopup(addRoomBtn)
        }
        editRoom.setOnClickListener {
            showRoomsDialog()
        }
        back_btn.setOnClickListener {
            var intent = Intent(context, DashboardActivity::class.java)
            intent.putExtra("TAB_SET", Constant.ROOMS_TAB)
            this.startActivity(intent)
        }
    }

    /**
     * Handler url for esp
     */

    private fun typeUrl(uiudDevice: String, ip: String, name: String) {
        thread {
            var encryptedData: String? = null
            if (name == "Device Found") {
                var json = jsonHelper.initialData(uiudDevice)
                encryptedData = Encryption.encryptMessage(json)
            } else {
                if (name == "Control") {
                    encryptedData = Encryption.encryptMessage(uiudDevice)
                }
            }
            val url = URL("http://$ip/android")
            val connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "PUT"
            connection.connectTimeout = 300000
            connection.connectTimeout = 300000
            connection.doOutput = true

            connection.setRequestProperty("charset", "utf-8")
            connection.setRequestProperty("Content-lenght", encryptedData)
            connection.setRequestProperty("Content-Type", "application/json")
            connection.setRequestProperty("authorization", "Bearer teNa5F6AGgIKwxg54fOzJRjNoWyGZaCv")

            try {
                val outputStream: DataOutputStream = DataOutputStream(connection.outputStream)
                outputStream.write(encryptedData!!.toByteArray())
                outputStream.flush()
            } catch (exception: Exception) {

            }
            if (connection.responseCode == HttpURLConnection.HTTP_OK) {
                try {
                    val reader: BufferedReader = BufferedReader(InputStreamReader(connection.inputStream))
                    val output: String = reader.readLine()
                    val decryptedData = Encryption.decryptMessage(output)
                    updateStates(decryptedData)
                } catch (exception: Exception) {
                    Log.d("ERROR", "Exception while push the Notification")
                }
            } else {
                Log.d("error", "check connection")
                exitProcess(0)
            }
        }
    }

    override fun onLoadPressed(auraDevice: Device, longPressed: Boolean) {

        if (roomNameOld == auraDevice.roomName) {

            var dimValue: Int = -1
            var cFlag = false
            if (longPressed) {
                cFlag = true
            }
            for (l in IpListDevices) {
                if (l.name == auraDevice.deviceName) {
                    val data = jsonHelper.serialize(auraDevice, l.uiud!!, cFlag)
                    l.condition[auraDevice.index] = "update"
                        if (l.ip == null) {
                            if (l.thing != null) {
                                if (context is RoomActivity) {
                                    var flag = (context as RoomActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeDataToAws(auraDevice, l.uiud!!, cFlag))
                                    if (!flag) {
                                        l.condition[auraDevice.index] = "fail"

                                    }
                                }
                            }
                        } else {
                            if (l.local) {
                                ConnectTask(context!!, this, data, l.ip!!, auraDevice.deviceName).execute()
                            } else {
                                if (l.aws) {
                                    if (l.thing != null) {
                                        if (context is RoomActivity) {
                                            var flag = (context as RoomActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeDataToAws(auraDevice, l.uiud!!, cFlag))
                                            if (!flag) {
                                                l.condition[auraDevice.index] = "fail"

                                            }
                                        }
                                    }
                                } else {
                                    if ((!l.local) and (!l.aws)) {
                                        l.condition[auraDevice.index] = "fail"
                                    }
                                }

                            }
                        }
                    IP.registerIpDevice(l)
                }

            }

            roomsAdapter.updateAppearance(IpListDevices)
        }
    }

    override fun onScenePressed(scene: Scenes) {

        val sceneList = sceneHandler.convertToSceneModel(scene.rooms)
        val isSceneOn = !scene.isOn
        for (sceneFromList in sceneList) {
            for (l in IpListDevices) {
                if (l.name == sceneFromList.device) {
                    val data = jsonHelper.serializeSceneData(sceneFromList, l.uiud!!, isSceneOn)
                    if (l.ip == null) {
                        l.aws = true
                        if (l.thing != null) {
                            if (context is RoomActivity) {
                                (context as RoomActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeSceneDataForAws(sceneFromList, isSceneOn))
                                // scene.isOn = isSceneOn
                                roomsAdapter.updateScene(scene)
                            }
                        }
                    } else {
                        if (l.aws == false) {
                            ConnectTask(context!!, this, data, l.ip!!, sceneFromList.device!!).execute()
                            //scene.isOn = isSceneOn
                            roomsAdapter.updateScene(scene)
                        } else {
                            if (l.thing != null) {
                                if (context is RoomActivity) {
                                    (context as RoomActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeSceneDataForAws(sceneFromList, isSceneOn))
                                    //scene.isOn = isSceneOn
                                    roomsAdapter.updateScene(scene)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Data from the TcpClient
     */
    override fun onTcpMessageReceived(message: String) {
        for (iplist in IpListDevices) {
            if (iplist.room == roomNameOld) {
                IpListDevices = roomsAdapter.updateState(message, IpListDevices, sceneDeviceList)
                roomsAdapter.updateAppearance(IpListDevices)
            }
        }

    }

    fun updateStates(message: String) {
        for (iplist in IpListDevices) {
            if (iplist.room == roomNameOld) {
                IpListDevices = roomsAdapter.updateState(message, IpListDevices, sceneDeviceList)
                roomsAdapter.updateAppearance(IpListDevices)
            }
        }
    }

    /**
     * Data from the TcpServer
     */
    private val onTcpServerMessageReceived = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            val data = intent.getStringExtra("key")
            for (ip in IpListDevices) {
                if (ip.room == roomNameOld) {
                    IpListDevices = roomsAdapter.updateState(data, IpListDevices, sceneDeviceList)
                    roomsAdapter.updateAppearance(IpListDevices)
                }
            }
        }
    }

    /**
     * Data from the AwsShadowUpdate
     */
    private val onAwsMessageReceived = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val shadow = intent.getStringExtra("data")
            val segments = shadow.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (shadow !== "Connected") {
                val device = localSqlDatabase.getDeviceForThing(mDb!!, segments[1])
                val data = jsonHelper.deserializeAwsData(segments[0])
                for (ip in IpListDevices) {
                    if (ip.room == roomNameOld) {
                        IpListDevices = roomsAdapter.updateStateFromAws(device, data, IpListDevices)
                        roomsAdapter.updateAppearance(IpListDevices)
                    }
                }
            }
        }
    }

    /**
     * Data from the TcpServer : Asynchronous
     */
    private val onNSDServiceResolved = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d("NSD_DEBUG_HOME", "nsd onReceive in HomeFragment")
            val name = intent.getStringExtra("name")
            val ip = intent.getStringExtra("ip")
            val device = name.substring(name.length - 6, name.length)
            //check model and insert data
            var ipDevice = IpModel()
            ipDevice.ip = ip
            ipDevice.name = device
            var isDeviceExists = false
            for (l in IpListDevices) {
                if (l.name == ipDevice.name) {
                    l.ip = ip
                    ipDevice = l
                    isDeviceExists = true
                    break
                }
            }
            if (!isDeviceExists) {
                ipDevice.aws = false
                ipDevice.uiud = "xxxxxxxxxxxxxxxxx"
                ipDevice.owned = 1
            }
            IP.registerIpDevice(ipDevice)
        }
    }

    private fun sendTcpConnect(uiud: String, ip: String, name: String) {
        ConnectTask(context!!, this, jsonHelper.initialData(uiud), ip, name).execute("")

    }

    private fun nsdDiscovery() {
        if (nsd.getDiscoveryStatus()) {
            nsd.setBroadcastType("ROOM")
            nsd.stopDiscovery()
            nsd.initializeNsd()
            nsd.discoverServices()
        } else {
            nsd.setBroadcastType("ROOM")
            nsd.initializeNsd()
            nsd.discoverServices()
        }

    }

    fun showPopup(v: View) {
        val popup = context?.let { PopupMenu(it, v) }
        val inflater = popup?.menuInflater
        inflater?.inflate(R.menu.room_options_menu, popup.menu)
        popup?.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.addScene -> {
                    if (usertype) {
                        val intent = Intent(context, CreateSceneActivity::class.java)
                        intent.putExtra("inputSceneType", "create")
                        intent.putExtra("inputSceneName", "")
                        intent.putExtra("inputSceneIconUrl", 0)
                        startActivity(intent)
                    } else {
                        longToast("Guest cannot edit Shared Room")
                    }
                    true


                }
                R.id.editRoom -> {
                    // showRoomsDialog()
                    if (usertype) {
                        val intent = Intent(activity, AddRoomActivity::class.java)
                        intent.putExtra("NAME_ROOM", roomNameOld)
                        intent.putExtra("ROOM_EDIT_TYPE", "edit")
                        startActivity(intent)
                    } else {
                        longToast("Guest cannot edit Shared Room")
                    }
                    true
                }
                else -> false
            }
        }
        popup?.show()
    }

    private fun showRoomsDialog() {

        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_rooms)
        dialog.setCanceledOnTouchOutside(true)
        val roomsList = ArrayList<Room>()
        val room = Room()
        val room1 = Room()
        val room2 = Room()
        room.roomName = "Living Room"
        roomsList.add(room)
        room1.roomName = "Hall"
        roomsList.add(room1)
        //room2.roomName = "Kitchen"
        //roomsList.add(room2)
        val roomsRv = dialog.findViewById<RecyclerView>(R.id.roomsRv)
        roomsRv.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        roomsRv.adapter = RoomsListAdapter(roomsList) { _: Room ->
            val intent = Intent(activity, AddRoomActivity::class.java)
            startActivity(intent)
            dialog.dismiss()
        }
        dialog.show()
    }
}


