package com.wozart.aura.ui.dashboard.home

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wozart.aura.R
import com.wozart.aura.aura.ui.dashboard.home.Home
import com.wozart.aura.utilities.Constant.Companion.EDIT_HOME
import com.wozart.aura.utilities.Constant.Companion.SELECTED_HOME
import com.wozart.aura.utilities.Constant.Companion.SHARE_HOME
import kotlinx.android.synthetic.main.item_home.view.*

/***
 * Created by Kiran on 16-03-2018.
 */
class HomeListAdapter(private val homeList: ArrayList<Home>, val listener: (Home, Int) -> Unit) : RecyclerView.Adapter<HomeListAdapter.HomeHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHolder {
        val inflatedView = LayoutInflater.from(parent?.context).inflate(R.layout.item_home, parent, false)
        return HomeHolder(inflatedView)
    }

    override fun getItemCount(): Int {
        return homeList.size
    }

    override fun onBindViewHolder(holder: HomeHolder, position: Int) {
        holder?.bind(homeList.get(position), listener)
    }

    class HomeHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(home: Home, listener: (Home, Int) -> Unit) = with(itemView) {
            itemView.homeTitle.text = home.title
            if (!home.sharedFlag) {
                if (home.isInActive) {
                    itemView.homeTitle.setTextColor(resources.getColor(R.color.colorPrimary))
                    itemView.shareHome.setImageResource(R.drawable.svg_share_active)
                    itemView.editHome.setImageResource(R.drawable.svg_edit_active)
                } else {
                    itemView.homeTitle.setTextColor(resources.getColor(R.color.black))
                    itemView.shareHome.setImageResource(R.drawable.svg_share_inactive)
                    itemView.editHome.setImageResource(R.drawable.svg_edit_inactive)
                }
            } else {
                if (home.isInActive) {
                    itemView.homeTitle.setTextColor(resources.getColor(R.color.colorPrimary))
                    itemView.shareHome.visibility = View.INVISIBLE
                    itemView.editHome.visibility = View.INVISIBLE
                } else {
                    itemView.homeTitle.setTextColor(resources.getColor(R.color.black))
                    itemView.shareHome.visibility = View.INVISIBLE
                    itemView.editHome.visibility = View.INVISIBLE
                }
            }

            itemView.shareHome.setOnClickListener() {
                listener(home, SHARE_HOME)
            }

            itemView.setOnClickListener {
                listener(home, SELECTED_HOME)
            }

            itemView.editHome.setOnClickListener() {
                listener(home, EDIT_HOME)
            }

        }
    }
}