package com.wozart.aura.ui.dashboard.home

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.*
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import aura.wozart.com.aura.entity.network.Nsd
import com.google.gson.Gson
import com.warkiz.widget.IndicatorSeekBar
import com.wozart.aura.R
import com.wozart.aura.ui.createscene.CreateSceneActivity
import com.wozart.aura.aura.ui.dashboard.home.Home
import com.wozart.aura.aura.ui.dashboard.listener.OnDeviceOptionsListener
import com.wozart.aura.aura.ui.dashboard.listener.OnOptionsListener
import com.wozart.aura.ui.home.HomeDetailsActivity
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.device.SceneHandler
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.entity.network.ConnectTask
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.ui.dashboard.*
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.Constant.Companion.CREATE_HOME_ROOM
import com.wozart.aura.utilities.Constant.Companion.EDIT_HOME
import com.wozart.aura.utilities.Constant.Companion.SELECTED_HOME
import com.wozart.aura.utilities.Constant.Companion.SHARE_HOME
import com.wozart.aura.utilities.JsonHelper
import kotlinx.android.synthetic.main.dialog_configure_edit_dimming.*
import org.jetbrains.anko.support.v4.toast
import android.preference.PreferenceManager
import android.support.v4.widget.SwipeRefreshLayout
import android.widget.*
import com.bumptech.glide.Glide
import com.wozart.aura.aura.ui.dashboard.rooms.AddRoomActivity
import com.wozart.aura.aura.utilities.Utils.getIconDrawable
import com.wozart.aura.data.device.IpHandler
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.model.IpModel
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraComplete
import com.wozart.aura.entity.model.aura.AuraType
import com.wozart.aura.entity.model.scene.Scene
import com.wozart.aura.entity.service.AwsPubSub
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.more.ShareAuraActivity
import com.wozart.aura.ui.wifisettings.*
import com.wozart.aura.utilities.Encryption
import com.wozart.aura.utilities.Encryption.isInternetWorking
import kotlinx.android.synthetic.main.dialogue_edit_home.*
import kotlinx.android.synthetic.main.fragment_home.*
import okhttp3.*
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.intentFor
import org.jetbrains.anko.support.v4.onUiThread
import org.json.JSONArray
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import kotlin.concurrent.thread
import kotlin.system.exitProcess


/***
 * Created by Kiran on 14-03-2018.
 */

class HomeFragment : Fragment(), OnOptionsListener, OnDeviceOptionsListener, ConnectTask.TcpMessageReceiver, FetchData.FetchDataCallbackInterface {


    private val LOG_TAG = HomeFragment::class.java.simpleName

    private var optionType: Int = 0
    private val localSqlDatabase = DeviceTable()
    private val localSqlScene = SceneTable()
    private val localSqlUtils = UtilsTable()
    private var mDbScene: SQLiteDatabase? = null
    private var mDb: SQLiteDatabase? = null
    private var mDbUtils: SQLiteDatabase? = null
    private lateinit var nsd: Nsd
    private var sceneHandler: SceneHandler = SceneHandler()
    private var jsonHelper: JsonHelper = JsonHelper()
    private lateinit var deviceAdapter: DevicesAdapter
    private lateinit var sceneAdapter: ScenesAdapter
    private var devicesList: ArrayList<Device>? = null
    private lateinit var clientConnect: OkHttpClient
    private var userHomeType = true
    val scenesList = ArrayList<Scenes>()
    var scenes = ArrayList<Scene>()
    var allDeviceList = ArrayList<AuraComplete>()
    var IpListDevices: MutableList<IpModel> = ArrayList()
    private var IP = IpHandler()
    lateinit var homeTitleTv: TextView
    var debugDataHitFlag = true
    var debugDataSetFlag = true
    lateinit var addAuraSwitchBtn: ImageView
    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    var deviceError: Int = -1
    var debugData = ArrayList<AuraType>()
    internal var mBounded: Boolean = false
    private var awsPubSub: AwsPubSub? = null
    private var serviceConnection = true
    var urlType: String? = null
    val contactModels = java.util.ArrayList<ContactModel>()
    private lateinit var adapter: ZmoteListAdapter
    private var InternetUpdate = false
    private val handler = Handler()
    lateinit var mProgress: LinearLayout

    private val isInternetWorking: Boolean
        get() {
            var success = false
            try {
                val url = URL("https://google.com")
                val connection = url.openConnection() as HttpURLConnection
                connection.connectTimeout = 10000
                connection.connect()
                success = connection.responseCode == 200
            } catch (e: IOException) {
                Log.d(LOG_TAG, "Check Internet connection")
            }
            return success
        }


    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onDeviceOptionsClicked(view: View) {
        optionType = 1
        showPopup(view)
    }

    override fun onOptionsClicked(view: View) {
        optionType = 0
        showPopup(view)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun fetchDataCallback(result: String?) {
        if (result != null) {
            var data = result
            val jsonArray = JSONArray(data)
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val contactModel = ContactModel()
                contactModel.zChipId = jsonObject.getString("chipID")
                contactModel.zname = jsonObject.getString("name")
                contactModel.zmake = jsonObject.getString("make")
                contactModel.ztype = jsonObject.getString("type")
                contactModel.zLocalIp = jsonObject.getString("localIP")
                contactModel.zmodel = jsonObject.getString("model")
                contactModel.zstate = jsonObject.getString("state")
                contactModels.add(contactModel)

            }
            adapter.notifyDataSetChanged()
        } else {

        }
    }

    @SuppressLint("StringFormatInvalid")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        favDevicesRv.isNestedScrollingEnabled = false
        homeTitleTv = view!!.findViewById<TextView>(R.id.homeTitleTv)
        addAuraSwitchBtn = view!!.findViewById(R.id.addAuraSwitchBtn)
        swipeRefreshLayout = view!!.findViewById(R.id.swipeRefreshLayout)

        mProgress = view!!.findViewById(R.id.progress_layout) as LinearLayout

        val dbScene = SceneDbHelper(context!!)
        mDbScene = dbScene.writableDatabase

        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase

        val dbUtils = UtilsDbHelper(context!!)
        mDbUtils = dbUtils.writableDatabase

        scanData()
        nsd = Nsd(context!!, "HOME")
        nsdDiscovery()
        startServices()
        LocalBroadcastManager.getInstance(context!!).registerReceiver(
                onNSDServiceResolved, IntentFilter("nsdDiscoverHome"))
        LocalBroadcastManager.getInstance(context!!).registerReceiver(
                onTcpServerMessageReceived, IntentFilter("intentKey"))
        LocalBroadcastManager.getInstance(context!!).registerReceiver(
                onAwsMessageReceived, IntentFilter("AwsShadow"))
        init()

//        Thread(Runnable {
//            try {
//                IpListDevices = IP.getIpDevices()
//                Thread.sleep(1000)
//                for (l in IpListDevices) {
//                    if (l.owned == 0) {
//                        if (!l.local) {
//                            if (!l.aws) {
//                                if (l.thing != null) {
//                                    if (serviceConnection) {
//                                        if (context is DashboardActivity) (context as DashboardActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeLEDData())
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//
//            } catch (e: Exception) {
//                Log.d(LOG_TAG, "Error : " + e.stackTrace)
//            }
//        }).start()


        swipeRefreshLayout.setOnRefreshListener {
            val ft = fragmentManager!!.beginTransaction()
            ft.detach(this).attach(this).commit()
        }
    }

    fun init() {

        IpListDevices = IP.getIpDevices()

        for (l in IpListDevices) {
            if (l.owned == 0) {
                try {
                    sendTcpConnect(l.uiud!!, l.ip!!)
                } catch (e: Exception) {
                    Log.d(LOG_TAG, "Error : " + e.stackTrace)
                }
            }
        }

        zmoteDeviceCheck()

        if (Constant.HOME!!.length > 13) {
            val ttl = Constant.HOME!!.substring(0, 13)
            homeTitleTv.text = ttl + ".."
        } else {
            homeTitleTv.text = Constant.HOME
        }

        val ctx = context
        var listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")
        userHomeType = true
        addAuraSwitchBtn.visibility = View.VISIBLE
        for (x in listRoom) {
            if (x.type == "home") {
                if (x.name == Constant.HOME) {
                    if (x.sharedHome == "guest") {
                        //remove plus
                        val contentView = activity!!.findViewById(R.id.activity_main) as RelativeLayout
                        Utils.setDrawable(ctx!!, contentView, x.bgUrl.toInt())
                        addAuraSwitchBtn.visibility = View.INVISIBLE
                        userHomeType = false
                    } else {
                        val contentView = activity!!.findViewById(R.id.activity_main) as RelativeLayout
                        Utils.setDrawable(ctx!!, contentView, x.bgUrl.toInt())
                    }
                }
            }
        }

        adapter = ZmoteListAdapter(context!!, contactModels) { contactModel, b ->
            if (!b) {
                communicateDevice(contactModel)
            }

        }
        zmoteDeviceList.adapter = adapter
        zmoteDeviceList.layoutManager = GridAutoFitLayoutManager(context, resources.getDimensionPixelSize(R.dimen.device_item_size))
        zmoteDeviceList.setHasFixedSize(true)
        var spacings = resources.getDimensionPixelSize(R.dimen.uniform_half_spacing)
        zmoteDeviceList.addItemDecoration(GridListSpacingItemDecoration(spacings))

        /*
        * All data related to devices will be rendered here
        * */
        allDeviceList = localSqlDatabase.getAllDevicesScenes(mDb!!, Constant.HOME!!)
        devicesList = ArrayList<Device>()
        for (device in allDeviceList) {
            if (device.loads.size > 0) {
                for (l in IpListDevices) {
                    if (device.name == l.name) {
                        for (i in 0..3) {
                            if (device.loads[i].favourite!!) {
                                val load = Device(device.loads[i].icon!!, l.state[i]!!, l.dim[i], device.loads[i].name!!, device.room!!, device.name, device.loads[i].index!!, device.loads[i].dimmable!!)
                                if (l.condition[i] == "ready") {
                                    if (l.local) {
                                        load.status = "on"
                                    } else {
                                        load.status = "cloud"
                                    }
                                } else {
                                    load.status = "update"
                                }

                                devicesList!!.add(load)
                            }
                        }
                        break
                    }
                }
            }

        }

        deviceAdapter = DevicesAdapter(devicesList!!, "Any", this) { device: Device, isLongPressed: Boolean ->
            if (isLongPressed) {
                if (device.dimmable == false) {
                    val gson = Gson()
                    context!!.startActivity<EditLoadActivity>("DEVICE" to gson.toJson(device))
                } else openDimmingDialog(device)
            } else {
                controlDevice(device, false)
            }
        }
        favDevicesRv.adapter = deviceAdapter
        // val cellSize = resources.getDimensionPixelSize(R.dimen.device_item_size)
        favDevicesRv.layoutManager = GridAutoFitLayoutManager(context, resources.getDimensionPixelSize(R.dimen.device_item_size))
        favDevicesRv.setHasFixedSize(true)
        val spacing = resources.getDimensionPixelSize(R.dimen.uniform_half_spacing)
        favDevicesRv.addItemDecoration(GridListSpacingItemDecoration(spacing))

        scenesList.clear()
        scenes = localSqlScene.getAllScenes(mDbScene!!, Constant.HOME!!)
        for (scene in scenes) {
            scenesList.add(Scenes(scene.name, scene.icon, scene.room, false))
        }

        sceneAdapter = ScenesAdapter(scenesList, devicesList!!, this) { scenes: Scenes, isLongPressed: Boolean ->
            if (isLongPressed) {
                if (userHomeType) {
                    dialogueEditHome(scenes)
                } else {
                    toast("Guest can not edit..")
                }

            } else {
                val sceneList = sceneHandler.convertToSceneModel(scenes.rooms)
                val isSceneOn = !scenes.isOn
                for (scene in sceneList) {
                    for (l in IpListDevices) {
                        if (l.name == scene.device) {
                            val data = jsonHelper.serializeSceneData(scene, l.uiud!!, isSceneOn)
                            if (l.ip == null) {
                                if (l.thing != null) {
                                    if (context is DashboardActivity) {
                                        (context as DashboardActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeSceneDataForAws(scene, isSceneOn))
                                        scenes.isOn = isSceneOn
                                        sceneAdapter.notifyDataSetChanged()
                                    }
                                }
                            } else {
                                if (l.aws == false) {
                                    ConnectTask(context!!, this, data, l.ip!!, scene.device!!).execute()
                                    //scenes.isOn = isSceneOn
                                    sceneAdapter.notifyDataSetChanged()
                                } else {
                                    if (l.thing != null) {
                                        if (context is DashboardActivity) {
                                            (context as DashboardActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeSceneDataForAws(scene, isSceneOn))
                                            // scenes.isOn = isSceneOn
                                            sceneAdapter.notifyDataSetChanged()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        favScenesRv.adapter = sceneAdapter
        favScenesRv.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)

        spinnerHomes.setOnClickListener {
            showHomesDialog()
        }
        addAuraSwitchBtn.setOnClickListener {
            showPopup(addAuraSwitchBtn)
        }
        homeTitleTv.setOnClickListener {
            showHomesDialog()
        }
        updateDeviceCounts()
    }

    private fun zmoteDeviceCheck() {
        FetchData(this).execute()
    }

    private fun communicateDevice(contactModel: ContactModel) {
        if (contactModel.zstate == "connected") {
            val intent = Intent(context, OpenRemote::class.java)
            startActivity(intent)
        }

    }

    fun scanData() {
        Thread(Runnable {
            IpListDevices = IP.getIpDevices()
            try {
                for (l in IpListDevices) {
                    if (l.owned == 0) {
                        if ((l.local) || (l.aws)) {
                            mProgress.visibility = View.GONE
                        } else {
                            handler.post {
                                mProgress.visibility = View.VISIBLE
                            }
                            Thread.sleep(5000)
                        }
                    }
                }

            } catch (e: InterruptedException) {
                Log.d("Error", "error $e")
            }
        }).start()
    }

    private fun dialogueEditHome(scenes: Scenes) {
        var dialogue = Dialog(context)
        val sceneDynamoDb = RulesTableHandler()
        dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogue.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogue.setContentView(R.layout.dialogue_edit_home)
        dialogue.tv_title.text = "Edit Scene"
        dialogue.btn_edit.setOnClickListener {
            val intent = Intent(activity, CreateSceneActivity::class.java)
            intent.putExtra("inputSceneType", "edit")
            intent.putExtra("inputSceneName", scenes.title)
            intent.putExtra("inputSceneIconUrl", scenes.iconUrl)
            startActivity(intent)
        }
        dialogue.btn_delete_home.setOnClickListener {
            val dbScene = SceneDbHelper(context!!)
            mDbScene = dbScene.writableDatabase
            val intent = Intent()
            var sceneNameOld = scenes.title
            localSqlScene.deleteScene(mDbScene!!, sceneNameOld.toString(), Constant.HOME!!)
            deleteSceneFromLoads(sceneNameOld.toString())
            thread {
                sceneDynamoDb.deleteUserScene(Constant.IDENTITY_ID!!, Constant.HOME!!, sceneNameOld.toString())
            }
            startActivity(intentFor<DashboardActivity>().newTask())
            toast("Scene Deleted")
        }
        dialogue.show()
    }

    fun deleteSceneFromLoads(scene: String) {
        var roomsList: MutableList<RoomModel> = ArrayList()
        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase
        val dummyRooms = localSqlDatabase.getRooms(mDb!!, Constant.HOME!!)
        for (x in dummyRooms) {
            val room = RoomModel()
            room.name = x.roomName
            roomsList.add(room)
        }
        if (!roomsList.isEmpty()) {
            for (x in roomsList) {
                val devices = localSqlDatabase.getDevicesForRoom(mDb!!, Constant.HOME!!, x.name!!)
                //NEWSCENES delete all scenes
            }
        }
    }

    private fun updateDeviceCounts() {
        var totalDevice = 0
        var onDevices = 0
        var offDevices = 0

        for (IplistDevice in IpListDevices) {
            for (device in allDeviceList) {
                if ((IplistDevice.name == device.name) and (IplistDevice.owned == 0)) {
                    if (IplistDevice.home == Constant.HOME) {
                        for (i in IplistDevice.state) {
                            totalDevice++
                            if (i) {
                                onDevices++
                            } else {
                                offDevices++
                            }
                        }
                    }
                }
            }
        }
        totalDevicesCountTv?.text = totalDevice.toString()
        devicesOffCountTv?.text = offDevices.toString()
        devicesOnCountTv?.text = onDevices.toString()
    }


    /**
     * Data from the TCP : Synchronous
     */
    override fun onTcpMessageReceived(message: String) {
        updateStates(message)
        updateAppearance()
    }

    private fun startServices() {
        val mIntent = Intent(context, AwsPubSub::class.java)
        context!!.bindService(mIntent, mConnection, Context.BIND_AUTO_CREATE)
    }

    private var mConnection: ServiceConnection = object : ServiceConnection {

        override fun onServiceDisconnected(name: ComponentName) {
            mBounded = false
            awsPubSub = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            mBounded = true
            val mLocalBinder = service as AwsPubSub.LocalAwsBinder
            awsPubSub = mLocalBinder.getServerInstance()
            serviceConnection = true
        }
    }

    fun updateAppearance() {

        for (ip in IpListDevices) {
            for (device in devicesList!!) {
                if (device.deviceName == ip.name) {
                    if (ip.condition[device.index] == "fail") {
                        device.status = "off"
                    } else if ((ip.condition[device.index] == "update") and (deviceError == 0)) {
                        device.status = "update"
                        device.devicePresent = true
                    } else if ((ip.condition[device.index] == "update") and (deviceError != 0)) {
                        if (ip.aws) {
                            device.status = "update"
                            device.devicePresent = true
                        } else {
                            device.status = "update"
                            device.devicePresent = true
                        }
                    } else if (ip.condition[device.index] == "ready") {
                        if (ip.local) {
                            device.status = "on"
                        } else {
                            device.status = "cloud"
                        }
                    }
                    device.isTurnOn = ip.state[device.index]
                    device.dimVal = ip.dim[device.index]
                }
            }
        }
        deviceAdapter.notifyDataSetChanged()

        for (scene in scenesList) {
            var sceneTurnedState = true
            for (sRoom in scene.rooms) {
                for (sDevice in sRoom.deviceList) {
                    for (ip in IpListDevices) {
                        if (sDevice.deviceName == ip.name) {
                            if (sDevice.isTurnOn != ip.state[sDevice.index]) {
                                sceneTurnedState = false
                                break
                            }
                        }
                    }
                }
                if (!sceneTurnedState) {
                    break
                }
            }
            scene.isOn = sceneTurnedState
        }
        sceneAdapter.notifyDataSetChanged()
        updateDeviceCounts()
    }

    /**
     * Data from the TcpServer : Asynchronous
     */
    private val onTcpServerMessageReceived = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            val data = intent.getStringExtra("key")
            if (data != null) {
                updateStates(data)
                updateAppearance()
            }
        }
    }


    /**
     * Data from the AwsShadowUpdate
     */
    private val onAwsMessageReceived = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val shadow = intent.getStringExtra("data")
            val segments = shadow.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (shadow != "Connected") {
                val device = localSqlDatabase.getDeviceForThing(mDb!!, segments[1])
                val data = jsonHelper.deserializeAwsData(segments[0])
                if (data.led != 0) {
                    var flag = false
                    for (l in IpListDevices) {
                        if (l.name == device) {
                            if (data.uiud != null) {
                                if (data.uiud == l.uiud) {
                                    if (!l.local) {
                                        l.aws = true
                                        for (i in 0..3) {
                                            l.condition[i] = "ready"
                                        }
                                        l.dim[0] = data.dim["d0"]!!
                                        l.dim[1] = data.dim["d1"]!!
                                        l.dim[2] = data.dim["d2"]!!
                                        l.dim[3] = data.dim["d3"]!!
                                        for (i in 0..3) {
                                            l.state[i] = data.state["s$i"] == 1
                                            l.failure[i] = 0
                                            l.curn_load[i] = false
                                        }
                                        IP.registerIpDevice(l)
                                        updateAppearance()
                                        mProgress.visibility = View.GONE
                                    }

                                }

                            }
                            break
                        }
                    }
                } else {
                    for (l in IpListDevices) {
                        if (data.uiud == null) {
                            if (l.name == device) {
                                if (!l.local) {
                                    for (i in 0..3) {
                                        l.failure[i] = l.failure[i] + 1
                                        if (l.failure[i] > 2) {
                                            l.aws = false
                                            if (l.curn_load[i]) {
                                                l.curn_load[i] = false
                                                l.condition[i] = "fail"
                                            } else {
                                                if (!l.curn_load[i]) {
                                                    l.condition[i] = "fail"
                                                }
                                            }
                                        }

                                    }
                                }
                                IP.registerIpDevice(l)
                                updateAppearance()
                                mProgress.visibility = View.GONE
                                break
                            }
                        }
                    }
                }
            }
        }
    }

    private fun nsdDiscovery() {
        if (!nsd.getDiscoveryStatus()) {
            nsd.setBroadcastType("HOME")
            nsd.initializeNsd()
            nsd.discoverServices()
        }
    }

    /**
     * Data from the TcpServer : Asynchronous
     */
    private val onNSDServiceResolved = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d("NSD_DEBUG_HOME", "nsd onReceive in HomeFragment")
            val name = intent.getStringExtra("name")
            val ip = intent.getStringExtra("ip")
            val device = name.substring(name.length - 6, name.length)
            //check model and insert data
            var ipDevice = IpModel()
            ipDevice.ip = ip
            ipDevice.name = device
            ipDevice.full_device_name = name
            var isDeviceExists = false
            for (l in IpListDevices) {
                if (l.name == ipDevice.name) {
                    l.ip = ip
                    ipDevice = l
                    isDeviceExists = true
                    break
                }
            }
            if (!isDeviceExists) {
                ipDevice.aws = false
                ipDevice.uiud = "xxxxxxxxxxxxxxxxx"
                ipDevice.owned = 1
            }
            IP.registerIpDevice(ipDevice)
            if (ipDevice.owned == 0) {
                if (name.endsWith("6010D4")) {
                    urlType = "Device Found"
                    typeUrl(ipDevice.uiud!!, ipDevice.ip!!, urlType!!)
                } else {
                    sendTcpConnect(ipDevice.uiud!!, ipDevice.ip!!)
                }
            }

        }
    }

    private fun sendTcpConnect(uiud: String, ip: String) {
        try {
            Log.d("NULL_POINTER", "DATA - " + jsonHelper.initialData(uiud) + " IP -  " + ip)
            ConnectTask(context!!, this, jsonHelper.initialData(uiud), ip, "XXXXXX").execute("")
        } catch (e: Exception) {
            Log.d(LOG_TAG, "Before error connect task")
            e.printStackTrace()
        }
    }

    private fun controlDevice(auraDevice: Device, longPressed: Boolean) {
        var dimValue: Int = -1
        // val deviceData: AuraSwitch = deviceHandler.getStatesDims(auraDevice.deviceName)
        var cFlag = false
        if (longPressed) {
            //dimValue = auraDevice.dimVal
            cFlag = true
        }
        for (l in IpListDevices) {
            if (l.name == auraDevice.deviceName) {
                val data = jsonHelper.serialize(auraDevice, l.uiud!!, cFlag)
                Log.d("UNAUTHORIZED", "TCP DATA : $data")
                l.condition[auraDevice.index] = "update"
                l.curn_load[auraDevice.index] = true
                if (l.ip == null) {
                    if (l.thing != null) {
                        if (context is DashboardActivity) {
                            var flag = (context as DashboardActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeDataToAws(auraDevice, l.uiud!!, cFlag))
                            if (!flag) {
                                l.condition[auraDevice.index] = "fail"

                            }
                        }
                    }
                } else {
                    if (l.local) {
                        ConnectTask(context!!, this, data, l.ip!!, auraDevice.deviceName).execute()
                    } else {
                        if (l.aws) {
                            if (l.thing != null) {
                                if (context is DashboardActivity) {
                                    var flag = (context as DashboardActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeDataToAws(auraDevice, l.uiud!!, cFlag))
                                    if (!flag) {
                                        l.condition[auraDevice.index] = "fail"
                                    }
                                }
                            }
                        } else {
                            if ((!l.local) and (!l.aws)) {
                                l.condition[auraDevice.index] = "fail"
                            }
                        }

                    }
                }
                IP.registerIpDevice(l)
                updateAppearance()
                break
            }
        }
    }

    private fun typeUrl(uiudDevice: String, ip: String, name: String) {
        thread {
            var encryptedData: String? = null
            if (name == "Device Found") {
                var json = jsonHelper.initialData(uiudDevice)
                encryptedData = Encryption.encryptMessage(json)
            } else {
                if (name == "Control") {
                    encryptedData = Encryption.encryptMessage(uiudDevice)
                }
            }
            val url = URL("http://$ip/android")
            val connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "PUT"
            connection.connectTimeout = 300000
            connection.connectTimeout = 300000
            connection.doOutput = true

            connection.setRequestProperty("charset", "utf-8")
            connection.setRequestProperty("Content-lenght", encryptedData)
            connection.setRequestProperty("Content-Type", "application/json")
            connection.setRequestProperty("authorization", "Bearer teNa5F6AGgIKwxg54fOzJRjNoWyGZaCv")

            try {
                val outputStream: DataOutputStream = DataOutputStream(connection.outputStream)
                outputStream.write(encryptedData!!.toByteArray())
                outputStream.flush()
            } catch (exception: Exception) {

            }
            if (connection.responseCode == HttpURLConnection.HTTP_OK) {
                try {
                    val reader: BufferedReader = BufferedReader(InputStreamReader(connection.inputStream))
                    val output: String = reader.readLine()
                    val decryptedData = Encryption.decryptMessage(output)
                    updateStates(decryptedData)
                } catch (exception: Exception) {
                    Log.d("ERROR", "Exception while push the Notification")
                }
            } else {
                Log.d("error", "check connection")
                exitProcess(0)
            }
        }
    }


    private fun updateStates(message: String) {

        if (message.contains("ERROR")) {
            deviceError = -1
            var data = message.split(":")
            if (deviceError != 0) {
                for (x in data) {
                    for (l in IpListDevices) {
                        if (l.aws) {
                            for (i in 0..3) {
                                l.condition[i] = "ready"
                            }
                        } else {
                            Thread(Runnable {
                                if (isInternetWorking) {
                                    try {
                                        //onUiThread {
                                        handler.postDelayed({
                                            for (l in IpListDevices) {
                                                if (l.owned == 0) {
                                                    l.local = false
                                                    for (i in 0..3) {
                                                        l.condition[i] = "update"
                                                    }

                                                    InternetUpdate = true
                                                    if (l.thing != null) {
                                                        if (serviceConnection) {
                                                            if (context is DashboardActivity) (context as DashboardActivity).pusblishDataToShadow(l.thing!!, jsonHelper.serializeLEDData())
                                                        }
                                                    }
                                                }

                                                IP.registerIpDevice(l)
                                                break
                                            }
                                        },5000)

                                        //}

                                    } catch (e: Exception) {
                                        Log.d("Error", "Check Internet connection")
                                    }

                                } else {
                                    onUiThread {
                                        toast("Check internet connection.")
                                        for (l in IpListDevices) {
                                            l.local = false
                                            l.aws = false
                                            for (i in 0..3) {
                                                l.condition[i] = "fail"
                                            }
                                            IP.registerIpDevice(l)
                                            updateAppearance()
                                            mProgress.visibility = View.GONE
                                            break
                                        }
                                    }
                                }
                            }).start()

                        }

                    }
                }
            }

        } else {
            val updatedDevice: AuraSwitch = jsonHelper.deserializeTcp(message)
            mProgress.visibility = View.GONE
            deviceError = updatedDevice.error

            when (updatedDevice.type) {
                1 -> {
                    for (l in IpListDevices) {
                        if (l.name == updatedDevice.name) {
                            l.local = true
                            l.aws = false
                            l.state[0] = updatedDevice.state[0] == 1
                            l.state[1] = updatedDevice.state[1] == 1
                            l.state[2] = updatedDevice.state[2] == 1
                            l.state[3] = updatedDevice.state[3] == 1
                            l.dim[0] = updatedDevice.dim[0]
                            l.dim[1] = updatedDevice.dim[1]
                            l.dim[2] = updatedDevice.dim[2]
                            l.dim[3] = updatedDevice.dim[3]
                            for (i in 0..3) {
                                l.condition[i] = "ready"
                            }
                            IP.registerIpDevice(l)
                            break
                        }
                    }

                }

                4 -> {
                    if (updatedDevice.error == 1) {
                        for (l in IpListDevices) {
                            if (l.name == updatedDevice.name) {
                                l.local = false
                                l.aws = false
                                for (i in 0..3) {
                                    l.condition[i] = "fail"
                                }
                                IP.registerIpDevice(l)
                                break
                            }
                        }
                        toast("Device used by someone,Unauthorized Access")
                    } else {
                        for (l in IpListDevices) {
                            if (l.name == updatedDevice.name) {
                                l.local = true
                                l.state[0] = updatedDevice.state[0] == 1
                                l.state[1] = updatedDevice.state[1] == 1
                                l.state[2] = updatedDevice.state[2] == 1
                                l.state[3] = updatedDevice.state[3] == 1
                                l.dim[0] = updatedDevice.dim[0]
                                l.dim[1] = updatedDevice.dim[1]
                                l.dim[2] = updatedDevice.dim[2]
                                l.dim[3] = updatedDevice.dim[3]
                                for (i in 0..3) {
                                    l.condition[i] = "ready"
                                }
                                l.aws = false
                                IP.registerIpDevice(l)
                                break
                            }
                        }
                    }
                }

                12 -> {

                    if (debugDataSetFlag == true) {
                        var data_single = AuraType()
                        data_single.auid = updatedDevice.id
                        data_single.data = updatedDevice.data
                        debugData.add(data_single)
                        Log.d("Type-12", "$debugData")
                    }

                    if (debugDataHitFlag == true) {
                        runHandler()
                        debugDataHitFlag = false
                    }

                }

                else -> {
                    for (l in IpListDevices) {
                        if (l.name == updatedDevice.name) {
                            l.state[0] = updatedDevice.state[0] == 1
                            l.state[1] = updatedDevice.state[1] == 1
                            l.state[2] = updatedDevice.state[2] == 1
                            l.state[3] = updatedDevice.state[3] == 1
                            l.dim[0] = updatedDevice.dim[0]
                            l.dim[1] = updatedDevice.dim[1]
                            l.dim[2] = updatedDevice.dim[2]
                            l.dim[3] = updatedDevice.dim[3]
                            l.local = true
                            l.aws = false
                            for (i in 0..3) {
                                l.condition[i] = "ready"
                            }
                            IP.registerIpDevice(l)
                            break
                        }
                    }
                }

            }
        }
    }


    private fun checkIntenet() {

    }

    private fun runHandler() {
        clientConnect = OkHttpClient()


        Handler().postDelayed({

            debugDataSetFlag = false
            val json = Gson().toJson(debugData)
            debugData.clear()

            thread {
                val url = URL("https://43edvbkpgc.execute-api.ap-south-1.amazonaws.com/production/")
                val connection = url.openConnection() as HttpURLConnection
                connection.requestMethod = "POST"
                connection.connectTimeout = 300000
                connection.connectTimeout = 300000
                connection.doOutput = true

                connection.setRequestProperty("charset", "utf-8")
                connection.setRequestProperty("Content-lenght", json.toString())
                connection.setRequestProperty("Content-Type", "application/json")
                connection.setRequestProperty("authorization", "Bearer teNa5F6AGgIKwxg54fOzJRjNoWyGZaCv")

                try {
                    val outputStream: DataOutputStream = DataOutputStream(connection.outputStream)
                    outputStream.write(json.toByteArray())
                    outputStream.flush()
                } catch (exception: Exception) {

                }
                if (connection.responseCode != HttpURLConnection.HTTP_OK && connection.responseCode != HttpURLConnection.HTTP_CREATED) {
                    try {
                        val reader: BufferedReader = BufferedReader(InputStreamReader(connection.inputStream))
                        val output: String = reader.readLine()
                        Log.d(LOG_TAG, "check connection $output")
                        System.exit(0)

                    } catch (exception: Exception) {
                        Log.d(LOG_TAG, "Exception while push the Notification")
                    }
                    debugDataHitFlag = true
                    debugDataSetFlag = true
                }
            }
        }, 2000)

    }


    private fun openDimmingDialog(device: Device) {
        val dialog = Dialog(context)
        var dimVal = device.dimVal
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_configure_edit_dimming)
        if (device.name == "Fan" && device.isTurnOn) {
            Glide.with(this).load(getIconDrawable(device.type, device.isTurnOn))
                    .into(dialog.iconDevice)
        } else {
            dialog.iconDevice.setImageResource(Utils.getIconDrawable(device.type, device.isTurnOn))
        }
        dialog.tvDialogTitle.text = String.format(getString(R.string.text_configure_device_dim), device.name)
        dialog.rangeBarDim.setOnRangeBarChangeListener { _, _, _, _, rightPinValue ->
            dimVal = Integer.parseInt(rightPinValue)
        }
        dialog.sickbar.setProgress(dimVal.toFloat())
        dialog.sickbar.setOnSeekChangeListener(object : IndicatorSeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar?, thumbPosOnTick: Int) {
            }

            override fun onSectionChanged(seekBar: IndicatorSeekBar?, thumbPosOnTick: Int, textBelowTick: String?, fromUserTouch: Boolean) {
            }

            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar?) {
            }

            override fun onProgressChanged(seekBar: IndicatorSeekBar?, progress: Int, progressFloat: Float, fromUserTouch: Boolean) {
                dimVal = progress
                device.dimVal = dimVal
                controlDevice(device, true)
            }
        })

        dialog.btnEdit.setOnClickListener {
            if (userHomeType) {
                val gson = Gson()
                context!!.startActivity<EditLoadActivity>("DEVICE" to gson.toJson(device))
            } else {
                Toast.makeText(context, "Guest can not edit.", Toast.LENGTH_SHORT).show()
            }

        }
        dialog.btnDone.setOnClickListener {
            // controlDevice(device, true)
            dialog.cancel()
        }
        dialog.show()
    }

    private fun showHomesDialog() {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_homes)
        dialog.setCanceledOnTouchOutside(true)

        val homesList = ArrayList<Home>()
        var sharedHomeFlag = false

        var list_room = localSqlUtils.getHomeData(mDbUtils!!, "home")
        for (home in list_room) {
            if (home.type == "home") {
                if (home.sharedHome == "master") {
                    if (home.name == Constant.HOME) {
                        homesList.add(Home(home.name, true, false))
                    } else {
                        homesList.add(Home(home.name, false, false))
                    }
                } else {
                    if (home.sharedHome == "guest") {
                        if (home.name == Constant.HOME) {
                            homesList.add(Home(home.name, true, true))
                        } else {
                            homesList.add(Home(home.name, false, true))
                        }
                    }
                }

            }
        }

        if (homesList.isEmpty()) homesList.add(Home("My Home", true, false))
        val homesRv = dialog.findViewById<RecyclerView>(R.id.homesRv)
        homesRv.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        homesRv.adapter = HomeListAdapter(homesList) { home: Home, action: Int ->
            when (action) {
                EDIT_HOME -> {

                    val intent = Intent(activity, HomeDetailsActivity::class.java)
                    intent.putExtra(CREATE_HOME_ROOM, "edit")
                    intent.putExtra("HOME_NAME", Constant.HOME)
                    startActivity(intent)

                }
                SELECTED_HOME -> {
                    val homeSelected = home.title.toString()
                    if (Constant.HOME != homeSelected) {
                        Constant.HOME = homeSelected
                        val prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit()
                        prefEditor.putString("HOME", homeSelected)
                        prefEditor.apply()
                        init()
                    }
                }
                SHARE_HOME -> {
                    if (userHomeType) {
                        val intent = Intent(activity, ShareAuraActivity::class.java)
                        startActivity(intent)
                    } else {
                        Toast.makeText(context, "Guest can not share.", Toast.LENGTH_SHORT).show()
                    }

                }
            }
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun showPopup(v: View) {
        val popup = context?.let { PopupMenu(it, v) }
        val inflater = popup?.menuInflater
        inflater?.inflate(R.menu.options_menu, popup.menu)
        popup?.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.addAuraSwitch -> {
                    val intent = Intent(activity, WifiSettingsActivity::class.java)
                    startActivity(intent)
                    true
                }
                R.id.createScenes -> {
                    val intent = Intent(activity, CreateSceneActivity::class.java)
                    intent.putExtra("inputSceneType", "create")
                    intent.putExtra("inputSceneName", "")
                    intent.putExtra("inputSceneIconUrl", 0)
                    startActivity(intent)
                    true
                }
                R.id.addRoom -> {
                    val intent = Intent(activity, AddRoomActivity::class.java)
                    intent.putExtra("ROOM_NAME", "Room Name")
                    intent.putExtra("ROOM_EDIT_TYPE", "create")
                    startActivity(intent)
                    true
                }
                R.id.addHome -> {
                    val intent = Intent(activity, HomeDetailsActivity::class.java)
                    intent.putExtra(Constant.CREATE_HOME_ROOM, "create")
                    intent.putExtra("HOME_NAME", "Home Name")
                    startActivity(intent)
                    true
                }
                R.id.ir_device -> {
                    val intent = Intent(activity, ZemoteActivty::class.java)
                    startActivity(intent)
                    true
                }
                else -> false
            }
        }
        popup?.show()
    }
}

