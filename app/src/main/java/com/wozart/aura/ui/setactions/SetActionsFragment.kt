package com.wozart.aura.aura.ui.setactions

import android.annotation.SuppressLint
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.PorterDuff
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wozart.aura.R
import com.wozart.aura.aura.ui.createautomation.SetAutomationActivity
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.model.*
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.ScheduleTable
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.entity.sql.scheduling.ScheduleDbHelper
import com.wozart.aura.ui.base.basesetactions.BaseSetActionsFragment
import com.wozart.aura.ui.base.basesetactions.SetActionsRoomAdapter
import com.wozart.aura.ui.createautomation.AutomationScene
import com.wozart.aura.ui.createautomation.GeoModal
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.ui.selectdevices.SelectDevicesFragment
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.fragment_select_devices.*
import kotlinx.android.synthetic.main.layout_header.*
import org.jetbrains.anko.support.v4.intentFor
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.toast
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.thread


/**
 * Created by Saif on 11/20/2018.
 * Wozart Technology Pvt Ltd
 * mds71964@gmail.com
 */

class SetActionsFragment  : BaseSetActionsFragment() {

    private var localSqlSchedule = ScheduleTable()
    private val localSqlDatabase = DeviceTable()
    private var mDbSchedule: SQLiteDatabase? = null
    private var mDbScene:SQLiteDatabase ?= null
    private var mDb: SQLiteDatabase? = null
    var roomsList: MutableList<RoomModel> = ArrayList()
    private var adapter = SetActionsRoomAdapter()
    lateinit var lati: String
    lateinit var longi : String
    var geoDetails : MutableList<GeoModal> = ArrayList()
    var geoData = GeoModal()
    var automationEnable : Boolean = false
    var geoTriggeringType : String ?= null
    private var roomsSelected: MutableList<RoomModel> = ArrayList()

    companion object {
        private  var automationScheduleType:String?=null
        private var automationNameOld: String?= null
        private var automationName : String?= null
        private var selectedIcon : Int ?= 0
        private var scheduleType : String?= null
        private var Selectedrooms: MutableList<RoomModel> = ArrayList()
        var NewGoeRadius :Float ?= 0f
        var roomList = arrayListOf<RoomModel>()
        var schedule = AutomationModel("title",0,roomList,false,"","","","","")
        fun newInstance(rooms: MutableList<RoomModel>, scheduleDetails: AutomationModel,automationSceneNameOld:String,automationSceneType: String): SetActionsFragment {
            BaseSetActionsFragment.rooms = rooms
            Selectedrooms = rooms
            BaseSetActionsFragment.data= scheduleDetails
            BaseSetActionsFragment.automationNameOld = automationSceneNameOld
            BaseSetActionsFragment.automationScheduleType = automationSceneType
            return SetActionsFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_automation_select_device, container, false)
        var listRooms = view.findViewById<RecyclerView>(R.id.listRooms)
        var btnSceneDelete = view.findViewById(R.id.btnSceneDelete) as Button
        var dbSchedule = ScheduleDbHelper(context!!)
        val pref = PreferenceManager.getDefaultSharedPreferences(context)
        lati = pref.getString(Constant.GEOFENCE_POSITION_LATITUDE,"null")
        longi = pref.getString(Constant.GEOFENCE_POSITION_LONGITUDE, "null")
        roomsSelected = SelectDevicesFragment.rooms

        if(data.type != "time"){
            if(context is SetAutomationActivity){
                automationNameOld = (context as SetAutomationActivity).getAutomationOldName()
                automationScheduleType = (context as SetAutomationActivity).getAutomationType()
                automationName = (context as SetAutomationActivity).getAutomationName()
                selectedIcon = (context as SetAutomationActivity).getIcon()
                scheduleType = (context as SetAutomationActivity).getScheduleType()
                NewGoeRadius = (context as SetAutomationActivity).getAddedGeoRadius()
                automationEnable = (context as SetAutomationActivity).getAutomationEnable()
                geoTriggeringType = (context as SetAutomationActivity).getTriggeringType()
                schedule = AutomationModel(automationName, selectedIcon!!, roomList,automationEnable,"", scheduleType,"","",geoTriggeringType!!)

            }
        }

        if(scheduleType == "geo"){
            listRooms.layoutManager = LinearLayoutManager(activity)
            listRooms.adapter = adapter
            adapter.init(roomsSelected)
        }else{
            listRooms.layoutManager = LinearLayoutManager(activity)
            listRooms.adapter = adapter
            adapter.init(Selectedrooms)
        }

        val scheduleDynamoDb = RulesTableHandler()
        mDbSchedule = dbSchedule.writableDatabase

        val dbScene = SceneDbHelper(context!!)
        mDbScene = dbScene.writableDatabase




        btnSceneDelete.setOnClickListener {
            if (automationScheduleType == "create") {
                btnSceneDelete.visibility = View.INVISIBLE
                longToast(" Automation not Create to Delete")
            } else {
                btnSceneDelete.visibility = View.VISIBLE
                localSqlSchedule.deleteAutomationScene(mDbSchedule!!, automationNameOld!!,Constant.HOME!!)
                deleteAutomationScene(automationNameOld!!)

                thread{
                    scheduleDynamoDb.deleteUserSchedule(Constant.IDENTITY_ID!!,Constant.HOME!!,automationNameOld!!)
                }
                startActivity(intentFor<DashboardActivity>())
                toast("Automation Scene Deleted")
            }
        }
        return view
    }

    fun deleteAutomationScene(automationScene: String){
        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase
        val dummyRooms = localSqlDatabase.getRooms(mDb!!, Constant.HOME!!)
        for (x in dummyRooms) {
            val room = RoomModel()
            room.name = x.roomName
            roomsList.add(room)
        }
        if (!roomsList.isEmpty()) {
            for (x in roomsList) {
                val devices = localSqlDatabase.getDevicesForRoom(mDb!!,  Constant.HOME!!, x.name!!)
                //NEWSCENES delete all scenes
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    override fun onFinish() {
        val scheduleDynamoDb = RulesTableHandler()
        var schedulaTable = ScheduleTable()
        var dbHelper = ScheduleDbHelper(context!!)
        var mdb: SQLiteDatabase = dbHelper.writableDatabase
        val automations : MutableList<RoomModel>


        if(data.type != "time"){
             automations = roomsSelected
        }else{
            automations = Selectedrooms
        }

        val automationroomsScene: ArrayList<String> = ArrayList()
        for (automation in automations){
            for(device in automation.deviceList){
                automationroomsScene.add(device.roomName)
            }
        }
        if(data.type != "time"){
            geoData.newGeoLat = lati.toDouble()
            geoData.newGeolong = longi.toDouble()
            geoData.newGeoRadius = NewGoeRadius
            geoData.AutomationEnable = automationEnable
            geoData.triggerType = geoTriggeringType
            geoDetails.add(geoData)
            var scheduleName = automationName
            var scheduleType = scheduleType
            var scheduleIcon = selectedIcon
            var scheduleGMTTime = ""
            var scheduleLocalTime = ""
            var scheduleRoutine = ""
            //var scheduleStatus = schedule.isOn
            var automationScheduleNameOld = automationNameOld
            var automationScheduleType = automationScheduleType
            var scheduleGeoDetails = geoDetails

            val gson = Gson()
            if (schedulaTable.insertSchedule(mdb, scheduleName.toString(), scheduleIcon!!, gson.toJson(automationroomsScene), gson.toJson(automations), gson.toJson(scheduleGeoDetails), scheduleGMTTime, scheduleType.toString(), scheduleLocalTime, scheduleRoutine, Constant.HOME!!, automationScheduleNameOld!!, automationScheduleType!!)) {

                thread {
                    var home = Constant.HOME
                    var userId = Constant.IDENTITY_ID
                    var auto = AutomationScene()
                    auto.name = scheduleName.toString()
                    auto.icon = scheduleIcon!!
                    auto.room = automations as ArrayList<RoomModel>
                    auto.load = automations
                    auto.property = geoDetails
                    auto.type = scheduleType
                    auto.time = ""
                    auto.endTime = ""
                    auto.routine = ""

                    scheduleDynamoDb.updateUserSchedule(userId!!, home!!, auto, automationScheduleType, automationScheduleNameOld)
                }


            } else {
                longToast("Scene with Same Name Present..")
            }
            var intent = Intent(activity,DashboardActivity::class.java)
            intent.putExtra("TAB_SET",Constant.AUTOMATION_TAB)
            startActivity(intent)

        }else {
            geoData.AutomationEnable = data.Automationenable
            geoDetails.add(geoData)
            var scheduleName = data.title
            var scheduleType = data.type
            var scheduleIcon = data.iconUrl
            var scheduleGMTTime = data.time
            var scheduleLocalTime = data.endTime
            var scheduleRoutine = data.routine
            var scheduleStatus = geoDetails
            var automationScheduleNameOld = getAutomationSceneName()
            var automationScheduleType = getAutomationSceneType()
            val sceneIcon = getSceneIcon()

            val gson = Gson()
            if (schedulaTable.insertSchedule(mdb, scheduleName.toString(), scheduleIcon, gson.toJson(automationroomsScene), gson.toJson(automations), gson.toJson(scheduleStatus), scheduleGMTTime.toString(), scheduleType.toString(), scheduleLocalTime.toString(), scheduleRoutine.toString(), Constant.HOME!!, automationScheduleNameOld!!, automationScheduleType!!)) {

                thread {
                    var home = Constant.HOME
                    var userId = Constant.IDENTITY_ID
                    var auto = AutomationScene()
                    auto.name = scheduleName.toString()
                    auto.icon = scheduleIcon
                    auto.room = automations as ArrayList<RoomModel>
                    auto.load = automations
                    auto.property = geoDetails
                    auto.type = scheduleType
                    auto.time = data.time.toString()
                    auto.endTime = scheduleLocalTime
                    val type = object : TypeToken<HashMap<String, Boolean>>() {}.type
                    val gson = Gson()
                    val routine: HashMap<String, Boolean>
                    routine = gson.fromJson(data.routine, type)
                    auto.routine = "[${routine["Sunday"]},${routine["Monday"]},${routine["Tuesday"]},${routine["Wednesday"]},${routine["Thursday"]},${routine["Friday"]},${routine["Saturday"]}]"

                    scheduleDynamoDb.updateUserSchedule(userId!!, home!!, auto, automationScheduleType, automationScheduleNameOld)
                }

            } else {
                longToast("Scene with Same Name Present..")
            }
            val intent = Intent(activity,DashboardActivity::class.java)
            intent.putExtra("TAB_SET",Constant.AUTOMATION_TAB)
            startActivity(intent)
        }
    }

    fun initialize()
    {
        tvSelectLoadIcon.visibility= View.GONE
        tvTitle.setTextColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
        home.setColorFilter(ContextCompat.getColor(activity!!.baseContext,R.color.white), PorterDuff.Mode.SRC_ATOP);
        tvNext.setTextColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
    }
}
