package com.wozart.aura.ui.adapter

import android.content.Intent
import android.preference.PreferenceManager
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread
import com.wozart.aura.R
import com.wozart.aura.aura.ui.dashboard.listener.OnOptionsListener
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.entity.amazonaws.models.nosql.RulesDO
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.DialogListener
import kotlinx.android.synthetic.main.share_guest_layout.view.*
import kotlin.concurrent.thread

/***************************************************************************
 * File Name :
 * Author : Saif
 * Date of Creation : 14/09/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class ShareMasterAdapter(val shareList : MutableList<MutableMap<String,String>>, val optionerListener: OnOptionsListener,val owner:RulesDO): RecyclerView.Adapter<ShareMasterAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShareMasterAdapter.ViewHolder{
        val rootView = LayoutInflater.from(parent?.context)
                .inflate(R.layout.share_guest_layout, parent, false)
        return ShareMasterAdapter.ViewHolder(rootView)
    }

    override fun getItemCount(): Int {
        return shareList.size
    }

    override fun onBindViewHolder(holder: ShareMasterAdapter.ViewHolder, position: Int) {
     holder?.bind(position,shareList,optionerListener)

        holder?.itemView?.layout_guest_access?.setOnClickListener {
            var dataMessage ="Are you sure? "+ holder.itemView.tv_contact_name.text+" will no longer have Acces to "+holder.itemView.homeName.text+"."
            var rulesDynamoDb = RulesTableHandler()
            var userType= false
            if(owner != null){
                userType = true
            }
            Utils.showCustomDialog(holder.itemView.context,holder.itemView.context.getString(R.string.share_text_invoke_master), dataMessage, R.layout.dialogue_invoke_guest, object : DialogListener {
                override fun onOkClicked() {
                    if (userType) {
                        val name = shareList[position]["Name"]
                        var homeNmae = shareList[position]["Home"]
                        var email = shareList[position]["Email"]

                        thread{
                            rulesDynamoDb.deleteGuestAccessmaster(homeNmae!!,Constant.IDENTITY_ID!!,email)

                        }
                       runOnUiThread {
                            val prefEditor = PreferenceManager.getDefaultSharedPreferences(it.context).edit()
                            prefEditor.putString("HOME", "My Home")
                            prefEditor.apply()
                            val intent = Intent(it.context, DashboardActivity::class.java)
                            intent.putExtra("TAB_SET",Constant.MORE_TAB)
                            it.context.startActivity(intent)
                        }

                    }
                }

                override fun onCancelClicked() {

                }
            })

        }

    }
    class ViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView){
        fun bind(position: Int, shareList: MutableList<MutableMap<String, String>>, optionerListener: OnOptionsListener?) = with(itemView){
            itemView.tv_contact_name.text = shareList[position]["Name"]
            itemView.homeName.text = shareList[position]["Home"]!!.split("?")[0]
            if(shareList[position]["Status"] == "error"){
                itemView.homeName.setTextColor(ContextCompat.getColor(context!!, R.color.Red))
                itemView.tv_contact_name.setTextColor(ContextCompat.getColor(context,R.color.Red))
            }else{
                itemView.homeName.setTextColor(ContextCompat.getColor(context!!, R.color.black))
                itemView.tv_contact_name.setTextColor(ContextCompat.getColor(context,R.color.black))
            }
        }
    }
}