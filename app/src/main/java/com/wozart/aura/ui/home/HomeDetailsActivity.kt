package com.wozart.aura.ui.home

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wozart.aura.R
import com.wozart.aura.data.model.Room
import com.wozart.aura.aura.data.model.Wallpaper
import com.wozart.aura.ui.adapter.RoomAdapter
import com.wozart.aura.aura.ui.adapter.WallpaperAdapter
import com.wozart.aura.aura.ui.dashboard.home.Home
import com.wozart.aura.aura.ui.dashboard.rooms.AddRoomActivity
import com.wozart.aura.utilities.Constant
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.dynamoDb.DeviceTableHandler
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.dynamoDb.UserTableHandler
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.data.sqlLite.ScheduleTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.entity.sql.scheduling.ScheduleDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.utilities.DialogListener
import kotlinx.android.synthetic.main.activity_home_details.*
import kotlinx.android.synthetic.main.layout_header.*
import org.jetbrains.anko.custom.onUiThread
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import kotlin.concurrent.thread


class HomeDetailsActivity : AppCompatActivity(), WallpaperAdapter.WallpaperListener, RoomAdapter.RoomListener {
    override fun onAddRoomClicked() {
    }

    override fun onRoomClicked() {
    }

    private val localSqlDatabase = DeviceTable()
    private val localSqlSceneDatabase = SceneTable()
    private val localSqlScheduleDatabase = ScheduleTable()
    private var mDb: SQLiteDatabase? = null
    private var mDbScene: SQLiteDatabase? = null
    private var mDbSchedule: SQLiteDatabase? = null
    private val deviceDynamoDb = DeviceTableHandler()
    private val userDynamoDb = UserTableHandler()
    private val localSqlUtils = UtilsTable()
    var mDbUtils: SQLiteDatabase? = null
    private var rulesTableDo = RulesTableHandler()
    var wallpaperList: MutableList<Wallpaper> = ArrayList()
    var wallPaperAdapter: WallpaperAdapter? = null


    private var HomeEditType: String ?= null
    private lateinit var HomeEditName: String
    var homeDetails : MutableList<RoomModelJson> = java.util.ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_details)


        val dbUtils = UtilsDbHelper(this)
        mDbUtils = dbUtils.writableDatabase

        val dbHelper = DeviceDbHelper(this)
        mDb = dbHelper.writableDatabase

        val dbHelperScene = SceneDbHelper(this)
        mDbScene = dbHelperScene.writableDatabase

        val dbHelperSchedule = ScheduleDbHelper(this)
        mDbSchedule = dbHelperSchedule.writableDatabase



        btn_delete.setOnClickListener {
            if(HomeEditName == "My Home"){
                Utils.showCustomDialogOk(this,"Delete Home",this.getString(R.string.dialog_delete_default_home),R.layout.dialog_layout,object: DialogListener {
                    override fun onOkClicked() {

                    }
                    override fun onCancelClicked() {

                    }
                })
            }else{
                Utils.showCustomDialog(this,"Delete Home",this.getString(R.string.dialog_delete_home),R.layout.dialog_layout,object: DialogListener {
                    override fun onOkClicked() {
                        deleteHome()

                    }
                    override fun onCancelClicked() {

                    }
                })

            }

        }
        homeDetails = localSqlUtils.getHomeData(mDbUtils!!, "home")

        init()


    }
    fun deleteHome(){
        var usertype= true
            if (HomeEditType == "edit") {
                var devices = localSqlDatabase.getDevicesForHome(mDb!!, HomeEditName)

                var homeDetailsNew : MutableList<RoomModelJson> = java.util.ArrayList()
                var guestHomes : MutableList<String> = ArrayList()
                for (homes in homeDetails) {
                    if((homes.type == "home") and (homes.sharedHome == "guest")){
                        guestHomes.add(homes.name)
                    }
                }
                for(item in homeDetails){
                      if(item.type == "home"){
                         if(item.name != HomeEditName){
                             if(item.sharedHome == "guest"){
                                 usertype = false
                             }
                             homeDetailsNew.add(item)
                          }
                      }else if(item.sharedHome != HomeEditName){
                          var flag = true
                          for(h in guestHomes){
                              if(h == item.sharedHome){
                                  flag = false
                                  break
                              }
                          }
                          if(flag){
                              homeDetailsNew.add(item)
                          }
                      }
                }
                localSqlUtils.replaceHome(mDbUtils!!,"home",homeDetailsNew)
                localSqlDatabase.deleteHome(mDb!!, HomeEditName)
                localSqlSceneDatabase.deleteHomeScenes(mDbScene!!, HomeEditName)
                localSqlScheduleDatabase.deleteHomeSchedules(mDbSchedule!!, HomeEditName)

                    thread {
                        var userId = Constant.IDENTITY_ID

                        if(usertype){
                            rulesTableDo.deleteGuestAccessFromMaster(HomeEditName,userId!!)
                        }else{
                            rulesTableDo.deleteHomeDynamo(homeDetailsNew,userId!!,HomeEditName)
                        }

                        runOnUiThread {
                            progressBar.visibility = View.INVISIBLE
                            val prefEditor = PreferenceManager.getDefaultSharedPreferences(this).edit()
                            prefEditor.putString("HOME", "My Home")
                            prefEditor.apply()
                            val intent = Intent(applicationContext, DashboardActivity::class.java)
                            this.startActivity(intent)
                            this.finish()
                        }
                    }

            }else{
                Toast.makeText(this,"Home is not yet created to delete", Toast.LENGTH_SHORT).show()
            }


    }




    fun init() {
        //get edit or create type input
        HomeEditType = intent.getStringExtra(Constant.CREATE_HOME_ROOM)
        HomeEditName = intent.getStringExtra("HOME_NAME")


        //initialize delete and input text buttons
        if(HomeEditType == "create"){
            btn_delete.visibility = View.INVISIBLE
            var listRoom: MutableList<RoomModelJson> = ArrayList()
            input_name.visibility = View.VISIBLE
            listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")

            for(x in listRoom){
                if(x.name == Constant.HOME) {

                    Utils.setDrawable(this, activity_main, x.bgUrl.toInt())

                }

            }
            //Utils.setDrawable(this, activity_main)  //set background image
        }else{
            btn_delete.visibility = View.VISIBLE
            Utils.setDrawable(this, activity_main,HomeEditName)
            text_home_static.visibility = View.VISIBLE
            text_home_static.text = HomeEditName
            input_name.visibility = View.GONE
        }

        text_pick.visibility = View.VISIBLE
        wallpaper_grid.visibility = View.VISIBLE
        tvTitle.text = getString(R.string.title_home_details)
        initializeWallPaperAdapter()

        tvNext.text = getString(R.string.text_save)
        tvNext.setTextColor(Color.WHITE)
        tvNext.setOnClickListener {
                 var isEditDone = false
                if (HomeEditType == "create") {
                    input_name.visibility = View.VISIBLE
                    if(input_name.text.toString().isEmpty()){
                        toast("Cannot change the wallpaper for Default Home")
                    }else{

                        var isHomeExists = false
                        for(item in homeDetails){
                            if((item.type == "home") and (item.name == input_name.text.toString())){
                                isHomeExists = true
                            }
                        }
                        if(!isHomeExists){
                            var defaultRoom = RoomModelJson(input_name.text.toString(),"home","master",wallPaperAdapter!!.getWallPaperId().toString(),0)
                            homeDetails.add(defaultRoom)


                            localSqlUtils.replaceHome(mDbUtils!!,"home",homeDetails)

                            thread {
                                var userId = Constant.IDENTITY_ID
                                rulesTableDo.homeSQLtoDYNAMO(homeDetails,userId!!)
                            }
                            wallPaperAdapter.let {
                                Utils.saveWallPaperId(applicationContext, wallPaperAdapter!!.getWallPaperId(),input_name.text.toString())
                            }
                            isEditDone = true
                        }else{
                            toast("Home already exists!!")
                        }
                    }

                } else {
                    input_name.visibility = View.GONE
                    for(item in homeDetails){
                        if((item.type == "home") and (item.name ==  text_home_static.text)){
                            item.bgUrl = wallPaperAdapter!!.getWallPaperId().toString()
                        }
                    }

                    localSqlUtils.replaceHome(mDbUtils!!,"home",homeDetails)
                    thread {
                        var userId = Constant.IDENTITY_ID
                        rulesTableDo.homeSQLtoDYNAMO(homeDetails,userId!!)
                    }
                    wallPaperAdapter.let {
                        Utils.saveWallPaperId(applicationContext, wallPaperAdapter!!.getWallPaperId(),HomeEditName)
                    }
                    isEditDone = true

                }
                if(isEditDone){
                    val intent = Intent(this, DashboardActivity::class.java)
                    startActivity(intent)
                    this.finish()
                }
        }
        home.setOnClickListener {
            //            val intent = Intent(this, DashboardActivity::class.java)
//            startActivity(intent)
            finish()
        }


    }

    fun initializeWallPaperAdapter() {
        val drawables: IntArray = intArrayOf(R.drawable.one_new, R.drawable.two_new, R.drawable.three_new, R.drawable.four_new, R.drawable.five_new, R.drawable.six_new, R.drawable.seven_new,R.drawable.eight_new,R.drawable.nine_new)
        for (i in 0 until drawables.size) {
            val wallpaper = Wallpaper()
            wallpaper.id = drawables[i]
            wallpaper.isSelected = false

            if(HomeEditType == "create"){
                if (i == Utils.getWallpaperId(this))
                    wallpaper.isSelected = true
            }else{
                if (i == Utils.getWallpaperId(this,HomeEditName!!))
                    wallpaper.isSelected = true
            }
            wallpaperList.add(wallpaper)
        }

        wallPaperAdapter = WallpaperAdapter(this, this, wallpaperList)
        wallpaper_grid.isNestedScrollingEnabled = false
        wallpaper_grid.layoutManager = GridLayoutManager(this, 3)
        wallpaper_grid.adapter = wallPaperAdapter
    }

    override fun onWallpaperSelect(wallpaerId: Int) {
        Utils.setDrawable(this, activity_main, wallpaerId)
    }


    override fun onBackPressed() {
        super.onBackPressed()
//        val intent=Intent(this,DashboardActivity::class.java)
//        startActivity(intent)
        this.finish()
    }

}
