package com.wozart.aura.ui.base.baseselectdevices

import android.view.ViewGroup
import com.wozart.aura.ui.createautomation.baseadapters.BaseRoomAdapter
import com.wozart.aura.ui.createautomation.RoomModel
import kotlinx.android.synthetic.main.item_select_room.view.*

/**
 * Created by Niranjan P on 3/22/2018.
 */

class SelectRoomAdapter : BaseRoomAdapter() {
    override fun bindAdapter(holder: ViewHolder?, room: RoomModel) {
        holder?.itemView?.listDevices?.adapter = SelectDevicesAdapter(room.deviceList)
    }

}
