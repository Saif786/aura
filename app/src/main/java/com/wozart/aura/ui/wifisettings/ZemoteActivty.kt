package com.wozart.aura.ui.wifisettings


import android.os.Bundle
import com.wozart.aura.R
import com.wozart.aura.aura.ui.wifisettings.OnFragmentInteractionListener
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.ui.base.BaseAbstractActivity

class ZemoteActivty : BaseAbstractActivity(),OnFragmentInteractionListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        val themeId: Int = Utils.getThemeId(applicationContext)
        if (themeId != 0)
            setTheme(themeId)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_automation)
        //  Utils.setDrawable(this, container)
        navigateToFragment(ZmoteSettingFragment(), getString(R.string.tag_wifi_settings), true, true)
    }

    override fun onWifiSettingsButtonClick() {
        navigateToFragment(ZemoteConfigurationFragment(), getString(R.string.tag_wifi_settings))
    }

    override fun onSkipClick() {

    }
}