package com.wozart.aura.aura.ui.createautomation

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.PorterDuff
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.SwitchCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.wozart.aura.R
import com.wozart.aura.data.model.AutomationModel
import com.wozart.aura.entity.model.scene.Scene
import com.wozart.aura.entity.sql.scheduling.ScheduleDbHelper
import com.wozart.aura.ui.base.SceneIconAdapter
import com.wozart.aura.ui.createautomation.AutomationScene
import com.wozart.aura.ui.createautomation.OnFragmentInteractionListener
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.customview.CustomEditText
import kotlinx.android.synthetic.main.fragment_create_automation.*
import kotlinx.android.synthetic.main.layout_header.*
import org.jetbrains.anko.support.v4.longToast

/**
 * Created by Saif on 11/11/2018.
 * Wozart Technology Pvt Ltd
 * mds71964@gmail.com
 */

class CreateAutomationFragment : Fragment() {
    private val drawables = arrayListOf(R.drawable.ic_enter_off, R.drawable.ic_exit_off, R.drawable.ic_good_morning_off,
            R.drawable.ic_good_night_off,R.drawable.ic_party_off,R.drawable.ic_reading_off,R.drawable.ic_movie_off)
    private var mListener: OnFragmentInteractionListener? = null
    private var sceneIconurl : Int ?=0
    var automationName: String? =null
    private var mDBAutomation: SQLiteDatabase? =null
    var automationSceneNameOld: String? = null
    var automationSceneType: String? = null
    private var sceneIconPosition : Int ?=0
    private lateinit var sceneIconAdapter: SceneIconAdapter
    private var automationScene = AutomationScene()
    var roomList = arrayListOf<RoomModel>()
     var automationModal = AutomationModel("",0,roomList,false,"","","","","")
    var scheduleType = "time"
    var automationEnable : Boolean = false
    var sceneList = ArrayList<Scene>()
    var selectedSceneIcon: Boolean = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.fragment_create_automation, container, false)

        var tvTitle = view.findViewById<TextView>(R.id.tvTitle)

        var radioTime = view.findViewById(R.id.radioTime) as RadioButton
        var radioGeo = view.findViewById<RadioButton>(R.id.radioGeo)
        var automationEnableLayout = view.findViewById<RelativeLayout>(R.id.enableCard)
        var switchenable = view!!.findViewById<SwitchCompat>(R.id.switchenable)

        automationEnableLayout.visibility = View.INVISIBLE

        automationModal.Automationenable = true
        automationEnable = automationModal.Automationenable



        if(context is CreateAutomationActivity){
            automationSceneNameOld = (context as CreateAutomationActivity).getAutomationSceneName()
            automationSceneType = (context as CreateAutomationActivity).getAutomationSceneType()
            automationScene = (context as CreateAutomationActivity).getAutomationScene()

        }

        if(automationSceneType == "edit"){
            var input_name = view.findViewById<CustomEditText>(R.id.input_name)
            tvTitle.text = "Edit Automation"
            input_name.setText(automationSceneNameOld)
            scheduleType = automationScene.type!!
            automationEnableLayout.visibility = View.VISIBLE
            for(automationEnableCheck in automationScene.property){
                if(automationEnableCheck.AutomationEnable == true){
                    automationEnable = automationEnableCheck.AutomationEnable
                    switchenable.isChecked = automationEnable
                }else{
                    automationEnable = automationEnableCheck.AutomationEnable
                    switchenable.isChecked = automationEnable
                }

            }

            switchenable?.setOnCheckedChangeListener { _, isChecked ->
                automationEnable = switchenable.isChecked
            }

            if(scheduleType == "geo"){
                radioGeo.isChecked = true
            }else{
                radioTime.isChecked = true
            }
        }
        val dbAutomation = ScheduleDbHelper(context!!)
        mDBAutomation = dbAutomation.writableDatabase

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (context is CreateAutomationActivity) {
            sceneIconurl = (context as CreateAutomationActivity).getSceneIconUrl()
        }
        sceneIconPosition = 0
        var pos = 0
        for(icons in drawables){
            if(icons == automationScene.icon){
                sceneIconPosition = pos
            }else{
                pos = pos!! + 1
            }
        }

        for (i in drawables.indices) {
            var scene = Scene()
            scene.icon = drawables[i]
            if (i == sceneIconurl) {
                sceneIconPosition = pos
                selectedSceneIcon = true
            } else {
                selectedSceneIcon = false
            }
            sceneList.add(scene)

        }

        sceneIconAdapter = SceneIconAdapter(sceneList) { pos: Int, Boolean ->

        }
        init()
    }

    fun getIcon() : Int{
        return drawables[0]
    }


    @SuppressLint("ResourceType")
    private fun init() {
        tvTitle.setTextColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
        home.setColorFilter(ContextCompat.getColor(activity!!.baseContext,R.color.white), PorterDuff.Mode.SRC_ATOP)
        tvNext.setTextColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
        home.setOnClickListener { mListener?.onHomeBtnClicked() }
        tvNext.setOnClickListener {
            if(radioTime.isChecked == true){
                openNextScreen(radioTime.id)
            }else if(radioGeo.isChecked == true){
                openNextScreen(radioGeo.id)
            }
            else{
                Toast.makeText(context,"Please select radio for time based.", Toast.LENGTH_SHORT).show()
            }

        }
        radioTime.setOnCheckedChangeListener { button, b ->
            if(b){
                radioTime.isChecked = true
                radioGeo.isChecked = false
                radioTime.id = 0
                scheduleType = "time"
            }
        }

        radioGeo.setOnCheckedChangeListener{ btn , b ->
            if(b){
                radioGeo.isChecked = true
                radioTime.isChecked = false
                radioGeo.id = 1
                scheduleType = "geo"
            }
        }



        tvTimeBase.setOnClickListener {
            radioTime.isChecked = true
            radioGeo.isChecked = false
            radioTime.id = 0
            scheduleType = "time"
        }
        tvLocBase.setOnClickListener {
            radioGeo.isChecked = true
            radioTime.isChecked = false
            radioGeo.id = 1
            scheduleType = "geo"
        }
        input_name.visibility=View.VISIBLE
//        tvSelectSceneIcon.visibility=View.VISIBLE
//        listScenes.visibility=View.VISIBLE
//        listScenes.adapter=iconsAdapter
//        listScenes.layoutManager=LinearLayoutManager(context,LinearLayout.HORIZONTAL,false)
        populateData()
    }

    private fun populateData(){
        automationSceneList()
    }
    private fun automationSceneList(){
        sceneIconAdapter.init(drawables,sceneIconPosition!!)
    }

    private fun openNextScreen(checkedRadioButtonId: Int) {
        when(checkedRadioButtonId) {
            radioTime.id -> openSetTimeScreen()
            radioGeo.id -> openSetGeoScreen()
        }

    }

    private fun openSetGeoScreen() {
        automationName = input_name.text.toString()

        var pref  = PreferenceManager.getDefaultSharedPreferences(context).edit()
        pref.putString("AUTOMATION_NAME",automationName)
        pref.apply()

        if(automationName!!.isEmpty()){
            longToast("Please Enter Automation Name")
        }else{
            var icon = getIcon()
            scheduleType = "geo"
            var intent = Intent(activity,SetGeoAutomationActivity::class.java)
            intent.putExtra("automationNameOld" ,automationSceneNameOld)
            intent.putExtra("automationSceneType" ,automationSceneType)
            intent.putExtra("automationName",automationName)
            intent.putExtra("scheduleBasedType",scheduleType)
            intent.putExtra("scheduleEnable",automationEnable)
            intent.putExtra("icon",icon)
            startActivity(intent)
        }

    }

    private fun openSetTimeScreen() {
        automationName = input_name.text.toString()
        if(automationName!!.isEmpty()){
            longToast("Please Enter Automation Name")
        }
        else{
            scheduleType = "time"
            var icon = getIcon()
            mListener?.navigateToFragment(SetTimeAutomationFragment.newInstance(automationName!!,scheduleType,icon,automationSceneNameOld!!,automationSceneType!!,automationEnable))
        }

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnAutomationListInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

}
