package com.wozart.aura.ui.dashboard.more

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.wozart.aura.R
import com.wozart.aura.aura.data.model.Customization
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.dynamoDb.UserTableHandler
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.data.sqlLite.ScheduleTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.entity.sql.scheduling.ScheduleDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.login.GoogleLoginActivity
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.DialogListener
import kotlinx.android.synthetic.main.activity_profile.*
import org.jetbrains.anko.longToast
import kotlin.concurrent.thread

class ProfileActivity: AppCompatActivity() {
    private var localSQLDtabaseDevice = DeviceTable()
    private var localSQLiteDatabase = SceneTable()
    private var localSqlSchedule = ScheduleTable()
    private var localSqlUtils = UtilsTable()
    private var mdbDevice: SQLiteDatabase ?= null
    private var mdbScene: SQLiteDatabase ?= null
    private var mdbSchedule: SQLiteDatabase ?= null
    private var mdbUtils: SQLiteDatabase ?= null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        val dbHelper = DeviceDbHelper(this)
        mdbDevice = dbHelper.writableDatabase
        val scenDbHelper = SceneDbHelper(this)
        mdbScene = scenDbHelper.writableDatabase

        val scheduleDbHelper = ScheduleDbHelper(this)
        mdbSchedule = scheduleDbHelper.writableDatabase
        val dbUtils = UtilsDbHelper(this)
        mdbUtils = dbUtils.writableDatabase

        val prefs = PreferenceManager.getDefaultSharedPreferences(this@ProfileActivity)
        val userName = prefs.getString("USERNAME", "defaultStringIfNothingFound")
        val email = prefs.getString("EMAIL", "defaultStringIfNothingFound")
        val userProfilePicture = prefs.getString("PROFILE_PICTURE", "defaultStringIfNothingFound")

        input_name.setText(userName)
        input_email.setText(email)
        Glide.with(this).load(userProfilePicture).into(logo_iv)
        delete_profile.setOnClickListener {
            Utils.showCustomDialog(this, getString(R.string.title_logout), getString(R.string.text_logout_message), R.layout.dialog_layout, object : DialogListener {
                override fun onOkClicked() {
                    getSharedPreferences("USERNAME", 0).edit().clear().apply()
                    getSharedPreferences("EMAIL", 0).edit().clear().apply()
                    getSharedPreferences("ID", 0).edit().clear().apply()
                    getSharedPreferences("PROFILE_PICTURE", 0).edit().clear().apply()
                    getSharedPreferences("HOME", 0).edit().clear().apply()
                    getSharedPreferences("HOME_ROOM_DATA", 0).edit().remove("HOME_ROOM_DATA").apply()

                    val prefEditor = PreferenceManager.getDefaultSharedPreferences(applicationContext).edit()
                    prefEditor.clear()
                    prefEditor.apply()
                    thread {
                        var userId = Constant.IDENTITY_ID
                        val userDynamo = UserTableHandler()
                        var userDelete = userDynamo.deleteUser(userId!!)
                        if(userDelete == "SUCCESS"){
                            localSQLDtabaseDevice.deleteTable(mdbDevice!!)
                            localSQLiteDatabase.deleteTable(mdbScene!!)
                            localSqlUtils.deleteUtilstTable(mdbUtils!!)
                            localSqlSchedule.deleteTable(mdbSchedule!!)

                        }
                        runOnUiThread {
                            val intent = Intent(applicationContext, GoogleLoginActivity::class.java)
                            intent.putExtra("type","Login")
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                            finish()
                        }

                    }

                /*    val intent = Intent(applicationContext, GoogleLoginActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)*/

                }

                override fun onCancelClicked() {

                }
            })
        }

        back.setOnClickListener {
           this.finish()
        }
    }
}
