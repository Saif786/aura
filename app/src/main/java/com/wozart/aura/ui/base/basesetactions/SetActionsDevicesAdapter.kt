package com.wozart.aura.ui.base.basesetactions

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.annotation.ColorInt
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat.getColor
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.bumptech.glide.Glide
import com.warkiz.widget.IndicatorSeekBar
import com.wozart.aura.R
import com.wozart.aura.ui.createautomation.baseadapters.BaseDevicesAdapter
import com.wozart.aura.ui.dashboard.Device
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.aura.utilities.Utils.getIconDrawable
import com.wozart.aura.ui.createautomation.baseadapters.BaseActionDeviceAdapter
import com.wozart.aura.utilities.ToastFactory
import kotlinx.android.synthetic.main.dialog_configure_dimming.*
import kotlinx.android.synthetic.main.item_set_actions_device.view.*
import org.jetbrains.anko.backgroundColor

class SetActionsDevicesAdapter(deviceList: ArrayList<Device>) : BaseActionDeviceAdapter(deviceList) {

    override fun customizeUI(itemView: View?, device: Device) {
        itemView?.let {
            val res = itemView.context.resources
            // device.isSelected = false
            device.isTurnOn = false
            itemView.deiceIcon?.setImageResource(Utils.getIconDrawable(device.type, device.isTurnOn))
            itemView.deviceCard.setCardBackgroundColor(res.getColor(R.color.list_item_inactive))
            itemView.deviceStatus.text = "off"

        }
    }

    override fun getLayoutType(): Int {
        return R.layout.item_set_actions_device
    }

    override fun registerListeners(holder: DeviceHolder?, position: Int) {
        /*val clickAnimation = AnimationUtils.loadAnimation(holder?.itemView?.context,R.anim.bounce);
        val interpolator = BounceInterpolator(0.2, 20.0)
        clickAnimation.setInterpolator(interpolator)*/
        val zoomin = AnimationUtils.loadAnimation(holder?.itemView?.context, R.anim.zoomin)
        val zoomout = AnimationUtils.loadAnimation(holder?.itemView?.context, R.anim.zoomout)
        val device = deviceList[position]
        holder?.itemView?.setOnClickListener {
            holder.itemView?.deviceCard?.startAnimation(zoomin)
            holder.itemView?.deviceCard?.startAnimation(zoomout)
            onClick(holder, device)
        }
        holder?.itemView?.setOnLongClickListener { onLongClick(holder, device) }
    }

    private fun onLongClick(holder: DeviceHolder, device: Device): Boolean {
        //TODO dynamic check for non dimmablew
        if (device.dimmable) {
            openDimmingDialog(holder, device)
        } else {
            //TODO Create toast for non dimmable
            //Toast.makeText(coxt,"Load is not dimmable",Toast.LENGTH_SHORT).show()
        }
        return false
    }

    private fun openDimmingDialog(holder: DeviceHolder, device: Device) {
        val dialog = Dialog(holder.itemView.context)
        var dimVal = device.dimVal
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_configure_dimming)
        if (device.name.equals("Fan") && device.isTurnOn) {
            Glide.with(holder.itemView).load(Utils.getIconDrawable(device.type, device.isTurnOn))
                    .into(holder.itemView.deiceIcon)
        }else{
            dialog.iconDevice.setImageResource(Utils.getIconDrawable(device.type, device.isTurnOn))
        }

        dialog.tvDialogTitle.text = String.format(holder.itemView.resources.getString(R.string.text_configure_device_dim), device.name)
        dialog.rangeBarDim.setOnRangeBarChangeListener { _, _, _, _, rightPinValue ->
            dimVal = Integer.parseInt(rightPinValue)
        }
        dialog.sickbar.setProgress(dimVal.toFloat())
        dialog.sickbar.setOnSeekChangeListener(object : IndicatorSeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar?, thumbPosOnTick: Int) {
            }

            override fun onSectionChanged(seekBar: IndicatorSeekBar?, thumbPosOnTick: Int, textBelowTick: String?, fromUserTouch: Boolean) {
            }

            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar?) {
            }

            override fun onProgressChanged(seekBar: IndicatorSeekBar?, progress: Int, progressFloat: Float, fromUserTouch: Boolean) {
                dimVal = progress
                device.dimVal = dimVal
                device.isSelected = true
                holder.itemView.deviceStatus.text = device.dimVal.toString() + "%"
                change(holder,device)
            }
        })
        dialog.btnDone.setOnClickListener {
            device.isTurnOn = true // may be it needs to be happen automatically
            dialog.cancel()
        }
        dialog.show()
    }

    private fun onClick(holder: DeviceHolder, device: Device) {
        val res = holder.itemView.context.resources
        if (device.name.equals("Fan") && device.isTurnOn) {
            Glide.with(holder.itemView).load(Utils.getIconDrawable(device.type, device.isTurnOn))
                    .into(holder.itemView.deiceIcon)

        } else if (device.name.equals("Exhaust Fan") && device.isTurnOn) {
            Glide.with(holder.itemView).load(Utils.getIconDrawable(device.type, device.isTurnOn))
                    .into(holder.itemView.deiceIcon)

        }
        else{
            holder.itemView.deiceIcon.setImageResource(getIconDrawable(device.type, device.isTurnOn))
        }
        if (device.isSelected) {
            device.isTurnOn = false
            holder.itemView?.deiceIcon?.setImageResource(Utils.getIconDrawable(device.type, device.isTurnOn))
            holder.itemView.deviceCard.setCardBackgroundColor(res.getColor(R.color.list_item_inactive))
            holder.itemView.deviceStatus.text = device.status
            device.isSelected = !device.isSelected
        } else {
            device.isTurnOn = true
            if (device.dimmable == false) {
                holder.itemView.deviceStatus.text = "ON"
                holder.itemView?.deiceIcon?.setImageResource(Utils.getIconDrawable(device.type, device.isTurnOn))
                holder.itemView.deviceCard.setCardBackgroundColor(res.getColor(R.color.white))
                device.isSelected = !device.isSelected
            } else {
                holder.itemView?.deiceIcon?.setImageResource(Utils.getIconDrawable(device.type, device.isTurnOn))
                holder.itemView.deviceCard.setCardBackgroundColor(res.getColor(R.color.white))
                device.isSelected = !device.isSelected
                holder.itemView.deviceStatus.text = device.dimVal.toString() + "%"
            }
        }

        //notifyDataSetChanged()
    }

    fun change(holder: DeviceHolder,device: Device){
        val res = holder.itemView.context.resources
        device.isTurnOn = true
        if (device.dimmable == false) {
            holder.itemView.deviceStatus.text = "ON"
            holder.itemView?.deiceIcon?.setImageResource(Utils.getIconDrawable(device.type, device.isTurnOn))
            holder.itemView.deviceCard.setCardBackgroundColor(res.getColor(R.color.white))
            device.isSelected = !device.isSelected
        } else {
            holder.itemView?.deiceIcon?.setImageResource(Utils.getIconDrawable(device.type, device.isTurnOn))
            holder.itemView.deviceCard.setCardBackgroundColor(res.getColor(R.color.white))
            device.isSelected = !device.isSelected
            holder.itemView.deviceStatus.text = device.dimVal.toString() + "%"
        }
    }

}