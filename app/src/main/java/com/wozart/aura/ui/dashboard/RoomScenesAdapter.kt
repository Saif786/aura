package com.wozart.aura.ui.dashboard

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.wozart.aura.R
import com.wozart.aura.aura.ui.dashboard.listener.OnOptionsListener
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.utilities.DialogListener
import kotlinx.android.synthetic.main.item_room_scenes.view.*

/***
 * Created by Kiran on 14-03-2018.
 */
class RoomScenesAdapter(val scenesList: ArrayList<Scenes>, val optionerListener: OnOptionsListener?, val listener: (Scenes, Boolean) -> Unit) : RecyclerView.Adapter<RoomScenesAdapter.ScenesHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScenesHolder {
        val inflatedView = LayoutInflater.from(parent?.context).inflate(R.layout.item_room_scenes, parent, false)
        return ScenesHolder(inflatedView);
    }

    override fun onBindViewHolder(holder: ScenesHolder, position: Int) {
        holder?.bind(position,scenesList[position],optionerListener, listener);
    }


    override fun getItemCount() :Int{
        return  scenesList.size
    }

    class ScenesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int, scenes: Scenes, optionerListener: OnOptionsListener?, listener: (Scenes, Boolean) -> Unit) = with(itemView){

            val drawables = arrayListOf(R.drawable.ic_enter_off, R.drawable.ic_exit_off, R.drawable.ic_good_morning_off,
                    R.drawable.ic_good_night_off, R.drawable.ic_party_off, R.drawable.ic_reading_off, R.drawable.ic_movie_off)
            /*val clickAnimation = AnimationUtils.loadAnimation(context,R.anim.bounce);
            val interpolator = BounceInterpolator(0.2, 20.0)
            clickAnimation.setInterpolator(interpolator)*/
            val zoomin = AnimationUtils.loadAnimation(itemView?.context, R.anim.zoomin)
            val zoomout = AnimationUtils.loadAnimation(itemView?.context, R.anim.zoomout)
            itemView.scenesTitle.text = scenes.title
            itemView.scenesIconRoom.setImageResource(scenes.iconUrl)
            itemView.setOnClickListener {
                itemView.card_scene.startAnimation(zoomin)
                itemView.card_scene.startAnimation(zoomout)
                listener(scenes, false) }
            itemView.setOnLongClickListener{
                listener(scenes, true)
            true
            }
            for(i in drawables.indices){
                var icon = drawables[i]
                if(icon == scenes.iconUrl){
                    if(scenes.isOn){
                        itemView.scenesIconRoom.setImageResource(Utils.getSceneDrawable(i,scenes.isOn))
                        itemView.card_scene.setCardBackgroundColor(resources.getColor(R.color.white))
                    }else{
                        itemView.scenesIconRoom.setImageResource(Utils.getSceneDrawable(i,scenes.isOn))
                        itemView.card_scene.setCardBackgroundColor(resources.getColor(R.color.list_item_inactive))
                    }
                }
            }

//            if (scenes.isOn)
//                itemView.card_scene.setCardBackgroundColor(resources.getColor(R.color.white))
//            else
//                itemView.card_scene.setCardBackgroundColor(resources.getColor(R.color.list_item_inactive))

            itemView.delete_scene.setOnClickListener {
                Utils.showCustomDialog(context,"${scenes.title} scene","Delete this Scene?",R.layout.dialog_layout,object : DialogListener {
                    override fun onOkClicked() {

                        Toast.makeText(context,"Delet clicked.",Toast.LENGTH_SHORT).show()
                    }

                    override fun onCancelClicked() {

                        Toast.makeText(context,"Cancel clicked.",Toast.LENGTH_SHORT).show()
                    }
                })
            }


        }
    }


}