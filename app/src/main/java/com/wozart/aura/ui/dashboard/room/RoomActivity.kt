package com.wozart.aura.ui.dashboard.room

import android.content.*
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.os.IBinder
import android.support.v4.content.LocalBroadcastManager
import android.widget.TextView
import com.wozart.aura.R
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.service.AwsPubSub
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.base.BaseAbstractActivity
import com.wozart.aura.ui.createautomation.OnFragmentInteractionListener
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.ui.dashboard.rooms.RoomsFragment
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.activity_container.*
import kotlinx.android.synthetic.main.activity_create_automation.*
import kotlinx.android.synthetic.main.item_device.*

/***************************************************************************
 * File Name :
 * Author : Aarth Tandel
 * Date of Creation : 09/08/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class RoomActivity : BaseAbstractActivity(), OnFragmentInteractionListener {


    private var awsPubSub: AwsPubSub? = null
    internal var mBounded: Boolean = false
    private val localSqlDatabase = DeviceTable()
    private var mDb: SQLiteDatabase? = null
    private var serviceConnection = false
    private var mDbUtils: SQLiteDatabase? = null
    private val localSqlUtils = UtilsTable()
    var listRoom: MutableList<RoomModelJson> = ArrayList()
    override fun onRoomBtnClicked() {
        this.finish()
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private var roomName: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_automation)
        // Utils.setDrawable(this, containerAutomation)
        val dbUtils = UtilsDbHelper(this)
        mDbUtils = dbUtils.writableDatabase

        val dbHelper = DeviceDbHelper(this)
        mDb = dbHelper.writableDatabase
        LocalBroadcastManager.getInstance(this@RoomActivity).registerReceiver(
                onAwsMessageReceived, IntentFilter("AwsShadow"))
        init()

    }

    private fun init() {
        val intent = intent
        roomName = intent.getStringExtra("ROOM_NAME")
        listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")
        for (x in listRoom) {
            if ((x.type == "room") and (x.sharedHome == Constant.HOME)) {
                if (x.name == roomName) {

                    Utils.setRoomDrawable(this, containerAutomation, x.bgUrl.toInt())

                }
                break
            } else if ((x.type == "room") and (x.sharedHome == "default")) {
                if (x.name == roomName) {

                    Utils.setRoomDrawable(this, containerAutomation, x.bgUrl.toInt())

                }
            }
        }
        navigateToFragment(RoomsFragment(), getString(R.string.empty_tag), true, true)
    }

    override fun onHomeBtnClicked() {
        onBackPressed()
    }

    fun getRoomName(): String? {
        return roomName
    }

    /**
     * AWS IoT Subscribe to shadow broadcast receiver
     */

    override fun onStart() {
        super.onStart()
        val mIntent = Intent(this, AwsPubSub::class.java)
        bindService(mIntent, mConnection, Context.BIND_AUTO_CREATE)
    }

    private var mConnection: ServiceConnection = object : ServiceConnection {

        override fun onServiceDisconnected(name: ComponentName) {
            mBounded = false
            awsPubSub = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            mBounded = true
            val mLocalBinder = service as AwsPubSub.LocalAwsBinder
            awsPubSub = mLocalBinder.getServerInstance()
            serviceConnection = true


        }
    }

    override fun onStop() {
        super.onStop()
        if (mBounded) {
            unbindService(mConnection)
            mBounded = false
        }
    }

    private val onAwsMessageReceived = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            val shadow = intent.getStringExtra("data")
            val segments = shadow.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (shadow == "Connected") {
                val things = localSqlDatabase.getThing(mDb!!)
                Thread.sleep(1000)
                for (x in things) {
                    awsPubSub?.AwsGet(x)
                    // awsPubSub?.AwsGetPublish(x)
                    awsPubSub?.AwsSubscribe(x)
                    val led = (System.currentTimeMillis() / 1000)
                    val msg = "{\"state\":{\"desired\": {\"led\": $led}}}"
                    awsPubSub?.AWSPublish(x, msg)
                    Thread.sleep(500)
                }
            } else {
                // if (jsonHelper.deserializeAwsData(segments[0]).state.size == 0) return
//                val device = localSqlDatabase.getDeviceForThing(mDb!!, segments[1])
//                deviceHandler.cloudDevices(jsonHelper.deserializeAwsData(segments[0]), segments[1], device)
            }
        }
    }

    fun pusblishDataToShadow(thing: String, data: String): Boolean {
        if (awsPubSub != null) {
            return awsPubSub!!.AWSPublish(thing, data)
        } else {
            return false
        }
    }

    fun isServiceConnected(): Boolean {
        return serviceConnection
    }

    override fun onBackPressed() {
        super.onBackPressed()
        var intent = Intent(this, DashboardActivity::class.java)
        intent.putExtra("TAB_SET", Constant.ROOMS_TAB)
        this.startActivity(intent)
    }


}