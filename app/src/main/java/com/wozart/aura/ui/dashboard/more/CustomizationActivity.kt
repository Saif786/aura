package com.wozart.aura.ui.dashboard.more

import android.database.sqlite.SQLiteDatabase
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_customizations.*

import com.wozart.aura.R
import com.wozart.aura.aura.data.model.Customization
import com.wozart.aura.data.device.IpHandler
import com.wozart.aura.data.model.CustomizationList
import com.wozart.aura.data.model.IpModel
import com.wozart.aura.ui.adapter.CustomizationAdapter
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.entity.model.aura.DeviceTableModel
import com.wozart.aura.entity.network.ConnectTask
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.utilities.Constant

class CustomizationActivity : AppCompatActivity(), CustomizationAdapter.onDeviceDeletedListener, ConnectTask.TcpMessageReceiver {

    private val localSqlDatabase = DeviceTable()
    private var mDb: SQLiteDatabase? = null
    var userType: Boolean = true
    val localSqlUtils = UtilsTable()
    var mDbUtils: SQLiteDatabase? = null
    private var IP = IpHandler()
    var IpListDevices: MutableList<IpModel> = java.util.ArrayList()
    private var auraSwitches: MutableList<AuraSwitch> = java.util.ArrayList()
    var allDeviceList = java.util.ArrayList<DeviceTableModel>()
    private var mDbDevice: SQLiteDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customizations)
        val dbHelper = DeviceDbHelper(this)
        mDb = dbHelper.writableDatabase
        val dbUtils = UtilsDbHelper(this)
        mDbUtils = dbUtils.writableDatabase
        var listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")
        for (x in listRoom) {
            if (x.name == Constant.HOME) {
                if (x.sharedHome == "guest") {
                    userType = false
                }
            }
        }
        setUpRecyclerView()
        populateRecyclerView()
        init()
        back.setOnClickListener {
            this.finish()
        }
    }

    override fun onDeviceDeleted(name: String, ip: String,uiud:String) {
        ConnectTask(this, this, "{\"type\":8,\"name\":\"$name\",\"set\":1,\"uiud\":\"$uiud\",\"pair\":1}", ip!!, name!!).execute()
    }

    override fun onTcpMessageReceived(message: String) {
        val data = message
        Log.d("DATATCP", data)
    }

    private fun setUpRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(this)
        customization_recycler_view.layoutManager = linearLayoutManager
    }

    private fun populateRecyclerView() {
        val rooms = localSqlDatabase.getRooms(mDb!!, Constant.HOME!!)
        val sectionModelArrayList: ArrayList<CustomizationList> = ArrayList()

        for (room in rooms) {
            val itemArrayList: MutableList<Customization> = ArrayList()
            //for loop for items
            for (device in localSqlDatabase.getDevicesForRoom(mDb!!, Constant.HOME!!, room.roomName!!)) {
                val customization = Customization()
                customization.deviceName = device.name
                customization.isConnected = device.thing != null
                itemArrayList.add(customization)
            }
            sectionModelArrayList.add(CustomizationList(room.roomName!!, itemArrayList))
        }

        if (sectionModelArrayList.isEmpty()) noDevicetv.visibility = View.VISIBLE
        val adapter = CustomizationAdapter(this, sectionModelArrayList, userType, this, auraSwitches)
        customization_recycler_view.adapter = adapter
    }

    fun init() {
        val dbHelper = DeviceDbHelper(this)
        mDbDevice = dbHelper.writableDatabase
        allDeviceList = localSqlDatabase.getDeviceTable(mDbDevice!!)
        IpListDevices = IP.getIpDevices()
        val rooms = localSqlDatabase.getRooms(mDb!!, Constant.HOME!!)
        for (l in IpListDevices) {
            for (room in rooms) {
                for (device in localSqlDatabase.getDevicesForRoom(mDb!!, Constant.HOME!!, room.roomName!!)) {
                    val switch = AuraSwitch()
                    if (device.name == l.name) {
                        if (l.room == null) {

                        } else {
                            switch.room = l.room!!
                        }
                        switch.CloudPresence = l.aws
                        switch.uiud = l.uiud!!
                        switch.ip = l.ip
                        switch.loads = device.loads
                        switch.name = device.name!!
                        auraSwitches.add(switch)
                        break
                    }
                }
            }
        }
    }
}
