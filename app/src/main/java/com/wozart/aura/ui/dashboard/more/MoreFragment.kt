package com.wozart.aura.ui.dashboard.more

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.wozart.aura.R
import com.wozart.aura.aura.data.model.More
import com.wozart.aura.aura.ui.adapter.MoreAdapter
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import kotlinx.android.synthetic.main.fragment_more.*
import com.wozart.aura.ui.login.GoogleLoginActivity
import com.wozart.aura.utilities.DialogListener
import android.net.Uri
import com.wozart.aura.aura.ui.createautomation.SetGeoAutomationFinishActivity
import com.wozart.aura.data.sqlLite.ScheduleTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.sql.scheduling.ScheduleDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper


/***
 * Created by Kiran on 14-03-2018.
 */

class MoreFragment : Fragment(), MoreAdapter.OnAdapterInteractionListener {

    private val localSqlDatabase = DeviceTable()
    private val localSqlScene = SceneTable()
    private val localSqlDatabaseSchedule = ScheduleTable()
    private var mDbSchedule: SQLiteDatabase? = null
    val localSqlUtils = UtilsTable()
    var mDbUtils: SQLiteDatabase? = null
    private var mDbScene: SQLiteDatabase? = null
    private var mDb: SQLiteDatabase? = null

    companion object {
        fun newInstance(): MoreFragment {
            return MoreFragment()
        }
    }

    override fun onMenuOptionsSelected(position: Int) {

        when (position) {
//            0 -> {
//                val intent = Intent(activity, StatsticsActivity::class.java)
//                startActivity(intent)
//            }
            0 -> {
                val intent = Intent(activity, ShareAuraActivity::class.java)
                startActivity(intent)
            }
            1 -> {
                val intent = Intent(activity, CustomizationActivity::class.java)
                startActivity(intent)
            }
            2 -> {
                val intent = Intent(activity, NotificationActivity::class.java)
                startActivity(intent)
            }
            3 -> {
               val intent = Intent(activity,SetGeoAutomationFinishActivity::class.java)
                startActivity(intent)

            }
            4 -> {

                activity?.let { context ->
                    Utils.showCustomDialog(context, getString(R.string.title_logout), getString(R.string.text_logout_message), R.layout.dialog_layout, object : DialogListener {
                        override fun onOkClicked() {
                            context.getSharedPreferences("USERNAME", 0).edit().clear().apply()
                            context.getSharedPreferences("EMAIL", 0).edit().clear().apply()
                            context.getSharedPreferences("ID", 0).edit().clear().apply()
                            context.getSharedPreferences("PROFILE_PICTURE", 0).edit().clear().apply()
                            context.getSharedPreferences("HOME", 0).edit().clear().apply()
                            context.getSharedPreferences("HOME_ROOM_DATA", 0).edit().remove("HOME_ROOM_DATA").apply()
                            context.getSharedPreferences("VERIFIED_EMAIL",0).edit().clear().apply()
                            context.getSharedPreferences("TOKEN",0).edit().clear().apply()
                            val prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit()
                            prefEditor.clear()
                            prefEditor.apply()

                            localSqlDatabase.deleteTable(mDb!!)
                            localSqlScene.deleteTable(mDbScene!!)
                            localSqlUtils.deleteUtilstTable(mDbUtils!!)
                            localSqlDatabaseSchedule.deleteTable(mDbSchedule!!)

                            val intent = Intent(activity, GoogleLoginActivity::class.java)
                            intent.putExtra("type","Login")
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        }

                        override fun onCancelClicked() {

                        }
                    })
                }


            }
            else -> {

            }
        }
    }

    private var adapter: MoreAdapter = MoreAdapter(this)
    private var moreOptions: MutableList<More> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_more, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val dbScene = SceneDbHelper(context!!)
        mDbScene = dbScene.writableDatabase
        val versionName = context!!.getPackageManager()
                .getPackageInfo(context!!.getPackageName(), 0).versionName
        version_code.text = "V"+versionName

        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase
        val dbUtils = UtilsDbHelper(context!!)
        mDbUtils = dbUtils.writableDatabase

        val dbSchedules = ScheduleDbHelper(context!!)
        mDbSchedule = dbSchedules.writableDatabase
        init()
        initProfileClick()
    }

    private fun initProfileClick() {
        profile_layout.setOnClickListener {

            val intent = Intent(activity, ProfileActivity::class.java)
            startActivity(intent)
        }
    }

    fun init() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val userName = prefs.getString("USERNAME", "defaultStringIfNothingFound")
        val email = prefs.getString("EMAIL", "defaultStringIfNothingFound")
        val userProfilePicture = prefs.getString("PROFILE_PICTURE", "defaultStringIfNothingFound")

        user_name.text = (userName)
        user_email.setText(email)
        Glide.with(this).load(userProfilePicture).into(logo_iv)

        adapter.init(moreOptions)
        listOptions.layoutManager = LinearLayoutManager(activity)
        listOptions.adapter = adapter
        test()

    }

    private fun test() {
        val menus = resources.getStringArray(R.array.menus)
        val menuImages = resources.getStringArray(R.array.menu_images)

        for (i in 0 until menus.size) {
            val more = More()
            more.optionName = menus.get(i)
            more.url = menuImages.get(i)
            moreOptions.add(more)
        }
    }


}
