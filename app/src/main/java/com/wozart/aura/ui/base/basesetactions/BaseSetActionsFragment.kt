package com.wozart.aura.ui.base.basesetactions

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wozart.aura.R
import com.wozart.aura.aura.ui.createautomation.SetAutomationActivity
import com.wozart.aura.data.model.AutomationModel
import com.wozart.aura.ui.createautomation.OnFragmentInteractionListener
import com.wozart.aura.ui.createautomation.RoomModel
import kotlinx.android.synthetic.main.fragment_select_devices.*
import kotlinx.android.synthetic.main.layout_header.*


abstract class BaseSetActionsFragment : Fragment() {

    private var mListener: OnFragmentInteractionListener? = null
    companion object {
        var rooms: MutableList<RoomModel> = ArrayList()
        var sceneName: String ?= null
        var sceneIcon : Int= 0
        var sceneNameOld: String ?= null
        var sceneNameType: String ?= null
        var automationNameOld: String?= null
        var automationScheduleType:String?=null
        var room :ArrayList<RoomModel> = ArrayList()
        var data = AutomationModel("title",0,room,true," "," "," "," ","")
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_select_devices, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    protected fun getSceneName() : String{
        return  sceneName!!
    }

    protected fun getSceneNameOld() : String{
        return  sceneNameOld!!
    }

    protected fun getSceneNameType() : String{
        return  sceneNameType!!
    }

    protected fun getAutomationSceneName() : String?{
        return automationNameOld
    }
    protected fun getAutomationSceneType() : String? {
        return automationScheduleType
    }

    protected fun getSceneIcon() : Int{
        return sceneIcon
    }


    private fun init() {
        tvTitle.text  = getString(R.string.text_set_actions)
        tvNext.text = getString(R.string.text_finish)
        tvNext.setOnClickListener { onFinish() }
        tvNext.setTextColor(Color.WHITE)
        home.setOnClickListener { mListener?.onHomeBtnClicked() }
//        listRooms.layoutManager = LinearLayoutManager(activity)
//        listRooms.adapter = adapter
//        adapter.init(rooms)
    }

    abstract fun onFinish()


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnAutomationListInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

}
