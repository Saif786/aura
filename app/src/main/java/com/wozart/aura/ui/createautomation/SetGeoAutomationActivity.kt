package com.wozart.aura.aura.ui.createautomation


import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.*
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.preference.PreferenceManager
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.AppCompatEditText
import android.util.Log
import android.view.View
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationServices.API
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList
import com.wozart.aura.R
import com.wozart.aura.ui.createautomation.GeofenceBroadcastReceiver
import com.wozart.aura.ui.createautomation.GeofenceErrorMessages
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.activity_set_geo_automation.*
import org.jetbrains.anko.design.longSnackbar
import java.io.IOException


class SetGeoAutomationActivity : FragmentActivity(), OnCompleteListener<Void>, LocationListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        ResultCallback<Status> {


    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    var TAG: String = "MainActivity"
    private var mGeofencePendingIntent: PendingIntent? = null
    private var mgoogleapiClient: GoogleApiClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var fusedLocation: FusedLocationProviderApi? = null
    private var locationMarker: Marker? = null
    private var latitude: Double? = null
    private var longitude: Double? = null
    var map = SupportMapFragment()
    var googleMap: GoogleMap? = null
    private var GeofenceCircle: Circle? = null
    var geofencingClient: GeofencingClient? = null
    private var data: Location? = null
    private var latLong: LatLng? = null
    var geoModel = 100.0f
    var updateRadius: Float = 0.0f
    var Updated = 0f
    var lat: Double? = null
    var long: Double? = null
    var addressList: List<Address> = ArrayList()
    private val PLACE_PICKER_REQUEST = 3
    private var automationNameOld: String? = null
    private var automstionSceneType: String? = null
    private var automationName: String? = null
    private var scheduletype: String? = null
    private var selectedIcon: Int? = 0
    private var automationEnable: Boolean = false
    lateinit var arriving: TextView
    lateinit var leavingOut: TextView
    var geofenceTa: Geofence? = null
    var circleOptions = CircleOptions()
    var triggerType: String? = null
    lateinit var locationManager: LocationManager


    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_geo_automation)
        var intent = intent
        automationNameOld = intent.getStringExtra("automationNameOld")
        automstionSceneType = intent.getStringExtra("automationSceneType")
        automationName = intent.getStringExtra("automationName")
        scheduletype = intent.getStringExtra("scheduleBasedType")
        selectedIcon = intent.getIntExtra("icon", 0)
        automationEnable = intent.getBooleanExtra("scheduleEnable", false)

        map = supportFragmentManager.findFragmentById(R.id.map_id) as SupportMapFragment
        map.getMapAsync(this)

        geofencingClient = LocationServices.getGeofencingClient(this)!!

        if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
            initGoogleApiClient()

        } else {
            Log.i(TAG, "GOOGLE PLAY SERVICE IS NOT APPLICABLE")
        }

        init()
    }

    private fun init() {

        home.setOnClickListener { onBackPressed() }
        btnNext.setOnClickListener { openNextScreen() }

        arriving = findViewById(R.id.arriving_in)
        leavingOut = findViewById(R.id.leave_out)

        triggerType = "Arriving"
        arriving.setOnClickListener {
            arriving.setBackgroundColor(resources.getColor(R.color.secondairy_color))
            leavingOut.setBackgroundColor(resources.getColor(R.color.white))
            triggerType = "Arriving"

            //googleMap!!.setMapStyle(null)
            circleOptions = CircleOptions()
                    .center(locationMarker!!.position)
                    .strokeColor(Color.argb(191, 245, 245, 245))
                    .strokeWidth(5f)
                    .fillColor(Color.argb(79, 70, 135, 175))
                    .radius(geoModel.toDouble())
                    .clickable(showDialogueRadius(latLong!!))

            GeofenceCircle = googleMap!!.addCircle(circleOptions)
        }

        leavingOut.setOnClickListener {
            arriving.setBackgroundColor(resources.getColor(R.color.white))
            leavingOut.setBackgroundColor(resources.getColor(R.color.secondairy_color))
            triggerType = "Leaving"
            // googleMap!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(this,R.raw.style))
            circleOptions = CircleOptions()
                    .center(locationMarker!!.position)
                    .strokeColor(Color.argb(255, 255, 255, 255))
                    .strokeWidth(5f)
                    .fillColor(Color.argb(128, 255, 255, 255))
                    .radius(geoModel.toDouble())
                    .clickable(showDialogueRadius(latLong!!))
            GeofenceCircle = googleMap!!.addCircle(circleOptions)
        }

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this).edit()
        sharedPref.putFloat(Constant.KEY_GEOFENCE_RADIUS, geoModel)
        sharedPref.apply()

        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        Updated = pref.getFloat(Constant.KEY_GEOFENCE_UPDATE_RADIUS, 0f)
        if (Updated == 0f) {

        } else {
            geoModel = Updated
        }

        val fab = findViewById<AppCompatEditText>(R.id.fab)
        fab.setOnClickListener {
            loadPlacePicker()
        }

    }

    private fun loadPlacePicker() {
        val builder = PlacePicker.IntentBuilder()

        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST)
        } catch (e: GooglePlayServicesRepairableException) {
            e.printStackTrace()
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        }
    }


    override fun onMapReady(map: GoogleMap?) {
        googleMap = map
        initmap(googleMap!!)

    }

    fun initmap(googleMap: GoogleMap) {
        if (Build.VERSION.SDK_INT >= 23) {

            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                googleMap!!.uiSettings.isZoomControlsEnabled = true
                googleMap!!.setMinZoomPreference(15f)
                googleMap!!.isMyLocationEnabled = true
                googleMap!!.setOnMarkerClickListener(this)
                googleMap!!.setOnCameraIdleListener {
                    var mCameraLocation = googleMap!!.cameraPosition.target
                    getAddress(mCameraLocation.latitude, mCameraLocation.longitude)
                }
            } else {
                // Request permission.
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_PERMISSIONS_REQUEST_CODE)
            }
        }
    }

    fun initGoogleApiClient() {
        mgoogleapiClient = GoogleApiClient.Builder(this)
                .addApi(API)
                .addConnectionCallbacks(ConnectionAddListner())
                .addOnConnectionFailedListener(ConnectionFailedListner())
                .build()
        mgoogleapiClient?.connect()
    }

//
//    @SuppressLint("MissingPermission")
//    fun showCurrentLocationOnMap() {
//        if (isLocationAccessPermitted()) {
//            requestPermissions()
//        } else if (googleMap != null) {
//            googleMap!!.isMyLocationEnabled = true
//        }
//    }

    fun isLocationAccessPermitted(): Boolean {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
    }

    private fun ConnectionAddListner() = object : GoogleApiClient.ConnectionCallbacks {
        @SuppressLint("MissingPermission")

        override fun onConnected(p0: Bundle?) {

            getLastLocation()

            // recoverGeofenceMarker()

        }

        override fun onConnectionSuspended(p0: Int) {

            Log.i(TAG, "Connection suspended for some reason")
        }
    }


    override fun onResume() {
        super.onResume()

    }

    @SuppressLint("MissingPermission")
    fun getLastLocation() {
        if (checkPermissions()) {
            Log.i(TAG, "Successfully connected")
            data = LocationServices.FusedLocationApi.getLastLocation(mgoogleapiClient)
            if (data == null) {
                //onMapDrag(data!!)
                requestLocation()
                LocationServices.FusedLocationApi.requestLocationUpdates(mgoogleapiClient,
                        mLocationRequest, this)

                Log.i(TAG, "Location Request")
            } else {
                writeLocation(data!!)
                addGeofences()
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onStart() {
        super.onStart()
        mgoogleapiClient!!.connect()
    }

    fun writeLocation(data: Location) {
        latitude = data.latitude
        longitude = data.longitude
        latLong = LatLng(latitude!!, longitude!!)
        drawCustomGeofence(geoModel, latLong!!)
    }

    private fun ConnectionFailedListner() = GoogleApiClient.OnConnectionFailedListener {
        Log.i(TAG, "Connection failed due to some reason")
    }

    private fun requestLocation() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(1 * 1000)
                .setFastestInterval(1 * 1000)
    }


    fun checkPermissions(): Boolean {
        return ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun drawCustomGeofence(customGeoRadius: Float, latLong: LatLng) {

        if (GeofenceCircle != null)
            GeofenceCircle!!.remove()

        if (locationMarker != null) {
            locationMarker!!.remove()
        }
        locationMarker = googleMap!!.addMarker(MarkerOptions()
                .title(getplaceAddress(latLong))
                .position(latLong))

        circleOptions = CircleOptions()
                .center(locationMarker!!.position)
                .strokeColor(Color.argb(191, 245, 245, 245))
                .strokeWidth(5f)
                .fillColor(Color.argb(105, 88, 147, 174))
                .radius(customGeoRadius.toDouble())
                .clickable(showDialogueRadius(latLong))

        GeofenceCircle = googleMap!!.addCircle(circleOptions)

        if (customGeoRadius.toDouble() > 100) {
            var zoom = 18f
            var cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLong, zoom)
            googleMap!!.animateCamera(cameraUpdate)
        } else {
            var zoom = 16f
            var cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLong, zoom)
            googleMap!!.animateCamera(cameraUpdate)
        }

        googleMap!!.setOnMarkerClickListener {
            showDialogueRadius(latLong)
        }

        googleMap!!.setOnCircleClickListener {
            showDialogueRadius(latLong)
        }

        var lat = locationMarker!!.position.latitude.toString()
        var long = locationMarker!!.position.longitude.toString()

        var sharedPref = PreferenceManager.getDefaultSharedPreferences(this).edit()
        sharedPref.putString(Constant.GEOFENCE_POSITION_LATITUDE, lat)
        sharedPref.putString(Constant.GEOFENCE_POSITION_LONGITUDE, long)
        sharedPref.apply()

    }


    private fun getplaceAddress(latLng: LatLng): String {
        // 1
        val geocoder = Geocoder(this)
        val addresses: List<Address>?
        val address: Address?
        var addressText = ""

        try {
            // 2
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            // 3
            if (null != addresses && !addresses.isEmpty()) {
                address = addresses[0]
                for (i in 0 until address.maxAddressLineIndex) {
                    addressText += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(i)
                }
            }
        } catch (e: IOException) {
            Log.d("MapsActivity", e.localizedMessage)
        }

        return addressText
    }


    fun showDialogueRadius(latLong: LatLng): Boolean {

        var dialogue = AlertDialog.Builder(this)
        var view = layoutInflater.inflate(R.layout.dialogue_radus_set, null)
        dialogue.setView(view)
        dialogue.setTitle("Set Radius")
        dialogue.setCancelable(false)
        var seek_bar = view.findViewById<SeekBar>(R.id.seek_bar)
        var tv_meter = view.findViewById<TextView>(R.id.tv_meters)
        tv_meter.text = geoModel.toString()
        seek_bar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                tv_meter.text = progress.toString()
                updateRadius = progress.toFloat()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                Toast.makeText(this@SetGeoAutomationActivity, "Selected Radius is " + seekBar.progress + "m", Toast.LENGTH_SHORT).show()
            }
        })
        dialogue.setPositiveButton(R.string.add_radius) { dialog, p1 ->
            if (updateRadius > 100.0f) {
                geoModel = updateRadius
                var pref = PreferenceManager.getDefaultSharedPreferences(this).edit()
                getSharedPreferences(Constant.KEY_GEOFENCE_RADIUS, 0).edit().clear().apply()
                pref.putFloat(Constant.KEY_GEOFENCE_UPDATE_RADIUS, updateRadius)
                pref.apply()
                updatePopulateGeofence(latLong!!, updateRadius)
            }

        }
        dialogue.setNegativeButton(android.R.string.cancel) { dialog, p1 ->
            dialog.cancel()
        }
        dialogue.create()
        dialogue.show()
        return false
    }


    fun updatePopulateGeofence(latLong: LatLng, geoModel: Float) {
        drawCustomGeofence(geoModel, latLong)
        addGeofences()
    }

    private fun recoverGeofenceMarker() {

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        var radiusSaved = sharedPref.getFloat(Constant.KEY_GEOFENCE_RADIUS, 0.0f)
        var lati = sharedPref.getString(Constant.GEOFENCE_POSITION_LATITUDE, "null")
        var longi = sharedPref.getString(Constant.GEOFENCE_POSITION_LONGITUDE, "null")
        Updated = sharedPref.getFloat(Constant.KEY_GEOFENCE_UPDATE_RADIUS, 0f)

        if (Updated == 0f) {
            geoModel = radiusSaved
            lat = lati.toDouble()
            long = longi.toDouble()
            latLong = LatLng(lat!!, long!!)
            drawCustomGeofence(geoModel, latLong!!)
            addGeofences()
        } else {
            geoModel = Updated
            lat = lati.toDouble()
            long = longi.toDouble()
            latLong = LatLng(lat!!, long!!)
            drawCustomGeofence(geoModel, latLong!!)
            addGeofences()
        }
    }

    private fun populateGeofenceList(selectedLatLng: LatLng, customGeoRadius: Float): Geofence? {

        var circleRadius = customGeoRadius
        return Geofence.Builder()
                .setRequestId(Constant.GEOFENCES_ADDED_KEY)
                .setCircularRegion(selectedLatLng!!.latitude, selectedLatLng!!.longitude, circleRadius)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER or Geofence.GEOFENCE_TRANSITION_EXIT)
                .build()
    }


    private fun getGeofencesAdded(): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
                Constant.GEOFENCES_ADDED_KEY, false
        )
    }

    @SuppressLint("MissingPermission")
    private fun addGeofences() {
        geofenceTa = populateGeofenceList(locationMarker!!.position, geoModel)
        geofencingClient!!.addGeofences(getGeofencingRequest(geofenceTa), getGeofencePendingIntent())
                .addOnCompleteListener(this)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (permissions.size == 1 && permissions[0] == android.Manifest.permission.ACCESS_FINE_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initmap(googleMap!!)

            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.finish()
    }


    private fun updateGeofencesAdded(added: Boolean) {
        if (added == true) {
            PreferenceManager.getDefaultSharedPreferences(this)
                    .edit()
                    .putBoolean(Constant.GEOFENCES_ADDED_KEY, added)
                    .apply()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                val place = PlacePicker.getPlace(this, data)
                var addressText = place.name.toString()
                addressText += "\n" + place.address.toString()
                latLong = place.latLng
                drawCustomGeofence(geoModel, latLong!!)
                addGeofences()

            }
        }
    }

    override fun onResult(p0: Status) {
        Log.i(TAG, "onResult: $p0")

        if (p0.isSuccess()) {
            //drawGeofence(geoModel)
        } else {

        }
    }


    override fun onMarkerClick(p0: Marker?): Boolean {
        return false
    }

    override fun onLocationChanged(p0: Location?) {
        try {
            if (p0 == null) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mgoogleapiClient, this)
            }
        } catch (e: Exception) {
            Log.i(TAG, "ERROR $e")
        }
    }

    override fun onComplete(p0: Task<Void>) {
        if (p0.isSuccessful) {
            updateGeofencesAdded(getGeofencesAdded())
        } else {
            var error = GeofenceErrorMessages.getErrorString(this, p0.exception!!)
            Log.i(TAG, "Found error $error")
        }
    }


    private fun requestPermissions() {

        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
        )
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")
            showSnackbarText(R.string.permission_rationale, android.R.string.ok,
                    View.OnClickListener {
                        ActivityCompat.requestPermissions(
                                this,
                                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                REQUEST_PERMISSIONS_REQUEST_CODE
                        )
                    })
        } else {
            Log.i(TAG, "Requesting permission")
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_PERMISSIONS_REQUEST_CODE
            )
        }
    }


    fun getAddress(latitude: Double, longitude: Double) {

        latLong = LatLng(latitude, longitude)
        try {
            val geocoder = Geocoder(this, Locale.getDefault());
            addressList = geocoder.getFromLocation(latitude, longitude, 1)
            var mLocationMarkerText = findViewById<TextView>(R.id.locationMarkertext)
            mLocationMarkerText.setText("Lat : " + latitude + "," + "Long : " + longitude)
            // customMarker(latLong!!)
        } catch (e: Exception) {
            Log.i(TAG, "Error $e");
        }
    }

    @SuppressLint("MissingPermission")
    fun onMapDrag(location: Location) {
        if (checkPermissions()) {
            if (googleMap != null) {
                googleMap!!.uiSettings.isZoomControlsEnabled = false
                googleMap!!.isMyLocationEnabled = true
                googleMap!!.uiSettings.isMyLocationButtonEnabled = true

                var latLong = LatLng(location.latitude, location.longitude)
                var camperPosition = CameraPosition.builder().target(latLong).zoom(14f).tilt(70f).build()
                var cameraUpdateFactory = CameraUpdateFactory.newCameraPosition(camperPosition)
                var mLocationMarkerText = findViewById<TextView>(R.id.locationMarkertext)
                mLocationMarkerText.setText("Lat : " + latitude + "," + "Long : " + longitude)
                googleMap!!.animateCamera(cameraUpdateFactory)

            } else {
                Toast.makeText(this, "Sorry! Unable to create map", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun removeGeofences() {

        geofencingClient!!.removeGeofences(getGeofencePendingIntent()).addOnCompleteListener(this)

    }

    private fun openNextScreen() {
        if (googleMap == null) {
            longSnackbar(findViewById(android.R.id.content), "Just Refresh the App...")
        } else {
            var intent = Intent(this, SetAutomationActivity::class.java)
            intent.putExtra("automationSceneType", automstionSceneType)
            intent.putExtra("automationNameOld", automationNameOld)
            intent.putExtra("scheduleBasedType", scheduletype)
            intent.putExtra("automationName", automationName)
            intent.putExtra("icon", selectedIcon)
            intent.putExtra("customRadius", geoModel)
            intent.putExtra("scheduleEnable", automationEnable)
            intent.putExtra("triggerType", triggerType)
            startActivity(intent)
        }

    }


    private fun getGeofencingRequest(geofence: Geofence?): GeofencingRequest {
        return GeofencingRequest.Builder().apply {
            setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
            addGeofence(geofence)
        }.build()
    }

    private fun getGeofencePendingIntent(): PendingIntent {
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent!!
        }
        val intent = Intent(this, GeofenceBroadcastReceiver()::class.java)
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun showSnackbarText(mainTextStringId: Int, actionStringId: Int, listener: View.OnClickListener) {

    }

}
