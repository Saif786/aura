package com.wozart.aura.ui.wifisettings

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.wozart.aura.R

import kotlinx.android.synthetic.main.tv_remote.*

/***************************************************************************
 * File Name :
 * Author : Aarth Tandel
 * Date of Creation : 09/05/19
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class OpenRemote : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tv_remote)
        tv_title_IR.text = "Added Remote"
        scan()

        back.setOnClickListener {
            this.finish()
        }
    }
    fun scan(){
        loadUI()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadUI() {

        val url = "http://app.zmote.io/#/app/widget/5b3e40aaa5d0ca0100b794f1/view"
        webview_tv.webViewClient = CustomWebViewClient()
        webview_tv.settings.javaScriptEnabled = true
        webview_tv.loadUrl(url)
    }
    class CustomWebViewClient : WebViewClient(){

        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            view?.loadUrl(request?.url.toString())
            return true
        }
        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            // view?.loadUrl("javascript: var header = document.getElementsByClass(\"topbar md-altTheme-theme\").remove()")
            view?.loadUrl("javascript:(function() { " +
                    "var head = document.getElementsByClassName('topbar md-altTheme-theme')[0].style.display='none'; " +
                    "})()")
        }

    }
}