package com.wozart.aura.ui.wifisettings


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.wozart.aura.R
import kotlinx.android.synthetic.main.activity_ir_list.*
import okhttp3.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.util.HashMap


/***************************************************************************
 * File Name :
 * Author : Aarth Tandel
 * Date of Creation : 01/05/19
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class IRDeviceFoundActivity : AppCompatActivity() {
    private lateinit var client: OkHttpClient
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ir_list)
        client = OkHttpClient()
        scan()

        back.setOnClickListener {
            this.finish()
        }

        button.setOnClickListener {
           // scan()
            webview.reload()
        }


    }

    fun scan() {

        loadUI()

      // val url = "http://api.zmote.io/discover"
//        val request = Request.Builder().url(url)
//                .build()
//        client.newCall(request).enqueue(object : Callback {
//            override fun onFailure(call: Call?, e: IOException?) {
//
//            }
//
//            override fun onResponse(call: Call?, response: Response?) {
//                var body = response!!
//                Log.d("IRDEVICE_LIST", body.toString())
//                val type = object : TypeToken<HashMap<String, Boolean>>() {}.type
//                var data = JSONArray(body.toString())
//                arrayList_details= ArrayList()
//                var model = IRDeviceModal()
//                for (jsonIndex in 0..(data.length() - 1)){
//                    model.IRtype = data.getJSONObject(jsonIndex).getString("data")
//                    model.IrName = data.getJSONObject(jsonIndex).getString("name")
//                    model.zchipId = data.getJSONObject(jsonIndex).getString("chipID")
//                    model.fwVersion = data.getJSONObject(jsonIndex).getString("fwVersion")
//                    model.zipCoonnect = data.getJSONObject(jsonIndex).getString("localIP")
//                    model.zstate = data.getJSONObject(jsonIndex).getString("state")
//                    model.zmodel = data.getJSONObject(jsonIndex).getString("model")
//                    arrayList_details.add(model)
//
//                }
//
//
//            }
//        })


    }
    @SuppressLint("SetJavaScriptEnabled")
    private fun loadUI() {
        // settings url goes here currently https://www.google.co.in/'
        val url = "http://app.zmote.io"

        webview.webViewClient = CustomWebViewClient()
        webview.settings.javaScriptEnabled = true
        webview.loadUrl(url)
    }
    class CustomWebViewClient : WebViewClient(){

        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            view?.loadUrl(request?.url.toString())
            return true
        }

    }


}


