package com.wozart.aura.ui.dashboard.more

import android.app.Dialog
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import android.view.Window
import android.widget.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wozart.aura.R
import com.wozart.aura.aura.ui.dashboard.listener.OnOptionsListener
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.dynamoDb.UserTableHandler
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.amazonaws.models.nosql.RulesDO
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.adapter.ShareGuestAdapter
import com.wozart.aura.ui.adapter.ShareMasterAdapter
import com.wozart.aura.ui.base.BaseAbstractActivity
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.DialogListener
import com.wozart.aura.utilities.Encryption
import kotlinx.android.synthetic.main.activity_share_aura.*
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.longToast
import kotlin.concurrent.thread

/***************************************************************************
 * File Name :
 * Author : Saif
 * Date of Creation : 12/09/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0.6
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class ShareAuraActivity : BaseAbstractActivity(), OnOptionsListener {


    private var userDynamoDb: UserTableHandler = UserTableHandler()
    private lateinit var shareGuestAdapter: ShareGuestAdapter
    private lateinit var shareMasterAdapter: ShareMasterAdapter
    private var rulesDynamoDb: RulesTableHandler = RulesTableHandler()
    private val localSqlUtils = UtilsTable()
    private var mDbUtils: SQLiteDatabase? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_aura)
        val dbUtils = UtilsDbHelper(this)
        mDbUtils = dbUtils.writableDatabase

        init()
    }

    override fun onOptionsClicked(view: View) {

    }

    private fun init() {
        var sharedUserForAccess: MutableList<MutableMap<String, String>> = ArrayList()
        var sharedMasterForAccess: MutableList<MutableMap<String, String>> = ArrayList()

        var sharing_to_layout = findViewById<RelativeLayout>(R.id.sharing_to_layout)
        var selected_contacts_layout = findViewById<RelativeLayout>(R.id.selected_contacts_layout)
        var text_layout_share = findViewById<RelativeLayout>(R.id.text_layout_share)

        var guest_access = findViewById<RecyclerView>(R.id.guest_access)
        var master_access = findViewById(R.id.master_access) as RecyclerView
        var btn_add_email = findViewById(R.id.btn_add_email) as ImageView


        thread {
            val owner = rulesDynamoDb.getUser()!!
            if (owner == null) {
                longSnackbar(findViewById(android.R.id.content), "User not found..")
            } else {
                runOnUiThread {
                    val sharedData = HashMap<String, String>()
                    if (owner.master != null) {

                        for (sharing in owner.master!!) {
                            val sharedData = HashMap<String, String>()
                            sharedData["Access"] = sharing["Access"]!!
                            sharedData["Home"] = sharing["Home"]!!
                            sharedData["Name"] = sharing["Name"]!!
                            sharedData["Email"] = sharing["Email"]!!
                            sharedData["Status"] = sharing["Status"]!!
                            if (sharing["Access"] == "invite") {
                                sharedMasterForAccess.add(sharedData)
                            }
                        }
                        if (sharedMasterForAccess.isNotEmpty()) {
                            sharing_to_layout.visibility = View.VISIBLE
                            text_layout_share.visibility = View.GONE
                        }
                        shareMasterAdapter = ShareMasterAdapter(sharedMasterForAccess, this, owner)
                        master_access.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                        master_access.adapter = shareMasterAdapter
                        shareMasterAdapter.notifyDataSetChanged()

                    }
                    if (owner.guest != null) {
                        for (shareGuest in owner.guest!!) {
                            val sharedGuestData = HashMap<String, String>()
                            sharedGuestData["Access"] = shareGuest["Access"]!!
                            sharedGuestData["Home"] = shareGuest["Home"]!!
                            sharedGuestData["Name"] = shareGuest["Name"]!!
                            sharedGuestData["Email"] = shareGuest["Email"]!!

                            if (shareGuest["Access"] == "invite")  {
                                    sharedUserForAccess.add(sharedGuestData)
                            }
                        }

                        if (sharedUserForAccess.isNotEmpty()) {
                            selected_contacts_layout.visibility = View.VISIBLE
                            text_layout_share.visibility = View.GONE
                        }
                        shareGuestAdapter = ShareGuestAdapter(sharedUserForAccess, this, owner)
                        guest_access.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                        guest_access.adapter = shareGuestAdapter
                        shareGuestAdapter.notifyDataSetChanged()
                    }

                }

            }

        }

        back.setOnClickListener {
            this.finish()
        }
        btn_add_email.setOnClickListener {
            popupDialogue(btn_add_email)
        }

    }

    fun shareDevices(input_email: String) {

        Utils.showCustomDialog(this, "Share Home", getString(R.string.dialog_share), R.layout.dialog_layout, object : DialogListener {

            override fun onOkClicked() {
                val prefs = PreferenceManager.getDefaultSharedPreferences(this@ShareAuraActivity)
                val userName = prefs.getString("USERNAME", "defaultStringIfNothingFound")
                var list_room = localSqlUtils.getHomeData(mDbUtils!!, "home")
                for (x in list_room) {
                    if ((x.name == Constant.HOME) and (x.type == "home")) {
                        if (x.sharedHome == "master") {
                            showProgressDialog("Sharing Devices")
                            var result = rulesDynamoDb.shareDevices(input_email, Constant.HOME!!)
                            if (result) {
                                longSnackbar(findViewById(android.R.id.content), "Home shared successfully! ")
                            } else {
                                longSnackbar(findViewById(android.R.id.content), "Sharing failed, try again! ")
                            }

                            dismissProgressDialog()
                            break
                        } else {
                            longSnackbar(findViewById(android.R.id.content), "Guest can not share! ")
                        }
                    }
                }
            }

            override fun onCancelClicked() {

            }
        })

    }

    private fun popupDialogue(btn_add_email: ImageView) {
        var dialogue = Dialog(this)
        dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogue.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogue.setContentView(R.layout.dialogue_request_mail_share)
        val input_email = dialogue.findViewById<EditText>(R.id.input_email)

        fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
            this.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    afterTextChanged.invoke(s.toString())
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            })
        }

        fun EditText.validate(validator: (String) -> Boolean, message: String) {
            this.afterTextChanged {
                this.error = if (validator(it)) null else message
            }
            this.error = if (validator(this.text.toString())) null else message
        }
        input_email.validate({ s -> s.isValidEmail() },
                "Valid email address required")


        val btnCancel = dialogue.findViewById(R.id.btn_cancel) as Button
        var tv_share = dialogue.findViewById<Button>(R.id.tv_share)
        btnCancel.setOnClickListener { dialogue.dismiss() }
        tv_share.setOnClickListener {

            Utils.hideSoftKeyboard(this)
            if (input_email.text == null || input_email.text.isEmpty()) {
                longToast("Enter valid mailID")
            }
            thread {
                if (Encryption.isInternetWorking()) {
                    var rulesTableHandler = RulesTableHandler()
                    var checkExistMail = rulesTableHandler.checkEmailExist(input_email.text.toString())
                    runOnUiThread {
                        if (checkExistMail == "SUCCESS") {
                            shareDevices(input_email.text.toString())
                        } else {
                            longToast("Please Login/SignUp to Aura using ${input_email.text}")
                            longSnackbar(findViewById(android.R.id.content), "User not registered with Wozart Aura")
                        }

                    }
                } else {
                    longSnackbar(findViewById(android.R.id.content), "Please check connection! ")
                }
            }
            dialogue.cancel()
        }
        dialogue.show()
    }

    fun String.isValidEmail(): Boolean = this.isNotEmpty() &&
            Patterns.EMAIL_ADDRESS.matcher(this).matches()

    fun startActivity() {
        val mainIntent = Intent(this, DashboardActivity::class.java)
        mainIntent.putExtra("TAB_SET",Constant.MORE_TAB)
        this.startActivity(mainIntent)
        this.finish()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {

        }
    }

}



