package com.wozart.aura.ui.dashboard.more

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wozart.aura.R
import com.wozart.aura.data.device.IpHandler
import com.wozart.aura.data.dynamoDb.DeviceTableHandler
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.model.Notification
import com.wozart.aura.ui.adapter.NotificationAdapter
import com.wozart.aura.data.dynamoDb.UserTableHandler
import com.wozart.aura.data.model.IpModel
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.data.sqlLite.ScheduleTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraSwitchLoad
import com.wozart.aura.entity.model.aura.DeviceTableModel
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.entity.sql.scheduling.ScheduleDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.base.BaseAbstractActivity
import com.wozart.aura.ui.createautomation.GeoModal
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.ui.dashboard.Device
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.activity_notification.*
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.intentFor
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.thread

class NotificationActivity : AppCompatActivity(), NotificationAdapter.OnAcceptListener, NotificationAdapter.OnDeclineListener {


    private var notificationAdapter: NotificationAdapter? = null
    private var localSqlDevices = DeviceTable()
    private val localSqlSceneDatabase = SceneTable()
    var localSqlSchedule = ScheduleTable()
    val localSqlUtils = UtilsTable()
    private var mDb: SQLiteDatabase? = null
    private var mDbScene: SQLiteDatabase? = null
    private var mDbSchedule: SQLiteDatabase? = null
    var mDbUtils: SQLiteDatabase? = null
    var IpListDevices: MutableList<IpModel> = ArrayList()
    var IpListDevicesNew: MutableList<IpModel> = ArrayList()
    var allDeviceList = java.util.ArrayList<DeviceTableModel>()
    var rulesTableDo = RulesTableHandler()
    private var IP = IpHandler()
    var presentGuestHome: String? = null
    var guestPresnt: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        val dbHelper = DeviceDbHelper(this)
        mDb = dbHelper.writableDatabase

        val dbHelperScene = SceneDbHelper(this)
        mDbScene = dbHelperScene.writableDatabase

        val dbHelperScedule = ScheduleDbHelper(this)
        mDbSchedule = dbHelperScedule.writableDatabase

        val dbUtils = UtilsDbHelper(this)
        mDbUtils = dbUtils.writableDatabase

        progress_bar.visibility = View.VISIBLE
        var listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")
        for (x in listRoom) {
            if (x.sharedHome == "guest") {
                presentGuestHome = x.name
                guestPresnt.add(presentGuestHome!!)
            }
        }
        thread {
            val user = rulesTableDo.getUser()
            val notifications = ArrayList<Notification>()
            runOnUiThread {
                if(user != null){
                    if (user!!.guest != null) {
                        for (invites in user.guest!!) {
                            if (invites["Access"] == "invite") {
                                val notification = Notification()
                                val home = invites["Home"]!!.split("?")
                                if (guestPresnt.size > 0) {
                                    for (guestHome in guestPresnt) {
                                        if (guestHome == invites["Home"]!!) {
                                            notification.status = 1
                                            notification.notificationTitle = "invite from ${invites["Name"]} "
                                            notifications.add(notification)
                                        }
                                    }
                                    break
                                }

                                notification.notificationTitle = "Invite from ${invites["Name"]} "
                                notification.notificationMessage = "Here are the keys of ${home[0]}"
                                notification.sharedUserId = invites["Email"]
                                notification.masterName = invites["Name"]!!.split(" ")[0]
                                notification.sharedHome = invites["Home"]
                                notification.access = invites["Access"]
                                notification.ownerId = user.userId
                                notification.status = 0
                                notifications.add(notification)


                            }
                        }
                    } else if (user.guest == null){
                        if (user.notifications != null) {
                            if (user.notifications!!.size == 0) {

                                Toast.makeText(this, "Sorry no notification found.", Toast.LENGTH_SHORT).show()

                            } else {

                                var notification = Notification()
                                notification.notificationMessage = user.notifications!!.get(0)
                                notification.status = 2
                                notifications.add(notification)
                            }
                        }else{
                            var notification = Notification()
                            notification.notificationMessage = "Sorry no notification found."
                            notification.status = 2
                            notifications.add(notification)
                        }
                    }else if(user.guest!!.size == 0){
                        if (user.notifications != null) {
                            if (user.notifications!!.size == 0) {

                                Toast.makeText(this, "No notification..", Toast.LENGTH_SHORT).show()

                            } else {

                                var notification = Notification()
                                notification.notificationMessage = user.notifications!!.get(0)
                                notification.status = 2
                                notifications.add(notification)
                            }
                        }
                    }
                }



//                if((user.guest!!.isEmpty()) and (user.notifications!!.isEmpty())){
//                    emptyNotificationTv.visibility = View.VISIBLE
//                    Toast.makeText(this, "No notification..", Toast.LENGTH_SHORT).show()
//                }

                notificationAdapter = NotificationAdapter(this, notifications, this, this)
                list_notification.layoutManager = LinearLayoutManager(this)
                list_notification.adapter = notificationAdapter
                progress_bar.visibility = View.INVISIBLE
            }
        }

        back.setOnClickListener {
            finish()
        }
    }

    override fun onAcceptSelected(request: Notification) {
        progress_bar.visibility = View.VISIBLE
        val gson = Gson()
        thread {
            var rulesTable = rulesTableDo.getRulesTable()
            if (rulesTable!!.userId == request.ownerId) {
                //updating devices

                var masterDeviceflag = false
                if (rulesTable.masterDevices != null) {
                    if (rulesTable.masterDevices!!.size != 0) {
                        masterDeviceflag = true
                    }
                }
                if (masterDeviceflag) {
                    for (shareDevice in rulesTable.masterDevices!!) {
                        var sharedHome = shareDevice["home"]
                        var sharedroom = shareDevice["room"]
                        var uiud = shareDevice["uiud"]
                        var sharedname = shareDevice["name"]
                        var sharedthing = shareDevice["thing"]
                        var shareLoads = rulesTable.masterLoads
                        var localLoads: MutableList<AuraSwitchLoad> = ArrayList()
                        for (sharedloads in shareLoads!!) {
                            var flag = false
                            for (load in sharedloads) {
                                if ((load["type"] == "device") and (load["device"] == shareDevice["id"])) {
                                    flag = true
                                    break
                                }
                            }
                            if (flag) {
                                for (load in sharedloads) {
                                    if (load["type"] != "device") {
                                        var localLoad = AuraSwitchLoad()
                                        localLoad.index = load["index"]!!.toInt()
                                        localLoad.icon = load["icon"]!!.toInt()
                                        localLoad.name = load["name"]
                                        localLoad.favourite = load["favourite"]!!.toBoolean()
                                        localLoad.dimmable = load["dimmable"]!!.toBoolean()
                                        localLoads.add(localLoad)
                                    }
                                }
                                break
                            }
                        }
                        localSqlDevices.insertDevice(mDb!!, sharedHome!!, sharedroom!!, uiud!!, sharedname!!, gson.toJson(localLoads), sharedthing!!)
                    }
                }
                var allsharedDevice = localSqlDevices.getDeviceTable(mDb!!)

                //update sharing Scene
                var sharingSceneFlag = false
                if (rulesTable.masterScenes != null) {
                    if (rulesTable.masterScenes!!.size != 0) {
                        sharingSceneFlag = true
                    }
                }
                if (sharingSceneFlag) {
                    var allShareScenes = rulesTable.masterScenes
                    for (sharescenes in allShareScenes!!) {
                        var rooms: ArrayList<RoomModel> = ArrayList()
                        var keys = sharescenes.keys
                        var pair = ""
                        for (k in keys) {
                            pair = k
                        }
                        var sharedscene = sharescenes[pair]
                        var sharedsceneName = ""
                        var icon = 0
                        var sharedhome = Constant.HOME

                        //extract all room list
                        var roomList: MutableList<String> = ArrayList()
                        var deviceList: MutableList<Device> = ArrayList()
                        for (s in sharedscene!!) {
                            if (s["type"] != "scene") {
                                var device = Device(0, s["state"]!!.toBoolean(), s["level"]!!.toInt(), s["deviceName"]!!, "room", s["deviceName"]!!, s["index"]!!.toInt(), false)
                                for (d in allsharedDevice) {
                                    if (d.name == s["deviceName"]) {
                                        device.type = d.loads[device.index].icon!!
                                        device.name = d.loads[device.index].name!!
                                        device.roomName = d.room!!
                                        device.dimmable = d.loads[device.index].dimmable!!
                                        var flag = false
                                        for (r in roomList) {
                                            if (r == d.room) {
                                                flag = true
                                                break
                                            }
                                        }
                                        if (!flag) {
                                            roomList.add(d.room!!)
                                        }
                                        deviceList.add(device)
                                        break
                                    }
                                }

                            } else {
                                sharedsceneName = s["name"]!!
                                icon = s["icon"]!!.toInt()
                                sharedhome = s["home"]!!
                            }
                        }
                        for (r in roomList) {
                            var room = RoomModel()
                            room.name = r
                            for (d in deviceList) {
                                if (d.roomName == r) {
                                    room.deviceList.add(d)
                                }
                            }
                            rooms.add(room)
                        }
                        localSqlSceneDatabase.insertScene(mDbScene!!, sharedsceneName, gson.toJson(rooms), gson.toJson(roomList), sharedhome!!, icon, sharedsceneName, "create")
                    }
                }

                //update sharing master schedule
                var sharingSchedulingFlag = false
                if (rulesTable.masterSchedules != null) {
                    if (rulesTable.masterSchedules!!.size != 0) {
                        sharingSchedulingFlag = true
                    }
                }
                if (sharingSchedulingFlag) {
                    var allshareSchedule = rulesTable.masterSchedules
                    for (sharSchedule in allshareSchedule!!) {
                        var rooms: ArrayList<RoomModel> = ArrayList()
                        var keys = sharSchedule.keys
                        var pair = ""
                        for (k in keys) {
                            pair = k
                        }
                        var schedule = sharSchedule[pair]
                        var sharedscheduleName = ""
                        var icon = 0
                        var sharedhome: String? = null
                        var sharedscheduleStatus: MutableList<GeoModal> = ArrayList()
                        var geoData = GeoModal()
                        var sharedscheduleStartTime: String? = null
                        var sharedscheduleType: String? = null
                        var sharedscheduleEndTime: String? = null
                        var sharedscheduleRoutine: String? = null
                        var roomList: MutableList<String> = ArrayList()
                        var deviceList: MutableList<Device> = ArrayList()
                        for (sheduleshare in schedule!!) {
                            if (sheduleshare["type"] != "schedule") {
                                var device = Device(0, sheduleshare["state"]!!.toBoolean(), sheduleshare["level"]!!.toInt(), sheduleshare["deviceName"]!!, "room", sheduleshare["deviceName"]!!, sheduleshare["index"]!!.toInt(), false)
                                for (d in allsharedDevice) {
                                    if (d.name == sheduleshare["deviceName"]) {
                                        device.type = d.loads[device.index].icon!!
                                        device.name = d.loads[device.index].name!!
                                        device.roomName = d.room!!
                                        device.dimmable = d.loads[device.index].dimmable!!
                                        var flag = false
                                        for (r in roomList) {
                                            if (r == d.room) {
                                                flag = true
                                                break
                                            }
                                        }
                                        if (!flag) {
                                            roomList.add(d.room!!)
                                        }
                                        deviceList.add(device)
                                        break
                                    }
                                }

                            } else {
                                sharedscheduleType = sheduleshare["stype"]!!
                                if (sharedscheduleType == "geo") {
                                    geoData.newGeoRadius = sheduleshare["radius"]!!.toFloat()
                                    geoData.newGeoLat = sheduleshare["latitude"]!!.toDouble()
                                    geoData.newGeolong = sheduleshare["longitude"]!!.toDouble()
                                    geoData.AutomationEnable = sheduleshare["automationEnable"]!!.toBoolean()
                                    geoData.triggerType = sheduleshare["triggeringType"]
                                    sharedscheduleStatus.add(geoData)
                                    sharedscheduleName = sheduleshare["name"]!!
                                    icon = sheduleshare["icon"]!!.toInt()
                                    sharedhome = sheduleshare["home"]
                                    sharedscheduleType = sheduleshare["stype"]!!
                                    sharedscheduleStartTime = ""
                                    sharedscheduleEndTime = ""
                                    sharedscheduleRoutine = ""

                                } else {
                                    geoData.AutomationEnable = sheduleshare["automationEnable"]!!.toBoolean()
                                    sharedscheduleStatus.add(geoData)
                                    sharedscheduleName = sheduleshare["name"]!!
                                    icon = sheduleshare["icon"]!!.toInt()
                                    sharedhome = sheduleshare["home"]
                                    val calendar = Calendar.getInstance(TimeZone.getTimeZone("UGT"), Locale.getDefault())
                                    val currentLocalTime = calendar.getTime()
                                    val date = SimpleDateFormat("Z")
                                    val localTime = date.format(currentLocalTime)
                                    var offsetTimeHours = localTime.substring(1, 3).toInt()
                                    var offsetTimeMinutes = localTime.substring(3).toInt()
                                    val time = sheduleshare["time"]!!
                                    val values = time.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                                    //var val1 = values[0].split("\\s".toRegex())
                                    val timeHours24 = values[0].toInt()
                                    val timeMinutes24 = values[1].toInt()
                                    // val timeHours24 =sheduleshare["time"]!!.split(":")[0].toInt()
                                    //val timeMinutes24 = sheduleshare["time"]!!.split(":")[1].toInt()
                                    var totalTime = timeHours24 * 60 + timeMinutes24
                                    var totalOffset = offsetTimeHours * 60 + offsetTimeMinutes
                                    var timeSet = totalTime + totalOffset
                                    if (timeSet >= 1440) {
                                        timeSet = timeSet - 1440
                                    }
                                    var currentHour = timeSet / 60
                                    var currentMinutes = timeSet % 60
                                    if (currentHour > 11) {
                                        currentHour = currentHour - 12
                                        if (currentHour == 0) currentHour = 12
                                        sharedscheduleStartTime = "$currentHour : $currentMinutes PM"
                                    } else {
                                        sharedscheduleStartTime = "$currentHour : $currentMinutes AM"
                                    }

                                    sharedscheduleType = sheduleshare["stype"]!!
                                    sharedscheduleEndTime = sheduleshare["endtime"]!!
                                    var routine = sheduleshare["routine"]!!
                                    val type = object : TypeToken<ArrayList<String>>() {}.type
                                    val gson = Gson()
                                    var routinedata: MutableList<String> = ArrayList()
                                    routinedata = gson.fromJson(routine, type)
                                    var days = arrayOf("Monday", "Sunday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")

                                    var map = HashMap<String, String>()
                                    for ((i, name) in routinedata.withIndex()) {
                                        map[days[i]] = name
                                    }
                                    sharedscheduleRoutine = gson.toJson(map)
                                }
                            }
                        }
                        for (r in roomList) {
                            var room = RoomModel()
                            room.name = r
                            for (d in deviceList) {
                                if (d.roomName == r) {
                                    room.deviceList.add(d)
                                }
                            }
                            rooms.add(room)
                        }
                        localSqlSchedule.insertSchedule(mDbSchedule!!, sharedscheduleName, icon, gson.toJson(roomList), gson.toJson(rooms), gson.toJson(sharedscheduleStatus), sharedscheduleStartTime!!, sharedscheduleType!!, sharedscheduleEndTime!!, sharedscheduleRoutine!!, sharedhome!!, sharedscheduleName, "create")

                    }
                }

                //update homes
                var newHomeList: MutableList<RoomModelJson> = ArrayList()
                var homesFlag = false
                if (rulesTable.homes != null) {
                    if (rulesTable.homes!!.size != 0) {
                        homesFlag = true
                    }
                }
                if (homesFlag) {
                    for (homes in rulesTable.homes!!) {
                        var home = RoomModelJson(homes["name"]!!, homes["type"]!!, homes["access"]!!, homes["bg"]!!, homes["icon"]!!.toInt())
                        newHomeList.add(home)
                    }
                    //update shared home
                    var sharedHomeflag = false
                    if (rulesTable.masterHomes != null) {
                        if (rulesTable.masterHomes!!.size != 0) {
                            sharedHomeflag = true
                        }
                    }
                    if (sharedHomeflag) {
                        for (sharedHome in rulesTable.masterHomes!!) {
                            var Sharedhome = RoomModelJson(sharedHome["name"]!!, sharedHome["type"]!!, sharedHome["access"]!!, sharedHome["bg"]!!, sharedHome["icon"]!!.toInt())
                            newHomeList.add(Sharedhome)
                        }
                    }
                    localSqlUtils.replaceHome(mDbUtils!!, "home", newHomeList)

                } else {
                    newHomeList.add(RoomModelJson("My Home", "home", "master", "0", 0))
                    newHomeList.add(RoomModelJson("Bedroom", "room", "default", "0", 1))
                    newHomeList.add(RoomModelJson("Dining Room", "room", "default", "1", 2))
                    newHomeList.add(RoomModelJson("Bathroom", "room", "default", "2", 3))
                    newHomeList.add(RoomModelJson("Living Room", "room", "default", "0", 0))

                    var sharedHomeflag = false
                    if (rulesTable.masterHomes != null) {
                        if (rulesTable.masterHomes!!.size != 0) {
                            sharedHomeflag = true
                        }
                    }
                    if (sharedHomeflag) {
                        for (sharedHome in rulesTable.masterHomes!!) {
                            var Sharedhome = RoomModelJson(sharedHome["name"]!!, sharedHome["type"]!!, sharedHome["access"]!!, sharedHome["bg"]!!, sharedHome["icon"]!!.toInt())
                            newHomeList.add(Sharedhome)
                        }
                    }


                    localSqlUtils.replaceHome(mDbUtils!!, "home", newHomeList)
                    var roomList: MutableList<MutableMap<String, String>> = ArrayList()
                    for (homes in newHomeList) {
                        var map = HashMap<String, String>()
                        map["bg"] = homes.bgUrl
                        map["name"] = homes.name
                        map["type"] = homes.type
                        map["access"] = homes.sharedHome
                        map["icon"] = homes.roomIcon.toString()
                        roomList.add(map)
                    }
                    rulesTable.homes = roomList
                    rulesTableDo.saveRulesTable(rulesTable)
                }

            }

            allDeviceList = localSqlDevices.getDeviceTable(mDb!!)
            IpListDevices = localSqlUtils.getIpList(mDbUtils!!, "device")
            for (d in allDeviceList) {
                var ip = IpModel()
                var flag = false
                for (i in IpListDevices) {
                    if (i.name == d.name) {
                        ip = i
                        ip.thing = d.thing
                        ip.owned = 0
                        ip.uiud = d.uiud
                        ip.aws = false
                        ip.local = false
                        ip.home = d.home
                        ip.room = d.room
                        IpListDevicesNew.add(ip)
                        flag = true
                    }
                }
                if (!flag) {
                    ip.name = d.name
                    ip.thing = d.thing
                    ip.owned = 0
                    ip.uiud = d.uiud
                    ip.aws = false
                    ip.local = false
                    ip.home = d.home
                    ip.room = d.room
                    IpListDevicesNew.add(ip)
                }

            }
            IpListDevices = IP.startDevices(IpListDevicesNew)
            localSqlUtils.replaceIpList(mDbUtils!!, "device", IpListDevices)
            // val sharingSuccess = userDynamoDb.updateSharedDevices(request, this)
            runOnUiThread {
                request.status = 1
                notificationAdapter!!.notifyDataSetChanged()
                progress_bar.visibility = View.GONE
                if (rulesTable.guest!!.size > 0) {
                    longSnackbar(findViewById(android.R.id.content), "${request.sharedHome!!.split("?")[0]} successfully added")
                    if (Constant.NOTIFICATION_COUNT > 0) Constant.NOTIFICATION_COUNT = -1

                } else {
                    longSnackbar(findViewById(android.R.id.content), "Something went wrong please try again")
                }
                var intent = Intent(this,DashboardActivity::class.java)
                intent.putExtra("SharedHome",request.sharedHome)
                startActivity(intent)
            }
        }
    }


    override fun onDeclineSelected(request: Notification) {

        progress_bar.visibility = View.VISIBLE
//        val sharedData = HashMap<String, String>()
//        var userType = false
//        thread {
//            val user = userDynamoDb.getUser()!!
//            if (user != null) {
//                userType = true
//            }
//
//            if (user.sharedAccess != null) {
//                for (invites in user.sharedAccess!!) {
//                    if (invites["Access"] == "invite") {
//                        sharedData["Home"] = invites["Home"]!!
//                        sharedData["Name"] = invites["Name"]!!
//                    }
//                }
//            }
//            if (userType) {
//                var name = sharedData["Name"]
//                var HomeName = sharedData["Home"]
//
//                var devices = localSqlDevices.getDevicesForHome(mDb!!, HomeName!!)
//                localSqlDevices.deleteHome(mDb!!, HomeName!!)
//                localSqlSceneDatabase.deleteHomeScenes(mDbScene!!, HomeName!!)
//                userDynamoDb.deleteGuestAccessFromMaster(name!!, HomeName!!, userType, user)
//                for (device in devices) {
//                    val deviceId = device.uiud?.substring(0, Math.min(device.uiud!!.length, 12))
//                    // userDynamoDb.deleteUserDevice(deviceId)
//                    deviceDynamoDb.deleteDevice(deviceId!!)
//
//                }
//            }
//            runOnUiThread {
//                request.status = 2
//                notificationAdapter!!.notifyDataSetChanged()
//                progress_bar.visibility = View.GONE
//                startActivity(intentFor<DashboardActivity>())
//            }
//        }
    }

}
