package com.wozart.aura.ui.splashscreen

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import com.amazonaws.mobile.client.AWSMobileClient
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.vision.barcode.Barcode
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wozart.aura.R
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.entity.service.AwsPubSub
import com.wozart.aura.entity.service.TcpServer
import com.wozart.aura.ui.createautomation.GeofenceService
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.ui.login.GoogleLoginActivity
import kotlinx.android.synthetic.main.activity_google_login.*
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import kotlin.concurrent.thread

/***************************************************************************
 * File Name :
 * Author : Aarth Tandel
 * Date of Creation : 15/05/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class SplashActivity : AppCompatActivity() {

    private val SPLASH_DISPLAY_LENGTH = 1000
    private lateinit var client: OkHttpClient

    companion object {
        private val LOG_TAG = SplashActivity::class.java.simpleName
    }


    private val isLoggedIn: Boolean
        get() {
            val account = GoogleSignIn.getLastSignedInAccount(this)
            val prefs = PreferenceManager.getDefaultSharedPreferences(this)
            val userId = prefs.getString("ID", "NULL")
            var email = prefs.getString("EMAIL", "NULL")
            var flag = true
            if (userId == "NULL" && account == null) flag = false
            return flag
        }

    private val isInternetWorking: Boolean
        get() {
            var success = false
            try {
                val url = URL("https://google.com")
                val connection = url.openConnection() as HttpURLConnection
                connection.connectTimeout = 10000
                connection.connect()
                success = connection.responseCode == 200
            } catch (e: IOException) {
                Log.d(LOG_TAG, "Check Internet connection")
//               Toast.makeText(this,"Check Internet connection",Toast.LENGTH_SHORT).show()
            }

            return success
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        client = OkHttpClient()
        val window = window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.statusBarColor = Color.BLACK

        startService(Intent(this, TcpServer::class.java))


        Thread(Runnable {
            if (isInternetWorking) {
                try {
                    AWSMobileClient.getInstance().initialize(this).execute()
                    Thread.sleep(1000)
                    startService(Intent(this, AwsPubSub::class.java))
                    startService(Intent(this, GeofenceService::class.java))
                } catch (e: Exception) {
                    Log.d(LOG_TAG, "Check Internet connection")
                    // Toast.makeText(this,"Check Internet connection",Toast.LENGTH_SHORT).show()
                }

            } else{
                runOnUiThread {
                    Toast.makeText(this,"Check Internet connection",Toast.LENGTH_SHORT).show()
                }
            }
        }).start()

            val prefs = PreferenceManager.getDefaultSharedPreferences(this)
            val tokens = prefs.getString("TOKEN", "NULL")
            Log.d("Splash_Screen",tokens)
            val userId = prefs.getString("ID", "NULL")
            val verified = prefs.getString("VERIFIED_EMAIL", "NULL")
            Handler().postDelayed({
                //if (!isInternetWorking) {
                    if (isLoggedIn) {
                        if (verified == "true") {
                            val mainIntent = Intent(this, DashboardActivity::class.java)
                            this.startActivity(mainIntent)
                            this.finish()
                        } else {
                            val mainIntent = Intent(this, GoogleLoginActivity::class.java)
                            mainIntent.putExtra("type", "Verify account")
                            this.startActivity(mainIntent)
                            this.finish()
                        }
                    } else {
                        val mainIntent = Intent(this, GoogleLoginActivity::class.java)
                        mainIntent.putExtra("type", "Login")
                        this.startActivity(mainIntent)
                        this.finish()
                    }
                //}
                    //else {
                    if (isLoggedIn) {
                        if (verified == "false") {
                            getWebservice(tokens)
                        } else {
                            val mainIntent = Intent(this, DashboardActivity::class.java)
                            this.startActivity(mainIntent)
                            this.finish()
                        }
                    } else {
                        val mainIntent = Intent(this, GoogleLoginActivity::class.java)
                        mainIntent.putExtra("type", "Login")
                        this.startActivity(mainIntent)
                        this.finish()
                    }
                //}
            }, SPLASH_DISPLAY_LENGTH.toLong())
       // }


//        Handler().postDelayed({
//            if (isLoggedIn) {
//                val mainIntent = Intent(this, DashboardActivity::class.java)
//                this.startActivity(mainIntent)
//                this.finish()
//            } else {
//                val mainIntent = Intent(this, GoogleLoginActivity::class.java)
//                this.startActivity(mainIntent)
//                this.finish()
//            }
//        }, SPLASH_DISPLAY_LENGTH.toLong())
    }

    private fun getWebservice(tokens: String?) {
        val request = Request.Builder().url("https://wozart.auth0.com/userinfo")
                .addHeader("authorization", "Bearer " + tokens!!)
                .build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                runOnUiThread {
                    //token.setText("Failure !");
                }
            }

            override fun onResponse(call: Call, response: Response) {

                thread {
                    try {
                        var personName = "User"
                        val user = JSONObject(response.body()!!.string())
                        if (user.isNull("given_name")) {
                            personName = user.getString("nickname")
                            //email login
                        } else {
                            val personFirstName = user.getString("given_name")
                            val personLastName = user.getString("family_name")
                            personName = "$personFirstName $personLastName"
                            //gmail login
                        }
                        val verified = user.getString("email_verified")
                        val personEmail = user.getString("email")
                        val personId_string = user.getString("sub")
                        val splitIndex = personId_string.indexOf("|")
                        val personId = personId_string.substring(splitIndex + 1)
                        val userId = "us-east-1:$personId"
                        if (verified == "true") {
                            registerUserRulesTable(userId, verified, personEmail, personName)
                            runOnUiThread {
                                val mainIntent = Intent(applicationContext, DashboardActivity::class.java)
                                startActivity(mainIntent)
                                finish()
                            }
                        } else {
                            runOnUiThread {
                                val mainIntent = Intent(applicationContext, GoogleLoginActivity::class.java)
                                mainIntent.putExtra("type", "Verify account")
                                startActivity(mainIntent)
                                finish()
                            }
                            //make a toast for verify email
                        }

                    } catch (ioe: IOException) {
                        Log.d("LOGIN", "Login : " + "Error during get body")
                    }
                }
            }
        })
    }

    private fun registerUserRulesTable(userId: String, verified: String, email: String, personName: String) {
        var rulesDynamoTable = RulesTableHandler()
        Thread(Runnable {
            rulesDynamoTable.updateUserVerification(userId, verified, email, personName)
        }).start()
    }
}

