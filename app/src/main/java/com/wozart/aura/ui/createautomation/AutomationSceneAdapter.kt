package com.wozart.aura.ui.createautomation


import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wozart.aura.R
import com.wozart.aura.R.anim.zoomin
import com.wozart.aura.R.anim.zoomout
import com.wozart.aura.aura.ui.dashboard.listener.OnOptionsListener
import com.wozart.aura.data.model.AutomationModel
import kotlinx.android.synthetic.main.item_automation_scene.view.*
import org.jetbrains.anko.design.longSnackbar
import java.util.HashMap

class AutomationSceneAdapter(val automationScene: ArrayList<AutomationModel>, val optionerListener: OnOptionsListener?, val listener: (AutomationModel, Boolean) -> Unit) : RecyclerView.Adapter<AutomationSceneAdapter.AutomationHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AutomationHolder {
        val inflatedView = LayoutInflater.from(parent?.context).inflate(R.layout.item_automation_scene, parent, false)
        return AutomationHolder(inflatedView)
    }

    override fun getItemCount(): Int {
        return automationScene.size

    }

    override fun onBindViewHolder(holder: AutomationHolder, position: Int) {

        holder?.bind(position, automationScene[position], optionerListener, listener)
        if(automationScene[position].Automationenable == true){
            holder.itemView.card_scene_auto.setCardBackgroundColor(holder.itemView.resources.getColor(R.color.white))
        }else{
            holder.itemView.card_scene_auto.setCardBackgroundColor(holder.itemView.resources.getColor(R.color.list_item_inactive))
        }
    }

    class AutomationHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(position: Int, automatScene: AutomationModel, optionerListener: OnOptionsListener?, listener: (AutomationModel, Boolean) -> Unit) = with(itemView) {
            val zoomin = AnimationUtils.loadAnimation(itemView?.context, R.anim.zoomin)
            val zoomout = AnimationUtils.loadAnimation(itemView?.context, R.anim.zoomout)

            var scheduleTypeBased = automatScene.type
            if (scheduleTypeBased == "geo") {
                itemView.scenesIconAuto.setImageResource(R.drawable.ic_arrow_icon_new)
                if (automatScene.title!!.length > 13) {
                    val ttl = automatScene.title!!.substring(0, 13)
                    itemView.scheduleTimes.text = ttl + ".."
                } else {
                    itemView.scheduleTimes.text = automatScene.title
                }
            } else {
                itemView.scheduleTimes.text = automatScene.endTime
                itemView.scheduleEndTimes.text = automatScene.endTime
                itemView.scheduleRoutines.visibility = View.VISIBLE
                itemView.scenesIconAuto.setImageResource(R.drawable.ic_clock_icon_new)
                val type = object : TypeToken<HashMap<String, Boolean>>() {}.type
                val gson = Gson()
                val routine: HashMap<String, Boolean>
                routine = gson.fromJson(automatScene.routine, type)

                changeTvBg(tvSunday, routine["Sunday"]!!)
                changeTvBg(tvMonday, routine["Monday"]!!)
                changeTvBg(tvTuesday, routine["Tuesday"]!!)
                changeTvBg(tvWednesday, routine["Wednesday"]!!)
                changeTvBg(tvThursday, routine["Thursday"]!!)
                changeTvBg(tvFriday, routine["Friday"]!!)
                changeTvBg(tvSaturday, routine["Saturday"]!!)
            }


            itemView.setOnClickListener {
                itemView.card_scene_auto.startAnimation(zoomin)
                itemView.card_scene_auto.startAnimation(zoomout)
                listener(automatScene, false)
                enableEffect(automatScene)
            }
            itemView.setOnLongClickListener {
                listener(automatScene, true)
                true
            }

        }

        fun enableEffect(automatScene: AutomationModel){
            if(automatScene.Automationenable == true){
                Toast.makeText(itemView.context,"Automation Enable...",Toast.LENGTH_SHORT).show()
                itemView.card_scene_auto.setCardBackgroundColor(itemView.resources.getColor(R.color.white))
            }else{
                Toast.makeText(itemView.context,"Automation Disable...",Toast.LENGTH_SHORT).show()
                itemView.card_scene_auto.setCardBackgroundColor(itemView.resources.getColor(R.color.list_item_inactive))
            }
        }

        private fun changeTvBg(textView: TextView, selected: Boolean) {
            //This is a way to change bg but based on business logic check logic {selectWeekDays -> Hashmap) can change}
            itemView?.let {
                if (selected) {
                    textView.setTextColor(ContextCompat.getColor(itemView.context, R.color.colorAccent))
                } else {

                    textView.setTextColor(ContextCompat.getColor(itemView.context, R.color.black))

                }
            }
        }
    }
}

