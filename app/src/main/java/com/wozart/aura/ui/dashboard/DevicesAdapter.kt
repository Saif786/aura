package com.wozart.aura.ui.dashboard

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.bumptech.glide.Glide
import com.wozart.aura.R
import com.wozart.aura.aura.ui.dashboard.listener.OnDeviceOptionsListener
import com.wozart.aura.aura.utilities.Utils.getIconDrawable
import kotlinx.android.synthetic.main.item_device.view.*


/***
 * Created by Kiran on 14-03-2018.
 */
class DevicesAdapter(val deviceList: ArrayList<Device>, val room: String, val optionerListener: OnDeviceOptionsListener?, val listener: (Device, Boolean) -> Unit) : RecyclerView.Adapter<DevicesAdapter.DeviceHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceHolder {
        val inflatedView = LayoutInflater.from(parent?.context).inflate(R.layout.item_device, parent, false)
        return DeviceHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: DeviceHolder, position: Int) {
        holder?.bind(deviceList[position], room, optionerListener, listener)
    }

    override fun getItemCount(): Int {
        return deviceList.size
    }

    class DeviceHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(device: Device, room: String, optionsListener: OnDeviceOptionsListener?, listener: (Device, Boolean) -> Unit) = with(itemView) {

            val zoomin = AnimationUtils.loadAnimation(itemView.context, R.anim.zoomin)
            val zoomout = AnimationUtils.loadAnimation(itemView.context, R.anim.zoomout)

            itemView.deviceName.text = device.name
            itemView.roomName.text = device.roomName
            if (device.status == "off") {
                itemView.deviceStatus.visibility = View.INVISIBLE
                itemView.errorIcon.visibility = View.INVISIBLE
                //errorIcon
                itemView.error.visibility = View.VISIBLE
                itemView.error.setTextColor(resources.getColor(R.color.Red))
                itemView.error.text = "No Response"
                itemView.deviceCard.setCardBackgroundColor(resources.getColor(R.color.list_item_inactive))
            } else if ((device.status == "update") and (device.devicePresent == true)) {
                itemView.deviceStatus.visibility = View.INVISIBLE
                itemView.errorIcon.visibility = View.INVISIBLE
                //errorIcon
                itemView.error.visibility = View.VISIBLE
                itemView.error.setTextColor(resources.getColor(R.color.black))
                itemView.error.text = "Updating.."
                itemView.deviceCard.setCardBackgroundColor(resources.getColor(R.color.list_item_inactive))
            } else if ((device.status == "update") and (device.devicePresent == false)) {
                itemView.deviceStatus.visibility = View.INVISIBLE
                itemView.errorIcon.visibility = View.INVISIBLE
                //errorIcon
                itemView.error.visibility = View.VISIBLE
                itemView.error.setTextColor(resources.getColor(R.color.Red))
                itemView.error.text = "No Response"
                itemView.deviceCard.setCardBackgroundColor(resources.getColor(R.color.list_item_inactive))
            } else {
                if (device.status == "cloud") {
                    itemView.errorIcon.visibility = View.VISIBLE
                } else {
                    itemView.errorIcon.visibility = View.INVISIBLE
                }
                if (device.isTurnOn) {
                    if (device.dimmable == false) {
                        itemView.deviceStatus.text = "ON"
                        itemView.error.visibility = View.INVISIBLE
                        itemView.deviceStatus.visibility = View.VISIBLE
                        itemView.deviceCard.setCardBackgroundColor(resources.getColor(R.color.white))
                    } else {
                        itemView.deviceStatus.text = device.dimVal.toString() + "%"
                        itemView.error.visibility = View.INVISIBLE
                        itemView.deviceStatus.visibility = View.VISIBLE
                        itemView.deviceCard.setCardBackgroundColor(resources.getColor(R.color.white))
                    }
                    // itemView.errorIcon.visibility = View.GONE
                }
                if (!device.isTurnOn) {
                    itemView.deviceCard.setCardBackgroundColor(resources.getColor(R.color.list_item_inactive))
                    itemView.deviceStatus.text = "Off"
                    itemView.deviceStatus.visibility = View.VISIBLE
                    itemView.error.visibility = View.INVISIBLE
                    // itemView.errorIcon.visibility = View.GONE
                }
            }
            if (device.name == "Fan" && device.isTurnOn) {
                Glide.with(this).load(getIconDrawable(device.type, device.isTurnOn))
                        .into(itemView.deiceIcon)
            } else if (device.name == "Exhaust Fan" && device.isTurnOn) {
                Glide.with(this).load(getIconDrawable(device.type, device.isTurnOn))
                        .into(itemView.deiceIcon)
            } else {
                itemView.deiceIcon.setImageResource(getIconDrawable(device.type, device.isTurnOn))
            }

            itemView.setOnClickListener {
                itemView.deviceCard.startAnimation(zoomin)
                itemView.deviceCard.startAnimation(zoomout)

                listener(device, false)
            }
            itemView.setOnLongClickListener {
                listener(device, true)
                true
            }

        }
    }
}