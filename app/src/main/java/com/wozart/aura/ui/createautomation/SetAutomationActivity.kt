package com.wozart.aura.aura.ui.createautomation

import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import com.wozart.aura.R
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.model.AutomationModel
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.selectdevices.SelectDevicesFragment
import com.wozart.aura.ui.base.BaseAbstractActivity
import com.wozart.aura.ui.createautomation.OnFragmentInteractionListener
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.activity_create_automation.*


class SetAutomationActivity : BaseAbstractActivity(), OnFragmentInteractionListener {

    private var automationNameOld: String? = null
    private var automstionSceneType: String? = null
    private var automationName : String ?= null
    private var scheduletype : String ?= null
    private var selectedIcon : Int ?= 0
    private var customGeoRadius: Float ?= 0f
    private var automationEnable : Boolean = false
    var geoTriggeringType : String ?= null
    private var mDbUtils: SQLiteDatabase? = null
    private val localSqlUtils = UtilsTable()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_automation)
        val dbUtils = UtilsDbHelper(this)
        mDbUtils = dbUtils.writableDatabase

        var listRoom: MutableList<RoomModelJson> = ArrayList()
        listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")

        for(x in listRoom){
            if(x.name == Constant.HOME) {

                Utils.setDrawable(this, containerAutomation, x.bgUrl.toInt())

            }

        }
       // Utils.setDrawable(this,containerAutomation)

        var intent = intent
        automstionSceneType = intent.getStringExtra("automationSceneType")
        automationNameOld = intent.getStringExtra("automationNameOld")
        automationName = intent.getStringExtra("automationName")
        scheduletype = intent.getStringExtra("scheduleBasedType")
        selectedIcon = intent.getIntExtra("icon",0)
        customGeoRadius = intent.getFloatExtra("customRadius",0f)
        automationEnable = intent.getBooleanExtra("scheduleEnable",false)
        geoTriggeringType = intent.getStringExtra("triggerType")

        navigateToFragment(SelectDevicesFragment(),"",true,true)

    }

    override fun onRoomBtnClicked() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onHomeBtnClicked() {
       onBackPressed()
    }

    fun getAutomationOldName() : String?{
        return automationNameOld
    }

    fun getAutomationType() : String?{
        return automstionSceneType
    }

    fun getAutomationName() : String? {
        return automationName
    }

    fun getScheduleType() : String?{
        return scheduletype
    }

    fun getIcon() : Int?{
        return selectedIcon
    }

    fun getAddedGeoRadius():Float?{
        return customGeoRadius
    }
    fun getAutomationEnable():Boolean{
        return automationEnable
    }

    fun getTriggeringType():String?{
        return geoTriggeringType
    }

}
