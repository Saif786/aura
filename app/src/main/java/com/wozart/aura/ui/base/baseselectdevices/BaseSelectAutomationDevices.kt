package com.wozart.aura.ui.base.baseselectdevices

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.wozart.aura.R
import com.wozart.aura.aura.ui.createautomation.CreateAutomationActivity
import com.wozart.aura.aura.ui.createautomation.SetAutomationActivity
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.ScheduleTable
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scheduling.ScheduleDbHelper
import com.wozart.aura.ui.createautomation.AutomationScene
import com.wozart.aura.ui.createautomation.OnFragmentInteractionListener
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.Device
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.fragment_select_devices.*
import kotlinx.android.synthetic.main.layout_header.*

/***************************************************************************
 * File Name :
 * Author : Saif
 * Date of Creation : 06/09/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Saif - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0.5
 * ____________________________________________________________________________
 *
 *****************************************************************************/
abstract class BaseSelectAutomationDevices : Fragment() {

    private var sceneNameCheck : String ?= null
    protected var mListener: OnFragmentInteractionListener? = null
    protected var adapter: SelectRoomAdapter = SelectRoomAdapter()
    var roomsList: MutableList<RoomModel> = ArrayList()
    private val localSqlDatabaseSchedule = ScheduleTable()
    private var mDbSchedule: SQLiteDatabase? = null
    private val localSqlDatabase = DeviceTable()
    private var mDb: SQLiteDatabase? = null
    private var automationScene = AutomationScene()
    var automationSceneType:String? = null

    protected var automationSceneNameOld:String? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_automation_select_device, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(context is CreateAutomationActivity){
            automationScene= (context as CreateAutomationActivity).getAutomationScene()
            automationSceneNameOld = (context as CreateAutomationActivity).getAutomationSceneName()
            automationSceneType = (context as CreateAutomationActivity).getAutomationSceneType()
        }
        if(automationSceneType == "edit"){
            sceneNameCheck = automationScene.name

        }
        if(context is SetAutomationActivity){
            automationSceneNameOld = (context as SetAutomationActivity).getAutomationOldName()

        }
        init()
    }

    fun init() {
        var tvNext = view!!.findViewById<TextView>(R.id.tvNext)
        tvNext.text = getString(R.string.text_finish)
        tvTitle.text = getTitle()
        tvNext.setOnClickListener { openNextScreen() }
        tvNext.setTextColor(Color.WHITE)
        roomsList.clear()
        home.setOnClickListener { mListener?.onHomeBtnClicked() }
        listRooms.layoutManager = LinearLayoutManager(activity)
        listRooms.adapter = adapter
        adapter.init(roomsList)
        val visibility = if (showSceneInputs()) View.VISIBLE else View.INVISIBLE
//        inputSceneName.visibility = visibility
//        tvSelectSceneIcon.visibility = visibility
//        listScenes.visibility = visibility
//        if (visibility == View.VISIBLE) {
//            listScenes.adapter = iconsAdapter
//            listScenes.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
//        }
        populateData()
    }

    abstract fun showSceneInputs(): Boolean

    abstract fun getTitle(): String

    protected fun getSelectedRoomDeviceData(): MutableList<RoomModel> {
        val rooms: MutableList<RoomModel> = ArrayList()
        for (room in this.roomsList) {
            val deviceList = ArrayList(room.deviceList.filter { it.isSelected })  //for device isSelected.
            if (room.deviceList.size > 0) {
                val roomModel = room.copy()
                roomModel.deviceList = deviceList
                rooms.add(roomModel)
            }
        }
        return rooms
    }

    private fun populateData() {
        deviceList()
    }


  /*  private fun deviceList() {
        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase
        val dummyRooms = localSqlDatabase.getRooms(mDb!!, Constant.HOME!!)
        for (x in dummyRooms) {
            val room = RoomModel()
            room.name = x.roomName
            roomsList.add(room)
        }
        if (!roomsList.isEmpty()) {
            for (x in roomsList) {
                val allDevicesList = ArrayList<Device>()
                val devices = localSqlDatabase.getDevicesForRoom(mDb!!,  Constant.HOME!!, x.name!!)
                for (device in devices) {
                    for (i in 0..3) {
                        allDevicesList.add(Device(device.loads[i].icon!!, false,100, device.loads[i].name!!, localSqlDatabase.getRoomForDevice(mDb!!, device.name), device.name, device.loads[i].index!!,device.loads[i].dimmable!!))
                    }
                }
                x.deviceList = allDevicesList
            }
        }
    }*/

    private fun deviceList() {
        var sharedDataList : MutableList<RoomModelJson> = ArrayList()

        var dbSchedule = ScheduleDbHelper(context!!)
        mDbSchedule = dbSchedule.writableDatabase
        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase
        val dummyRooms = localSqlDatabase.getRooms(mDb!!, Constant.HOME!!)
        for (x in dummyRooms) {
            val room = RoomModel()
            room.name = x.roomName
            roomsList.add(room)
        }
        var scheduleSelected = localSqlDatabaseSchedule.getAutomationScheduleScene(mDbSchedule!!,automationSceneNameOld!!, Constant.HOME!!)


        if (!roomsList.isEmpty()) {

            for (x in roomsList) {
                val allDevicesList = ArrayList<Device>()
                val devices = localSqlDatabase.getDevicesForRoom(mDb!!,  Constant.HOME!!, x.name!!)



                for (device in devices) {
                    for (i in 0..3) {
                        var flag = false
                        for(room in scheduleSelected.load){
                            for(r in room.deviceList){
                                if((r.deviceName == device.name) and (r.index == i)){
                                    if(r.isSelected){
                                        flag = true
                                    }
                                    break
                                }
                            }
                        }
                        allDevicesList.add(Device(device.loads[i].icon!!, flag,100, device.loads[i].name!!, localSqlDatabase.getRoomForDevice(mDb!!, device.name), device.name, device.loads[i].index!!,device.loads[i].dimmable!!))
                    }
                }
                x.deviceList = allDevicesList
            }
        }
    }

    abstract fun openNextScreen()


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnAutomationListInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

}