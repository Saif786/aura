package com.wozart.aura.ui.automationlist

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.database.Observable
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.util.Consumer
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import aura.wozart.com.aura.entity.network.Nsd
import com.google.gson.Gson
import com.wozart.aura.R
import com.wozart.aura.data.model.AutomationModel
import com.wozart.aura.aura.ui.automationlist.OnAutomationListInteractionListener
import com.wozart.aura.aura.ui.createautomation.CreateAutomationActivity
import com.wozart.aura.aura.ui.dashboard.listener.OnOptionsListener
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.ScheduleTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraComplete
import com.wozart.aura.entity.network.ConnectTask
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scheduling.ScheduleDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.createautomation.AutomationScene
import com.wozart.aura.ui.createautomation.AutomationSceneAdapter
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.dialogue_edit_home.*
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.support.v4.intentFor
import org.jetbrains.anko.support.v4.onUiThread
import org.jetbrains.anko.support.v4.toast
import kotlin.concurrent.thread
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.xml.datatype.DatatypeConstants.SECONDS
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


/***************************************************************************
 * File Name :
 * Author : Saif
 * Date of Creation : 12/09/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0.6
 * ____________________________________________________________________________
 *
 *****************************************************************************/


class AutomationListFragment : Fragment(), OnOptionsListener, ConnectTask.TcpMessageReceiver {

    private val localSqlDatabase = DeviceTable()
    private val localSqlDatabaseSchedule = ScheduleTable()
    private var mDbSchedule: SQLiteDatabase? = null
    private var mDb: SQLiteDatabase? = null
    private val localSqlUtils = UtilsTable()
    private var mDbUtils: SQLiteDatabase? = null
    private var mDBAutomation: SQLiteDatabase? = null
    private var mListener: OnAutomationListInteractionListener? = null
    var automationScene = ArrayList<AutomationScene>()
    var automationSceneList = ArrayList<AutomationModel>()
    private lateinit var automationAdapter: AutomationSceneAdapter
    private lateinit var nsd: Nsd
    private var automationNameOld: String? = null
    val scheduleDynamoDb = RulesTableHandler()
    var userType = true
    var automationEnableTest: Boolean = false
    lateinit var progressBar: ProgressBar

    companion object {
        fun newInstance(): AutomationListFragment {
            return AutomationListFragment()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        var dbSchedule = ScheduleDbHelper(context!!)
        mDbSchedule = dbSchedule.writableDatabase
        val dbUtils = UtilsDbHelper(context!!)
        mDbUtils = dbUtils.writableDatabase

        var rootView = inflater.inflate(R.layout.fragment_automation_list, container, false)

        return rootView
    }

    override fun onOptionsClicked(view: View) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var btnCreateNew = view.findViewById<ImageView>(R.id.btnCreateNew)
        progressBar = view.findViewById(R.id.progressBar4)
        btnCreateNew.visibility = View.VISIBLE
        var listRoom: MutableList<RoomModelJson> = ArrayList()

        listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")

        for (x in listRoom) {
            if (x.name == Constant.HOME) {
                //val contentView = activity!!.findViewById(R.id.scheduleContainer) as LinearLayout
                //Utils.setDrawable(context!!, contentView, x.bgUrl.toInt())
                if (x.sharedHome == "guest") {
                    userType = false
                    break
                }
            }

        }

        if (userType) {
            btnCreateNew.visibility = View.VISIBLE
            btnCreateNew.setOnClickListener {
                //mListener?.onCreateAutomationBtnClicked()
                val intent = Intent(activity, CreateAutomationActivity::class.java)
                intent.putExtra("automationNameType", "create")
                intent.putExtra("input_name", "new")
                startActivity(intent)
            }
        } else {
            btnCreateNew.visibility = View.INVISIBLE
        }

        val dbAutomation = ScheduleDbHelper(context!!)
        mDBAutomation = dbAutomation.writableDatabase
        var listAutomations = view.findViewById(R.id.listAutomations) as RecyclerView

        listAutomations.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        init()
        listAutomations.adapter = automationAdapter
    }

    private fun init() {
        val gson = Gson()

        automationScene = localSqlDatabaseSchedule.getAutomationScene(mDBAutomation!!, Constant.HOME!!)
        for (automation in automationScene) {
            if (automation.type == "geo") {
                automationSceneList.add(AutomationModel(automation.name, automation.icon, automation.room, automation.property[0].AutomationEnable, automation.time, automation.type, automation.endTime, automation.routine!!, automation.property[0].triggerType!!))
            } else {
                automationSceneList.add(AutomationModel(automation.name, automation.icon, automation.room, automation.property[0].AutomationEnable, automation.time, automation.type, automation.endTime, automation.routine!!, ""))
            }
        }

        automationAdapter = AutomationSceneAdapter(automationSceneList, this) { automationScene1: AutomationModel, isLongPressed: Boolean ->
            if (isLongPressed) {
                if (userType) {
                    dialogEditSchedule(automationScene1)
                } else {
                    toast("Guest cannot edit schedule")
                }
            } else {
                for (automatioScenes in automationScene) {
                    progressBar.visibility = View.VISIBLE

                    if (automatioScenes.name == automationScene1.title) {
                        for (enableAutomationCheck in automatioScenes.property) {
                            if (enableAutomationCheck.AutomationEnable == true) {
                                enableAutomationCheck.AutomationEnable = false
                                // automationSceneList[0].Automationenable = false
                                automationScene1.Automationenable = false

                                automationEnableTest = enableAutomationCheck.AutomationEnable
                            } else {
                                enableAutomationCheck.AutomationEnable = true
                                //automationSceneList[0].Automationenable = true
                                automationScene1.Automationenable = true
                                automationEnableTest = enableAutomationCheck.AutomationEnable
                            }

                        }

                        localSqlDatabaseSchedule.updatechedule(mDbSchedule!!, automatioScenes.name!!, automatioScenes.icon, gson.toJson(automatioScenes.room), gson.toJson(automatioScenes.load), gson.toJson(automatioScenes.property), automatioScenes.time!!, automatioScenes.type!!, automatioScenes.endTime!!, automatioScenes.routine!!, Constant.HOME!!, automatioScenes.name!!)

                        thread {
                            var home = Constant.HOME
                            var userId = Constant.IDENTITY_ID

                            scheduleDynamoDb.updateUserSchedule(userId!!, home!!, automatioScenes, "edit", automatioScenes.name!!)

                            context!!.runOnUiThread {
                                automationAdapter.notifyDataSetChanged()
                                progressBar.visibility = View.GONE
                            }

                        }

                        break
                    }

                }
                onUiThread {
                    automationAdapter.notifyDataSetChanged()
                }

            }

        }
    }


    private fun dialogEditSchedule(automationScene: AutomationModel) {

        var dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialogue_edit_home)
        dialog.tv_title.text = "Edit Schedule"

        dialog.btn_edit.setOnClickListener {

            val intent = Intent(activity, CreateAutomationActivity::class.java)
            intent.putExtra("automationNameType", "edit")
            intent.putExtra("input_name", automationScene.title)
            intent.putExtra("automationIconUrl", automationScene.iconUrl)
            startActivity(intent)
        }


        dialog.btn_delete_home.setOnClickListener {
            automationNameOld = automationScene.title.toString()
            localSqlDatabaseSchedule.deleteAutomationScene(mDbSchedule!!, automationNameOld!!, Constant.HOME!!)
            deleteAutomationScene(automationNameOld!!)
            thread {
                scheduleDynamoDb.deleteUserSchedule(Constant.IDENTITY_ID!!, Constant.HOME!!, automationNameOld!!)
            }
            val intent = Intent(activity, DashboardActivity::class.java)
            intent.putExtra("TAB_SET", Constant.AUTOMATION_TAB)
            startActivity(intent)
            toast("Automation Scene Deleted")
        }
        dialog.show()

    }


    fun deleteAutomationScene(automationScene: String) {
        var roomsList: MutableList<RoomModel> = ArrayList()
        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase
        val dummyRooms = localSqlDatabase.getRooms(mDb!!, Constant.HOME!!)
        for (x in dummyRooms) {
            val room = RoomModel()
            room.name = x.roomName
            roomsList.add(room)
        }
        if (!roomsList.isEmpty()) {
            for (x in roomsList) {
                val devices = localSqlDatabase.getDevicesForRoom(mDb!!, Constant.HOME!!, x.name!!)
            }

        }
    }

    override fun onTcpMessageReceived(message: String) {
        updateScheduleScene()
    }

    fun updateScheduleScene() {
        for (scheduleScene in automationSceneList) {
            var sceneTurnedState = true
            var sceneDeviceList = ArrayList<AuraComplete>()
            for (sRoom in scheduleScene.rooms) {
                for (device in sceneDeviceList) {
                    if (sRoom.name == device.room) {
                        for (sDevice in sRoom.deviceList) {
                            if (sDevice.isTurnOn != device.loads[sDevice.index].status) {
                                sceneTurnedState = false
                                break
                            }
                        }
                        if (!sceneTurnedState) {
                            break
                        }
                    }
                }
                if (!sceneTurnedState) {
                    break
                }
            }
            scheduleScene.Automationenable = sceneTurnedState
        }
        automationAdapter.notifyDataSetChanged()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnAutomationListInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnAutomationListInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

}

