package com.wozart.aura.ui.createscene

import android.database.sqlite.SQLiteDatabase
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wozart.aura.R
import com.wozart.aura.ui.base.baseselectdevices.BaseSelectDevicesFragment
import kotlinx.android.synthetic.main.fragment_select_devices.*
import org.jetbrains.anko.support.v4.longToast
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.layout_header.*
import org.jetbrains.anko.newTask
import org.jetbrains.anko.support.v4.intentFor
import org.jetbrains.anko.support.v4.toast
import kotlin.concurrent.thread


class SelectDeviceForSceneFragment : BaseSelectDevicesFragment() {
    private val localSqlScene = SceneTable()
    private var mDbScene: SQLiteDatabase? = null

    private val localSqlDatabase = DeviceTable()
    private var mDb: SQLiteDatabase? = null

    var sceneNameOld: String? = null
    var sceneNameType: String? = null
    lateinit var tvTitle: TextView
    lateinit var home : ImageView
    lateinit var tvNext : TextView

    override fun showSceneInputs(): Boolean {
        return true
    }

    override fun getTitle(): String {
        if (sceneNameType == "create")
            return getString(R.string.title_create_scene)
        else
            return getString(R.string.title_edit_scene)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_select_devices, container, false)
        val sceneDynamoDb = RulesTableHandler()
        val btnSceneDelete = view.findViewById<Button>(R.id.btnSceneDelete)
        tvNext = view.findViewById(R.id.tvNext)
        home = view.findViewById(R.id.home)
        tvTitle = view.findViewById(R.id.tvTitle)
        //initialize()
        if (context is CreateSceneActivity) {
            sceneNameOld = (context as CreateSceneActivity).getSceneName()
            sceneNameType = (context as CreateSceneActivity).getSceneType()
        }
        if (sceneNameType == "edit") {
            val inputSceneName = view!!.findViewById<EditText>(R.id.inputSceneName)
            showSceneInputs()
            btnSceneDelete.visibility = View.VISIBLE
            inputSceneName.setText(sceneNameOld)
        }

        btnSceneDelete.setOnClickListener {
            if (sceneNameType == "create") {
                btnSceneDelete.visibility = View.INVISIBLE
                longToast("Scene Not created for Deletion..")
            } else {
                btnSceneDelete.visibility = View.VISIBLE
                localSqlScene.deleteScene(mDbScene!!, sceneNameOld!!, Constant.HOME!!)
                deleteSceneFromLoads(sceneNameOld!!)
                thread {
                    sceneDynamoDb.deleteUserScene(Constant.IDENTITY_ID!!, Constant.HOME!!, sceneNameOld!!)
                }
                startActivity(intentFor<DashboardActivity>().newTask())
                toast("Scene Deleted")
            }

        }
        val dbScene = SceneDbHelper(context!!)
        mDbScene = dbScene.writableDatabase
        return view
    }

    fun onClick(v: View) {
    }

    fun deleteSceneFromLoads(scene: String) {
        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase
        val dummyRooms = localSqlDatabase.getRooms(mDb!!, Constant.HOME!!)
        for (x in dummyRooms) {
            val room = RoomModel()
            room.name = x.roomName
            roomsList.add(room)
        }
        if (!roomsList.isEmpty()) {
            for (x in roomsList) {
                val devices = localSqlDatabase.getDevicesForRoom(mDb!!, Constant.HOME!!, x.name!!)
                //NEWSCENES delete all scenes
            }
        }
    }

    override fun openNextScreen() {
        var len: Int = 0
        val selectedRooms = getSelectedRoomDeviceData()
        val sceneName = inputSceneName.text.toString()
        val sceneIcon = getIcon()
        for (item in selectedRooms) {
            len += item.deviceList.size
        }
        if (len == 0) {
            longToast("Please Select Loads..")
        } else {
            if (sceneName.isEmpty()) {
                longToast("Please Enter Scene name..")
            } else {
                if (selectedRooms.size == 0) {
                    longToast("Please Add devices to create Scene ..")
                } else {

                    mListener?.navigateToFragment(SetActionsForSceneFragment.newInstance(selectedRooms, sceneName, sceneNameOld!!, sceneIcon, sceneNameType!!)) //pass selected devices with rooms data
                }

            }
        }
    }

    fun initialize() {
        //main_layout.setBackgroundColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
        // tvSelectLoadIcon.setTextColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
        tvTitle.setTextColor(ContextCompat.getColor(activity!!.baseContext, R.color.white))
        home.setColorFilter(ContextCompat.getColor(activity!!.baseContext, R.color.black), PorterDuff.Mode.SRC_ATOP)
        tvNext.setTextColor(ContextCompat.getColor(activity!!.baseContext, R.color.white))
    }
}
