package com.wozart.aura.ui.adapter


import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wozart.aura.R
import com.wozart.aura.data.model.Notification
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.utilities.DialogListener
import kotlinx.android.synthetic.main.item_notification.view.*

class NotificationAdapter(private val activity: Activity, notifications: MutableList<Notification>, onAcceptSelected : OnAcceptListener, onDeclineSelected : OnDeclineListener) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    private var notifications: MutableList<Notification> = notifications
    var listener : OnAcceptListener = onAcceptSelected
    var declineListener : OnDeclineListener = onDeclineSelected

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_notification, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var notification: Notification = notifications[position]
        holder?.itemView?.tv_notificationTitle?.text = notification.notificationTitle
        holder?.itemView?.tv_message?.text = notification.notificationMessage
        //holder?.itemView?.tv_timeStamp?.text=notification.timeStamp
        if (notification.status != 0) {
            holder.itemView.tv_status.visibility = View.VISIBLE
            holder.itemView.btn_accept.visibility = View.GONE
            holder.itemView.btn_reject.visibility = View.GONE
            holder.itemView.tv_notificationTitle.visibility = View.GONE
            holder.itemView.tv_message.visibility = View.GONE
            if (notification.status == 1) {
                holder.itemView.tv_status.visibility = View.VISIBLE
                holder.itemView.card_notification.visibility = View.INVISIBLE
                holder.itemView.tv_status.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorAccent))
                holder.itemView.tv_status.text = "Accepted "+ notification.notificationTitle
            } else {
                holder.itemView.layoutAccept.visibility = View.GONE
                holder.itemView.tv_status.visibility = View.INVISIBLE
                holder.itemView.card_notification.visibility = View.VISIBLE
                holder.itemView.notification_text.visibility = View.VISIBLE
                holder.itemView.notification_text.text = notification.notificationMessage
            }
        } else {
            holder.itemView.tv_notificationTitle.visibility = View.VISIBLE
            holder.itemView.tv_message.visibility = View.VISIBLE
            holder.itemView.btn_accept.visibility = View.VISIBLE
            holder.itemView.btn_reject.visibility = View.GONE
            holder.itemView.tv_status.visibility = View.GONE
            holder.itemView.notification_text.visibility = View.GONE
            holder.itemView.card_notification.visibility = View.GONE
        }

        holder.itemView.btn_accept.setOnClickListener {
            Utils.showCustomDialog(holder.itemView.context, holder.itemView.context.getString(R.string.txt_accept), holder.itemView.context.getString(R.string.dialog_accept_notification), R.layout.dialog_layout, object : DialogListener {
                override fun onOkClicked() {
                    notifyItemChanged(position)
                    listener.onAcceptSelected(notification)
                }

                override fun onCancelClicked() {
                }
            })

        }
        holder.itemView.btn_reject.setOnClickListener {
            Utils.showCustomDialog(holder.itemView.context, holder.itemView.context.getString(R.string.txt_reject), holder.itemView.context.getString(R.string.dialog_reject_notification), R.layout.dialog_layout, object : DialogListener {
                override fun onOkClicked() {
                    //notifications[position].status = 2
                    notifyItemChanged(position)
                    declineListener.onDeclineSelected(notification)
                }

                override fun onCancelClicked() {

                }
            })
        }
    }


    override fun getItemCount(): Int {
        return if (notifications == null) 0 else notifications!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    interface OnAcceptListener{
        fun onAcceptSelected(request : Notification)
    }

    interface OnDeclineListener{
        fun onDeclineSelected(request : Notification)
    }


}
