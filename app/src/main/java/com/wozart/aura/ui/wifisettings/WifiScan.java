package com.wozart.aura.ui.wifisettings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.wozart.aura.R;
import com.wozart.aura.espProvision.Provision;

import java.util.ArrayList;
import java.util.List;

public class WifiScan extends AppCompatActivity {

    private WifiManager wifiManager;
    private ListView listView;
    private Button buttonScan;
    private boolean isDeviceConnected;
    private int size = 0;
    private List<ScanResult> results;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter adapter;
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_scan_);
        isDeviceConnected = false;
        buttonScan = findViewById(R.id.scanBtn);
        progressBar = findViewById(R.id.wifi_progress_indicator);
        progressBar.setVisibility(View.VISIBLE);
        buttonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanWifi();
            }
        });
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        TextView titleText = (TextView) findViewById(R.id.textTitle);
        ImageView back = (ImageView) findViewById(R.id.back);
        setSupportActionBar(toolbar);

        listView = findViewById(R.id.wifiList);
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (!wifiManager.isWifiEnabled()) {
            Toast.makeText(this, "WiFi is disabled, Enable WiFi..", Toast.LENGTH_LONG).show();
            wifiManager.setWifiEnabled(true);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrayList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                isDeviceConnected = false;
                progressBar.setVisibility(View.VISIBLE);
                Log.d("WiFiScanList", "Device to be connected -" + arrayList.get(pos));
                callProvision(arrayList.get(pos));
            }

        });
        scanWifi();
    }

    private void callProvision(String ssid){
        Log.e("WiFiScanList", "Selected AP -" + ssid);
        finish();
        Intent launchProvisionInstructions = new Intent(getApplicationContext(), ProvisionActivity.class);
        launchProvisionInstructions.putExtras(getIntent());
        launchProvisionInstructions.putExtra(Provision.PROVISIONING_WIFI_SSID, ssid);
        startActivity(launchProvisionInstructions);

    }

    private void scanWifi() {
        arrayList.clear();
        registerReceiver(wifiReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        wifiManager.startScan();
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, "Scanning WiFi ...", Toast.LENGTH_SHORT).show();
    }

    BroadcastReceiver wifiReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            results = wifiManager.getScanResults();
            unregisterReceiver(this);

            for (ScanResult scanResult : results) {
               // arrayList.add(scanResult.SSID + " - " + scanResult.capabilities);
                arrayList.add(scanResult.SSID);
                adapter.notifyDataSetChanged();
            }
        };
    };
}
