package com.wozart.aura.ui.dashboard

import android.content.*
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.os.IBinder
import android.preference.PreferenceManager
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wozart.aura.R
import com.wozart.aura.data.model.AutomationModel
import com.wozart.aura.ui.automationlist.AutomationListFragment
import com.wozart.aura.aura.ui.automationlist.OnAutomationListInteractionListener
import com.wozart.aura.aura.ui.createautomation.CreateAutomationActivity
import com.wozart.aura.ui.dashboard.home.HomeFragment
import com.wozart.aura.ui.dashboard.more.MoreFragment
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.device.IpHandler
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.model.IpModel
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.data.sqlLite.ScheduleTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraSwitchLoad
import com.wozart.aura.entity.model.aura.DeviceTableModel
import com.wozart.aura.entity.service.AwsPubSub
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.entity.sql.scheduling.ScheduleDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.base.BaseAbstractActivity
import com.wozart.aura.ui.createautomation.GeoModal
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.room.RoomFragment
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.ui.helper.BottomNavigationViewHelper
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.Constant.Companion.AUTOMATION_TAB
import com.wozart.aura.utilities.Constant.Companion.HOME_TAB
import com.wozart.aura.utilities.Constant.Companion.MORE_TAB
import com.wozart.aura.utilities.Constant.Companion.ROOMS_TAB
import kotlinx.android.synthetic.main.activity_dashboard.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.concurrent.thread
import android.support.v4.view.ViewPager
import android.view.MenuItem
import android.widget.TextView
import com.wozart.aura.ui.adapter.PagerAdapter
import kotlinx.android.synthetic.main.notification_badge.*


class DashboardActivity : BaseAbstractActivity(), OnAutomationListInteractionListener {


    private var mDbDevice: SQLiteDatabase? = null
    private var mSelectedTab = HOME_TAB
    private var mCurrentTab = -1
    private var toast: Toast? = null
    private var lastBackPressTime: Long = 0
    var currentFragment: Fragment? = null
    internal var mBounded: Boolean = false
    private var awsPubSub: AwsPubSub? = null
    private val localSqlDatabase = DeviceTable()
    var IpListDevices: MutableList<IpModel> = ArrayList()
    var IpListDevicesNew: MutableList<IpModel> = ArrayList()
    val localSqlUtils = UtilsTable()
    var mDbUtils: SQLiteDatabase? = null
    var selectedFragment: Fragment? = null
    private var IP = IpHandler()
    var allDeviceList = java.util.ArrayList<DeviceTableModel>()
    private var mDbScene: SQLiteDatabase? = null
    private var mDbSchedule: SQLiteDatabase? = null
    private val localSqlScene = SceneTable()
    var localSqlSchedule = ScheduleTable()
    var sharedDataList: MutableList<RoomModelJson> = ArrayList()
    var tab_selected: Int = 0
    var shareHomeGuest: String? = null
    lateinit var pagerAdater: PagerAdapter
    var syncFlag = true


    companion object {
        private var syncAws = true

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        //mSelectedTab = HOME_TAB
        val bottomNavigationMenuView = bottomNavigationView.getChildAt(0) as BottomNavigationMenuView
        val v = bottomNavigationMenuView.getChildAt(3)
        val itemView = v as BottomNavigationItemView
        val badge = LayoutInflater.from(this)
                .inflate(R.layout.notification_badge, bottomNavigationMenuView, false)

        var intent = intent
        tab_selected = intent.getIntExtra("TAB_SET", 0)
        shareHomeGuest = intent.getStringExtra("SharedHome")

        if (tab_selected == 0) {
            mSelectedTab = HOME_TAB
        } else {
            mSelectedTab = tab_selected
        }


        val dbHelper = DeviceDbHelper(this)
        mDbDevice = dbHelper.writableDatabase

        val dbHelperScedule = ScheduleDbHelper(this)
        mDbSchedule = dbHelperScedule.writableDatabase

        val dbHelperScene = SceneDbHelper(this)
        mDbScene = dbHelperScene.writableDatabase

        //Local broadcost listerners
        LocalBroadcastManager.getInstance(this@DashboardActivity).registerReceiver(
                onAwsMessageReceived, IntentFilter("AwsShadow"))


        //initialise ip models
        val dbUtils = UtilsDbHelper(this)
        mDbUtils = dbUtils.writableDatabase
        allDeviceList = localSqlDatabase.getDeviceTable(mDbDevice!!)
        IpListDevices = localSqlUtils.getIpList(mDbUtils!!, "device")
        var listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")

        for (d in allDeviceList) {
            var ip = IpModel()
            var flag = false
            for (i in IpListDevices) {
                if (i.name == d.name) {
                    ip = i
                    ip.thing = d.thing
                    ip.owned = 0
                    ip.uiud = d.uiud
                    ip.aws = false
                    ip.local = false
                    ip.home = d.home
                    ip.room = d.room
                    IpListDevicesNew.add(ip)
                    flag = true
                    break
                }
            }
            if (!flag) {
                ip.name = d.name
                ip.thing = d.thing
                ip.owned = 0
                ip.uiud = d.uiud
                ip.aws = false
                ip.local = false
                ip.home = d.home
                ip.room = d.room
                IpListDevicesNew.add(ip)
            }

        }
        IpListDevices = IP.startDevices(IpListDevicesNew)
        localSqlUtils.replaceIpList(mDbUtils!!, "device", IpListDevices)

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val userName = prefs.getString("USERNAME", "NO USER")
        val email = prefs.getString("EMAIL", "NO USER")
        val userId = prefs.getString("ID", "NO USER")
        val home = prefs.getString("HOME", "NO HOME")
        Constant.EMAIL = email
        Constant.IDENTITY_ID = userId
        Constant.USERNAME = userName
        if (shareHomeGuest != null) {
            Constant.HOME = shareHomeGuest
        } else Constant.HOME = home

        if (listRoom.size > 0) {
            for (x in listRoom) {
                if (x.type == "home") {
                    if (x.name == Constant.HOME) {

                        Utils.setDrawable(this, activity_main, x.bgUrl.toInt())
                    }
                }
            }
        } else {
            Utils.setDrawable(this, activity_main, 0)
        }
        syncDevices()

        thread {
            var rulesTableDo = RulesTableHandler()
            val user = rulesTableDo.getUser()
            runOnUiThread {
                if (user != null) {
                    if (user!!.guest != null) {
                        Constant.NOTIFICATION_COUNT = 1

                    } else {

                    }
                }
            }
        }

        itemView.addView(badge)
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView)
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->

            when (item.itemId) {
                R.id.action_item1 -> {
                    //viewPager.currentItem = 0
                    mSelectedTab = HOME_TAB
                    selectedFragment = HomeFragment.newInstance()

                }
                R.id.action_item2 -> {
                    mSelectedTab = ROOMS_TAB
                    //viewPager.currentItem = 1
                    selectedFragment = RoomFragment.newInstance()
                }
                R.id.action_item3 -> {
                    selectedFragment = AutomationListFragment.newInstance()
                    mSelectedTab = AUTOMATION_TAB
                    //viewPager.currentItem = 2
                }
                R.id.action_item4 -> {
                    selectedFragment = MoreFragment.newInstance()
                    mSelectedTab = MORE_TAB
                    //viewPager.currentItem = 3
                }
            }
            if (selectedFragment != null && mCurrentTab != mSelectedTab) {
                mCurrentTab = mSelectedTab
                currentFragment = selectedFragment
                IpListDevices = IP.getIpDevices()
                localSqlUtils.replaceIpList(mDbUtils!!, "device", IpListDevices)
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.frame_layout, selectedFragment!!)
                transaction.commit()

            }
            true
        }
        performTabClick(mSelectedTab)

    }

    fun setupViewPager(viewPager: ViewPager) {
        pagerAdater = PagerAdapter(supportFragmentManager)
        pagerAdater.addFragment(HomeFragment.newInstance())
        pagerAdater.addFragment(RoomFragment.newInstance())
        pagerAdater.addFragment(AutomationListFragment())
        pagerAdater.addFragment(MoreFragment.newInstance())
        //viewPager.adapter = pagerAdater
    }


    /**
     * AWS IoT Subscribe to shadow broadcast receiver
     */

    override fun onStart() {
        super.onStart()
        val mIntent = Intent(this, AwsPubSub::class.java)
        bindService(mIntent, mConnection, Context.BIND_AUTO_CREATE)
    }

    private var mConnection: ServiceConnection = object : ServiceConnection {

        override fun onServiceDisconnected(name: ComponentName) {
            mBounded = false
            awsPubSub = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            mBounded = true
            val mLocalBinder = service as AwsPubSub.LocalAwsBinder
            awsPubSub = mLocalBinder.getServerInstance()
        }
    }

    override fun onStop() {

        IpListDevices = IP.getIpDevices()
        localSqlUtils.replaceIpList(mDbUtils!!, "device", IpListDevices)

        if (mBounded) {
            unbindService(mConnection)
            mBounded = false
        }

        super.onStop()
    }


    private val onAwsMessageReceived = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            val shadow = intent.getStringExtra("data")
            val segments = shadow.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (shadow == "Connected") {
                val things = localSqlDatabase.getThing(mDbDevice!!)
                Thread.sleep(1000)
                for (x in things) {
                    // awsPubSub?.AwsGetPublish(x)
                    awsPubSub?.AwsSubscribe(x)
                    val led = (System.currentTimeMillis() / 1000)
                    val msg = "{\"state\":{\"desired\": {\"led\": $led}}}"
                    awsPubSub?.AWSPublish(x, msg)
                    awsPubSub?.AwsGet(x)
                    Thread.sleep(500)
                }
            } else if (shadow == "Reconnect") {

            }
        }
    }

    fun pusblishDataToShadow(thing: String, data: String): Boolean {
        if (awsPubSub != null) {
            return awsPubSub!!.AWSPublish(thing, data)
        } else {
            return false
        }
    }

    fun getThingDataShadow(thing: String){
        if(awsPubSub != null){
           awsPubSub!!.AwsGet(thing)
        }

    }


    private fun syncDevices() {
        val rulesTableDo = RulesTableHandler()
        val homeData = localSqlUtils.getHomeData(mDbUtils!!, "home")

        if ((homeData == null) or (homeData.size == 0)) {

            syncCloud()

        } else {
            syncLocal()
//            thread{
//                val rulesTable = rulesTableDo.getRulesTable()
//                val deviceTable = localSqlDatabase.getDeviceTable(mDbDevice!!)
//                if (rulesTable!!.userId != "ERROR") {
//                    //updating devices
//                    var devicePresentFlag = false
//                    if (rulesTable.devices != null) {
//                        if (rulesTable.devices!!.size != 0) {
//                            devicePresentFlag = true
//                        }
//                    }
//                    if (devicePresentFlag) {
//                        if (deviceTable.size != 0) {
//                            if (rulesTable.devices!!.size == deviceTable.size) {
//                                syncLocal()
//                            } else {
//                                if (rulesTable.devices!!.size > deviceTable.size) {
//                                    syncCloud()
//                                } else {
//                                    syncLocal()
//                                }
//                            }
//                        }
//
//                    }
//                    if (!devicePresentFlag) {
//                        syncLocal()
//                    }
//                }
//            }

        }

    }

    private fun syncCloud() {
        var rulesTableDo = RulesTableHandler()
        var homeData = localSqlUtils.getHomeData(mDbUtils!!, "home")
        val gson = Gson()
        var newHomeList: MutableList<RoomModelJson> = ArrayList()
        thread {
            var rulesTable = rulesTableDo.getRulesTable()
            if (rulesTable!!.userId != "ERROR") {
                //updating devices
                var deviceFlag = false
                if (rulesTable.devices != null) {
                    if (rulesTable.devices!!.size != 0) {
                        deviceFlag = true
                    }
                }
                if (deviceFlag) {
                    for (d in rulesTable.devices!!) {
                        var home = d["home"]
                        var room = d["room"]
                        var uiud = d["uiud"]
                        var name = d["name"]
                        var thing = d["thing"]
                        var allLoads = rulesTable.loads
                        var localLoads: MutableList<AuraSwitchLoad> = ArrayList()
                        for (loads in allLoads!!) {
                            var flag = false
                            for (load in loads) {
                                if ((load["type"] == "device") and (load["device"] == d["id"])) {
                                    flag = true
                                    break
                                }
                            }
                            if (flag) {
                                for (load in loads) {
                                    if (load["type"] != "device") {
                                        var localLoad = AuraSwitchLoad()
                                        localLoad.index = load["index"]!!.toInt()
                                        localLoad.icon = load["icon"]!!.toInt()
                                        localLoad.name = load["name"]
                                        localLoad.favourite = load["favourite"]!!.toBoolean()
                                        localLoad.dimmable = load["dimmable"]!!.toBoolean()
                                        localLoads.add(localLoad)
                                    }
                                }
                                break
                            }
                        }
                        localSqlDatabase.insertDevice(mDbDevice!!, home!!, room!!, uiud!!, name!!, gson.toJson(localLoads), thing!!)
                    }
                }
                var allDevices = localSqlDatabase.getDeviceTable(mDbDevice!!)

                //updating Sharing devices

                var masterDeviceflag = false
                if (rulesTable.masterDevices != null) {
                    if (rulesTable.masterDevices!!.size != 0) {
                        masterDeviceflag = true
                    }
                }
                if (masterDeviceflag) {
                    for (shareDevice in rulesTable.masterDevices!!) {
                        var sharedHome = shareDevice["home"]
                        var sharedroom = shareDevice["room"]
                        var uiud = shareDevice["uiud"]
                        var sharedname = shareDevice["name"]
                        var sharedthing = shareDevice["thing"]
                        var shareLoads = rulesTable.masterLoads
                        var localLoads: MutableList<AuraSwitchLoad> = ArrayList()
                        for (sharedloads in shareLoads!!) {
                            var flag = false
                            for (load in sharedloads) {
                                if ((load["type"] == "device") and (load["device"] == shareDevice["id"])) {
                                    flag = true
                                    break
                                }
                            }
                            if (flag) {
                                for (load in sharedloads) {
                                    if (load["type"] != "device") {
                                        var localLoad = AuraSwitchLoad()
                                        localLoad.index = load["index"]!!.toInt()
                                        localLoad.icon = load["icon"]!!.toInt()
                                        localLoad.name = load["name"]
                                        localLoad.favourite = load["favourite"]!!.toBoolean()
                                        localLoad.dimmable = load["dimmable"]!!.toBoolean()
                                        localLoads.add(localLoad)
                                    }
                                }
                                break
                            }
                        }
                        localSqlDatabase.insertDevice(mDbDevice!!, sharedHome!!, sharedroom!!, uiud!!, sharedname!!, gson.toJson(localLoads), sharedthing!!)
                    }
                }
                var allsharedDevice = localSqlDatabase.getDeviceTable(mDbDevice!!)

                //updating scenes
                var sceneFlag = false
                if (rulesTable.scenes != null) {
                    if (rulesTable.scenes!!.size != 0) {
                        sceneFlag = true
                    }
                }
                if (sceneFlag) {
                    var allScenes = rulesTable.scenes
                    for (scenes in allScenes!!) {
                        var rooms: ArrayList<RoomModel> = ArrayList()
                        var keys = scenes.keys
                        var pair = ""
                        for (k in keys) {
                            pair = k
                        }
                        var scene = scenes[pair]
                        var sceneName = ""
                        var icon = 0
                        var home: String? = null

                        //extract all room list
                        var roomList: MutableList<String> = ArrayList()
                        var deviceList: MutableList<Device> = ArrayList()
                        for (s in scene!!) {
                            if (s["type"] != "scene") {
                                var device = Device(0, s["state"]!!.toBoolean(), s["level"]!!.toInt(), s["deviceName"]!!, "room", s["deviceName"]!!, s["index"]!!.toInt(), false)
                                for (d in allDevices) {
                                    if (d.name == s["deviceName"]) {
                                        device.type = d.loads[device.index].icon!!
                                        device.name = d.loads[device.index].name!!
                                        device.roomName = d.room!!
                                        device.dimmable = d.loads[device.index].dimmable!!
                                        var flag = false
                                        for (r in roomList) {
                                            if (r == d.room) {
                                                flag = true
                                                break
                                            }
                                        }
                                        if (!flag) {
                                            roomList.add(d.room!!)
                                        }
                                        deviceList.add(device)
                                        break
                                    }
                                }

                            } else {
                                sceneName = s["name"]!!
                                icon = s["icon"]!!.toInt()
                                home = s["home"]!!
                            }
                        }
                        for (r in roomList) {
                            var room = RoomModel()
                            room.name = r
                            for (d in deviceList) {
                                if (d.roomName == r) {
                                    room.deviceList.add(d)
                                }
                            }
                            rooms.add(room)
                        }
                        localSqlScene.insertScene(mDbScene!!, sceneName, gson.toJson(rooms), gson.toJson(roomList), home!!, icon, sceneName, "create")
                    }
                }

                //update sharing Scene
                var sharingSceneFlag = false
                if (rulesTable.masterScenes != null) {
                    if (rulesTable.masterScenes!!.size != 0) {
                        sharingSceneFlag = true
                    }
                }
                if (sharingSceneFlag) {
                    var allShareScenes = rulesTable.masterScenes
                    for (sharescenes in allShareScenes!!) {
                        var rooms: ArrayList<RoomModel> = ArrayList()
                        var keys = sharescenes.keys
                        var pair = ""
                        for (k in keys) {
                            pair = k
                        }
                        var sharedscene = sharescenes[pair]
                        var sharedsceneName = ""
                        var icon = 0
                        var sharedhome = Constant.HOME

                        //extract all room list
                        var roomList: MutableList<String> = ArrayList()
                        var deviceList: MutableList<Device> = ArrayList()
                        for (s in sharedscene!!) {
                            if (s["type"] != "scene") {
                                var device = Device(0, s["state"]!!.toBoolean(), s["level"]!!.toInt(), s["deviceName"]!!, "room", s["deviceName"]!!, s["index"]!!.toInt(), false)
                                for (d in allsharedDevice) {
                                    if (d.name == s["deviceName"]) {
                                        device.type = d.loads[device.index].icon!!
                                        device.name = d.loads[device.index].name!!
                                        device.roomName = d.room!!
                                        device.dimmable = d.loads[device.index].dimmable!!
                                        var flag = false
                                        for (r in roomList) {
                                            if (r == d.room) {
                                                flag = true
                                                break
                                            }
                                        }
                                        if (!flag) {
                                            roomList.add(d.room!!)
                                        }
                                        deviceList.add(device)
                                        break
                                    }
                                }

                            } else {
                                sharedsceneName = s["name"]!!
                                icon = s["icon"]!!.toInt()
                                sharedhome = s["home"]!!
                            }
                        }
                        for (r in roomList) {
                            var room = RoomModel()
                            room.name = r
                            for (d in deviceList) {
                                if (d.roomName == r) {
                                    room.deviceList.add(d)
                                }
                            }
                            rooms.add(room)
                        }
                        localSqlScene.insertScene(mDbScene!!, sharedsceneName, gson.toJson(rooms), gson.toJson(roomList), sharedhome!!, icon, sharedsceneName, "create")
                    }
                }

                //update schedules
                var scheduleFlag = false
                if (rulesTable.schedules != null) {
                    if (rulesTable.schedules!!.size != 0) {
                        scheduleFlag = true
                    }
                }
                if (scheduleFlag) {
                    var allSchedules = rulesTable.schedules
                    for (schedules in allSchedules!!) {
                        var rooms: ArrayList<RoomModel> = ArrayList()
                        var keys = schedules.keys
                        var pair = ""
                        for (k in keys) {
                            pair = k
                        }
                        var schedule = schedules[pair]
                        var scheduleName = ""
                        var icon = 0
                        var home: String? = null
                        var scheduleStatus: MutableList<GeoModal> = ArrayList()
                        var geoData = GeoModal()
                        var scheduleStartTime: String? = null
                        var scheduleType: String? = null
                        var scheduleEndTime: String? = null
                        var scheduleRoutine: String? = null
                        var roomList: MutableList<String> = ArrayList()
                        var deviceList: MutableList<Device> = ArrayList()
                        for (s in schedule!!) {
                            if (s["type"] != "schedule") {
                                var device = Device(0, s["state"]!!.toBoolean(), s["level"]!!.toInt(), s["deviceName"]!!, "room", s["deviceName"]!!, s["index"]!!.toInt(), false)
                                for (d in allDevices) {
                                    if (d.name == s["deviceName"]) {
                                        device.type = d.loads[device.index].icon!!
                                        device.name = d.loads[device.index].name!!
                                        device.roomName = d.room!!
                                        device.dimmable = d.loads[device.index].dimmable!!
                                        var flag = false
                                        for (r in roomList) {
                                            if (r == d.room) {
                                                flag = true
                                                break
                                            }
                                        }
                                        if (!flag) {
                                            roomList.add(d.room!!)
                                        }
                                        deviceList.add(device)
                                        break
                                    }
                                }

                            } else {
                                scheduleType = s["stype"]!!
                                if (scheduleType == "geo") {
                                    geoData.newGeoRadius = s["radius"]!!.toFloat()
                                    geoData.newGeoLat = s["latitude"]!!.toDouble()
                                    geoData.newGeolong = s["longitude"]!!.toDouble()
                                    geoData.AutomationEnable = s["automationEnable"]!!.toBoolean()
                                    geoData.triggerType = s["triggeringType"]
                                    scheduleStatus.add(geoData)
                                    scheduleName = s["name"]!!
                                    icon = s["icon"]!!.toInt()
                                    // scheduleStatus = s["status"]
                                    home = s["home"]
                                    scheduleType = s["stype"]!!
                                    scheduleStartTime = ""
                                    scheduleEndTime = ""
                                    scheduleRoutine = ""
                                } else {
                                    geoData.AutomationEnable = s["automationEnable"]!!.toBoolean()
                                    scheduleStatus.add(geoData)
                                    scheduleName = s["name"]!!
                                    icon = s["icon"]!!.toInt()
                                    //scheduleStatus = s["status"]!!
                                    home = s["home"]
                                    scheduleType = s["stype"]!!
                                    val calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault())
                                    val currentLocalTime = calendar.getTime()
                                    val date = SimpleDateFormat("Z")
                                    val localTime = date.format(currentLocalTime)
                                    var offsetTimeHours = localTime.substring(1, 3).toInt()
                                    var offsetTimeMinutes = localTime.substring(3).toInt()
                                    val time = s["time"]!!
                                    val values = time.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                                    val timeHours24 = values[0].toInt()
                                    val timeMinutes24 = values[1].toInt()
                                    //val timeHours24 =s["time"]!!.split(":")[0].toInt()
                                    // val timeMinutes24 = s["time"]!!.split(":")[1].toInt()
                                    var totalTime = timeHours24 * 60 + timeMinutes24
                                    var totalOffset = offsetTimeHours * 60 + offsetTimeMinutes
                                    var timeSet = totalTime + totalOffset
                                    if (timeSet >= 1440) {
                                        timeSet = timeSet - 1440
                                    }
                                    var currentHour = timeSet / 60
                                    var currentMinutes = timeSet % 60
                                    if (currentHour > 11) {
                                        currentHour = currentHour - 12
                                        if (currentHour == 0) currentHour = 12
                                        scheduleStartTime = "$currentHour : $currentMinutes PM"
                                    } else {
                                        scheduleStartTime = "$currentHour : $currentMinutes AM"
                                    }

                                    scheduleEndTime = s["endtime"]!!
                                    var routine = s["routine"]!!
                                    val type = object : TypeToken<ArrayList<String>>() {}.type
                                    val gson = Gson()
                                    var routinedata: MutableList<String> = ArrayList()
                                    routinedata = gson.fromJson(routine, type)
                                    var days = arrayOf("Monday", "Sunday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")

                                    var map = HashMap<String, String>()
                                    for ((i, name) in routinedata.withIndex()) {
                                        map[days[i]] = name
                                    }
                                    scheduleRoutine = gson.toJson(map)
                                }

                            }
                        }
                        for (r in roomList) {
                            var room = RoomModel()
                            room.name = r
                            for (d in deviceList) {
                                if (d.roomName == r) {
                                    room.deviceList.add(d)
                                }
                            }
                            rooms.add(room)
                        }
                        localSqlSchedule.insertSchedule(mDbSchedule!!, scheduleName, icon, gson.toJson(roomList), gson.toJson(rooms), gson.toJson(scheduleStatus), scheduleStartTime!!, scheduleType!!, scheduleEndTime!!, scheduleRoutine!!, home!!, scheduleName, "create")
                    }
                }

                //update sharing master schedule
                var sharingSchedulingFlag = false
                if (rulesTable.masterSchedules != null) {
                    if (rulesTable.masterSchedules!!.size != 0) {
                        sharingSchedulingFlag = true
                    }
                }
                if (sharingSchedulingFlag) {
                    var allshareSchedule = rulesTable.masterSchedules
                    for (sharSchedule in allshareSchedule!!) {
                        var rooms: ArrayList<RoomModel> = ArrayList()
                        var keys = sharSchedule.keys
                        var pair = ""
                        for (k in keys) {
                            pair = k
                        }
                        var schedule = sharSchedule[pair]
                        var sharedscheduleName = ""
                        var icon = 0
                        var sharedhome: String? = null
                        var sharedscheduleStatus: MutableList<GeoModal> = ArrayList()
                        var geoData = GeoModal()
                        var sharedscheduleStartTime: String? = null
                        var sharedscheduleType: String? = null
                        var sharedscheduleEndTime: String? = null
                        var sharedscheduleRoutine: String? = null
                        var roomList: MutableList<String> = ArrayList()
                        var deviceList: MutableList<Device> = ArrayList()
                        for (sheduleshare in schedule!!) {
                            if (sheduleshare["type"] != "schedule") {
                                var device = Device(0, sheduleshare["state"]!!.toBoolean(), sheduleshare["level"]!!.toInt(), sheduleshare["deviceName"]!!, "room", sheduleshare["deviceName"]!!, sheduleshare["index"]!!.toInt(), false)
                                for (d in allsharedDevice) {
                                    if (d.name == sheduleshare["deviceName"]) {
                                        device.type = d.loads[device.index].icon!!
                                        device.name = d.loads[device.index].name!!
                                        device.roomName = d.room!!
                                        device.dimmable = d.loads[device.index].dimmable!!
                                        var flag = false
                                        for (r in roomList) {
                                            if (r == d.room) {
                                                flag = true
                                                break
                                            }
                                        }
                                        if (!flag) {
                                            roomList.add(d.room!!)
                                        }
                                        deviceList.add(device)
                                        break
                                    }
                                }

                            } else {
                                sharedscheduleType = sheduleshare["stype"]!!
                                if (sharedscheduleType == "geo") {
                                    geoData.newGeoRadius = sheduleshare["radius"]!!.toFloat()
                                    geoData.newGeoLat = sheduleshare["latitude"]!!.toDouble()
                                    geoData.newGeolong = sheduleshare["longitude"]!!.toDouble()
                                    geoData.AutomationEnable = sheduleshare["automationEnable"]!!.toBoolean()
                                    geoData.triggerType = sheduleshare["triggeringType"]
                                    sharedscheduleStatus.add(geoData)
                                    sharedscheduleName = sheduleshare["name"]!!
                                    icon = sheduleshare["icon"]!!.toInt()
                                    sharedhome = sheduleshare["home"]
                                    sharedscheduleType = sheduleshare["stype"]!!
                                    sharedscheduleStartTime = ""
                                    sharedscheduleEndTime = ""
                                    sharedscheduleRoutine = ""

                                } else {
                                    geoData.AutomationEnable = sheduleshare["automationEnable"]!!.toBoolean()
                                    sharedscheduleStatus.add(geoData)
                                    sharedscheduleName = sheduleshare["name"]!!
                                    icon = sheduleshare["icon"]!!.toInt()
                                    sharedhome = sheduleshare["home"]
                                    val calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault())
                                    val currentLocalTime = calendar.getTime()
                                    val date = SimpleDateFormat("Z")
                                    val localTime = date.format(currentLocalTime)
                                    var offsetTimeHours = localTime.substring(1, 3).toInt()
                                    var offsetTimeMinutes = localTime.substring(3).toInt()
                                    val time = sheduleshare["time"]!!
                                    val values = time.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                                    //var val1 = values[0].split("\\s".toRegex())
                                    val timeHours24 = values[0].toInt()
                                    val timeMinutes24 = values[1].toInt()
                                    // val timeHours24 =sheduleshare["time"]!!.split(":")[0].toInt()
                                    //val timeMinutes24 = sheduleshare["time"]!!.split(":")[1].toInt()
                                    var totalTime = timeHours24 * 60 + timeMinutes24
                                    var totalOffset = offsetTimeHours * 60 + offsetTimeMinutes
                                    var timeSet = totalTime + totalOffset
                                    if (timeSet >= 1440) {
                                        timeSet = timeSet - 1440
                                    }
                                    var currentHour = timeSet / 60
                                    var currentMinutes = timeSet % 60
                                    if (currentHour > 11) {
                                        currentHour = currentHour - 12
                                        if (currentHour == 0) currentHour = 12
                                        sharedscheduleStartTime = "$currentHour : $currentMinutes PM"
                                    } else {
                                        sharedscheduleStartTime = "$currentHour : $currentMinutes AM"
                                    }

                                    sharedscheduleType = sheduleshare["stype"]!!
                                    sharedscheduleEndTime = sheduleshare["endtime"]!!
                                    var routine = sheduleshare["routine"]!!
                                    val type = object : TypeToken<ArrayList<String>>() {}.type
                                    val gson = Gson()
                                    var routinedata: MutableList<String> = ArrayList()
                                    routinedata = gson.fromJson(routine, type)
                                    var days = arrayOf("Monday", "Sunday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")

                                    var map = HashMap<String, String>()
                                    for ((i, name) in routinedata.withIndex()) {
                                        map[days[i]] = name
                                    }
                                    sharedscheduleRoutine = gson.toJson(map)
                                }
                            }
                        }
                        for (r in roomList) {
                            var room = RoomModel()
                            room.name = r
                            for (d in deviceList) {
                                if (d.roomName == r) {
                                    room.deviceList.add(d)
                                }
                            }
                            rooms.add(room)
                        }
                        localSqlSchedule.insertSchedule(mDbSchedule!!, sharedscheduleName, icon, gson.toJson(roomList), gson.toJson(rooms), gson.toJson(sharedscheduleStatus), sharedscheduleStartTime!!, sharedscheduleType!!, sharedscheduleEndTime!!, sharedscheduleRoutine!!, sharedhome!!, sharedscheduleName, "create")

                    }
                }

//                    localSqlUtils.replaceHome(mDbUtils!!, "home", newHomeList)

                //update homes
                var homesFlag = false
                if (rulesTable.homes != null) {
                    if (rulesTable.homes!!.size != 0) {
                        homesFlag = true
                    }
                }
                if (homesFlag) {
                    for (homes in rulesTable.homes!!) {
                        var home = RoomModelJson(homes["name"]!!, homes["type"]!!, homes["access"]!!, homes["bg"]!!, homes["icon"]!!.toInt())
                        newHomeList.add(home)
                    }
                    //update shared home
                    var sharedHomeflag = false
                    if (rulesTable.masterHomes != null) {
                        if (rulesTable.masterHomes!!.size != 0) {
                            sharedHomeflag = true
                        }
                    }
                    if (sharedHomeflag) {
                        for (sharedHome in rulesTable.masterHomes!!) {
                            var Sharedhome = RoomModelJson(sharedHome["name"]!!, sharedHome["type"]!!, sharedHome["access"]!!, sharedHome["bg"]!!, sharedHome["icon"]!!.toInt())
                            newHomeList.add(Sharedhome)
                        }
                    }
                    localSqlUtils.replaceHome(mDbUtils!!, "home", newHomeList)

                } else {
                    newHomeList.add(RoomModelJson("My Home", "home", "master", "0", 0))
                    newHomeList.add(RoomModelJson("Bedroom", "room", "default", "0", 1))
                    //newHomeList.add(RoomModelJson("Kitchen", "room", "default", "1", 1))
                    newHomeList.add(RoomModelJson("Dining Room", "room", "default", "1", 2))
                    newHomeList.add(RoomModelJson("Bathroom", "room", "default", "2", 3))
                    newHomeList.add(RoomModelJson("Living Room", "room", "default", "0", 0))

                    var sharedHomeflag = false
                    if (rulesTable.masterHomes != null) {
                        if (rulesTable.masterHomes!!.size != 0) {
                            sharedHomeflag = true
                        }
                    }
                    if (sharedHomeflag) {
                        for (sharedHome in rulesTable.masterHomes!!) {
                            var Sharedhome = RoomModelJson(sharedHome["name"]!!, sharedHome["type"]!!, sharedHome["access"]!!, sharedHome["bg"]!!, sharedHome["icon"]!!.toInt())
                            newHomeList.add(Sharedhome)
                        }
                    }


                    localSqlUtils.replaceHome(mDbUtils!!, "home", newHomeList)
                    var roomList: MutableList<MutableMap<String, String>> = ArrayList()
                    for (homes in newHomeList) {
                        var map = HashMap<String, String>()
                        map["bg"] = homes.bgUrl
                        map["name"] = homes.name
                        map["type"] = homes.type
                        map["access"] = homes.sharedHome
                        map["icon"] = homes.roomIcon.toString()
                        roomList.add(map)
                    }
                    rulesTable.homes = roomList
                    rulesTableDo.saveRulesTable(rulesTable)
                }


            } else {
                newHomeList.add(RoomModelJson("My Home", "home", "master", "0", 0))
                newHomeList.add(RoomModelJson("Bedroom", "room", "default", "0", 1))
                // newHomeList.add(RoomModelJson("Kitchen", "room", "default", "1", 1))
                newHomeList.add(RoomModelJson("Dining Room", "room", "default", "1", 2))
                newHomeList.add(RoomModelJson("Bathroom", "room", "default", "2", 3))
                newHomeList.add(RoomModelJson("Living Room", "room", "default", "0", 0))

                localSqlUtils.replaceHome(mDbUtils!!, "home", newHomeList)
                var roomList: MutableList<MutableMap<String, String>> = ArrayList()
                for (homes in newHomeList) {
                    var map = HashMap<String, String>()
                    map["bg"] = homes.bgUrl
                    map["name"] = homes.name
                    map["type"] = homes.type
                    map["access"] = homes.sharedHome
                    map["icon"] = homes.roomIcon.toString()
                    roomList.add(map)
                }
                val prefs = PreferenceManager.getDefaultSharedPreferences(this)
                val userName = prefs.getString("USERNAME", "defaultStringIfNothingFound")
                val email = prefs.getString("EMAIL", "defaultStringIfNothingFound")
                val userId = prefs.getString("ID", "NULL")
                val verified = prefs.getString("VERIFIED_EMAIL", "NULL")
                rulesTable.homes = roomList
                if (verified == "NULL") {
                    rulesTable.verified = "true"
                } else {
                    rulesTable.verified = verified
                }

                rulesTable.userId = userId
                rulesTable.name = userName
                rulesTable.email = email
                rulesTableDo.saveRulesTable(rulesTable)
            }

            allDeviceList = localSqlDatabase.getDeviceTable(mDbDevice!!)
            IpListDevices = localSqlUtils.getIpList(mDbUtils!!, "device")
            for (d in allDeviceList) {
                var ip = IpModel()
                var flag = false
                for (i in IpListDevices) {
                    if (i.name == d.name) {
                        ip = i
                        ip.thing = d.thing
                        ip.owned = 0
                        ip.uiud = d.uiud
                        ip.aws = false
                        ip.local = false
                        ip.home = d.home
                        ip.room = d.room
                        IpListDevicesNew.add(ip)
                        flag = true
                    }
                }
                if (!flag) {
                    ip.name = d.name
                    ip.thing = d.thing
                    ip.owned = 0
                    ip.uiud = d.uiud
                    ip.aws = false
                    ip.local = false
                    ip.home = d.home
                    ip.room = d.room
                    IpListDevicesNew.add(ip)
                }

            }
            IpListDevices = IP.startDevices(IpListDevicesNew)
            localSqlUtils.replaceIpList(mDbUtils!!, "device", IpListDevices)
        }
    }

    private fun syncLocal() {
        var rulesTableDo = RulesTableHandler()
        var homeData = localSqlUtils.getHomeData(mDbUtils!!, "home")
        val gson = Gson()
        var newHomeList: MutableList<RoomModelJson> = ArrayList()
        //local to dynamo
        thread {
            var rulesTable = rulesTableDo.getRulesTable()
            if (rulesTable!!.userId != "ERROR") {

                var roomList: MutableList<MutableMap<String, String>> = ArrayList()
                var guestHomes: MutableList<String> = ArrayList()
                for (homes in homeData) {
                    if ((homes.type == "home") and (homes.sharedHome == "guest")) {
                        guestHomes.add(homes.name)
                        sharedDataList.add(RoomModelJson(homes.name, "home", "false", "0", 0))
                    }
                }
                for (homes in homeData) {
                    if ((homes.type == "room") and (homes.sharedHome != "default")) {
                        for (s in sharedDataList!!) {
                            if (s.sharedHome == homes.sharedHome) {
                                sharedDataList.add(RoomModelJson(homes.name, "room", "false", "0", 0))
                            }
                        }
                    }
                }
                for (homes in homeData) {
                    if (homes.type == "home") {
                        if (homes.sharedHome != "guest") {
                            var map = HashMap<String, String>()
                            map["bg"] = homes.bgUrl
                            map["name"] = homes.name
                            map["type"] = homes.type
                            map["access"] = homes.sharedHome
                            map["icon"] = homes.roomIcon.toString()
                            roomList.add(map)
                        }
                    } else {
                        var flag = false
                        for (item in guestHomes) {
                            if (item == homes.sharedHome) {
                                flag = true
                                break
                            }
                        }
                        if (!flag) {
                            var map = HashMap<String, String>()
                            map["bg"] = homes.bgUrl
                            map["name"] = homes.name
                            map["type"] = homes.type
                            map["access"] = homes.sharedHome
                            map["icon"] = homes.roomIcon.toString()
                            roomList.add(map)
                        }
                    }


                }
                rulesTable.homes = roomList

                var deviceTable = localSqlDatabase.getDeviceTable(mDbDevice!!)
                for (d in deviceTable) {
                    for (s in sharedDataList!!) {
                        if ((s.name == d.home) and (s.type == "home")) {
                            sharedDataList.add(RoomModelJson(d.name, "device", "false", "0", 0))
                            break
                        }
                    }
                }
                //Add device to device list
                var devices: MutableList<MutableMap<String, String>> = ArrayList()
                var allLoads: MutableList<MutableList<MutableMap<String, String>>> = ArrayList()
                for (device in deviceTable) {
                    var deviceId = device.uiud.substring(0, Math.min(device.uiud.length, 12))
                    var flag = false
                    for (item in guestHomes) {
                        if (item == device.home) {
                            flag = true
                            break
                        }
                    }
                    if (!flag) {
                        var loadList: MutableList<MutableMap<String, String>> = ArrayList()
                        var map = HashMap<String, String>()
                        map["id"] = deviceId
                        map["name"] = device.name
                        map["thing"] = device.thing!!
                        map["uiud"] = device.uiud
                        map["room"] = device.room!!
                        map["home"] = device.home!!
                        devices.add(map)
                        var m = HashMap<String, String>()
                        m["device"] = deviceId
                        m["type"] = "device"
                        loadList.add(m)
                        for (load in device.loads) {
                            var m = HashMap<String, String>()
                            m["name"] = load.name!!
                            m["dimmable"] = load.dimmable.toString()
                            m["favourite"] = load.favourite.toString()
                            m["icon"] = load.icon.toString()
                            m["index"] = load.index.toString()
                            m["device"] = deviceId
                            m["type"] = "load"
                            loadList.add(m)
                        }
                        allLoads.add(loadList)

                    }
                }
                rulesTable.devices = devices
                rulesTable.loads = allLoads


                var allScenes = localSqlScene.getSceneTable(mDbScene!!)
                for (d in allScenes) {
                    for (s in sharedDataList!!) {
                        if ((s.name == d.home) and (s.type == "home")) {
                            sharedDataList.add(RoomModelJson(d.name!!, "scene", "false", d.home!!, 0))
                            break
                        }
                    }
                }
                var SceneList: MutableList<MutableMap<String, MutableList<MutableMap<String, String>>>> = ArrayList()
                for (scene in allScenes) {
                    var Scene = HashMap<String, MutableList<MutableMap<String, String>>>()
                    var flag = false
                    for (item in guestHomes) {
                        if (item == scene.home!!) {
                            flag = true
                            break
                        }
                    }
                    if (!flag) {
                        var deviceList: MutableList<MutableMap<String, String>> = ArrayList()
                        var map = HashMap<String, String>()
                        map["type"] = "scene"
                        map["name"] = scene.name!!
                        map["icon"] = scene.icon.toString()
                        map["home"] = scene.home!!
                        deviceList.add(map)
                        for (room in scene.room) {
                            for (device in room.deviceList) {
                                var map = HashMap<String, String>()
                                map["type"] = "device"
                                map["deviceName"] = device.deviceName
                                map["level"] = device.dimVal.toString()
                                map["index"] = device.index.toString()
                                map["state"] = device.isTurnOn.toString()
                                deviceList.add(map)
                            }
                        }
                        Scene[scene.name!!] = deviceList
                        SceneList.add(Scene)
                    }

                }
                rulesTable.scenes = SceneList


                var allSchedules = localSqlSchedule.getScheduleTable(mDbSchedule!!)
                for (d in allSchedules) {
                    for (s in sharedDataList!!) {
                        if ((s.name == d.home) and (s.type == "home")) {
                            sharedDataList.add(RoomModelJson(d.name!!, "schedule", "false", d.home!!, 0))
                            break
                        }
                    }
                }
                var ScheduleList: MutableList<MutableMap<String, MutableList<MutableMap<String, String>>>> = ArrayList()
                for (schedule in allSchedules) {
                    var Schedule = HashMap<String, MutableList<MutableMap<String, String>>>()
                    var deviceList: MutableList<MutableMap<String, String>> = ArrayList()
                    var geoDataList: MutableList<MutableMap<String, String>> = ArrayList()
                    var flag = false
                    for (item in guestHomes) {
                        if (item == schedule.home!!) {
                            flag = true
                            break
                        }
                    }
                    if (!flag) {
                        var map = HashMap<String, String>()
                        map["type"] = "schedule"
                        map["stype"] = schedule.type!!
                        if (map["stype"] == "geo") {
                            for (geoDetail in schedule.property) {
                                map["radius"] = geoDetail.newGeoRadius.toString()
                                map["latitude"] = geoDetail.newGeoLat.toString()
                                map["longitude"] = geoDetail.newGeolong.toString()
                                map["automationEnable"] = geoDetail.AutomationEnable.toString()
                                map["triggeringType"] = geoDetail.triggerType.toString()
                            }
                            map["stype"] = schedule.type!!
                            map["name"] = schedule.name!!
                            map["icon"] = schedule.icon.toString()
                            map["home"] = schedule.home!!

                        } else {
                            map["stype"] = schedule.type!!
                            map["name"] = schedule.name!!
                            map["icon"] = schedule.icon.toString()
                            map["endtime"] = schedule.endTime!!
                            map["home"] = schedule.home!!
                            // map["time"] = schedule.time!!
                            //time UGT to GMT
                            val calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault())
                            val currentLocalTime = calendar.getTime()
                            val date = SimpleDateFormat("Z")
                            val localTime = date.format(currentLocalTime)
                            var offsetTimetype = localTime[0]
                            var offsetTimeHours = localTime.substring(1, 3).toInt()
                            var offsetTimeMinutes = localTime.substring(3).toInt()


                            var time = schedule.time!!.replace("\\s".toRegex(), "")
                            if ((time.takeLast(2) == "PM") || (time.takeLast(2) == "AM")) {
                                var timeHours24 = schedule.time!!.split(":")[0].replace("\\s".toRegex(), "").toInt()
                                if (time.takeLast(2) == "PM") {
                                    timeHours24 = timeHours24 + 11
                                }
                                val data = schedule.time!!.split(":")[1].replace(" ", "")
                                var timeMinutes24 = data.replace("A", "").replace("M", "").replace("P", "").toInt()
                                var totalTime = timeHours24 * 60 + timeMinutes24
                                var totalOffset = offsetTimeHours * 60 + offsetTimeMinutes
                                var gmtTime = 0
                                if (offsetTimetype == '+') {
                                    if (totalTime > totalOffset) {
                                        gmtTime = totalTime - totalOffset
                                    } else {
                                        gmtTime = 1440 - totalOffset + totalTime
                                    }
                                } else {
                                    gmtTime = totalTime + totalOffset
                                    if (gmtTime > 1440) {
                                        gmtTime = gmtTime - 1440
                                    }
                                }
                                map["time"] = String.format("%02d", (gmtTime / 60)) + ":" + String.format("%02d", (gmtTime % 60))
                            } else {
                                map["time"] = time
                            }



                            for (geoDetails in schedule.property) {
                                map["automationEnable"] = geoDetails.AutomationEnable.toString()
                            }

                            //for routine conversion
                            var type = object : TypeToken<HashMap<String, Boolean>>() {}.type
                            val gson = Gson()
                            val routine: java.util.HashMap<String, Boolean>
                            routine = gson.fromJson(schedule.routine, type)
                            map["routine"] = "[${routine["Sunday"]},${routine["Monday"]},${routine["Tuesday"]},${routine["Wednesday"]},${routine["Thursday"]},${routine["Friday"]},${routine["Saturday"]}]"
                        }
                        deviceList.add(map)
                        for (room in schedule.load) {
                            for (device in room.deviceList) {
                                var map = HashMap<String, String>()
                                map["type"] = "device"
                                map["deviceName"] = device.deviceName
                                map["level"] = device.dimVal.toString()
                                map["index"] = device.index.toString()
                                map["state"] = device.isTurnOn.toString()
                                deviceList.add(map)
                            }
                        }

                        Schedule[schedule.name!!] = deviceList

                        ScheduleList.add(Schedule)
                    }
                }
                rulesTable.schedules = ScheduleList


                var masterDeviceflag = false
                if (rulesTable.masterDevices != null) {
                    if (rulesTable.masterDevices!!.size != 0) {
                        masterDeviceflag = true
                    }
                }
                if (masterDeviceflag) {
                    for (shareDevice in rulesTable.masterDevices!!) {
                        var sharedHome = shareDevice["home"]
                        var sharedroom = shareDevice["room"]
                        var uiud = shareDevice["uiud"]
                        var sharedname = shareDevice["name"]
                        var sharedthing = shareDevice["thing"]
                        var shareLoads = rulesTable.masterLoads
                        var localLoads: MutableList<AuraSwitchLoad> = ArrayList()
                        for (sharedloads in shareLoads!!) {
                            var flag = false
                            for (load in sharedloads) {
                                if ((load["type"] == "device") and (load["device"] == shareDevice["id"])) {
                                    flag = true
                                    break
                                }
                            }
                            if (flag) {
                                for (load in sharedloads) {
                                    if (load["type"] != "device") {
                                        var localLoad = AuraSwitchLoad()
                                        localLoad.index = load["index"]!!.toInt()
                                        localLoad.icon = load["icon"]!!.toInt()
                                        localLoad.name = load["name"]
                                        localLoad.favourite = load["favourite"]!!.toBoolean()
                                        localLoad.dimmable = load["dimmable"]!!.toBoolean()
                                        localLoads.add(localLoad)
                                    }
                                }
                                break
                            }
                        }
                        localSqlDatabase.insertDevice(mDbDevice!!, sharedHome!!, sharedroom!!, uiud!!, sharedname!!, gson.toJson(localLoads), sharedthing!!)
                        for (s in sharedDataList) {
                            if ((s.name == sharedname) and (s.type == "device")) {
                                s.sharedHome = "true"
                            }
                        }
                    }
                }
                var allsharedDevice = localSqlDatabase.getDeviceTable(mDbDevice!!)

                //update sharing Scene
                var sharingSceneFlag = false
                if (rulesTable.masterScenes != null) {
                    if (rulesTable.masterScenes!!.size != 0) {
                        sharingSceneFlag = true
                    }
                }
                if (sharingSceneFlag) {
                    var allShareScenes = rulesTable.masterScenes
                    for (sharescenes in allShareScenes!!) {
                        var rooms: ArrayList<RoomModel> = ArrayList()
                        var keys = sharescenes.keys
                        var pair = ""
                        for (k in keys) {
                            pair = k
                        }
                        var sharedscene = sharescenes[pair]
                        var sharedsceneName = ""
                        var icon = 0
                        var sharedhome: String? = null

                        //extract all room list
                        var roomList: MutableList<String> = ArrayList()
                        var roomName: String? = null
                        var deviceList: MutableList<Device> = ArrayList()
                        for (s in sharedscene!!) {
                            if (s["type"] != "scene") {
                                var device = Device(0, s["state"]!!.toBoolean(), s["level"]!!.toInt(), s["deviceName"]!!, "room", s["deviceName"]!!, s["index"]!!.toInt(), false)
                                for (d in allsharedDevice) {
                                    if (d.name == s["deviceName"]) {
                                        device.type = d.loads[device.index].icon!!
                                        device.name = d.loads[device.index].name!!
                                        device.roomName = d.room!!
                                        device.dimmable = d.loads[device.index].dimmable!!
                                        var flag = false
                                        for (r in roomList) {
                                            if (r == d.room) {
                                                flag = true
                                                break
                                            }
                                        }
                                        if (!flag) {
                                            roomList.add(d.room!!)
                                        }
                                        deviceList.add(device)
                                        break
                                    }
                                }

                            } else {
                                sharedsceneName = s["name"]!!
                                icon = s["icon"]!!.toInt()
                                sharedhome = s["home"]!!
                            }
                        }
                        for (r in roomList) {
                            var room = RoomModel()
                            room.name = r
                            roomName = r
                            for (d in deviceList) {
                                if (d.roomName == r) {
                                    room.deviceList.add(d)
                                }
                            }
                            rooms.add(room)
                        }
                        localSqlScene.updateScene(mDbScene!!, sharedsceneName, gson.toJson(rooms), roomName!!, sharedhome!!, icon, sharedsceneName)
                        for (s in sharedDataList) {
                            if ((s.name == sharedsceneName) and (s.type == "scene")) {
                                s.sharedHome = "true"
                                break
                            }
                        }
                    }
                }
                //update sharing master schedule
                var sharingSchedulingFlag = false
                if (rulesTable.masterSchedules != null) {
                    if (rulesTable.masterSchedules!!.size != 0) {
                        sharingSchedulingFlag = true
                    }
                }
                if (sharingSchedulingFlag) {
                    var allshareSchedule = rulesTable.masterSchedules
                    for (sharSchedule in allshareSchedule!!) {
                        var rooms: ArrayList<RoomModel> = ArrayList()
                        var keys = sharSchedule.keys
                        var pair = ""
                        for (k in keys) {
                            pair = k
                        }
                        var schedule = sharSchedule[pair]
                        var sharedscheduleName = ""
                        var icon = 0
                        var sharedhome: String? = null
                        var sharedscheduleStatus: MutableList<GeoModal> = ArrayList()
                        var geoData = GeoModal()
                        var sharedscheduleStartTime: String? = null
                        var sharedscheduleType: String? = null
                        var sharedscheduleEndTime: String? = null
                        var sharedscheduleRoutine: String? = null
                        var roomList: MutableList<String> = ArrayList()
                        var deviceList: MutableList<Device> = ArrayList()
                        for (sheduleshare in schedule!!) {
                            if (sheduleshare["type"] != "schedule") {
                                var device = Device(0, sheduleshare["state"]!!.toBoolean(), sheduleshare["level"]!!.toInt(), sheduleshare["deviceName"]!!, "room", sheduleshare["deviceName"]!!, sheduleshare["index"]!!.toInt(), false)
                                for (d in allsharedDevice) {
                                    if (d.name == sheduleshare["deviceName"]) {
                                        device.type = d.loads[device.index].icon!!
                                        device.name = d.loads[device.index].name!!
                                        device.roomName = d.room!!
                                        device.dimmable = d.loads[device.index].dimmable!!
                                        var flag = false
                                        for (r in roomList) {
                                            if (r == d.room) {
                                                flag = true
                                                break
                                            }
                                        }
                                        if (!flag) {
                                            roomList.add(d.room!!)
                                        }
                                        deviceList.add(device)
                                        break
                                    }
                                }

                            } else {
                                sharedscheduleType = sheduleshare["stype"]!!
                                if (sharedscheduleType == "geo") {
                                    geoData.newGeoRadius = sheduleshare["radius"]!!.toFloat()
                                    geoData.newGeoLat = sheduleshare["latitude"]!!.toDouble()
                                    geoData.newGeolong = sheduleshare["longitude"]!!.toDouble()
                                    geoData.AutomationEnable = sheduleshare["automationEnable"]!!.toBoolean()
                                    geoData.triggerType = sheduleshare["triggeringType"]
                                    sharedscheduleStatus.add(geoData)
                                    sharedscheduleName = sheduleshare["name"]!!
                                    icon = sheduleshare["icon"]!!.toInt()
                                    sharedhome = sheduleshare["home"]
                                    sharedscheduleStartTime = ""
                                    sharedscheduleEndTime = ""
                                    sharedscheduleRoutine = ""

                                } else {
                                    sharedscheduleName = sheduleshare["name"]!!
                                    icon = sheduleshare["icon"]!!.toInt()
                                    geoData.AutomationEnable = sheduleshare["automationEnable"]!!.toBoolean()
                                    sharedscheduleStatus.add(geoData)
                                    sharedhome = sheduleshare["home"]
                                    sharedscheduleStartTime = sheduleshare["time"]
                                    sharedscheduleEndTime = sheduleshare["endtime"]
                                    var routine = sheduleshare["routine"]!!
                                    val type = object : TypeToken<ArrayList<String>>() {}.type
                                    val gson = Gson()
                                    var routinedata: MutableList<String> = ArrayList()
                                    routinedata = gson.fromJson(routine, type)
                                    var days = arrayOf("Monday", "Sunday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")

                                    var map = HashMap<String, String>()
                                    for ((i, name) in routinedata.withIndex()) {
                                        map[days[i]] = name
                                    }
                                    sharedscheduleRoutine = gson.toJson(map)
                                }

                            }

                        }
                        for (r in roomList) {
                            var room = RoomModel()
                            room.name = r
                            for (d in deviceList) {
                                if (d.roomName == r) {
                                    room.deviceList.add(d)
                                }
                            }
                            rooms.add(room)
                        }
                        localSqlSchedule.updatechedule(mDbSchedule!!, sharedscheduleName, icon, gson.toJson(roomList), gson.toJson(rooms), gson.toJson(sharedscheduleStatus), sharedscheduleStartTime!!, sharedscheduleType!!, sharedscheduleEndTime!!, sharedscheduleRoutine!!, sharedhome!!, sharedscheduleName)

                        for (s in sharedDataList) {
                            if ((s.name == sharedscheduleName) and (s.type == "schedule")) {
                                s.sharedHome = "true"
                                break
                            }
                        }
                    }
                }
                var newHomeList: MutableList<RoomModelJson> = ArrayList()

                for (h in homeData) {
                    if (h.type == "home") {
                        if (h.sharedHome == "master") {
                            newHomeList.add(h)
                        }
                    } else {
                        var gFlag = false
                        for (g in guestHomes) {
                            if (g == h.sharedHome) {
                                gFlag = true
                                break
                            }
                        }
                        if (!gFlag) {
                            newHomeList.add(h)
                        }
                    }
                }
                var sharedHomeflag = false
                if (rulesTable.masterHomes != null) {
                    if (rulesTable.masterHomes!!.size != 0) {
                        sharedHomeflag = true
                    }
                }
                if (sharedHomeflag) {
                    for (sharedHome in rulesTable.masterHomes!!) {
                        var Sharedhome = RoomModelJson(sharedHome["name"]!!, sharedHome["type"]!!, sharedHome["access"]!!, sharedHome["bg"]!!, sharedHome["icon"]!!.toInt())
                        newHomeList.add(Sharedhome)
                    }
                }
                for (s in sharedDataList) {
                    if (s.sharedHome == "false") {
                        if (s.type == "device") {
                            localSqlDatabase.deleteDevice(mDbDevice!!, s.name)
                        } else if (s.type == "scene") {
                            localSqlScene.deleteScene(mDbScene!!, s.name, s.bgUrl)
                        } else if (s.type == "schedule") {
                            localSqlSchedule.deleteAutomationScene(mDbSchedule!!, s.name, s.bgUrl)
                        }
                    }
                }
                var flage = true
                for (n in newHomeList) {
                    if ((n.type == "home") && (n.name == Constant.HOME)) {
                        flage = false
                        break
                    }
                }
                if (flage) {
                    val prefEditor = PreferenceManager.getDefaultSharedPreferences(this).edit()
                    prefEditor.putString("HOME", "My Home")
                    prefEditor.apply()
                    Constant.HOME = "My Home"
                    val intent = Intent("refresh")
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
                    //homefragment.function
                }
                localSqlUtils.replaceHome(mDbUtils!!, "home", newHomeList)


                rulesTableDo.saveRulesTable(rulesTable)
            }
        }
    }

    private fun performTabClick(tab: Int) {
        val view: View? = when (tab) {
            HOME_TAB -> bottomNavigationView.findViewById(R.id.action_item1)
            ROOMS_TAB -> bottomNavigationView.findViewById(R.id.action_item2)
            AUTOMATION_TAB -> bottomNavigationView.findViewById(R.id.action_item3)
            else -> bottomNavigationView.findViewById(R.id.action_item1)
        }
        view?.performClick()
    }

    override fun onBackPressed() {

        if (lastBackPressTime + 2000 > System.currentTimeMillis()) {
            super.onBackPressed()
            this.finishAffinity()
        } else {
            Toast.makeText(getBaseContext(), getString(R.string.app_close_toast_text), Toast.LENGTH_SHORT).show()
            lastBackPressTime = System.currentTimeMillis()

        }


//        when {
//            this.lastBackPressTime < System.currentTimeMillis() - 2000 -> {
//                toast = Toast.makeText(this, getString(R.string.app_close_toast_text), Toast.LENGTH_LONG)
//                toast!!.show()
//                this.lastBackPressTime = System.currentTimeMillis()
//            }
//            else -> {
//                if (toast != null) {
//                    toast!!.cancel()
//                }
//                super.onBackPressed()
//            }
//
//        }
//        this.finish()
    }

    //ERRORCONFIGURE
    override fun onResume() {
        super.onResume()

    }

    override fun onCreateAutomationBtnClicked() {
        startActivity(Intent(this, CreateAutomationActivity::class.java))
    }

    override fun onDetailsBtnClicked(automation: AutomationModel) {
        val intent = Intent(this, CreateAutomationActivity::class.java)
        startActivity(intent)
    }

    override fun onDeleteBtnClicked(automation: AutomationModel) {
        showToast("Delete clicked ")
    }
}
