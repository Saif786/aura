package com.wozart.aura.ui.dashboard

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.View.INVISIBLE
import android.widget.Toast
import aura.wozart.com.aura.entity.network.Nsd
import com.google.gson.Gson
import com.wozart.aura.R
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.model.IpModel
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.entity.model.aura.AuraSwitchLoad
import com.wozart.aura.entity.network.ConnectTask
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.base.IconsAdapter
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.DialogListener
import com.wozart.aura.utilities.Encryption
import kotlinx.android.synthetic.main.activity_edit_load.*
import kotlinx.android.synthetic.main.layout_header.*
import kotlin.concurrent.thread

class EditLoadActivity : AppCompatActivity(),ConnectTask.TcpMessageReceiver {


    private var load: AuraSwitchLoad = AuraSwitchLoad()
    var auraDevice = AuraSwitch()
    private val localSqlDatabase = DeviceTable()
    private var mDb: SQLiteDatabase? = null
    private var mDbScene: SQLiteDatabase? = null

    private var mDbUtils: SQLiteDatabase? = null
    val switch_icons = arrayListOf(R.drawable.ic_bulb_off_new, R.drawable.ic_ceiling_lamp_off_new,R.drawable.ic_bed_lamp_off_new,
            R.drawable.ic_fan_off_,R.drawable.ic_switch_off,R.drawable.ic_ac_off,R.drawable.ic_exhaust_fan_off)
    lateinit var loadAdapter : IconsAdapter
    private val localSqlUtils = UtilsTable()
    private var rulesTableDo = RulesTableHandler()
    var roomDeviceCheck: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_load)

        val deviceString: String = intent.getStringExtra("DEVICE")
        val gson = Gson()
        val device = gson.fromJson(deviceString, Device::class.java)

        roomDeviceCheck = intent.getStringExtra("START_ROOM")

        val dbHelper = DeviceDbHelper(this)
        mDb = dbHelper.writableDatabase

        val scenDbHelper = SceneDbHelper(this)
        mDbScene = scenDbHelper.writableDatabase
        val dbUtils = UtilsDbHelper(this)
        mDbUtils = dbUtils.writableDatabase

        var listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")

        auraDevice = localSqlDatabase.getDevice(mDb!!, device.deviceName)
        load = auraDevice.loads[device.index]
        if ((load.name == "Switch") || (load.index == 3)) {
            dimmable_layout.visibility = View.GONE
        }
        init(device.deviceName)
    }

    private fun init(device: String) {

        loadAdapter = IconsAdapter { pos: Int, Boolean ->

        }
        // enableFields(false)

        enableFields(true)
        tvNext.setOnClickListener {
            progressBar.progress = View.VISIBLE
            load.icon = loadAdapter.selectedPostion()
            localSqlDatabase.updateLoad(mDb!!, device, load)
            thread {
                //deviceDynamoDb.updateLoadForDevice(deviceId!!, load)
                auraDevice.loads[load.index!!] = load
                rulesTableDo.insertDeviceLoads(auraDevice)
                //userDynamoDb.updateModifiedAccess(homeBG)
            }

            if (roomDeviceCheck == "RoomDevice") {
                val intent = Intent(applicationContext, DashboardActivity::class.java)
                intent.putExtra("TAB_SET",Constant.ROOMS_TAB)
                this.startActivity(intent)
                this.finish()
            } else {
                val intent = Intent(applicationContext, DashboardActivity::class.java)
                this.startActivity(intent)
                this.finish()
            }

        }


        favouriteSwitch.setOnClickListener {
            load.favourite = favouriteSwitch.isChecked
        }

        dimmableSwitch.setOnClickListener {
            load.dimmable = dimmableSwitch.isChecked
        }

        tvTitle.text = getString(R.string.title_edit_load)
        tvTitle.setTextColor(ContextCompat.getColor(this, R.color.black))
        tvNext.text = getString(R.string.text_finish)
        tvNext.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
        DrawableCompat.setTint(home.drawable, ContextCompat.getColor(this, R.color.black))
        home.setOnClickListener {
            this.onBackPressed()
        }
        sickbar.visibility = INVISIBLE
        dimmableSwitch.isChecked = load.dimmable!!
        favouriteSwitch.isChecked = load.favourite!!

        input_load.setText(load.name)

        input_load.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                load.name = input_load.text.toString()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
        btn_delete.setOnClickListener {
            progressBar.progress = View.VISIBLE

            Utils.showCustomDialog(this, "Delete Device", this.getString(R.string.dialog_delete_appliance), R.layout.dialog_layout, object : DialogListener {
                override fun onOkClicked() {
                    deleteAccessoryDone(device)
                }

                override fun onCancelClicked() {

                }
            })


        }


        val reverseLayout = false
        loadList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, reverseLayout)
        loadList.adapter = loadAdapter
        loadAdapter.init(switch_icons, load.icon!!)
    }

    private fun deleteAccessoryDone(device: String) {
        var IpListDevices: MutableList<IpModel> = ArrayList()
        val uiud = localSqlDatabase.getUiud(mDb!!, device)
        if (uiud != null) {
            thread {
                if (Encryption.isInternetWorking()) {
                    val auraDevice = localSqlDatabase.getDevice(mDb!!, device!!)
                    val error = rulesTableDo.deleteDevice(auraDevice)
                    if (error == "SUCCESS") {
                        localSqlDatabase.deleteDevice(mDb!!, device!!)
                        //NEWSCENES delete all scenes
                    } else {
                        Toast.makeText(this, "Failed to delete device try later.", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    runOnUiThread {
                        Toast.makeText(this, "Check Internet connection", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        thread {
            if (Encryption.isInternetWorking()) {
                localSqlDatabase.deleteDevice(mDb!!, device)
                IpListDevices = localSqlUtils.getIpList(mDbUtils!!, "device")
                var rmv = IpModel()
                var Nsd = Nsd(this, "IP")
                for (l in IpListDevices) {
                    if (l.name == device) {
                        Log.d("IP", "IP is ${l.ip},${device}")
                        if (l.ip == null) {
                            l.ip = (Nsd.getIP(l.name!!))
                        } else {
                            ConnectTask(this, this, "{\"type\":8,\"name\":\"$device\",\"set\":1,\"uiud\":\"$uiud\",\"pair\":1}", l.ip!!, device!!).execute()
                            rmv = l
                        }
                        // ConnectTask(this,   dataReceived, "{\"type\":8,\"name\":\"95FF5C\",\"set\":1,\"pair\":1}", l.ip!!,arrayList[position].deviceName!!).execute()
                    }
                }
                IpListDevices.remove(rmv)
                localSqlUtils.replaceIpList(mDbUtils!!, "device", IpListDevices)

                runOnUiThread {
                    progressBar.visibility = View.INVISIBLE

                    val intent = Intent(applicationContext, DashboardActivity::class.java)
                    this.startActivity(intent)
                    this.finish()
                }

            } else {
                runOnUiThread {
                    Toast.makeText(this, "Check Internet connection", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

//            localSqlDatabase.deleteDevice(mDb!!, device)
//            rulesTableDo.deleteDevice(auraDevice)
//
//            runOnUiThread {
//                progressBar.visibility = View.INVISIBLE
//                val intent = Intent(applicationContext, DashboardActivity::class.java)
//                this.startActivity(intent)
//                this.finish()
//            }
    // }

    private fun enableFields(enable: Boolean) {
        if (enable) {
            input_load.isEnabled = true
            favouriteSwitch.isEnabled = true
            dimmableSwitch.isEnabled = true
            loadList.isClickable = true
            load_image_layout.visibility = View.VISIBLE
            //sickbar.isEnabled=true
        } else {
            input_load.isEnabled = false
            favouriteSwitch.isEnabled = false
            dimmableSwitch.isEnabled = false
            loadList.isClickable = false
            load_image_layout.visibility = View.INVISIBLE
            // sickbar.isEnabled=false
        }

    }
    override fun onTcpMessageReceived(message: String) {
        val data = message
        Log.d("LOAD_DELETE_DATA", data)
    }
}
