package com.wozart.aura.ui.base.baseselectdevices

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.wozart.aura.R
import com.wozart.aura.ui.createautomation.OnFragmentInteractionListener
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.entity.model.scene.Scene
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.ui.base.SceneIconAdapter
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.createscene.CreateSceneActivity
import com.wozart.aura.ui.dashboard.Device
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.fragment_select_devices.*
import kotlinx.android.synthetic.main.layout_header.*
import kotlin.collections.ArrayList


abstract class BaseSelectDevicesFragment : Fragment() {

    private val drawables = arrayListOf(R.drawable.ic_enter_off, R.drawable.ic_exit_off, R.drawable.ic_good_morning_off,
            R.drawable.ic_good_night_off, R.drawable.ic_party_off, R.drawable.ic_reading_off, R.drawable.ic_movie_off)
    private var sceneNameCheck: String? = null
    private var sceneIconurl: Int? = 0
    private var sceneIconPosition: Int? = 0
    private var sceneType: String? = null
    protected var mListener: OnFragmentInteractionListener? = null
    protected var adapter: SelectRoomAdapter = SelectRoomAdapter()
    var roomsList: MutableList<RoomModel> = ArrayList()
    private lateinit var sceneIconAdapter: SceneIconAdapter
    private val localSqlDatabase = DeviceTable()
    private var mDb: SQLiteDatabase? = null
    private var sceneCreateType = true
    private val localSqlScene = SceneTable()
    private var sceneNameNew: String? = null
    var selectedSceneIcon: Boolean = false
    var sceneList = ArrayList<Scene>()

    private var mDbScene: SQLiteDatabase? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_select_devices, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (context is CreateSceneActivity) {
            sceneNameCheck = (context as CreateSceneActivity).getSceneName()
            sceneType = (context as CreateSceneActivity).getSceneType()
            if (sceneNameCheck != "") {
                sceneCreateType = false
            }
        }
        if (context is CreateSceneActivity) {
            sceneIconurl = (context as CreateSceneActivity).getSceneIconUrl()
        }
        sceneIconPosition = 0

        var pos = 0
        for (icons in drawables) {
            if (icons == sceneIconurl) {
                sceneIconPosition = pos
                selectedSceneIcon = true
            } else {
                pos = pos!! + 1
            }
        }
        for (i in drawables.indices) {
            var scene = Scene()
            scene.icon = drawables[i]
            if (i == sceneIconurl) {
                sceneIconPosition = pos
                selectedSceneIcon = true
            } else {
                sceneIconPosition = 0
                selectedSceneIcon = false
            }
            sceneList.add(scene)

        }

        val dbScene = SceneDbHelper(context!!)
        mDbScene = dbScene.writableDatabase

        if (sceneType == "create") {
            if (sceneIconPosition == 0) {
                sceneNameNew = "Enter"
                inputSceneName.setText(sceneNameNew)
            }
        }
        init()
    }


    fun init() {
        tvTitle.text = getTitle()
        tvNext.setOnClickListener { openNextScreen() }
        tvNext.setTextColor(Color.WHITE)
        roomsList.clear()
        home.setOnClickListener { mListener?.onHomeBtnClicked() }
        listRooms.layoutManager = LinearLayoutManager(activity)!!
        listRooms.adapter = adapter
        adapter.init(roomsList)
        val visibility = if (showSceneInputs()) View.VISIBLE else View.INVISIBLE
        inputSceneName.visibility = visibility
        tvSelectSceneIcon.visibility = visibility
        listScenes.visibility = visibility
        if (visibility == View.VISIBLE) {

            sceneIconAdapter = SceneIconAdapter(sceneList) { pos: Int, Boolean ->
                if (pos == 0) {
                    sceneNameNew = "Enter"
                    inputSceneName.setText(sceneNameNew)
                } else if (pos == 1) {
                    sceneNameNew = "Exit"
                    inputSceneName.setText(sceneNameNew)
                } else if (pos == 2) {
                    sceneNameNew = "Good Morning"
                    inputSceneName.setText(sceneNameNew)
                } else if (pos == 3) {
                    sceneNameNew = "Good Night"
                    inputSceneName.setText(sceneNameNew)
                }else if(pos== 4){
                    sceneNameNew = "Movie Scene"
                    inputSceneName.setText(sceneNameNew)
                }else if(pos == 5) {
                    sceneNameNew = "Party Scene"
                    inputSceneName.setText(sceneNameNew)
                }else if(pos == 6){
                    sceneNameNew = "Reading Scene"
                    inputSceneName.setText(sceneNameNew)
                }
            }
//            iconsAdapter = IconsAdapter{pos: Int,Boolean ->
//                if(pos == 0){
//                    sceneNameNew = "Enter"
//                    inputSceneName.setText(sceneNameNew)
//                }else if(pos == 1){
//                    sceneNameNew = "Exit"
//                    inputSceneName.setText(sceneNameNew)
//                }else if(pos == 2){
//                    sceneNameNew = "Good Morning"
//                    inputSceneName.setText(sceneNameNew)
//                }else if(pos == 3){
//                    sceneNameNew = "Good Night"
//                    inputSceneName.setText(sceneNameNew)
//                }
//            }
            listScenes.adapter = sceneIconAdapter
            listScenes.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
        }
        populateData()
    }

    abstract fun showSceneInputs(): Boolean

    abstract fun getTitle(): String

    protected fun getSelectedRoomDeviceData(): MutableList<RoomModel> {
        val rooms: MutableList<RoomModel> = ArrayList()
        for (room in this.roomsList) {
            val deviceList = ArrayList(room.deviceList.filter { it.isSelected })  //for device isSelected.
            if (room.deviceList.size > 0) {
                val roomModel = room.copy()
                roomModel.deviceList = deviceList
                rooms.add(roomModel)
            }
        }
        return rooms
    }

    protected fun getIcon(): Int {
        return drawables[sceneIconAdapter.selectedPostion()]
    }


    private fun populateData() {
        deviceList()
        sceneList()
    }

    private fun sceneList() {
        sceneIconAdapter.init(drawables, sceneIconPosition!!)

    }

    private fun deviceList() {
        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase
        val dummyRooms = localSqlDatabase.getRooms(mDb!!, Constant.HOME!!)
        for (x in dummyRooms) {
            val room = RoomModel()
            room.name = x.roomName
            roomsList.add(room)
        }
        var scene = localSqlScene.getSceneByName(mDbScene!!, sceneNameCheck!!, Constant.HOME!!)


        if (!roomsList.isEmpty()) {

            for (x in roomsList) {
                val allDevicesList = ArrayList<Device>()
                val devices = localSqlDatabase.getDevicesForRoom(mDb!!, Constant.HOME!!, x.name!!)
                var scenRoom = RoomModel()
                for (s in scene.room) {
                    if (s.name == x.name) {
                        scenRoom = s
                    }
                }

                for (device in devices) {
                    for (i in 0..3) {
                        var flag = false
                        for (d in scenRoom.deviceList) {
                            if (d.deviceName == device.name) {
                                if (d.index == i) {
                                    if (d.isSelected) {
                                        flag = true
                                    }
                                    break
                                }
                            }
                        }
                        allDevicesList.add(Device(device.loads[i].icon!!, flag, 100, device.loads[i].name!!, localSqlDatabase.getRoomForDevice(mDb!!, device.name), device.name, device.loads[i].index!!, device.loads[i].dimmable!!))
                    }
                }
                x.deviceList = allDevicesList
            }
        }
    }

    abstract fun openNextScreen()


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnAutomationListInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

}
