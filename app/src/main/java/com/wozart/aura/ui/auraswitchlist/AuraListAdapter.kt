package com.wozart.aura.ui.auraswitchlist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wozart.aura.R
import com.wozart.aura.aura.ui.auraswitchlist.OnAdapterInteractionListener
import com.wozart.aura.entity.model.aura.AuraSwitch
import kotlinx.android.synthetic.main.item_aura_switch.view.*
/**
 * Created by Niranjan P on 3/14/2018.
 */
class AuraListAdapter(val listener: OnAdapterInteractionListener) : RecyclerView.Adapter<AuraListAdapter.ViewHolder>() {

    var auraSwitches: MutableList<AuraSwitch> = ArrayList()

    fun init(auraSwitches: MutableList<AuraSwitch>) {
        this.auraSwitches = auraSwitches
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val switch = auraSwitches[position]
        holder?.itemView?.tvName?.text = "Aura Switch"
        var switchNames = switch.name.split("-")
        var deviceName = switchNames[1]
        holder?.itemView?.roomName?.text = deviceName
        holder?.itemView?.setOnClickListener {
            if(switch.name.endsWith("6010D4")){
                switch.aws = 1
            }
            listener.onSelectAuraDevice(switch)
        }
    }

    override fun getItemCount(): Int = auraSwitches.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_aura_switch, parent, false)
        return ViewHolder(view)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}