package com.wozart.aura.ui.base

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wozart.aura.R
import kotlinx.android.synthetic.main.item_icon.view.*





class IconsAdapter(var listner :(Int,Boolean)->Unit) : RecyclerView.Adapter<IconsAdapter.ViewHolder>() {
    private var drawables: MutableList<Int> = ArrayList()
    private var selectedPos = 0


    fun init(drawables: MutableList<Int>,posNumber : Int) {
        this.drawables = drawables
        this.selectedPos = posNumber
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_icon, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val drawable = drawables[position]
        holder.itemView.imgThumbnail.setImageDrawable(ContextCompat.getDrawable(holder.itemView.context, drawable))

        if (selectedPos != position) {
            holder.itemView?.background = holder.itemView.context.getDrawable(R.drawable.image_border)
        }
        else {
            holder.itemView?.background = holder.itemView.context.getDrawable(R.drawable.image_border_selected)
        }

        holder.itemView.setOnClickListener {
            val prevPos = selectedPos
            selectedPos = position
            listner(selectedPos,false)
            if (prevPos != -1)
                notifyItemChanged(prevPos)
                notifyItemChanged(selectedPos)
        }
    }

    fun selectedPostion(): Int{
        return selectedPos
    }
    fun selectIconPosition(pos : Int){
        val prevPos = selectedPos
        selectedPos = pos
        if (prevPos != -1)
            notifyItemChanged(prevPos)
        notifyItemChanged(selectedPos)
    }

    override fun getItemCount(): Int {
        return this.drawables.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}
}