package com.wozart.aura.ui.wifisettings

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.wozart.aura.R
import kotlinx.android.synthetic.main.layout_zmote_configuration.*

class ZemoteConfigurationFragment : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var rootview = inflater.inflate(R.layout.layout_zmote_configuration,container,false)
        return rootview
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadUI()
        back.setOnClickListener {
            val manager = activity!!.supportFragmentManager
            val trans = manager.beginTransaction()
            trans.remove(this)
            trans.commit()
            manager.popBackStack()
        }
        tvNext.setOnClickListener {
            val intent = Intent(context,IRDeviceFoundActivity::class.java)
            startActivity(intent)
        }

    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadUI() {
        // settings url goes here currently https://www.google.co.in/'

        webview.webViewClient = CustomWebViewClient()
        webview.settings.javaScriptEnabled = true
        webview.loadUrl("http://www.zmote.io/start")
    }
    class CustomWebViewClient : WebViewClient(){

        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            view?.loadUrl(request?.url.toString())
            return true
        }

    }
}