package com.wozart.aura.ui.home

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import com.google.gson.Gson
import com.wozart.aura.R
import com.wozart.aura.ui.adapter.ConfigurationIconsAdapter
import com.wozart.aura.ui.base.IconsAdapter
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.entity.model.aura.AuraSwitchLoad
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import kotlinx.android.synthetic.main.activity_configure_load.*
import com.wozart.aura.data.dynamoDb.DeviceTableHandler
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.dynamoDb.ThingTableHandler
import com.wozart.aura.data.dynamoDb.UserTableHandler
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.entity.model.scene.Scene
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.Device
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.utilities.Constant
import kotlin.concurrent.thread

class ConfigureLoadActivity : AppCompatActivity(), ConfigurationIconsAdapter.OnLoadSelected {

    val switch_icons = arrayListOf<Int>(R.drawable.ic_bulb_off_new, R.drawable.ic_ceiling_lamp_off_new, R.drawable.ic_bed_lamp_off_new,
            R.drawable.ic_fan_off_, R.drawable.ic_switch_off, R.drawable.ic_ac_off, R.drawable.ic_exhaust_fan_off)

    private lateinit var iconsAdapter: IconsAdapter
    //private var configureIconAdapter = ConfigurationIconsAdapter(this)

    private var roomList: MutableList<String> = ArrayList()
    private var Device: AuraSwitch = AuraSwitch()

    private val localSqlDatabase = DeviceTable()
    private var mDb: SQLiteDatabase? = null
    private lateinit var gson: Gson

    private var LOAD_POS: Int = 0
    private lateinit var UIUD: String
    private lateinit var deviceName: String

    private val deviceDynamoDb = DeviceTableHandler()
    private val thingDynamoDb = ThingTableHandler()
    private val userDynamoDb = UserTableHandler()
    private var editLoadType: Boolean = false
    private var roomIconSelected: Int = 0
    private var listRoom: MutableList<RoomModelJson> = ArrayList()
    private val localSqlScene = SceneTable()
    private var mDbScene: SQLiteDatabase? = null

    private var mDbUtils: SQLiteDatabase? = null
    private val localSqlUtils = UtilsTable()
    var roomSelected: String? = null
    private var rulesTableDo = RulesTableHandler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configure_load)
        Utils.setDrawable(this, activity_main)
        val deviceString: String = intent.getStringExtra("DEVICE")
        gson = Gson()
        Device = gson.fromJson(deviceString, AuraSwitch::class.java)
        UIUD = intent.getStringExtra("UIUD")

        val dbHelper = DeviceDbHelper(this)
        mDb = dbHelper.writableDatabase
        val dbScene = SceneDbHelper(this)
        mDbScene = dbScene.writableDatabase

        val dbUtils = UtilsDbHelper(this)
        mDbUtils = dbUtils.writableDatabase

        iconsAdapter = IconsAdapter { pos: Int, Boolean ->

        }

        init()
    }

    private fun init() {

        listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")
        roomList.add(Device.room)
        for (dummy in listRoom) {
            if (dummy.name == Constant.HOME) {
                var activity_main = findViewById<LinearLayout>(R.id.activity_main)
                Utils.setDrawable(this, activity_main, dummy.bgUrl.toInt())
            }
            if (dummy.type == "room") {
                if ((dummy.sharedHome == Constant.HOME) or (dummy.sharedHome == "default")) {
                    if (dummy.name != Device.room) {
                        roomList.add(dummy.name)
                    }
                }

            }
        }

        val loads = AuraSwitchLoad()

        roomSelected = Device.room
        val spinnerAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, roomList)
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        roomlistSpinner!!.adapter = spinnerAdapter

        roomlistSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {


            override fun onNothingSelected(parent: AdapterView<*>?) {
                roomSelected = Device.room
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                roomSelected = roomlistSpinner.selectedItem.toString()
                roomIconSelected = position
            }
        }

        if (UIUD == "FARGIUIUD") {
            editLoadType = true
        } else {
            Device.loads = loads.defaultLoadList()
            Device.uiud = UIUD
        }

        input_load.setText(Device.loads[0].name)
        switchDim.isChecked = Device.loads[0].dimmable!!
        switchFav.isChecked = Device.loads[0].favourite!!

        back.setOnClickListener {

            var finale = false
            val selectedPosition = iconsAdapter.selectedPostion()
            Device.loads[LOAD_POS].icon = selectedPosition

            if (LOAD_POS == 0) {
                back.visibility = View.INVISIBLE
                btn_submit.text = "Next"
            } else if (LOAD_POS == 1) {
                back.visibility = View.INVISIBLE
                btn_submit.text = "Next"
                LOAD_POS--
            } else if (LOAD_POS == 2) {
                back.visibility = View.VISIBLE
                btn_submit.text = "Next"
                LOAD_POS--
            } else if (LOAD_POS == 3) {
                dimmable_layout.visibility = View.GONE
                back.visibility = View.VISIBLE
                btn_submit.text = "Next"
                LOAD_POS--
            }
            configureScreenDetails(LOAD_POS, finale)

        }

        btn_submit.setOnClickListener {
            var finale = false
            val selectedPosition = iconsAdapter.selectedPostion()
            Device.loads[LOAD_POS].icon = selectedPosition

            if (LOAD_POS == 0) {
                back.visibility = View.VISIBLE
                btn_submit.text = "Next"
                LOAD_POS++
            } else if (LOAD_POS == 1) {
                back.visibility = View.VISIBLE
                btn_submit.text = "Next"
                LOAD_POS++
            } else if (LOAD_POS == 2) {
                dimmable_layout.visibility = View.GONE
                back.visibility = View.VISIBLE
                btn_submit.text = getString(R.string.text_finish)
                LOAD_POS++
            } else {
                finale = true
            }
            configureScreenDetails(LOAD_POS, finale)
        }

        switchFav?.setOnCheckedChangeListener { _, isChecked ->
            Device.loads[LOAD_POS].favourite = isChecked
        }

        switchDim?.setOnCheckedChangeListener { _, isChecked ->
            Device.loads[LOAD_POS].dimmable = isChecked
        }

        input_load.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                Device.loads[LOAD_POS].name = input_load.text.toString()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        // back.setOnClickListener { onBackPressed() }

        iconsAdapter = IconsAdapter { pos: Int, Boolean ->

        }
        //configureIconAdapter = ConfigurationIconsAdapter(this)
        val reverseLayout = false
        loadList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, reverseLayout)
        loadList.adapter = iconsAdapter
//        configLoadList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, reverseLayout)
//        configLoadList.adapter = configureIconAdapter
        generateIcons(0)
        selected_load.text = "Load 1"
    }

    private fun generateIcons(pos: Int) {
        iconsAdapter.init(switch_icons, pos)
        //val loadList: MutableList<Load> = ArrayList()
//        for (i in switch_icons.indices) {
//            val load = Load()
//            load.loadImage = switch_icons[i]
//            load.loadName = "Load $i"
//            loadList.add(load)
//        }
//        configureIconAdapter.init(loadList)

    }

    override fun onLoadCallback(load: Int) {
        LOAD_POS = load
        //  input_load.setText(Device.loads[load].name)
        // switchDim.isChecked = Device.loads[load].dimmable!!
        // switchFav.isChecked = Device.loads[load].favourite!!
    }

    fun configureScreenDetails(position: Int, save: Boolean) {

        input_load.setText(Device.loads[position].name)
        switchDim.isChecked = Device.loads[position].dimmable!!
        switchFav.isChecked = Device.loads[position].favourite!!
        iconsAdapter.selectIconPosition(Device.loads[position].icon!!)
        selected_load.text = "Load " + (position + 1)

        if (save) {
            if (Constant.HOME == null) {
                val prefs = PreferenceManager.getDefaultSharedPreferences(this)
                val home = prefs.getString("HOME", "NO HOME")
                Constant.HOME = home
            }
            progress_bar.visibility = View.VISIBLE

            if (Device.thing == null) {
                localSqlDatabase.insertDevice(mDb!!, Constant.HOME!!, roomSelected!!, Device.uiud, Device.name, gson.toJson(Device.loads), "null")
            } else {
                localSqlDatabase.insertDevice(mDb!!, Constant.HOME!!, roomSelected!!, Device.uiud, Device.name, gson.toJson(Device.loads), Device.thing!!)
            }

            localSqlScene.updateRoom(mDbScene!!, Device.room, roomSelected!!)
            //rename room names
            if (roomSelected != Device.room) {
                Device.room = roomSelected as String


                val sceneList = localSqlScene.getSceneForRoom(mDbScene!!, Constant.HOME!!, Device.room)

                for ((index, scene) in sceneList.withIndex()) {
                    var dList: MutableList<Device> = ArrayList()
                    var devicePresent = false
                    val roomList = scene.room
                    for (r in roomList) {
                        var devices = r.deviceList
                        for (d in devices) {
                            if (d.deviceName == Device.name) {
                                devicePresent = true
                                break
                            }
                        }
                    }

                    if (!devicePresent) {
                        break
                    } else {
                        var rooms: ArrayList<String> = ArrayList()
                        for (r in roomList) {
                            var devices = r.deviceList
                            for (d in devices) {
                                if (d.deviceName == Device.name) {
                                    d.roomName = roomSelected!!
                                }
                                dList.add(d)
                                var nameExists = false
                                for (n in rooms) {
                                    if (n == d.roomName) {
                                        nameExists = true
                                    }
                                }
                                if (!nameExists) {
                                    rooms.add(d.roomName)
                                }
                            }
                        }
                        var newScene = Scene()
                        newScene.icon = scene.icon
                        newScene.name = scene.name
                        for (r in rooms) {
                            var dummy = RoomModel()
                            dummy.name = r
                            newScene.room.add(dummy)
                        }
                        for (d in dList) {
                            for (r in newScene.room) {
                                if (r.name == d.roomName) {
                                    r.deviceList.add(d)
                                }
                            }
                        }
                        localSqlScene.deleteScene(mDbScene!!, newScene.name!!, Constant.HOME!!)
                        //localSqlScene.insertScene(mDbScene!!, newScene.name!!, newScene.room, rooms: String, home: String, icon : Int,oldScene: String,sceneType:String): Boolean {
                        localSqlScene.insertScene(mDbScene!!, newScene.name!!, gson.toJson(newScene.room), Device.room, Constant.HOME!!, newScene.icon, newScene.name!!, "create")
                    }
                }
            }
            thread {
                //get thing from sql and store it dynamo db with version 2
                var thingDetails = localSqlUtils.getThingData(mDbUtils!!, Device.name)
                if ((thingDetails.version != "Version2") and (thingDetails.version != null)) {
                    thingDynamoDb.updateAvailability(Device.thing!!)
                    deviceDynamoDb.insertDevice(Device, thingDetails)
                }
                rulesTableDo.insertDeviceLoads(Device)

                runOnUiThread {
                    progress_bar.visibility = View.INVISIBLE
                    val intent = Intent(applicationContext, DashboardActivity::class.java)
                    this.startActivity(intent)
                    this.finish()
                }

            }
        }

    }

}
