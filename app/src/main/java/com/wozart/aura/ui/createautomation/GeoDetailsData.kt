package com.wozart.aura.ui.createautomation

/***************************************************************************
 * File Name :
 * Author : Aarth Tandel
 * Date of Creation : 11/02/19
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class GeoDetailsData {
    var passage: String ?= null
    var Geomessage : String ?= null
    var geoUserId : String ?= null
    var scheduleName : String ?= null
    var Geohome : String ?= null

}