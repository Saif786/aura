package com.wozart.aura.ui.wifisettings

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wozart.aura.R
import kotlinx.android.synthetic.main.activity_zmote_device_found.view.*

/***************************************************************************
 * File Name :
 * Author : Saif
 * Date of Creation : 2019-10-14
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class ZmoteListAdapter(var context: Context, var contactModelList : ArrayList<ContactModel>, var listner : (ContactModel,Boolean) -> Unit) : RecyclerView.Adapter<ZmoteListAdapter.ZmoteViewHolder>() {


    override fun onBindViewHolder(holder: ZmoteViewHolder, position: Int) {
        var zmoteDevice = contactModelList[position]
        holder.bind(zmoteDevice,listner)
    }

    override fun getItemCount(): Int {
        return contactModelList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ZmoteViewHolder {
       var rootView = LayoutInflater.from(parent.context).inflate(R.layout.activity_zmote_device_found,parent,false)
        return ZmoteViewHolder(rootView)
    }

    class ZmoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun bind(contactModel: ContactModel,listner: (ContactModel, Boolean) -> Unit){
            itemView.zmoteDeviceName.text = contactModel.zname
            itemView.status.text = contactModel.zstate

            itemView.setOnClickListener {
                listner(contactModel,false)
            }
        }
    }



}