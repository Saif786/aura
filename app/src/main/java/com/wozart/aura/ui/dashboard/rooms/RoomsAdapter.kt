package com.wozart.aura.ui.dashboard.rooms

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.warkiz.widget.IndicatorSeekBar
import com.wozart.aura.R
import com.wozart.aura.data.model.Room
import com.wozart.aura.ui.createscene.CreateSceneActivity
import com.wozart.aura.aura.ui.dashboard.listener.OnDeviceOptionsListener
import com.wozart.aura.aura.ui.dashboard.listener.OnOptionsListener
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.device.DeviceHandler
import com.wozart.aura.data.device.IpHandler
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.entity.model.aws.AwsState
import com.wozart.aura.data.model.IpModel
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraComplete
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.*
import com.wozart.aura.utilities.JsonHelper
import kotlinx.android.synthetic.main.item_room.view.*
import kotlinx.android.synthetic.main.dialog_configure_edit_dimming.*
import com.wozart.aura.ui.wifisettings.WifiSettingsActivity
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.Encryption
import kotlinx.android.synthetic.main.dialogue_edit_home.*
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.startActivity
import kotlin.concurrent.thread


/***
 * Created by Kiran on 20-03-2018.
 */
class RoomsAdapter(private var context: Context, onLoadPressed: OnLoadPressedListiner, onScenePressed: OnScenePressedListener, private val roomsList: ArrayList<Room>,
                   val optionerListener: OnOptionsListener?, val deviceOptionerListener: OnDeviceOptionsListener?,
                   val listener: (Int, Int) -> Unit) : RecyclerView.Adapter<RoomsAdapter.RoomViewHolder>() {

    var prevPosition: Int = -1
    private val localSqlDatabase = DeviceTable()
    private val localSqlScene = SceneTable()
    private var mDbScene: SQLiteDatabase? = null
    private var mDb: SQLiteDatabase? = null
    private var loadListener: OnLoadPressedListiner = onLoadPressed
    private var sceneListener: OnScenePressedListener = onScenePressed
    private var jsonHelper: JsonHelper = JsonHelper()
    private var deviceHandelr: DeviceHandler = DeviceHandler()
    private var allDevicesList = ArrayList<Device>()
    private val allScenesList = ArrayList<Scenes>()
    private var deviceAdapter: DevicesAdapter? = null
    private var sceneAdapter: RoomScenesAdapter? = null
    var usertype = true
    var scenesList = ArrayList<Scenes>()
    private var IP = IpHandler()
    private val localSqlUtils = UtilsTable()
    private var mDbUtils: SQLiteDatabase? = null
    var devicesList: MutableList<Device> = ArrayList()
    var deviceError: Int = -1
    var checkInternetFlag = false


    fun updateScene(scene: Scenes) {
        for (scenes in allScenesList) {
            if (scenes.title == scene.title)
                scenes.isOn = scene.isOn
        }
        sceneAdapter!!.notifyDataSetChanged()
    }

    fun updateAppearance(IpListDevices: MutableList<IpModel>) {
        for (ip in IpListDevices) {
            if (ip.room == roomsList[0].roomName) {
                for (device in devicesList!!) {
                    if (device.deviceName == ip.name) {
                        if (ip.condition[device.index] == "fail") {
                            device.status = "off"
                        } else if ((ip.condition[device.index] == "update") and (deviceError == 0)) {
                            device.status = "update"
                            device.devicePresent = true
                        } else if ((ip.condition[device.index] == "update") and (deviceError != 0)) {
                            if (ip.aws) {
                                device.status = "update"
                                device.devicePresent = true
                            } else {
                                device.status = "update"
                                device.devicePresent = true
                            }
                        } else if (ip.condition[device.index] == "ready") {
                            if (ip.local) {
                                device.status = "on"
                            } else {
                                device.status = "cloud"
                            }
                        }
                        device.isTurnOn = ip.state[device.index]
                        device.dimVal = ip.dim[device.index]
                    }
                }
            }
            deviceAdapter!!.notifyDataSetChanged()
        }


        for (scene in scenesList) {
            var sceneTurnedState = true
            for (sRoom in scene.rooms) {
                for (sDevice in sRoom.deviceList) {
                    for (ip in IpListDevices) {
                        if (sDevice.deviceName == ip.name) {
                            if (sDevice.isTurnOn != ip.state[sDevice.index]) {
                                sceneTurnedState = false
                                break
                            }
                        }
                    }
                }
                if (!sceneTurnedState) {
                    break
                }
            }
            scene.isOn = sceneTurnedState
        }
        sceneAdapter!!.notifyDataSetChanged()
    }

    fun updateState(message: String, IpListDevices: MutableList<IpModel>, sceneDeviceList: ArrayList<AuraComplete>): MutableList<IpModel> {

        if (message.contains("ERROR")) {
            deviceError = -1
            var data = message.split(":")
            if (deviceError != 0) {
                for (x in data) {
                    for (l in IpListDevices) {
                        if (l.aws) {
                            for (i in 0..3) {
                                l.condition[i] = "ready"
                            }
                        } else {
                            checkInternet(IpListDevices)
                        }
                        IP.registerIpDevice(l)
                        break
                    }

                }
            }
            if (deviceAdapter != null) {
                deviceAdapter!!.notifyDataSetChanged()
            }
            return IpListDevices
        } else {
            val updatedDevice: AuraSwitch = jsonHelper.deserializeTcp(message)
            deviceError = updatedDevice.error
            when (updatedDevice.type) {
                1 -> {
                    for (l in IpListDevices) {
                        if (l.name == updatedDevice.name) {
                            l.local = true
                            l.aws = false
                            l.state[0] = updatedDevice.state[0] == 1
                            l.state[1] = updatedDevice.state[1] == 1
                            l.state[2] = updatedDevice.state[2] == 1
                            l.state[3] = updatedDevice.state[3] == 1
                            l.dim[0] = updatedDevice.dim[0]
                            l.dim[1] = updatedDevice.dim[1]
                            l.dim[2] = updatedDevice.dim[2]
                            l.dim[3] = updatedDevice.dim[3]
                            for (i in 0..3) {
                                l.condition[i] = "ready"
                            }
                            break
                        }
                    }
                }

                4 -> {
                    if (updatedDevice.error == 1) {
                        for (l in IpListDevices) {
                            if (l.name == updatedDevice.name) {
                                l.local = false
                                l.aws = false
                                for (i in 0..3) {
                                    l.condition[i] = "fail"
                                }
                                break
                            }
                        }
                    } else {
                        for (l in IpListDevices) {
                            if (l.name == updatedDevice.name) {
                                l.local = true
                                l.state[0] = updatedDevice.state[0] == 1
                                l.state[1] = updatedDevice.state[1] == 1
                                l.state[2] = updatedDevice.state[2] == 1
                                l.state[3] = updatedDevice.state[3] == 1
                                l.dim[0] = updatedDevice.dim[0]
                                l.dim[1] = updatedDevice.dim[1]
                                l.dim[2] = updatedDevice.dim[2]
                                l.dim[3] = updatedDevice.dim[3]
                                for (i in 0..3) {
                                    l.condition[i] = "ready"
                                }
                                l.aws = false
                                break
                            }
                        }
                    }
                }

                else -> {
                    for (l in IpListDevices) {
                        if (l.name == updatedDevice.name) {
                            l.state[0] = updatedDevice.state[0] == 1
                            l.state[1] = updatedDevice.state[1] == 1
                            l.state[2] = updatedDevice.state[2] == 1
                            l.state[3] = updatedDevice.state[3] == 1
                            l.dim[0] = updatedDevice.dim[0]
                            l.dim[1] = updatedDevice.dim[1]
                            l.dim[2] = updatedDevice.dim[2]
                            l.dim[3] = updatedDevice.dim[3]
                            l.local = true
                            l.aws = false
                            for (i in 0..3) {
                                l.condition[i] = "ready"
                            }
                            break
                        }
                    }
                }

            }
            if (deviceAdapter != null) {
                deviceAdapter!!.notifyDataSetChanged()
            }
            return IpListDevices
        }
    }

    private fun checkInternet(ipListDevices: MutableList<IpModel>) {

        Thread(Runnable {
            checkInternetFlag = Encryption.isInternetWorking()
            if (checkInternetFlag) {
                try{
                    for(l in ipListDevices){
                        l.local = false
                        for (i in 0..3) {
                            l.condition[i] = "update"
                        }
                    }
                }catch (e: Exception){
                    Log.d("Error", "Check Internet connection")
                }
            } else {
                context!!.runOnUiThread {
                    for (l in ipListDevices) {
                        l.local = false
                        l.aws = false
                        for (i in 0..3) {
                            l.condition[i] = "fail"
                        }
                        IP.registerIpDevice(l)
                        break
                    }
                }
            }
        }).start()
    }

    fun updateStateFromAws(device: String, data: AwsState, IpListDevices: MutableList<IpModel>): MutableList<IpModel> {

        if (data.led != 0) {
            var flag = false
            for (l in IpListDevices) {
                if (l.name == device) {
                    if (data.uiud != null) {
                        if (data.uiud == l.uiud) {
                            if (!l.local) {
                                l.aws = true
                                for (i in 0..3) {
                                    l.condition[i] = "ready"
                                }
                                l.dim[0] = data.dim["d0"]!!
                                l.dim[1] = data.dim["d1"]!!
                                l.dim[2] = data.dim["d2"]!!
                                l.dim[3] = data.dim["d3"]!!
                                for (i in 0..3) {
                                    l.state[i] = data.state["s$i"] == 1
                                    l.failure[i] = 0
                                    l.curn_load[i] = false
                                }
                            }
                        }
                    }
                    break
                }
            }
        } else {
            for (l in IpListDevices) {
                if (l.name == device) {
                    if (!l.local) {
                        if(!l.aws){
                            for (i in 0..3) {
                                if (!l.curn_load[i]) {
                                    l.failure[i] = l.failure[i] + 1
                                    if (l.failure[i] >= 2) {
                                        l.aws = false
                                        if (!l.local) {
                                            for (i in 0..3) {
                                                l.condition[i] = "fail"
                                            }
                                        }
                                        for (j in 0..3) {
                                            l.failure[j] = 0
                                        }
                                    }
                                } else {
                                    if (l.curn_load[i]) {
                                        l.failure[i] = l.failure[i] + 1
                                        if (l.failure[i] >= 2) {
                                            l.aws = false
                                            if (!l.local) {
                                                for (i in 0..3) {
                                                    l.condition[i] = "fail"
                                                }
                                            }
                                            for (j in 0..3) {
                                                l.failure[j] = 0
                                            }
                                        }
                                        l.curn_load[i] = false

                                    }
                                }
                            }
                        }
                    }
                    break
                }
            }
        }
        deviceAdapter!!.notifyDataSetChanged()
        return IpListDevices
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_room, parent, false)

        val dbUtils = UtilsDbHelper(context!!)
        mDbUtils = dbUtils.writableDatabase
        var listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")
        for (x in listRoom) {
            if (x.name == Constant.HOME) {
                if (x.sharedHome == "guest") {
                    usertype = false
                }
            }
        }

        val dbScene = SceneDbHelper(parent!!.context)
        mDbScene = dbScene.writableDatabase

        val dbHelper = DeviceDbHelper(parent.context)
        mDb = dbHelper.writableDatabase

        return RoomViewHolder(view)
    }

    override fun onBindViewHolder(holder: RoomViewHolder, position: Int) {
        holder?.bind(roomsList[position], optionerListener, deviceOptionerListener, listener, position)

    }

    override fun getItemCount(): Int {
        return roomsList.size
    }

    inner class RoomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // var room_back = itemView.findViewById<ImageView>(R.id.room_back)

        fun bind(room: Room, optionerListener: OnOptionsListener?, deviceOptionerListener: OnDeviceOptionsListener?, listener: (Int, Int) -> Unit, position: Int) = with(itemView) {
            roomTitleTv.text = room.roomName
            val sceneList = localSqlScene.getSceneForRoom(mDbScene!!, Constant.HOME!!, room.roomName!!)

            for (scene in sceneList) {
                allScenesList.add(Scenes(scene.name, scene.icon, scene.room, false))
            }

            scenesList = allScenesList

            val listOfDevices: ArrayList<AuraSwitch> = localSqlDatabase.getDevicesForRoom(mDb!!, Constant.HOME!!, room.roomName!!)

            var noDeviceFlag = false
            var ipList = IP.getIpDevices()
            if (listOfDevices.isEmpty()) noDeviceFlag = true
            else {
                for (device in listOfDevices) {
                    for (l in ipList) {
                        if (device.name == l.name) {
                            for (i in 0..3) {
                                val load = Device(device.loads[i].icon!!, l.state[i]!!, l.dim[i], device.loads[i].name!!, localSqlDatabase.getRoomForDevice(mDb!!, device.name), device.name, device.loads[i].index!!, device.loads[i].dimmable!!)
                                if (l.condition[i] == "ready") {
                                    if (l.local) {
                                        load.status = "on"
                                    } else {
                                        load.status = "cloud"
                                    }
                                } else {
                                    load.status = "update"
                                }
                                allDevicesList.add(load)
                            }
                        }

                    }
                }
                devicesList = allDevicesList.toMutableList()
            }

            btn_add_accessory.setOnClickListener {
                if (usertype) {
                    context.startActivity<WifiSettingsActivity>()
                } else {
                    Toast.makeText(context, "Guest cannot add Scene To Share Room", Toast.LENGTH_SHORT).show()
                }

            }

            if (sceneList.isEmpty()) btn_add_scene.visibility = View.VISIBLE

            btn_add_scene.setOnClickListener {

                if (usertype) {

                    val intent = Intent(context, CreateSceneActivity::class.java)
                    intent.putExtra("inputSceneType", "create")
                    intent.putExtra("inputSceneName", "")
                    intent.putExtra("inputSceneIconUrl", 0)
                    context.startActivity(intent)
                } else {
                    Toast.makeText(context, "Guest cannot add Scene To Share Room", Toast.LENGTH_SHORT).show()
                }
            }

            sceneAdapter = RoomScenesAdapter(scenesList, optionerListener) { scenes: Scenes, isLongPressed: Boolean ->
                if (isLongPressed) {
                    if (usertype) {
                        dialogueEditRoomScene(scenes)
                    } else {
                        Toast.makeText(context, "Guest cannot edit scene", Toast.LENGTH_SHORT).show()
                    }
                } else sceneListener.onScenePressed(scenes)
            }
            scenesRv.adapter = sceneAdapter

            if (noDeviceFlag) {
                viewAllDevicesTv.visibility = View.INVISIBLE
                btn_add_accessory.visibility = View.VISIBLE
            } else {
                viewAllDevicesTv.visibility = View.INVISIBLE
                btn_add_accessory.visibility = View.INVISIBLE

                deviceAdapter = DevicesAdapter(allDevicesList, room.roomName!!, deviceOptionerListener) { device: Device, isLongPressed: Boolean ->
                    if (isLongPressed) {
                        if (device.dimmable == false) {
                            val gson = Gson()
                            context.startActivity<EditLoadActivity>("DEVICE" to gson.toJson(device))
                        } else openDimmingDialog(context, device)
                    } else {
                        loadListener.onLoadPressed(device, false)
                    }

                }

            }
            scenesRv.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
            val spacing = resources.getDimensionPixelSize(R.dimen.uniform_half_spacing)
            fudRv.adapter = deviceAdapter
            fudRv.layoutManager = GridAutoFitLayoutManager(context, resources.getDimensionPixelSize(R.dimen.device_item_size))
            fudRv.setHasFixedSize(true)
            fudRv.addItemDecoration(GridListSpacingItemDecoration(spacing))

            viewAllScenesTv.visibility = View.INVISIBLE
            if (!noDeviceFlag)
                viewAllDevicesTv.visibility = View.INVISIBLE
            else
                viewAllDevicesTv.visibility = View.INVISIBLE

            viewAllScenesTv.text = resources.getString(R.string.view_all_text)
            viewAllScenesTv.setOnClickListener {
                /*    if (viewAllScenesTv.text.toString().trim() == resources.getString(R.string.view_all_text)) {
                        if (allScenesList.size > 2)
                            (0..1).mapTo(scenesList) { allScenesList[it] }
                        else scenesList = allScenesList
                        viewAllScenesTv.text = resources.getString(R.string.show_less_text)
                        scenesRv.adapter.notifyDataSetChanged()
                    } else {
                        viewAllScenesTv.text = resources.getString(R.string.view_all_text)
                        if (allScenesList.size > 2)
                            (0..1).mapTo(scenesList) { allScenesList[it] }
                        else scenesList = allScenesList
                        scenesRv.adapter.notifyDataSetChanged()
                    }
    */
                listener(prevPosition, position)
                prevPosition = position
            }

            // if (allDevicesList.size < 6) viewAllDevicesTv.visibility = View.INVISIBLE
            // if (allScenesList.size < 2) viewAllScenesTv.visibility = View.INVISIBLE

            viewAllDevicesTv.text = resources.getString(R.string.view_all_text)
            viewAllDevicesTv.setOnClickListener {
                if (viewAllDevicesTv.text.toString().trim() == resources.getString(R.string.view_all_text)) {
                    devicesList.clear()
                    val dummyDevice: ArrayList<Device> = ArrayList()
                    for (devices in allDevicesList) {
                        if (devices.roomName == room.roomName)
                            dummyDevice.add(devices)
                    }
                    devicesList.addAll(dummyDevice)
                    viewAllDevicesTv.text = resources.getString(R.string.show_less_text)
                    fudRv.adapter!!.notifyDataSetChanged()
                } else {
                    viewAllDevicesTv.text = resources.getString(R.string.view_all_text)
                    devicesList.clear()
                    if (allDevicesList.size > 6)
                        (0..5).mapTo(devicesList) { allDevicesList[it] }
                    else devicesList = allDevicesList.toMutableList()
                    fudRv.adapter!!.notifyDataSetChanged()
                }
                listener(prevPosition, position)
                prevPosition = position
            }
        }
    }

    fun checkDuplicate(load: Device): Boolean {
        for (device in allDevicesList) {
            if (device.deviceName == load.deviceName && device.name == load.name) {
                device.dimVal = load.dimVal
                device.isTurnOn = load.isTurnOn
                device.status = load.status
                return true
            }
        }
        return false
    }

    interface OnLoadPressedListiner {
        fun onLoadPressed(auraDevice: Device, longPressed: Boolean)
    }

    private fun dialogueEditRoomScene(scenes: Scenes) {
        var dialogue = Dialog(context)
        val sceneDynamoDb = RulesTableHandler()
        dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogue.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogue.setContentView(R.layout.dialogue_edit_home)
        dialogue.tv_title.text = "Edit Room Scenes"
        dialogue.btn_edit.setOnClickListener {
            val intent = Intent(context, CreateSceneActivity::class.java)
            intent.putExtra("inputSceneType", "edit")
            intent.putExtra("inputSceneName", scenes.title)
            intent.putExtra("inputSceneIconUrl", scenes.iconUrl)
            context.startActivity(intent)
        }
        dialogue.btn_delete_home.setOnClickListener {
            val dbScene = SceneDbHelper(context!!)
            mDbScene = dbScene.writableDatabase
            // val intent = Intent()
            var sceneNameOld = scenes.title
            localSqlScene.deleteScene(mDbScene!!, sceneNameOld.toString(), Constant.HOME!!)
            deleteSceneFromLoads(sceneNameOld.toString())
            thread {
                sceneDynamoDb.deleteUserScene(Constant.IDENTITY_ID!!, Constant.HOME!!, sceneNameOld.toString())
            }
            val intent = Intent(context, DashboardActivity::class.java)
            context.startActivity(intent)
            Toast.makeText(context, "Scene deleted", Toast.LENGTH_SHORT).show()
        }
        dialogue.show()
    }

    fun deleteSceneFromLoads(scene: String) {
        var roomsList: MutableList<RoomModel> = ArrayList()
        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase
        val dummyRooms = localSqlDatabase.getRooms(mDb!!, Constant.HOME!!)
        for (x in dummyRooms) {
            val room = RoomModel()
            room.name = x.roomName
            roomsList.add(room)
        }
        if (!roomsList.isEmpty()) {
            for (x in roomsList) {
                val devices = localSqlDatabase.getDevicesForRoom(mDb!!, Constant.HOME!!, x.name!!)
                //NEWSCENES delete all scenes
            }
        }
    }

    interface OnScenePressedListener {
        fun onScenePressed(scene: Scenes)
    }

    private fun openDimmingDialog(context: Context, auraDevice: Device) {
        val dialog = Dialog(context)
        var dimVal = auraDevice.dimVal
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_configure_edit_dimming)
        if (auraDevice.name == "Fan" && auraDevice.isTurnOn) {
            Glide.with(context).load(Utils.getIconDrawable(auraDevice.type, auraDevice.isTurnOn))
                    .into(dialog.iconDevice)
        } else {
            dialog.iconDevice.setImageResource(Utils.getIconDrawable(auraDevice.type, auraDevice.isTurnOn))
        }
        dialog.tvDialogTitle.text = String.format(context.getString(R.string.text_configure_device_dim), auraDevice.name)
        dialog.rangeBarDim.setOnRangeBarChangeListener { _, _, _, _, rightPinValue ->
            dimVal = Integer.parseInt(rightPinValue)
        }
        dialog.sickbar.setProgress(dimVal.toFloat())
        dialog.sickbar.setOnSeekChangeListener(object : IndicatorSeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar?, thumbPosOnTick: Int) {
            }

            override fun onSectionChanged(seekBar: IndicatorSeekBar?, thumbPosOnTick: Int, textBelowTick: String?, fromUserTouch: Boolean) {
            }

            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar?) {
            }

            override fun onProgressChanged(seekBar: IndicatorSeekBar?, progress: Int, progressFloat: Float, fromUserTouch: Boolean) {
                dimVal = progress
                auraDevice.dimVal = dimVal
                loadListener.onLoadPressed(auraDevice, true)
            }
        })

        dialog.btnEdit.setOnClickListener {
            if (usertype) {
                val gson = Gson()
                var intent = Intent(context, EditLoadActivity::class.java)
                intent.putExtra("DEVICE", gson.toJson(auraDevice))
                intent.putExtra("START_ROOM", "RoomDevice")
                context.startActivity(intent)

                // context.startActivity<EditLoadActivity>("DEVICE" to gson.toJson(auraDevice))
            } else {
                Toast.makeText(context, "Guest can not edit.", Toast.LENGTH_SHORT).show()
            }
        }
        dialog.btnDone.setOnClickListener {
           // loadListener.onLoadPressed(auraDevice, true)
            dialog.cancel()
        }
        dialog.show()
    }

}

