package com.wozart.aura.aura.ui.dashboard.rooms

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.wozart.aura.R
import com.wozart.aura.aura.data.model.Wallpaper
import com.wozart.aura.data.model.Room
import com.wozart.aura.aura.ui.adapter.RoomWallpaperAdapter
import com.wozart.aura.aura.ui.adapter.WallpaperAdapter
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.dynamoDb.DeviceTableHandler
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.dynamoDb.UserTableHandler
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.adapter.RoomAdapter
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.DialogListener
import com.wozart.aura.utilities.Encryption
import kotlinx.android.synthetic.main.activity_room_wallpaper.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import java.io.File
import kotlin.concurrent.thread


class AddRoomActivity : AppCompatActivity(), WallpaperAdapter.WallpaperListener, RoomAdapter.RoomListener {

    private val localSqlDatabase = DeviceTable()
    private val localSqlDatabaseScene = SceneTable()
    private var mDb: SQLiteDatabase? = null
    private var mdbScene: SQLiteDatabase? = null

    override fun onAddRoomClicked() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRoomClicked() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var room_icons = arrayOf(R.drawable.ic_living_room_off)
    var wallpaperList: MutableList<Wallpaper> = ArrayList()
    var wallPaperAdapter: WallpaperAdapter? = null
    private var roomWallpaper: RoomWallpaperAdapter? = null
    var roomList: MutableList<Room> = ArrayList()
    private var wallpaperIndex: Int = 0
    private var roomIconIndex: Int = 0
    private var editRoomType: String? = null
    private var editRoomName: String? = null
    private var rulesTableDo = RulesTableHandler()
    private val localSqlUtils = UtilsTable()
    var mDbUtils: SQLiteDatabase? = null
    var homeDetails: MutableList<RoomModelJson> = java.util.ArrayList()
    var editableRoomlist: MutableList<RoomModelJson> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_room_wallpaper)
        secondBar.visibility = View.INVISIBLE

        editRoomType = intent.getStringExtra("ROOM_EDIT_TYPE")
        editRoomName = intent.getStringExtra("NAME_ROOM")

        val dbUtils = UtilsDbHelper(this)
        mDbUtils = dbUtils.writableDatabase

        val dbHelper = DeviceDbHelper(this)
        mDb = dbHelper.writableDatabase

        val dbhelper = SceneDbHelper(this)
        mdbScene = dbhelper.writableDatabase

        homeDetails = localSqlUtils.getHomeData(mDbUtils!!, "home")
        for (dummy in homeDetails) {
            if(dummy.type == "room"){
                if (dummy.name == editRoomName) {
                    wallpaperIndex = dummy.bgUrl.toInt()
                    selected_wallpaper.setImageResource(Utils.getRoomWallpaperDrawables(wallpaperIndex))
                    Utils.setRoomDrawable(this, activity_main_room, wallpaperIndex)
                    roomIconIndex = dummy.roomIcon
                    break
                }
            }

        }

        btn_delete_room.setOnClickListener {

            if (editRoomName == null) {
                Utils.showCustomDialogOk(this, this.getString(R.string.delete_room), this.getString(R.string.dialog_delete_room_name), R.layout.dialog_layout, object : DialogListener {
                    override fun onOkClicked() {}
                    override fun onCancelClicked() {}
                })
            } else {
                var flag = true
                for (dummy in homeDetails) {
                    if(dummy.type == "room"){
                        if ((dummy.name == editRoomName) and (dummy.sharedHome == "default")) {
                            flag = false
                        }
                    }
                }
                if (flag) {
                    Utils.showCustomDialogOk(this, "Delete Room", this.getString(R.string.dialog_delete_room), R.layout.dialog_layout, object : DialogListener {
                        override fun onOkClicked() {
                            deleteRoom()
                        }

                        override fun onCancelClicked() {}
                    })
                } else {
                    longToast("Default room cannot be deleted.")
                }
            }
        }

        init()
        initializeRoom()
    }

    fun deleteRoom() {
        val listOfDevices: ArrayList<AuraSwitch> = localSqlDatabase.getDevicesForRoom(mDb!!, Constant.HOME!!,editRoomName!!)
        if(listOfDevices.size > 0){
            toast("Device having devices")
        }else{
            var removeIndexCount = 0
            var removeIndex = 1
            for (dummy in homeDetails) {
                if(dummy.type == "room"){
                    if ((dummy.name == editRoomName) and (dummy.sharedHome == Constant.HOME)) {
                        removeIndex = removeIndexCount
                    }
                    removeIndexCount++
                }else{
                    removeIndexCount++
                }
            }
            homeDetails.removeAt(removeIndex)
            localSqlUtils.replaceHome(mDbUtils!!, "home", homeDetails)
            thread {
                var userId = Constant.IDENTITY_ID
                if(Encryption.isInternetWorking()){
                    rulesTableDo.homeSQLtoDYNAMO(homeDetails, userId!!)
                }else{
                    runOnUiThread {
                        Toast.makeText(this,"Check Internet connection",Toast.LENGTH_SHORT).show()
                    }
                }
                runOnUiThread {
                    //progressBar.visibility = View.INVISIBLE
                    val intent = Intent(applicationContext, DashboardActivity::class.java)
                    intent.putExtra("TAB_SET", Constant.ROOMS_TAB)
                    this.startActivity(intent)
                    this.finish()
                }
            }
        }

    }

    fun initializeWallPaperAdapter() {
        val drawables: IntArray = intArrayOf(R.drawable.eleven_new, R.drawable.twelve_new, R.drawable.one_new, R.drawable.two_new, R.drawable.three_new, R.drawable.four_new, R.drawable.five_new, R.drawable.six_new, R.drawable.seven_new, R.drawable.eight_new, R.drawable.nine_new, R.drawable.ten_new)
        for (i in 0 until drawables.size) {
            val wallpaper = Wallpaper()
            wallpaper.id = drawables[i]
            wallpaper.isSelected = false
            if (editRoomType == "create") {
                if (i == Utils.getWallpaperId(this))
                    wallpaper.isSelected = true

            } else {
                if (i == wallpaperIndex)
                    wallpaper.isSelected = true
            }

            wallpaperList.add(wallpaper)
        }

        wallPaperAdapter = WallpaperAdapter(this, this, wallpaperList)
        wallpaper_room_grid.isNestedScrollingEnabled = false
        wallpaper_room_grid.layoutManager = GridLayoutManager(this, 3)
        wallpaper_room_grid.adapter = wallPaperAdapter
    }

    private fun initializeRoom() {
        for (i in room_icons.indices) {
            var room = Room()
            room.url = room_icons[i]

            room.isSelected = false
            if (i == roomIconIndex)
                room.isSelected = true
            else
                room.isSelected = false

            roomList.add(room)
        }

        roomWallpaper = RoomWallpaperAdapter(this, roomList) { pos: Int, Boolean ->

        }
//        rooms_grid.layoutManager = GridLayoutManager(this, 3)
//        rooms_grid.adapter = roomWallpaper
        initializeWallPaperAdapter()
    }

    private fun init() {
        var listRoom: MutableList<RoomModelJson> = ArrayList()
        if (editRoomType == "edit") {
            input_room_name.setText(editRoomName)
            btn_delete_room.visibility = View.VISIBLE

            //  Utils.setDrawable(this, activity_main_room, editRoomName!!)
        } else {
            btn_delete_room.visibility = View.GONE

            listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")

            for (x in listRoom) {
                if (x.name == Constant.HOME) {

                    Utils.setDrawable(this, activity_main_room, x.bgUrl.toInt())

                }

            }
            //Utils.setDrawable(this, activity_main_room)
        }
        //text_room_pick.visibility = View.VISIBLE
        wallpaper_room_grid.visibility = View.VISIBLE
        //tvTitle.text = getString(R.string.title_room_details)
        tvTitle.text = editRoomName
        // tvNext.text = getString(R.string.text_save)
        //tvNext.setTextColor(Color.WHITE)
        home.setOnClickListener {
            this.finish()
        }
        btn_cancel.setOnClickListener {
            this.finish()
        }

        tvNext.setOnClickListener {
            secondBar.visibility = View.VISIBLE

            if (editRoomType == "edit") {
                if(editableRoomlist.size > 0 ){

                }else{
                    for (dummy in homeDetails) {
                        if(dummy.type == "room"){
                            if((dummy.name == input_room_name.text.toString()) and (dummy.sharedHome == Constant.HOME)){
                                Toast.makeText(this, "Room already Exist with same name", Toast.LENGTH_SHORT).show()
                                dummy.bgUrl = wallPaperAdapter!!.getWallPaperId().toString()
                                dummy.roomIcon = 0
                            }else if((dummy.name == input_room_name.text.toString()) and (dummy.sharedHome == "default")){
                                longToast("Default room cannot be edited.")
                                dummy.bgUrl = wallPaperAdapter!!.getWallPaperId().toString()
                                dummy.roomIcon = 0
                            }else {
                                if(dummy.name == input_room_name.text.toString()){
                                    Toast.makeText(this, "Room already Exist with same name", Toast.LENGTH_SHORT).show()
                                    dummy.bgUrl = wallPaperAdapter!!.getWallPaperId().toString()
                                    dummy.roomIcon = 0
                                }else{
                                    var flag = false
                                    if(dummy.name == editRoomName){
                                        for(roomAvailable in editableRoomlist){
                                            if(roomAvailable.name == editRoomName){
                                                flag = true
                                                break
                                            }
                                        }
                                        if(!flag){
                                            dummy.name = input_room_name.text.toString()
                                            dummy.sharedHome = Constant.HOME!!
                                            dummy.bgUrl = wallPaperAdapter!!.getWallPaperId().toString()
                                            dummy.roomIcon = 0
                                        }
                                    }
                                }
                            }
//                        if ((dummy.name == editRoomName) and (dummy.sharedHome == Constant.HOME)) {
//                            if (editRoomName == input_room_name.text.toString()) {
//                                Toast.makeText(this, "Room already Exist with same name", Toast.LENGTH_SHORT).show()
//                                dummy.bgUrl = wallPaperAdapter!!.getWallPaperId().toString()
//                                dummy.roomIcon = 0
//                            } else {
//                                if((input_room_name.text.toString() == "Living Room") || (input_room_name.text.toString() == "Bedroom") || (input_room_name.text.toString() == "Dining Room") || (input_room_name.text.toString() == "Bathroom")){
//                                    Toast.makeText(this, "Default room already exist.", Toast.LENGTH_SHORT).show()
//                                }else{
//                                    dummy.name = input_room_name.text.toString()
//                                    dummy.bgUrl = wallPaperAdapter!!.getWallPaperId().toString()
//                                    dummy.roomIcon = 0
//                                }
//                            }
//
//                        } else if ((dummy.name == editRoomName) and (dummy.sharedHome == "default")) {
//                            if (editRoomName == input_room_name.text.toString()) {
//                                longToast("Default room cannot be edited.")
//                                dummy.bgUrl = wallPaperAdapter!!.getWallPaperId().toString()
//                                dummy.roomIcon = 0
//                            } else {
//                                dummy.name = input_room_name.text.toString()
//                                dummy.sharedHome = Constant.HOME!!
//                                dummy.bgUrl = wallPaperAdapter!!.getWallPaperId().toString()
//                                dummy.roomIcon = 0
//                            }
//                        }

                        }

                        editableRoomlist.add(dummy)

                    }
                }


                if((input_room_name.text.toString() == "Living Room") || (input_room_name.text.toString() == "Bedroom") || (input_room_name.text.toString() == "Dining Room") || (input_room_name.text.toString() == "Bathroom")){

                }else{
                    if((editRoomName == "Living Room") || (editRoomName == "Bedroom") || (editRoomName == "Dining Room") || (editRoomName == "Bathroom")){
                        var flag = false
                        for(roomAvailable in editableRoomlist){
                            if(roomAvailable.name == editRoomName){
                                flag = true
                                break
                            }
                        }
                        if(!flag){
                            var new_room = RoomModelJson(editRoomName!!, "room", "default", "0", 0)
                            editableRoomlist.add(new_room)
                        }
                    }

                }


                localSqlUtils.replaceHome(mDbUtils!!, "home", editableRoomlist)
                localSqlDatabase.updateRoom(mDb!!, editRoomName!!, input_room_name.text.toString())
                localSqlDatabaseScene.updateRoom(mdbScene!!, editRoomName!!, input_room_name.text.toString())

                thread {
                    var userId = Constant.IDENTITY_ID
                    rulesTableDo.homeSQLtoDYNAMO(editableRoomlist, userId!!)
                    runOnUiThread {
                        //progressBar.visibility = View.INVISIBLE
                        val intent = Intent(applicationContext, DashboardActivity::class.java)
                        intent.putExtra("TAB_SET", Constant.ROOMS_TAB)
                        this.startActivity(intent)
                        this.finish()
                    }
                }

                wallPaperAdapter.let {
                    Utils.saveWallPaperId(applicationContext, wallPaperAdapter!!.getWallPaperId(), input_room_name.text.toString())
                }


            } else {
                var index: Int = 0
                var text = input_room_name.text.toString()
                if (text.isNullOrEmpty()) {
                    toast("Empty name is not allowed")
                } else {
                    wallPaperAdapter.let {
                        Utils.saveWallPaperId(applicationContext, wallPaperAdapter!!.getWallPaperId(), text)
                    }
                    var flagExists: Int = 0
                    for (dummy in homeDetails) {
                        if(dummy.type == "room"){
                            if ((dummy.name == text) and (dummy.sharedHome == Constant.HOME)) {
                                flagExists = 1
                                break
                            } else if ((dummy.name == text) and (dummy.sharedHome == "default")) {
                                flagExists = 1
                                break
                            }
                        }
                    }
                    if (flagExists == 0) {
                        var home = Constant.HOME
                        var new_room = RoomModelJson(text, "room", home!!, wallPaperAdapter!!.getWallPaperId().toString(), 0)
                        homeDetails.add(new_room)
                        localSqlUtils.replaceHome(mDbUtils!!, "home", homeDetails)
                        thread {
                            var userId = Constant.IDENTITY_ID
                            if(Encryption.isInternetWorking()){
                                rulesTableDo.homeSQLtoDYNAMO(homeDetails, userId!!)
                            }else{
                                runOnUiThread {
                                    Toast.makeText(this,"Check Internet connection",Toast.LENGTH_SHORT).show()
                                }
                            }
                            runOnUiThread {
                                //progressBar.visibility = View.INVISIBLE
                                val intent = Intent(applicationContext, DashboardActivity::class.java)
                                intent.putExtra("TAB_SET", Constant.ROOMS_TAB)
                                this.startActivity(intent)
                                this.finish()
                            }
                        }
                    } else {
                        Toast.makeText(this, "Room already Exist with same name", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }


    override fun onWallpaperSelect(wallpaerId: Int) {
        Utils.setRoomDrawable(this, activity_main_room, wallpaerId)
        selected_wallpaper.setImageResource(Utils.getRoomWallpaperDrawables(wallpaerId))

    }


    private fun addWallPaper(file: File?) {
        Uri.fromFile(file)
        val wallpaper = Wallpaper()
        wallpaper.url = Uri.fromFile(file).toString()
        wallpaper.isSelected = false
        wallpaperList.add(wallpaper)
        wallPaperAdapter?.update(wallpaperList)

    }

    override fun onBackPressed() {
        super.onBackPressed()
//        val intent=Intent(this,DashboardActivity::class.java)
//        startActivity(intent)
        this.finish()
    }
}
