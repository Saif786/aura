package com.wozart.aura.ui.dashboard.room


import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.support.v7.widget.CardView
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SwitchCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import com.wozart.aura.R
import com.wozart.aura.aura.ui.dashboard.rooms.AddRoomActivity
import com.wozart.aura.data.device.IpHandler
import com.wozart.aura.data.model.IpModel
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.list_room_view.view.*

/***************************************************************************
 * File Name :
 * Author : Saif
 * Date of Creation : 09/08/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0.2
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class RoomAdapter(private val context: Context, onAllDeviceChecked: OnAllDevice, offAllDeviceChecked: OffAllDevice, private val roomModel: ArrayList<RoomModel>) : RecyclerView.Adapter<RoomAdapter.RoomsViewHolder>() {

    var usertype = true
    private val localSqlUtils = UtilsTable()
    private var mDbUtils: SQLiteDatabase? = null
    var listRoom: MutableList<RoomModelJson> = ArrayList()
    var IpListDevices: MutableList<IpModel> = ArrayList()
    var ondeviceListner: OnAllDevice = onAllDeviceChecked
    var offDeviceListner: OffAllDevice = offAllDeviceChecked
    private var mDb: SQLiteDatabase? = null
    private var IP = IpHandler()
    var offAll: Boolean = true


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomsViewHolder {
        val v = LayoutInflater.from(parent!!.context).inflate(R.layout.list_room_view, parent, false)

        val dbUtils = UtilsDbHelper(context!!)
        mDbUtils = dbUtils.writableDatabase
        IpListDevices = IP.getIpDevices()

        listRoom = localSqlUtils.getHomeData(mDbUtils!!, "home")

        for (x in listRoom) {
            if (x.name == Constant.HOME) {
                if (x.sharedHome == "guest") {
                    usertype = false
                }
            }
        }


        return RoomsViewHolder(v)
    }

    override fun getItemCount(): Int {
        return roomModel.size
    }

    override fun onBindViewHolder(holder: RoomsViewHolder, position: Int) {
        val romDetails = roomModel[position]
        holder?.bindRoom(romDetails)

        holder?.roomCardList!!.setOnClickListener {
            val intent = Intent(context, RoomActivity::class.java)
            intent.putExtra("ROOM_NAME", romDetails.name_room)
            context.startActivity(intent)
        }

        for (rooms in roomModel) {
            if (rooms.name_room == romDetails.name_room) {
                if (rooms.room_total_device_count == "0") {
                    holder.text_on_off.text = "All Off"
                    break
                } else {
                    if (rooms.room_deviceCount_on == rooms.room_total_device_count) {
                        holder.on_switch.isChecked = true
                        holder.text_on_off.text = "All On"
                    } else {
                        holder.text_on_off.text = "All Off"
                    }
                }
            }
        }


        holder.on_switch.setOnCheckedChangeListener { button, isChecked ->

            // do whatever you need to do when the switch is toggled here
            if (isChecked) {
                ondeviceListner.onAlldevice(romDetails.name_room, true)
                holder.text_on_off.text = "All On"
            } else {
                offDeviceListner.offAllDevice(romDetails.name_room, false)
                holder.text_on_off.text = "All Off"
            }

        }


    }

    inner class RoomsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val name_room = itemView.findViewById<TextView>(R.id.name_room)
        val roomCardList = itemView.findViewById<CardView>(R.id.roomCardList)
        private var roomView: View = itemView
        val on_switch = itemView.findViewById<Switch>(R.id.on_switch)
        var text_on_off = itemView.findViewById<TextView>(R.id.text_on)


        fun init() {
            itemView.setOnClickListener {
                this
            }

        }

        fun bindRoom(roomModelJson: RoomModel) {
            //roomView.roomImage.setImageResource(roomModelJson.roomIcon)
            roomView.name_room.text = roomModelJson.name_room
            roomView.totalDevicesCountTv?.text = roomModelJson.room_total_device_count
            roomView.devicesOffCountTv?.text = roomModelJson.room_deviceCount_off
            roomView.devicesOnCountTv?.text = roomModelJson.room_deviceCount_on
        }

    }

    private fun showPopup(v: View, name_room: String?) {
        val popup = PopupMenu(context, v)
        val inflater = popup?.menuInflater
        inflater?.inflate(R.menu.menu_rooms, popup.menu)
        popup?.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.action_add_favourite -> {
                    if (usertype) {
                        val intent = Intent(context, AddRoomActivity::class.java)
                        intent.putExtra("NAME_ROOM", name_room)
                        intent.putExtra("ROOM_EDIT_TYPE", "edit")
                        context.startActivity(intent)
                    } else {
                        Toast.makeText(context, "Guest cannot edit Shared Room", Toast.LENGTH_SHORT).show()
                    }

                    true
                }

                else -> false
            }
        }
        popup?.show()
    }

    interface OnAllDevice {
        fun onAlldevice(name_room: String?, b: Boolean)
    }

    interface OffAllDevice {
        fun offAllDevice(name_room: String?, b: Boolean)
    }

}