package com.wozart.aura.ui.adapter

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.preference.PreferenceManager
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread
import com.wozart.aura.R
import com.wozart.aura.aura.ui.dashboard.listener.OnOptionsListener
import com.wozart.aura.aura.utilities.Utils
import com.wozart.aura.data.dynamoDb.DeviceTableHandler
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.dynamoDb.UserTableHandler
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.data.sqlLite.ScheduleTable
import com.wozart.aura.data.sqlLite.UtilsTable
import com.wozart.aura.entity.amazonaws.models.nosql.RulesDO
import com.wozart.aura.entity.amazonaws.models.nosql.UserTableDO
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.entity.sql.scheduling.ScheduleDbHelper
import com.wozart.aura.entity.sql.utils.UtilsDbHelper
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.DialogListener
import kotlinx.android.synthetic.main.activity_home_details.*
import kotlinx.android.synthetic.main.share_guest_layout.view.*
import kotlin.concurrent.thread


/***************************************************************************
 * File Name :
 * Author : Saif
 * Date of Creation : 14/09/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class ShareGuestAdapter(val shareList : MutableList<MutableMap<String,String>>, val optionerListener: OnOptionsListener,var owner:RulesDO):RecyclerView.Adapter<ShareGuestAdapter.ViewHolder>(){

    private var localSqlDevices = DeviceTable()
    private val localSqlSceneDatabase = SceneTable()
    private var mDb: SQLiteDatabase? = null
    private var mDbScene: SQLiteDatabase? = null
    private val deviceDynamoDb = DeviceTableHandler()

    private var mDbSchedule: SQLiteDatabase? = null
    private val localSqlScheduleDatabase = ScheduleTable()

    private val localSqlUtils = UtilsTable()
    var mDbUtils: SQLiteDatabase? = null

    var homeDetails : MutableList<RoomModelJson> = java.util.ArrayList()
    private var rulesTableDo = RulesTableHandler()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShareGuestAdapter.ViewHolder {
        val itemView = LayoutInflater.from(parent?.context)
                .inflate(R.layout.share_guest_layout, parent, false)

        val dbHelper = DeviceDbHelper(itemView.context)
        mDb = dbHelper.writableDatabase

        val dbHelperScene = SceneDbHelper(itemView.context)
        mDbScene = dbHelperScene.writableDatabase

        val dbHelperSchedule = ScheduleDbHelper(itemView.context)
        mDbSchedule = dbHelperSchedule.writableDatabase

        val dbUtils = UtilsDbHelper(itemView.context)
        mDbUtils = dbUtils.writableDatabase


        return ShareGuestAdapter.ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return shareList.size
    }

    override fun onBindViewHolder(holder:ShareGuestAdapter.ViewHolder, position: Int) {

       holder?.bind(position,shareList,optionerListener)

        holder?.itemView?.layout_guest_access?.setOnClickListener {
             var userDynamoDb: UserTableHandler = UserTableHandler()
            var rulesDynamoDb = RulesTableHandler()
            var userType= false
            if(owner != null){
                userType = true
            }
            var dataMessage = "You will no longer have access to "+holder.itemView.homeName.text + "."
            Utils.showCustomDialog(holder.itemView.context, holder.itemView.context.getString(R.string.share_text_invoke),dataMessage , R.layout.dialogue_invoke_guest, object : DialogListener  {
                override fun onOkClicked() {

                    if (userType) {

                       // val name = shareList[position]["Name"]
                        var homeName = shareList[position]["Home"]
                        var homeaccess = shareList[position]["Access"]
                        homeDetails = localSqlUtils.getHomeData(mDbUtils!!, "home")
                        var homeDetailsNew : MutableList<RoomModelJson> = java.util.ArrayList()
                        var guestHomes : MutableList<String> = ArrayList()
                        for (homes in homeDetails) {
                            if((homes.type == "home") and (homes.sharedHome == "guest")){
                                guestHomes.add(homes.name)
                            }
                        }
                        for(item in homeDetails){
                            if(item.type == "home"){
                                if(item.name != homeName){
                                    homeDetailsNew.add(item)
                                }
                            }else if(item.sharedHome != homeName){
                                homeDetailsNew.add(item)
                            }
                        }

                        localSqlUtils.replaceHome(mDbUtils!!,"home",homeDetailsNew)
                        localSqlDevices.deleteHome(mDb!!, homeName!!)
                        localSqlSceneDatabase.deleteHomeScenes(mDbScene!!,homeName!!)
                        localSqlScheduleDatabase.deleteHomeSchedules(mDbSchedule!!,homeName)

                        thread {
                            var userId = Constant.IDENTITY_ID

                            rulesTableDo.deleteGuestAccessFromMaster(homeName,userId!!)

                            runOnUiThread {
                                val prefEditor = PreferenceManager.getDefaultSharedPreferences(it.context).edit()
                                prefEditor.putString("HOME", "My Home")
                                prefEditor.apply()
                                Constant.HOME = "My Home"
                                val intent = Intent(it.context, DashboardActivity::class.java)
                                intent.putExtra("TAB_SET",Constant.MORE_TAB)
                                it.context.startActivity(intent)
                            }
                        }
                    }
                }
                override fun onCancelClicked() {

                }
            })

        }
    }
    class ViewHolder(itemView: View) :  RecyclerView.ViewHolder(itemView){
        fun bind(position: Int, shareList: MutableList<MutableMap<String, String>>, optionerListener: OnOptionsListener)= with(itemView){
            itemView.tv_contact_name.text= shareList[position]["Name"]
            itemView.homeName.text = shareList[position]["Home"]!!.split("?")[0]
        }
    }


}