package com.wozart.aura.ui.createscene

import android.database.sqlite.SQLiteDatabase
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.google.gson.Gson
import com.wozart.aura.R
import com.wozart.aura.aura.ui.setactions.SetActionsFragment
import com.wozart.aura.data.dynamoDb.RulesTableHandler
import com.wozart.aura.data.sqlLite.DeviceTable
import com.wozart.aura.data.sqlLite.SceneTable
import com.wozart.aura.entity.sql.device.DeviceDbHelper
import com.wozart.aura.entity.sql.scenes.SceneDbHelper
import com.wozart.aura.ui.base.basesetactions.BaseSetActionsFragment
import com.wozart.aura.ui.base.basesetactions.SetActionsRoomAdapter
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.DashboardActivity
import com.wozart.aura.utilities.Constant
import kotlinx.android.synthetic.main.layout_header.*
import org.jetbrains.anko.newTask
import org.jetbrains.anko.support.v4.*
import kotlin.concurrent.thread


class SetActionsForSceneFragment : BaseSetActionsFragment() {

    private val localSqlScene = SceneTable()
    private var mDbScene: SQLiteDatabase? = null

    private val localSqlDatabase = DeviceTable()
    private var mDb: SQLiteDatabase? = null
    var roomsList: MutableList<RoomModel> = ArrayList()
    val scheduleDynamoDb = RulesTableHandler()
    private var adapter = SetActionsRoomAdapter()


    companion object {
        private var selectedRoom :MutableList<RoomModel> = ArrayList()
        fun newInstance(room: MutableList<RoomModel>, sceneName: String,sceneNameOld: String, sceneIcon: Int,sceneNameType:String): SetActionsForSceneFragment {
            selectedRoom = room
            BaseSetActionsFragment.rooms = room
            BaseSetActionsFragment.sceneName = sceneName
            BaseSetActionsFragment.sceneIcon = sceneIcon
            BaseSetActionsFragment.sceneNameOld = sceneNameOld
            BaseSetActionsFragment.sceneNameType = sceneNameType
            return SetActionsForSceneFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_select_devices_scene, container, false)
        val btnSceneDelete = view.findViewById<Button>(R.id.btnSceneDelete)
        var listRooms = view.findViewById<RecyclerView>(R.id.listRooms)
        val dbScene = SceneDbHelper(context!!)
        //initialize()
        mDbScene = dbScene.writableDatabase

        if(sceneNameType == "edit"){
            btnSceneDelete.visibility = View.VISIBLE
        }

        btnSceneDelete.setOnClickListener {
            if (sceneNameType == "create"){
                btnSceneDelete.visibility = View.INVISIBLE
                longToast("Scene Not created for Deletion..")
            }else{
                btnSceneDelete.visibility = View.VISIBLE
                localSqlScene.deleteScene(mDbScene!!, sceneNameOld!!,Constant.HOME!!)
                deleteSceneFromLoads(sceneNameOld!!)
                thread {
                    var home = Constant.HOME
                    var userId = Constant.IDENTITY_ID

                    scheduleDynamoDb.deleteUserScene(userId!!,home!!,sceneNameOld!!)
                }
                startActivity(intentFor<DashboardActivity>().newTask())
                toast("Scene Deleted")
            }
        }

        listRooms.layoutManager = LinearLayoutManager(activity)
        listRooms.adapter = adapter
        adapter.init(selectedRoom)

        return view
    }

    fun deleteSceneFromLoads(scene:String){
        val dbHelper = DeviceDbHelper(context!!)
        mDb = dbHelper.writableDatabase
        val dummyRooms = localSqlDatabase.getRooms(mDb!!, Constant.HOME!!)
        for (x in dummyRooms) {
            val room = RoomModel()
            room.name = x.roomName
            roomsList.add(room)
        }
        if (!roomsList.isEmpty()) {
            for (x in roomsList) {
                val devices = localSqlDatabase.getDevicesForRoom(mDb!!,  Constant.HOME!!, x.name!!)
                //NEWSCENES delete all scenes
            }
        }
    }


    override fun onFinish() {

        val sceneTable = SceneTable()
        val deviceTable = DeviceTable()
        val dbHelper = SceneDbHelper(context!!)
        val dbHelperDevice = DeviceDbHelper(context!!)
        val mDbDevice: SQLiteDatabase = dbHelperDevice.writableDatabase
        val mDb: SQLiteDatabase = dbHelper.writableDatabase
        val rooms = selectedRoom
        var roomName : String ?= null
        val roomsForScenes: ArrayList<String> = ArrayList()
        var roomModelForScenes :MutableList<RoomModel> = ArrayList()
        for (room in rooms) {
            if(room.deviceList.size != 0){
                roomModelForScenes.add(room)
                for (device in room.deviceList) {
                    roomName = device.roomName
                    roomsForScenes.add(device.roomName)
                    break
                }
            }

        }
        val name = getSceneName()
        val sceneIcon = getSceneIcon()
        val sceneNameOld = getSceneNameOld()
        val sceneNameType = getSceneNameType()
        val gson = Gson()

        if (sceneTable.insertScene(mDb, name, gson.toJson(roomModelForScenes), roomName!!, Constant.HOME!!, sceneIcon,sceneNameOld,sceneNameType)) {
            thread {
                var home = Constant.HOME
                var userId = Constant.IDENTITY_ID

                scheduleDynamoDb.updateUserScene(userId!!,home!!,name,roomModelForScenes,sceneIcon ,sceneNameType,sceneNameOld)
            }

        } else {
            toast("Scene already present")
        }

        startActivity<DashboardActivity>()
    }

    fun initialize()
    {
        //main_layout.setBackgroundColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
        // tvSelectLoadIcon.setTextColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
        tvTitle.setTextColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
        home.setColorFilter(ContextCompat.getColor(activity!!.baseContext,R.color.black), PorterDuff.Mode.SRC_ATOP)
        tvNext.setTextColor(ContextCompat.getColor(activity!!.baseContext,R.color.white))
    }
}
