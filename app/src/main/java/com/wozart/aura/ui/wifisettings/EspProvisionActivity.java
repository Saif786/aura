package com.wozart.aura.ui.wifisettings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wozart.aura.BuildConfig;
import com.wozart.aura.R;
import com.wozart.aura.espProvision.Provision;
import com.wozart.aura.espProvision.transport.BLETransport;
import com.wozart.aura.ui.auraswitchlist.AuraListActivity;

import java.util.HashMap;
import java.util.UUID;

public class EspProvisionActivity extends AppCompatActivity {

    private static final String TAG = "Espressif::" + EspProvisionActivity.class.getSimpleName();
    private static final String SSID = "Tejonidhi";
    private static final String PASSPHRASE = "Zoey2611";


    private String POP, BASE_URL, NETWORK_NAME_PREFIX, SERVICE_UUID, SESSION_UUID, CONFIG_UUID, DEVICE_NAME_PREFIX;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esp_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textView = (TextView) findViewById(R.id.tvSkip);
        TextView titleText = (TextView) findViewById(R.id.textTitle);
        ImageView back = (ImageView) findViewById(R.id.back);

        titleText.setText("Configure Device");

        setSupportActionBar(toolbar);
        final Activity thisActivity = this;
        Button provision = findViewById(R.id.provision_button);

        final String productDSN = generateProductDSN();

        BASE_URL = getResources().getString(R.string.wifi_base_url);
        NETWORK_NAME_PREFIX = getResources().getString(R.string.wifi_network_name_prefix);
        POP = getResources().getString(R.string.proof_of_possesion);
        SERVICE_UUID = getResources().getString(R.string.ble_service_uuid);
        SESSION_UUID = getResources().getString(R.string.ble_session_uuid);
        CONFIG_UUID = getResources().getString(R.string.ble_config_uuid);
        DEVICE_NAME_PREFIX = getResources().getString(R.string.ble_device_name_prefix);

        final String transportVersion, securityVersion;

        if (BuildConfig.FLAVOR_security.equals("sec1")) {
            securityVersion = Provision.CONFIG_SECURITY_SECURITY1;
        } else {
            securityVersion = Provision.CONFIG_SECURITY_SECURITY0;
        }

        if (BuildConfig.FLAVOR_transport.equals("wifi")) {
            transportVersion = Provision.CONFIG_TRANSPORT_BLE;
        } else {
            transportVersion = Provision.CONFIG_TRANSPORT_WIFI;
        }

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),AuraListActivity.class);
                startActivity(intent);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                thisActivity.finish();
            }
        });

        provision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> config = new HashMap<>();
                config.put(Provision.CONFIG_TRANSPORT_KEY, transportVersion);
                config.put(Provision.CONFIG_SECURITY_KEY, securityVersion);

                config.put(Provision.CONFIG_PROOF_OF_POSSESSION_KEY, POP);
                config.put(Provision.CONFIG_BASE_URL_KEY, BASE_URL);
                config.put(Provision.CONFIG_WIFI_AP_KEY, NETWORK_NAME_PREFIX);

                config.put(BLETransport.SERVICE_UUID_KEY, SERVICE_UUID);
                config.put(BLETransport.SESSION_UUID_KEY, SESSION_UUID);
                config.put(BLETransport.CONFIG_UUID_KEY, CONFIG_UUID);
                config.put(BLETransport.DEVICE_NAME_PREFIX_KEY, DEVICE_NAME_PREFIX);
                Provision.showProvisioningUI(thisActivity, config);
            }
        });
    }

    private void toggleFormState(boolean isEnabled) {
        final Button provision = findViewById(R.id.provision_button);
        final View loadingIndicator = findViewById(R.id.progress_indicator);
        loadingIndicator.setVisibility(isEnabled ? View.GONE : View.VISIBLE);

        provision.setEnabled(isEnabled);
    }

    private String generateProductDSN() {
        return UUID.randomUUID().toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Provision.REQUEST_PROVISIONING_CODE &&
                resultCode == RESULT_OK) {
            setResult(resultCode);
            finish();
        }
    }

    private void goToSuccessPage(String statusText) {
        Intent goToSuccessPage = new Intent(getApplicationContext(), ProvisionSuccessActivity.class);
        goToSuccessPage.putExtra("status", statusText);
        startActivity(goToSuccessPage);
        this.setResult(RESULT_OK);
        this.finish();
    }
}

