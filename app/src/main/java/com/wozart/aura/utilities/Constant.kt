package com.wozart.aura.utilities

import android.transition.Scene
import com.wozart.aura.entity.sql.device.DeviceContract
import com.wozart.aura.entity.sql.scenes.SceneContract
import com.wozart.aura.entity.sql.scheduling.ScheduleContract
import com.wozart.aura.entity.sql.utils.UtilsContract
import java.nio.charset.Charset

class Constant {
    companion object {
        //User's and Device Constants
        var EMAIL: String ?= null
        var IDENTITY_ID: String? = null
        var USERNAME: String? = null
        var HOME: String? = null
        val CLOSED_CONNECTION = "client_closed_connection"
        val SERVER_NOT_REACHABLE = "Server Not Reachable"
        val UNPAIRED = "xxxxxxxxxxxxxxxxx"
        val UNAUTHORIZED = "Not Authorized to access this device"
        val NETWORK_SSID = "Aura"
        var NOTIFICATION_COUNT = 0

        //for google map
        private val PACKAGE_NAME = "com.wozart.aura.aura.ui.createautomation"

        val GEOFENCES_ADDED_KEY = "$PACKAGE_NAME.GEOFENCES_ADDED_KEY"
        private val GEOFENCE_EXPIRATION_IN_HOURS: Long = 12
        val GEOFENCE_EXPIRATION_IN_MILLISECONDS = GEOFENCE_EXPIRATION_IN_HOURS * 60 * 60 * 1000
        val GEOFENCE_POSITION_LATITUDE = "GEOFENCE POSITION LATITUDE"
        val CURRENT_LATITUDE = "GEOFENCE CURRENT LATITUDE"
        val CURRENT_LONGITUDE= "GEOFENCE CURRENT LONGITUDE"
        val GEOFENCE_POSITION_LONGITUDE = "GEOFENCE POSITION LONGITUDE"
        val KEY_GEOFENCE_CUSTOM = "CUSTOM RADIUS"
        val KEY_GEOFENCE_CUSTOM_UPDATE_RADIUS = "CUSTOM UPDATE RADIUS"
        val KEY_GEOFENCE_RADIUS = "GEOFENCE RADIUS"
        var KEY_GEOFENCE_UPDATE_RADIUS = "GEOFENCE UPDATE rADIUS"

        //SQL - Lite Queries for Device DB
        val GET_ALL_LOADS = "select " + DeviceContract.DeviceEntry.DEVICE + " from " + DeviceContract.DeviceEntry.TABLE_NAME
        val GET_LOAD = "select " + DeviceContract.DeviceEntry.DEVICE + " , " + DeviceContract.DeviceEntry.LOAD + " , " + DeviceContract.DeviceEntry.THING + " , " + DeviceContract.DeviceEntry.ROOM + " , "+DeviceContract.DeviceEntry.UIUD +" from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.DEVICE + " =?"
        val GET_ALL_LOADS_HOME = "select " + DeviceContract.DeviceEntry.DEVICE + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.HOME + " = ?"
        val GET_ALL_HOME = "select distinct " + DeviceContract.DeviceEntry.HOME + " from " + DeviceContract.DeviceEntry.TABLE_NAME
        val GET_ROOMS_QUERY = "select distinct " + DeviceContract.DeviceEntry.ROOM + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.HOME + " = ?"
        val GET_ROOMS_QUERY_NEW = "select distinct " + DeviceContract.DeviceEntry.ROOM +" , "+DeviceContract.DeviceEntry.DEVICE + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.HOME + " = ?"
        val INSERT_ROOMS_QUERY = "select " + DeviceContract.DeviceEntry.HOME + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.ROOM + " = ?"
        val CRUD_ROOM = DeviceContract.DeviceEntry.HOME + " =? and " + DeviceContract.DeviceEntry.ROOM + " =? "
        val GET_DEVICES_IN_ROOM_QUERY = "select " + DeviceContract.DeviceEntry.DEVICE + " , " + DeviceContract.DeviceEntry.LOAD + " , " + DeviceContract.DeviceEntry.THING + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.HOME + " = ? and " + DeviceContract.DeviceEntry.ROOM + " =?"
        val UPDATE_ROOM_DETAILS = DeviceContract.DeviceEntry.ROOM + "=?"







        val GET_DEVICE_TABLE =  "select " + DeviceContract.DeviceEntry.LOAD + " , " + DeviceContract.DeviceEntry.HOME + " , " + DeviceContract.DeviceEntry.ROOM + " ," +DeviceContract.DeviceEntry.THING + " , " + DeviceContract.DeviceEntry.UIUD  + " , " + DeviceContract.DeviceEntry.ACCESS + " , " +DeviceContract.DeviceEntry.DEVICE +" from " + DeviceContract.DeviceEntry.TABLE_NAME



        val GET_DEVICES_IN_HOME_QUERY = "select " + DeviceContract.DeviceEntry.DEVICE + " , " + DeviceContract.DeviceEntry.LOAD + " , " + DeviceContract.DeviceEntry.THING + " , " + DeviceContract.DeviceEntry.UIUD + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.HOME + " = ? "
        val GET_ALL_LOADS_SCENES = "select " + DeviceContract.DeviceEntry.DEVICE + " , " + DeviceContract.DeviceEntry.LOAD + " , " + DeviceContract.DeviceEntry.ROOM + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.HOME + " = ? "


        val GET_LOADS_JSON = "select " + DeviceContract.DeviceEntry.LOAD + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.DEVICE + " = ?"
        val CHECK_ADD_QUERY = "select " + DeviceContract.DeviceEntry.DEVICE + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.DEVICE + " = ?"
        val CHECK_DEVICES = "select " + DeviceContract.DeviceEntry.DEVICE + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.DEVICE + " = ?"
        val GET_THING_NAME_QUERY = "select " + DeviceContract.DeviceEntry.THING + " from " + DeviceContract.DeviceEntry.TABLE_NAME
        val GET_DEVICES_FOR_THING_QUERY = "select " + DeviceContract.DeviceEntry.DEVICE + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.THING + " = ?"
        val GET_THING_FOR_DEVICES_QUERY = "select " + DeviceContract.DeviceEntry.THING + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.DEVICE + " = ?"
        val UPDATE_DEVICE = DeviceContract.DeviceEntry.DEVICE + "=?"
        val UPDATE_LOAD = DeviceContract.DeviceEntry.DEVICE + "=?"
        val GET_ROOM_FOR_DEVICE_QUERY = "select " + DeviceContract.DeviceEntry.ROOM + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.DEVICE + " =?"
        val DELETE_DEVICE_QUERY = DeviceContract.DeviceEntry.DEVICE + " = ?"
        val DELETE_HOME_QUERY = DeviceContract.DeviceEntry.HOME + " = ?"
        val DELETE_HOME = DeviceContract.DeviceEntry.HOME + " =?"
        val GET_UIUD_QUERY = "select " + DeviceContract.DeviceEntry.UIUD + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.DEVICE + " = ?"
        val UPDATE_UIUD = DeviceContract.DeviceEntry.DEVICE + " = ? "
        val GET_UIUD_QUERY_ALL = "select " + DeviceContract.DeviceEntry.UIUD + " , " + DeviceContract.DeviceEntry.DEVICE  + " from " + DeviceContract.DeviceEntry.TABLE_NAME

        //SQL -Lite Queries for Favourites
        val GET_ALL_FAV_QUERY = "select " + DeviceContract.DeviceEntry.LOAD + ", " + DeviceContract.DeviceEntry.DEVICE + " from " + DeviceContract.DeviceEntry.TABLE_NAME + " where " + DeviceContract.DeviceEntry.HOME + " =?"

        //SQL- Lite Queries for Scene
        val CHECK_SCENE_QUERY = "select " + SceneContract.SceneEntry.SCENE_ID + " from " + SceneContract.SceneEntry.TABLE_NAME + " where " + SceneContract.SceneEntry.SCENE_ID + " =?"
        val UPDATE_SCENE_PARAMS = SceneContract.SceneEntry.SCENE_ID  + "=?"
        val GET_SCENE_QUERY_ROOM = "select " + SceneContract.SceneEntry.SCENE_ID + " , " + SceneContract.SceneEntry.LOAD + " , " + SceneContract.SceneEntry.ROOM + " , " + SceneContract.SceneEntry.ICON + " from " + SceneContract.SceneEntry.TABLE_NAME + " where " + SceneContract.SceneEntry.HOME + " = ? "
        val GET_SCENE_QUERY = "select " + SceneContract.SceneEntry.SCENE_ID + " , " + SceneContract.SceneEntry.LOAD + " , " + SceneContract.SceneEntry.ICON + " from " + SceneContract.SceneEntry.TABLE_NAME + " where " + SceneContract.SceneEntry.HOME + " = ? "
        val GET_SCENE_FOR_ROOM_QUERY = "select " + SceneContract.SceneEntry.SCENE_ID + " , " + SceneContract.SceneEntry.LOAD + " , " + SceneContract.SceneEntry.ROOM + " , " +  SceneContract.SceneEntry.ICON  +  " from " + SceneContract.SceneEntry.TABLE_NAME + " where " + SceneContract.SceneEntry.HOME + " = ? and " + SceneContract.SceneEntry.ROOM + " =?"
        val DELETE_SCENE = SceneContract.SceneEntry.SCENE_ID + " = ? and " + SceneContract.SceneEntry.HOME + " =?"
        val DELETE_SCENE_DEVICE = SceneContract.SceneEntry.SCENE_ID + " = ? and " + SceneContract.SceneEntry.LOAD + " +?"
        val GET_SELECTED_SCENE = "select " +  SceneContract.SceneEntry.SCENE_ID + " , " +  SceneContract.SceneEntry.LOAD + " , " +  SceneContract.SceneEntry.ICON + " from " +  SceneContract.SceneEntry.TABLE_NAME + " where " +  SceneContract.SceneEntry.SCENE_ID + " = ? and " + SceneContract.SceneEntry.HOME + " =?"
        val DELETE_HOME_SCENE_QUERY = SceneContract.SceneEntry.HOME + " = ?"
        val GET_ALL_SCENES = "select "  + SceneContract.SceneEntry.HOME + " , " + SceneContract.SceneEntry.LOAD + " , "+SceneContract.SceneEntry.SCENE_ID + " , "  + SceneContract.SceneEntry.ROOM + " , "+ SceneContract.SceneEntry.ICON  +  " from " + SceneContract.SceneEntry.TABLE_NAME
        val UPDATE_ROOM_DETAILS_SCENE = SceneContract.SceneEntry.ROOM + " =?"

        //sql-lite query for Automation Scheduling
        val AUTOMATION_SCENE_QUERY = "select " + ScheduleContract.ScheduleEntry.SCHEDULE_NAME + " from " + ScheduleContract.ScheduleEntry.TABLE_NAME + " where " + ScheduleContract.ScheduleEntry.SCHEDULE_NAME + " =?"
        val UPDATE_AUTOMATION_SCENE = ScheduleContract.ScheduleEntry.SCHEDULE_NAME + " =?"
        val GET_AUTOMATION_SCENE_QUERY = "select " + ScheduleContract.ScheduleEntry.SCHEDULE_NAME + " , " + ScheduleContract.ScheduleEntry.ICON  + " , " + ScheduleContract.ScheduleEntry.ROOM + " , " + ScheduleContract.ScheduleEntry.LOAD + " , " + ScheduleContract.ScheduleEntry.SCHEDULE_PROPERTY +  " , " + ScheduleContract.ScheduleEntry.START_TIME + " , " + ScheduleContract.ScheduleEntry.SCHEDULE_TYPE + " , " + ScheduleContract.ScheduleEntry.END_TIME + " , " + ScheduleContract.ScheduleEntry.ROUTINE + " from " + ScheduleContract.ScheduleEntry.TABLE_NAME + " where " + ScheduleContract.ScheduleEntry.HOME + " =?"
        val CHECK_AUTOMATION_QUERY = "select " + ScheduleContract.ScheduleEntry.SCHEDULE_NAME + " , " + ScheduleContract.ScheduleEntry.ICON + " , " + ScheduleContract.ScheduleEntry.ROOM + " , " + ScheduleContract.ScheduleEntry.LOAD + " , " + ScheduleContract.ScheduleEntry.SCHEDULE_PROPERTY +  " , " + ScheduleContract.ScheduleEntry.START_TIME + " , " + ScheduleContract.ScheduleEntry.SCHEDULE_TYPE + " , " + ScheduleContract.ScheduleEntry.END_TIME + " , " + ScheduleContract.ScheduleEntry.ROUTINE  + " from " + ScheduleContract.ScheduleEntry.TABLE_NAME + " where " + ScheduleContract.ScheduleEntry.SCHEDULE_NAME + " =? and " + ScheduleContract.ScheduleEntry.HOME + " =?"
        val GET_AUTOMATION_SELECTED_SCENE = " select" + ScheduleContract.ScheduleEntry.SCHEDULE_NAME + " , " + ScheduleContract.ScheduleEntry.ICON + " , " + ScheduleContract.ScheduleEntry.ROOM + " , " + ScheduleContract.ScheduleEntry.LOAD + " from " +ScheduleContract.ScheduleEntry.TABLE_NAME + " where " + ScheduleContract.ScheduleEntry.SCHEDULE_NAME + " =?"
        val DELETE_AUTOMATION_SCENE = ScheduleContract.ScheduleEntry.SCHEDULE_NAME + " =? and " + ScheduleContract.ScheduleEntry.HOME + " =? "
        val GET_AUTOMATION_SCENE_QUERY_NAME = "select " + ScheduleContract.ScheduleEntry.SCHEDULE_NAME + " , " + ScheduleContract.ScheduleEntry.ICON + " , " + ScheduleContract.ScheduleEntry.ROOM + " , " + ScheduleContract.ScheduleEntry.LOAD + " , " + ScheduleContract.ScheduleEntry.SCHEDULE_PROPERTY +  " , " + ScheduleContract.ScheduleEntry.START_TIME + " , " + ScheduleContract.ScheduleEntry.SCHEDULE_TYPE + " , " + ScheduleContract.ScheduleEntry.END_TIME + " , " + ScheduleContract.ScheduleEntry.ROUTINE + " from " + ScheduleContract.ScheduleEntry.TABLE_NAME + " where " + ScheduleContract.ScheduleEntry.SCHEDULE_NAME + " =? "
        val DELETE_HOME_SCENES =  ScheduleContract.ScheduleEntry.HOME + " = ? "

        val GET_KEY_VALUE_QUERY = "select " + UtilsContract.UtilsEntry.VALUE + " from " +  UtilsContract.UtilsEntry.TABLE_NAME + " where " +  UtilsContract.UtilsEntry.KEY + " =?"
        val UPDATE_KEY_VALUE_QUERY   = UtilsContract.UtilsEntry.KEY + "=?"
        val GET_ALL_AUTOMATION   = "select " + ScheduleContract.ScheduleEntry.SCHEDULE_NAME + " , " + ScheduleContract.ScheduleEntry.ICON  + " , " + ScheduleContract.ScheduleEntry.ROOM + " , " + ScheduleContract.ScheduleEntry.LOAD + " , " + ScheduleContract.ScheduleEntry.SCHEDULE_PROPERTY +  " , " + ScheduleContract.ScheduleEntry.START_TIME + " , " + ScheduleContract.ScheduleEntry.SCHEDULE_TYPE + " , " + ScheduleContract.ScheduleEntry.END_TIME + " , " + ScheduleContract.ScheduleEntry.ROUTINE + " , " + ScheduleContract.ScheduleEntry.HOME + " from " + ScheduleContract.ScheduleEntry.TABLE_NAME


        //AWS MQTT Messages
        val AWS_UPDATE_ACCEPTED = "\$aws/things/%s/shadow/update/accepted"
        val AWS_GET_ACCEPTED = "\$aws/things/%s/shadow/get/accepted"
        val AWS_UPDATE = "\$aws/things/%s/shadow/update"
        val AWS_GET = "\$aws/things/%s/shadow/get"

        //tab Ids
        val HOME_TAB = 0
        val ROOMS_TAB = 1
        val AUTOMATION_TAB = 2
        val MORE_TAB = 3

        //DAYS
        val SUNDAY = "Sunday"
        val MONDAY = "Monday"
        val TUESDAY = "Tuesday"
        val WEDNESDAY = "Wednesday"
        val THURSDAY = "Thursday"
        val FRIDAY = "Friday"
        val SATURDAY = "Saturday"


        //App
        val CREATE_HOME_ROOM = "create_home_room"
        val CREATE_ROOM_NAME = "create_room"
        val CREATE_ROOM = 1
        val CREATE_HOME = 0


        val SHARE_HOME = 0
        val EDIT_HOME = 1
        val SELECTED_HOME = 2

        //Device Types
        val BULB = 0
        val LAMP = 1
        val BED_LAMP = 2
        val FAN = 3
        val SOCKET = 4
        val AC =5
        val EXAUSTFAN = 6

    }


}