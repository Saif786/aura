@file:Suppress("DEPRECATION")

package com.wozart.aura.aura.utilities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Context.WINDOW_SERVICE
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Environment
import android.support.v7.graphics.Palette
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import com.wozart.aura.R
import com.wozart.aura.utilities.Constant.*
import com.wozart.aura.utilities.Constant.Companion.AC
import com.wozart.aura.utilities.Constant.Companion.BED_LAMP
import com.wozart.aura.utilities.Constant.Companion.BULB
import com.wozart.aura.utilities.Constant.Companion.EXAUSTFAN
import com.wozart.aura.utilities.Constant.Companion.FAN
import com.wozart.aura.utilities.Constant.Companion.LAMP
import com.wozart.aura.utilities.Constant.Companion.SOCKET
import com.wozart.aura.utilities.DialogListener
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Drona Sahoo on 3/16/2018.
 */

object Utils {

    fun getImage(imageName: String?, context: Context): Int {
        return context.resources.getIdentifier(imageName, "drawable", context.packageName)
    }

    fun showCustomDialog(context: Context, title: String, message: String, layout: Int, listener: DialogListener) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(layout)
        val no = dialog.findViewById<View>(R.id.btn_no) as Button
        val yes = dialog.findViewById<View>(R.id.btn_yes) as Button
        val titleTV = dialog.findViewById<View>(R.id.tv_title) as TextView
        val messageTV = dialog.findViewById<View>(R.id.message) as TextView
        titleTV.text = title
        messageTV.text = message

        no.setOnClickListener {
            listener.onCancelClicked()
            dialog.dismiss()
        }
        yes.setOnClickListener {
            listener.onOkClicked()
            dialog.dismiss()
        }
        dialog.show()
    }

    fun showCustomDialogOk(context: Context, title: String, message: String, layout: Int, listener: DialogListener) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(layout)
        val no = dialog.findViewById<View>(R.id.btn_no) as Button
        val yes = dialog.findViewById<View>(R.id.btn_yes) as Button
        val titleTV = dialog.findViewById<View>(R.id.tv_title) as TextView
        val messageTV = dialog.findViewById<View>(R.id.message) as TextView
        titleTV.text = title
        messageTV.text = message
        no.visibility = View.GONE
        yes.text = "ok"
        no.setOnClickListener {
            listener.onCancelClicked()
            dialog.dismiss()
        }
        yes.setOnClickListener {
            listener.onOkClicked()
            dialog.dismiss()
        }
        dialog.show()
    }

    fun hideSoftKeyboard(activity: Activity) {
        val inputMethodManager = activity.getSystemService(
                Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        if (activity.currentFocus != null)
            inputMethodManager.hideSoftInputFromWindow(
                    activity.currentFocus!!.windowToken, InputMethodManager.SHOW_FORCED)
    }

    public fun getScreenWidthInDP1(context: Context): Int {
        Resources.getSystem().getDisplayMetrics().widthPixels;
        val dm = DisplayMetrics()
        val windowManager = context.getSystemService(WINDOW_SERVICE) as WindowManager
        windowManager.defaultDisplay.getMetrics(dm)
        val widthInDP = Math.round(dm.widthPixels / dm.density)
        return widthInDP.toInt() * 2
    }

    fun getScreenWidthInDP(context: Context): Int {
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = windowManager.defaultDisplay
        var outPoint: Point = Point()
        if (Build.VERSION.SDK_INT >= 19) {
            display.getRealSize(outPoint)
        } else {
            display.getSize(outPoint)
        }
        if (outPoint.y > outPoint.x) {
            return outPoint.x
        } else {
            return outPoint.y
        }
    }

    @SuppressLint("ObsoleteSdkInt")
    fun setDrawable(context: Context, view: ViewGroup) {
        val originalBitmap: Bitmap?
        val f = File(getWallpaperPath(context))
        if (f.exists()) {
            val bitmapSizeOptions = BitmapFactory.Options()
            bitmapSizeOptions.inJustDecodeBounds = true
            bitmapSizeOptions.inDither = false
            bitmapSizeOptions.inPurgeable = true
            bitmapSizeOptions.inInputShareable = true
            originalBitmap = BitmapFactory.decodeFile(f.absolutePath, bitmapSizeOptions)
            //originalBitmap = BitmapFactory.decodeFile(f.absolutePath)
        } else
            originalBitmap = BitmapFactory.decodeResource(context.resources,
                    getWallpaperDrawables(getWallpaperId(context)))
        // val blurredBitmap = BlurBuilder.blur(context, originalBitmap!!)
        view.background = BitmapDrawable(context.resources, originalBitmap)
    }

    @SuppressLint("ObsoleteSdkInt")
    fun setDrawable(context: Context, view: ViewGroup, home: String) {
        val originalBitmap: Bitmap?
        val f = File(getWallpaperPath(context))
        if (f.exists()) {
            val bitmapSizeOptions = BitmapFactory.Options()
            bitmapSizeOptions.inJustDecodeBounds = true
            bitmapSizeOptions.inDither = false
            bitmapSizeOptions.inPurgeable = true
            bitmapSizeOptions.inInputShareable = true
            originalBitmap = BitmapFactory.decodeFile(f.absolutePath, bitmapSizeOptions)
            //originalBitmap = BitmapFactory.decodeFile(f.absolutePath)
        } else
            originalBitmap = BitmapFactory.decodeResource(context.resources,
                    getWallpaperDrawables(getWallpaperId(context, home)))
        // val blurredBitmap = BlurBuilder.blur(context, originalBitmap!!)
        view.background = BitmapDrawable(context.resources, originalBitmap)
    }

    fun setRoomDrawable(context: Context, view: ViewGroup,wallpaperId:Int){
        val originalBitmap: Bitmap?
        val f = File(getWallpaperPath(context))
        if (f.exists()) {
            val bitmapSizeOptions = BitmapFactory.Options()
            bitmapSizeOptions.inJustDecodeBounds = true
            bitmapSizeOptions.inDither = false
            bitmapSizeOptions.inPurgeable = true
            bitmapSizeOptions.inInputShareable = true
            originalBitmap = BitmapFactory.decodeFile(f.absolutePath, bitmapSizeOptions)
            //originalBitmap = BitmapFactory.decodeFile(f.absolutePath)
        }
        else
            originalBitmap =  BitmapFactory.decodeResource(context.resources,
                    getRoomWallpaperDrawables(wallpaperId))
        // val blurredBitmap = BlurBuilder.blur(context, originalBitmap!!)
        view.background = BitmapDrawable(context.resources, originalBitmap)
    }

    @SuppressLint("ObsoleteSdkInt")
    fun setDrawable(context: Context, view: ViewGroup, wallpaperId: Int) {
        val originalBitmap: Bitmap?
        val f = File(getWallpaperPath(context))
        if (f.exists()) {
            val bitmapSizeOptions = BitmapFactory.Options()
            bitmapSizeOptions.inJustDecodeBounds = true
            bitmapSizeOptions.inDither = false
            bitmapSizeOptions.inPurgeable = true
            bitmapSizeOptions.inInputShareable = true
            originalBitmap = BitmapFactory.decodeFile(f.absolutePath, bitmapSizeOptions)
            //originalBitmap = BitmapFactory.decodeFile(f.absolutePath)
        } else
            originalBitmap = BitmapFactory.decodeResource(context.resources,
                    getWallpaperDrawables(wallpaperId))
        // val blurredBitmap = BlurBuilder.blur(context, originalBitmap!!)
        view.background = BitmapDrawable(context.resources, originalBitmap)
    }

    @SuppressLint("ApplySharedPref")
    fun saveWallPaperId(context: Context, value: Int) {
        val sharedPref = context.getSharedPreferences("wozart", Context.MODE_PRIVATE)
        sharedPref.edit().putInt(context.getString(R.string.saved_wallpaper_key), value).commit()
        //89655
        saveThemeType(context)
    }

    @SuppressLint("ApplySharedPref")
    fun saveWallPaperId(context: Context, value: Int, home: String) {
        val sharedPref = context.getSharedPreferences("wozart", Context.MODE_PRIVATE)
        sharedPref.edit().putInt(home, value).commit()
        //89655
        saveThemeType(context)
    }

    @SuppressLint("ApplySharedPref")
    fun saveWallPaperPath(context: Context, value: String) {
        val sharedPref = context.getSharedPreferences("wozart", Context.MODE_PRIVATE)
        sharedPref.edit().putString(context.getString(R.string.saved_wallpaper_path_key), value).commit()
        saveThemeType(context)
    }

    @SuppressLint("ApplySharedPref")
    private fun saveThemeId(context: Context, value: Int) {
        val sharedPref = context.getSharedPreferences("wozart", Context.MODE_PRIVATE)
        sharedPref.edit().putInt(context.getString(R.string.saved_theme_key), value).commit()
    }

    fun getWallpaperId(context: Context): Int {
        val sharedPref = context.getSharedPreferences("wozart", Context.MODE_PRIVATE)
        val wallpaperId: Int = sharedPref.getInt(context.getString(R.string.saved_wallpaper_key), 0)
        if (wallpaperId > 12)
            return 0
        else return wallpaperId
    }

    fun getWallpaperId(context: Context, home: String): Int {
        val sharedPref = context.getSharedPreferences("wozart", Context.MODE_PRIVATE)
        val wallpaperId: Int = sharedPref.getInt(home, 0)
        if (wallpaperId > 12)
            return 0
        else return wallpaperId
    }

    private fun getWallpaperPath(context: Context): String {
        val sharedPref = context.getSharedPreferences("wozart", Context.MODE_PRIVATE)
        return sharedPref.getString(context.getString(R.string.saved_wallpaper_path_key), "")
    }

    fun getThemeId(context: Context): Int {
        val sharedPref = context.getSharedPreferences("wozart", Context.MODE_PRIVATE)
        val themeId: Int = sharedPref.getInt(context.getString(R.string.saved_theme_key), 0)
        if (themeId != 0) {
            return themeId
        } else {
            Utils.saveWallPaperId(context, 0)
            return sharedPref.getInt(context.getString(R.string.saved_theme_key), 0)
        }
    }

    private fun createPalette(context: Context): Palette = Palette.from(getBitmapFromDrawable(context)).generate()

    private fun getBitmapFromDrawable(context: Context): Bitmap {
        val f = File(getWallpaperPath(context))
        if (f.exists()) {
            val bitmapSizeOptions = BitmapFactory.Options()
            bitmapSizeOptions.inJustDecodeBounds = true
            bitmapSizeOptions.inDither = false
            bitmapSizeOptions.inPurgeable = true
            bitmapSizeOptions.inInputShareable = true
            return BitmapFactory.decodeFile(f.absolutePath, bitmapSizeOptions)
        } else {
            return BitmapFactory.decodeResource(context.resources,
                    getWallpaperDrawables(getWallpaperId(context)))
        }
    }

    private fun getWallpaperDrawables(position: Int): Int {
        val images = intArrayOf(R.drawable.one_new, R.drawable.two_new, R.drawable.three_new, R.drawable.four_new, R.drawable.five_new, R.drawable.six_new, R.drawable.seven_new,R.drawable.eight_new,R.drawable.nine_new)
        return images[position]

    }

    fun getRoomWallpaperDrawables(position: Int): Int {
        val images = intArrayOf(R.drawable.eleven_new,R.drawable.twelve_new,R.drawable.one_new, R.drawable.two_new, R.drawable.three_new, R.drawable.four_new, R.drawable.five_new, R.drawable.six_new, R.drawable.seven_new,R.drawable.eight_new,R.drawable.nine_new,R.drawable.ten_new)
        return images[position]

    }

    fun getThemeType(colorDark: Int, colorLight: Int): Int {
        var darkness = 1 - (0.299 * Color.red(colorDark) + 0.587 * Color.green(colorDark) + 0.114 * Color.blue(colorDark)) / 255
        if (darkness == 1.0)
            darkness = 0.0
        var lightness = 1 - (0.299 * Color.red(colorLight) + 0.587 * Color.green(colorLight) + 0.114 * Color.blue(colorLight)) / 255
        if (lightness == 1.0)
            lightness = 0.0
        if (darkness < lightness)
            return 0
        else return 1
    }

    private fun saveThemeType(context: Context) {
        val p = createPalette(context)
        val default = 0x000000
        val mutedLightVibrant = p.getLightVibrantColor(default)
        val mutedDarkVibrant = p.getDarkVibrantColor(default)
        if (getThemeType(mutedDarkVibrant, mutedLightVibrant) == 0) {
            saveThemeId(context, R.style.AppThemeLight)
        } else saveThemeId(context, R.style.AppThemeDark)
    }

    fun getOutputMediaFile(): File? {

        // External sdcard location
        val mediaStorageDir = File(
                Environment
                        .getExternalStorageDirectory(),
                "Wozart")

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {

                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss",
                Locale.getDefault()).format(Date())
        val mediaFile: File

        mediaFile = File(mediaStorageDir.path + File.separator
                + "WAP_" + timeStamp + ".jpeg")


        return mediaFile
    }




    fun getIconDrawable(type: Int, status: Boolean): Int {
        when (type) {
            BULB -> if (status) {
                return R.drawable.ic_bulb_on_new
            } else {
                return R.drawable.ic_bulb_off_new
            }
            LAMP -> if (status) {
                return R.drawable.ic_ceiling_lamp_on_new
            } else {
                return R.drawable.ic_ceiling_lamp_off_new
            }
            BED_LAMP -> if (status) {
                return R.drawable.ic_bed_lamp_on_new
            } else {
                return R.drawable.ic_bed_lamp_off_new
            }
            FAN -> if (status) {
                return R.drawable.fan_active_on
            } else {
                return R.drawable.ic_fan_off_
            }
            AC -> if (status) {
                return R.drawable.ic_ac_on
            } else {
                return R.drawable.ic_ac_off
            }
            EXAUSTFAN -> if (status) {
                return R.drawable.exhaust_fan_active
            } else {
                return R.drawable.ic_exhaust_fan_off
            }
            SOCKET -> if (status) {
                return R.drawable.ic_switch_on
            } else {
                return R.drawable.ic_switch_off
            }
            else -> if (status) {
                return R.drawable.ic_switch_on
            } else {
                return R.drawable.ic_switch_on
            }
        }
    }


    fun getRoomDrawable(position: Int, selected: Boolean): Int {
        when (position) {
            0 -> if (selected) {
                return R.drawable.ic_living_room_on
            } else {
                return R.drawable.ic_living_room_off
            }
            else -> if (selected) {
                return R.drawable.ic_living_room_on
            } else {
                return R.drawable.ic_living_room_on
            }
        }

    }

    fun getSceneDrawable(position: Int, selected: Boolean): Int {
        when (position) {
            0 -> if (selected) {
                return R.drawable.ic_enter_on
            } else {
                return R.drawable.ic_enter_off
            }
            1 -> if (selected) {
                return R.drawable.ic_exit_on
            } else {
                return R.drawable.ic_exit_off
            }
            2 -> if (selected) {
                return R.drawable.ic_good_morning_on
            } else {
                return R.drawable.ic_good_morning_off
            }
            3 -> if (selected) {
                return R.drawable.ic_good_night_on
            } else {
                return R.drawable.ic_good_night_off
            }
            4 -> if (selected) {
                return R.drawable.ic_movie_on
            } else {
                return R.drawable.ic_movie_off
            }
            5 -> if (selected) {
                return R.drawable.ic_party_on
            } else {
                return R.drawable.ic_party_off
            }
            6 -> if (selected) {
                return R.drawable.ic_reading_on
            } else {
                return R.drawable.ic_reading_off
            }

            else -> if (selected) {
                return R.drawable.ic_enter_on
            } else {
                return R.drawable.ic_enter_on
            }
        }
    }

    fun getSceneTestDrawable(position: Int, selected: Boolean): Int {
        when (position) {
            0 -> if (selected) {
                return R.drawable.ic_enter_scene_on
            } else {
                return R.drawable.ic_enter_off
            }
            1 -> if (selected) {
                return R.drawable.ic_exit_scene_off
            } else {
                return R.drawable.ic_exit_off
            }
            2 -> if (selected) {
                return R.drawable.ic_good_morning_scene_off
            } else {
                return R.drawable.ic_good_morning_off
            }
            3 -> if (selected) {
                return R.drawable.ic_good_night_scene_off
            } else {
                return R.drawable.ic_good_night_off
            }
            4 -> if (selected) {
                return R.drawable.ic_movie_scene_off
            } else {
                return R.drawable.ic_movie_off
            }
            5 -> if (selected) {
                return R.drawable.ic_party_scene_off
            } else {
                return R.drawable.ic_party_off
            }
            6 -> if (selected) {
                return R.drawable.ic_reading_scene_off
            } else {
                return R.drawable.ic_reading_off
            }

            else -> if (selected) {
                return R.drawable.ic_enter_off
            } else {
                return R.drawable.ic_enter_off
            }
        }
    }


}
