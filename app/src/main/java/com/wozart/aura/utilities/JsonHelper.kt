package com.wozart.aura.utilities

import android.content.Context
import android.net.wifi.WifiManager
import android.util.Log
import com.facebook.FacebookSdk.getApplicationContext
import com.google.gson.Gson
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.entity.model.aws.AwsState
import com.wozart.aura.entity.model.scene.SceneList
import com.wozart.aura.ui.dashboard.Device
import org.json.JSONObject
import java.net.UnknownHostException
import java.util.ArrayList

/***************************************************************************
 * File Name :
 * Author : Aarth Tandel
 * Date of Creation : 15/05/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class JsonHelper {
    private val LOG_TAG = JsonHelper::class.java.simpleName

    fun deserializeAwsData(Data: String): AwsState {
        val gson = Gson()
        var receivedData = AwsState()
        try {
            val reported = JSONObject(Data)
            val flag = reported.getJSONObject("state").isNull("reported")
            if(!flag){
                receivedData = gson.fromJson<AwsState>(reported.getJSONObject("state").getString("reported"), AwsState::class.java)
            }else{
                return receivedData
            }
        } catch (e: Exception) {
            Log.d(LOG_TAG, "Error Parsing JsonHelper Data: $e")
        }
        return receivedData
    }

    fun awsRegionThing(region: String, thing: String): String {
        var data: String? = null
        data = "{\"type\":7,\"thing\":\"$thing\",\"region\":\"$region\"}"
        return data
    }

    fun certificates(data: String): ArrayList<String> {
        val certificates = SegregateData.Segregate(data)
        var jsonCertificate: String
        val dataCertificates = ArrayList<String>()
        var length = 0
        for ((pktNo, fragment) in certificates.withIndex()) {
            jsonCertificate = "{\"type\":6,\"pos\":$length,\"pktno\":$pktNo,\"data\":\"$fragment\"}"
            dataCertificates.add(jsonCertificate)
            length += fragment.length + 1
        }
        return dataCertificates
    }

    fun privateKeys(data: String): ArrayList<String> {
        val privateKey = SegregateData.Segregate(data)
        var jsonPrivateKey: String
        var length = 0
        val dataPrivarteKey = ArrayList<String>()
        for ((pktNo, fragment) in privateKey.withIndex()) {
            jsonPrivateKey = "{\"type\":5,\"pos\":$length,\"pktno\":$pktNo,\"data\":\"$fragment\"}"
            dataPrivarteKey.add(jsonPrivateKey)
            length += fragment.length + 1
        }
        return dataPrivarteKey
    }

    @Throws(UnknownHostException::class)
    fun serialize(device: Device, uiud: String, type: Boolean): String {
        val plc = IntArray(4)
        plc[device.index] = 1
        val states = intArrayOf(0,0,0,0)
        val dims = intArrayOf(100,100,100,100)
        if(!type){
            if(device.isTurnOn){
                states[device.index] = 0
            }else{
                states[device.index] = 1
            }
        }else{
            if(device.dimVal > 0){
                states[device.index] = 1
            }else{
                states[device.index] = 0
            }
        }
        dims[device.index] = device.dimVal

        return ("{\"type\":4,\"ip\":" + convertIP() + ",\"uiud\":\"" + uiud + "\",\"state\":[" + states[0] + "," + states[1] + "," + states[2] + "," + states[3] + "],\"dim\":["
                + dims[0] + "," + dims[1] + "," + dims[2] + "," + dims[3] + "],\"plc\":[" + plc[0] + "," + plc[1] + "," + plc[2] + "," + plc[3] + "]}")
//        return ("{\"type\":4, \"ip\":" + convertIP() + ",\"name\":\"" + device.deviceName + "\",\"uiud\":\"" + uiud + "\",\"state\":[" + states[0] + "," + states[1] + "," + states[2] + "," + states[3] + "],\"dim\":["
//                + dims[0] + "," + dims[1] + "," + dims[2] + "," + dims[3] + "], \"plc\":[" + plc[0] + "," + plc[1] + "," + plc[2] + "," + plc[3] + "]}")
    }

    fun serializeDataToAws(device: Device, uiud: String, type: Boolean): String {
        val data: String
        var state = 0
        val led = System.currentTimeMillis()/1000

        if(!type){
            if(device.isTurnOn){
                state= 0
            }else{
                state = 1
            }
        }else{
            if(device.dimVal > 0){
                state = 1
            } else{
                state = 0
            }
        }
        data = "{\"state\":{\"desired\": {\"led\": " + led  + ", \"dim\": {\"d" + device.index + "\":" + device.dimVal + "},\"state\": {\"s" + device.index + "\":" + state + "}}}}"
        return data
    }
    fun serializeLEDData(): String {
        val data: String
        val led = (System.currentTimeMillis() / 1000)
        data = "{\"state\":{\"desired\": {\"led\": $led}}}"
        return data
    }

    @Throws(UnknownHostException::class)
    fun initialData(uiud: String): String {

        return "{\"type\":1,\"ip\":" + convertIP() + ",\"time\":" + System.currentTimeMillis() / 1000 + ",\"uiud\":\"" + uiud + "\" }"
    }

    fun deserializeTcp(data: String): AuraSwitch {
        val gson = Gson()
        var device = AuraSwitch()
        try {
            device = gson.fromJson<AuraSwitch>(data, AuraSwitch::class.java)
        } catch (e: Exception) {
            Log.d("DATA_ERROR : ",data)
            Log.d(LOG_TAG, "Illegal Message $e")
        } finally {
            return device
        }
    }

    fun pairingData(uiud: String, pin: String): String {
        return "{\"type\":2,\"hash\":\"$pin\",\"uiud\":\"$uiud\"}"
    }

    /**
     * Scenes Methods
     */

    fun serializeSceneData(scene: SceneList, uiud: String, isSceneOn: Boolean): String {
        val plc = IntArray(4)
        val states = IntArray(4)
        val dims: IntArray = intArrayOf(100, 100, 100, 100)

        for (device in scene.loads) {
            plc[device.index] = 1
            if (isSceneOn) {
                if (device.isTurnOn) states[device.index] = 1
                else states[device.index] = 0
                dims[device.index] = device.dimVal
            } else {
                states[device.index] = 0
            }
        }

        return ("{\"type\":4, \"ip\":" + convertIP() + ",\"name\":\"" + scene.device + "\",\"uiud\":\"" + uiud + "\",\"state\":[" + states[0] + "," + states[1] + "," + states[2] + "," + states[3] + "],\"dim\":["
                + dims[0] + "," + dims[1] + "," + dims[2] + "," + dims[3] + "], \"plc\":[" + plc[0] + "," + plc[1] + "," + plc[2] + "," + plc[3] + "]}")
    }

    fun serializeSceneDataForAws(scene: SceneList, isSceneOn: Boolean): String {
        val plc = IntArray(4)
        val states = IntArray(4)
        val dims = IntArray(4)
        var aws_data = "{\"state\":{\"desired\":{"
        val led = System.currentTimeMillis()/1000


        aws_data = aws_data + "\"led\": " + led  + ",\"state\":{"

        for (device in scene.loads) {
            plc[device.index] = 1
            if (device.isTurnOn) states[device.index] = 1
            else states[device.index] = 0
            if (isSceneOn)
                aws_data = aws_data + "\"s" + device.index + "\":" + states[device.index] + ","
            else
                aws_data = aws_data + "\"s" + device.index + "\":0,"

        }
        aws_data = aws_data.substring(0, aws_data.length - 1)
        aws_data += "},\"dim\":{"
        for (device in scene.loads) {
            plc[device.index] = 1
            dims[device.index] = device.dimVal
            aws_data = aws_data + "\"d" + device.index + "\":" + dims[device.index] + ","
        }
        aws_data = aws_data.substring(0, aws_data.length - 1)
        aws_data += "}}}}"

        return aws_data
        //{"state":{"desired": {"led": -285489, "dim": {"d0":0},"state": {"s0":1}}}}

    }

    @Throws(UnknownHostException::class)
     fun convertIP(): Int {
        val mWifi = getApplicationContext().getSystemService(Context.WIFI_SERVICE) as WifiManager
        val wifiInfo = mWifi.connectionInfo
        return wifiInfo.ipAddress
    }

    /**
    *All Device On
     */

    fun serializeOnAllDevice(devices: SceneList, uiud: String, isSceneOn: Boolean,name:String): String {
        val plc = IntArray(4)
        val states = IntArray(4)
        val dims: IntArray = intArrayOf(100, 100, 100, 100)

        for (device in devices.loads) {
            plc[device.index] = 1
            if (isSceneOn) {
                if (device.isTurnOn) states[device.index] = 1
                else states[device.index] = 0
                dims[device.index] = device.dimVal
            } else {
                states[device.index] = 0
            }
        }

        return ("{\"type\":4, \"ip\":" + convertIP() + ",\"name\":\"" + name + "\",\"uiud\":\"" + uiud + "\",\"state\":[" + states[0] + "," + states[1] + "," + states[2] + "," + states[3] + "],\"dim\":["
                + dims[0] + "," + dims[1] + "," + dims[2] + "," + dims[3] + "], \"plc\":[" + plc[0] + "," + plc[1] + "," + plc[2] + "," + plc[3] + "]}")
    }
    fun serializeOnAllDeviceAws(devices: SceneList,isSceneOn: Boolean): String {
        val plc = IntArray(4)
        val states = IntArray(4)
        val dims = IntArray(4)
        var aws_data = "{\"state\":{\"desired\":{"
        val led = System.currentTimeMillis()/1000


        aws_data = aws_data + "\"led\": " + led  + ",\"state\":{"

        for (device in devices.loads) {
            plc[device.index] = 1
            if (device.isTurnOn) states[device.index] = 1
            else states[device.index] = 0
            if (isSceneOn)
                aws_data = aws_data + "\"s" + device.index + "\":" + states[device.index] + ","
            else
                aws_data = aws_data + "\"s" + device.index + "\":0,"

        }
        aws_data = aws_data.substring(0, aws_data.length - 1)
        aws_data += "},\"dim\":{"
        for (device in devices.loads) {
            plc[device.index] = 1
            dims[device.index] = device.dimVal
            aws_data = aws_data + "\"d" + device.index + "\":" + dims[device.index] + ","
        }
        aws_data = aws_data.substring(0, aws_data.length - 1)
        aws_data += "}}}}"

        return aws_data
    }


}