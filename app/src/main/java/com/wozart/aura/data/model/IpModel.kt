package com.wozart.aura.data.model

class IpModel {
    var name: String? = null
    var ip: String? = null
    var full_device_name: String? = null
    var thing:String?=null
    var state = booleanArrayOf(false, false, false, false)
    var dim = intArrayOf(100, 100, 100, 100)
    var uiud:String?=null
    var owned = 0
    var local = false
    var aws = false
    var failure = intArrayOf(0, 0, 0, 0)
    var curn_load =  booleanArrayOf(false, false, false, false)
    var home:String?=null
    var room:String?=null
    //var status="update"
    var condition = arrayListOf<String>("update","update","update","update")
}