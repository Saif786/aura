package com.wozart.aura.aura.data.model

/**
 * Created by Drona Sahoo on 3/20/2018.
 */

class User {


    var firstName: String? = null

    var lastName: String? = null

    var mobile: String? = null

    var profileImage: String? = null

    var email: String? = null

}
