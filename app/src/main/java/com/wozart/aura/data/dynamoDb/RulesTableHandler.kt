package com.wozart.aura.data.dynamoDb

import android.util.Log
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBScanExpression
import com.amazonaws.regions.Regions
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.facebook.FacebookSdk.getApplicationContext
import com.github.mikephil.charting.charts.Chart.LOG_TAG
import com.google.gson.Gson
import com.wozart.aura.data.model.SchedulingList
import com.wozart.aura.entity.amazonaws.models.nosql.RulesDO
import com.wozart.aura.entity.model.aura.AuraSwitch
import com.wozart.aura.ui.createautomation.AutomationScene
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.room.RoomModelJson
import com.wozart.aura.utilities.Constant
import kotlin.concurrent.thread

/***************************************************************************
 * File Name :
 * Author : Saif
 * Date of Creation : 28/09/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class RulesTableHandler {

    private var LOG_TAG_SCH = RulesTableHandler :: class.java


    companion object {
        private var dynamoDBMapper: DynamoDBMapper?
        var credentialsProvider = CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "us-east-1:52da6706-7a78-41f4-950c-9d940b890788", // Identity Pool ID
                Regions.US_EAST_1 // Region
        )
        init {
            val dynamoDBClient = AmazonDynamoDBClient(credentialsProvider)
            this.dynamoDBMapper = DynamoDBMapper.builder()
                    .dynamoDBClient(dynamoDBClient)
                    .awsConfiguration(AWSMobileClient.getInstance().configuration)
                    .build()
        }
    }


    fun isUserAlreadyRegistered(id: String): Boolean {
         return try {
            val checkUser = dynamoDBMapper!!.load<RulesDO>(RulesDO::class.java, id)
             if(checkUser == null){
                  return false
             }else{
                 return !((checkUser.email == null) or (checkUser.name == null) or (checkUser.verified == null))
             }
        } catch (e: Exception) {
            Log.i(LOG_TAG, "Error : $e")
            return false
        }
    }

    fun insertUser(userId: String, userName: String?, email: String?,verified: String) {
        try {
            val checkUser = dynamoDBMapper!!.load<RulesDO>(RulesDO::class.java, userId)

            if(checkUser == null){
                val rulesTableDO = RulesDO()
                rulesTableDO.userId = userId
                rulesTableDO.name = userName
                rulesTableDO.email = email
                rulesTableDO.verified = verified
                dynamoDBMapper!!.save<RulesDO>(rulesTableDO)
            }else{
                checkUser.name = userName
                checkUser.email = email
                checkUser.verified = verified
                dynamoDBMapper!!.save<RulesDO>(checkUser)
            }

        } catch (e: Exception) {
            Log.i(LOG_TAG, "Error : $e")
        }
    }


    fun getUser(): RulesDO? {
        try {
            return dynamoDBMapper!!.load(RulesDO::class.java, Constant.IDENTITY_ID)
        } catch (e: Exception) {
            Log.i(LOG_TAG, "Error : $e")
            return null
        }
    }

    fun checkEmailExist(emails:String):String{
        try{

            val emailCheck = HashMap<String, AttributeValue>()
            emailCheck[":val1"] = AttributeValue().withS(emails)
            val scanExpression = DynamoDBScanExpression()
                    .withFilterExpression("Email = :val1").withExpressionAttributeValues(emailCheck)
            var checkUserEmail = dynamoDBMapper!!.scan<RulesDO>(RulesDO::class.java,scanExpression)
            if(checkUserEmail != null){
                if(checkUserEmail[0].email == emails){
                    return "SUCCESS"
                }else{
                    return "ERROR"
                }
            }else return "ERROR"

        }catch (e:Exception){
            Log.i(LOG_TAG, "Error : $e")
            return "ERROR"
        }

    }


    fun updateUserVerification(userId: String,verified: String,email: String,personName:String) {
        try{
            val user =  dynamoDBMapper!!.load(RulesDO::class.java, userId)
            user.verified = verified
            user.email = email
            user.name = personName
            dynamoDBMapper!!.save<RulesDO>(user)
        }catch (e:Exception){
            Log.i("Error","error ")
        }
    }


    fun insertScheduleScene(userId :String, schedulingList: MutableList<SchedulingList>) {
        try {
            var scheduleTableDta = ArrayList<String>()
            for (schedule in schedulingList){
                scheduleTableDta.add(schedule.home!!)
                scheduleTableDta.add(schedule.schduleData[0].name.toString())
                scheduleTableDta.add(schedule.schduleData[0].time.toString())
                scheduleTableDta.add(schedule.schduleData[0].endTime.toString())
                scheduleTableDta.add(schedule.schduleData[0].routine.toString())
                scheduleTableDta.add(schedule.schduleData[0].load.toString())
            }
            var rulesDo = RulesDO()
            rulesDo.userId = userId
            //rulesDo.schedules = scheduleTableDta

            dynamoDBMapper!!.save<RulesDO>(rulesDo)
        } catch (e: Exception) {
            Log.i(LOG_TAG_SCH.toString(), "Error : $e")
        }

    }

    fun updateUserSchedule(userId :String, home:String,schedule:AutomationScene ,type:String,oldScheduleName:String) {
        try {
            var gson = Gson()

            var user = dynamoDBMapper!!.load(RulesDO::class.java , userId)
            var newUser = RulesDO()
            var SceneList : MutableList<MutableMap<String,MutableList<MutableMap<String,String>>>> = ArrayList()
            var Scene = HashMap<String,MutableList<MutableMap<String,String>>>()

            var deviceList : MutableList<MutableMap<String,String>> = ArrayList()
            var array = arrayOf(0,0,0,0)
            for(room in schedule.load){
                for(device in room.deviceList){
                    var map = HashMap<String,String>()
                    map["type"] = "device"
                    map["deviceName"] = device.deviceName
                    map["level"] = device.dimVal.toString()
                    map["index"] = device.index.toString()
                    map["state"] = device.isTurnOn.toString()
                    deviceList.add(map)
                }
            }
            var map = HashMap<String,String>()
            map["type"] = "schedule"
            if(schedule.type == "geo"){
                for(geoData in schedule.property){
                    map["stype"] = schedule.type.toString()
                    map["radius"] = geoData.newGeoRadius.toString()
                    map["latitude"] = geoData.newGeoLat.toString()
                    map["longitude"] = geoData.newGeolong.toString()
                    map["automationEnable"] = geoData.AutomationEnable.toString()
                    map["triggeringType"] = geoData.triggerType.toString()
                }
                map["name"] = schedule.name!!
                map["home"] = Constant.HOME!!
                map["icon"] = schedule.icon.toString()
                deviceList.add(map)
            }else{

                map["type"] = "schedule"
                map["name"] = schedule.name!!
                map["stype"] = schedule.type!!
                map["icon"] = schedule.icon.toString()
                map["routine"] = schedule.routine!!
                map["time"] = schedule.time!!
                map["endtime"] = schedule.endTime!!
                map["home"] = Constant.HOME!!
                map["automationEnable"] = schedule.property[0].AutomationEnable.toString()
                deviceList.add(map)
            }
            Scene[schedule.name!!] = deviceList

            if(user == null){
                SceneList.add(Scene)
                newUser.schedules = SceneList
                newUser.userId = userId
                dynamoDBMapper!!.save<RulesDO>(newUser)
            }else{
                if(user.schedules == null){
                    SceneList.add(Scene)
                    user.schedules = SceneList
                    user.userId = userId
                    dynamoDBMapper!!.save<RulesDO>(user)
                }else{
                    if(type == "edit"){
                        for(s in  user.schedules!!){
                            var keys = s.keys
                            var flag = false
                            for(k in keys){
                                if(k == oldScheduleName){
                                    flag = true
                                }
                            }
                            if(!flag){
                                SceneList.add(s)
                            }
                        }
                    }else{
                        SceneList = user.schedules!!
                    }
                    if(user!!.master != null){
                        for(modifiedCheck in user.master!!){
                            if(modifiedCheck["Home"] == home){
                                modifiedCheck["Status"] = "modified"
                            }

                        }
                    }
                    SceneList.add(Scene)
                    user.schedules = SceneList
                    user.userId = userId
                    dynamoDBMapper!!.save<RulesDO>(user)
                }
            }
            Log.i(LOG_TAG, "User Device Updated")
        } catch (e: Exception) {
            Log.i(LOG_TAG, "Error : $e")
        }
    }

    fun deleteUserSchedule(userId :String, home:String,name:String) {
        try {
            var gson = Gson()
            var user = dynamoDBMapper!!.load(RulesDO::class.java , userId)
            var SceneList : MutableList<MutableMap<String,MutableList<MutableMap<String,String>>>> = ArrayList()
            if(user == null){
            }else{
                if(user.schedules == null){
                }else{
                    for(s in  user.schedules!!){
                        var keys = s.keys
                        var flag = false
                        for(k in keys){
                            if(k == name){
                                flag = true
                            }
                        }
                        if(!flag){
                            SceneList.add(s)
                        }
                    }
                    if(user!!.master != null){
                        for(modifiedCheck in user.master!!){
                            if(modifiedCheck["Home"] == home){
                                modifiedCheck["Status"] = "modified"
                            }

                        }
                    }
                    user.schedules = SceneList
                    user.userId = userId
                    dynamoDBMapper!!.save<RulesDO>(user)
                }
            }
            Log.i(LOG_TAG, "User Device Updated")
        } catch (e: Exception) {
            Log.i(LOG_TAG, "Error : $e")
        }
    }

    fun deleteUserScene(userId :String, home:String,name:String) {
        try {
            var gson = Gson()
            var user = dynamoDBMapper!!.load(RulesDO::class.java , userId)
            var SceneList : MutableList<MutableMap<String,MutableList<MutableMap<String,String>>>> = ArrayList()
            if(user == null){
            }else{
                if(user.scenes == null){
                }else{
                    for(s in  user.scenes!!){
                        var keys = s.keys
                        var flag = false
                        for(k in keys){
                            if(k == name){
                                flag = true
                            }
                        }
                        if(!flag){
                            SceneList.add(s)
                        }
                    }
                    if(user!!.master != null){
                        for(modifiedCheck in user.master!!){
                            if(modifiedCheck["Home"] == home){
                                modifiedCheck["Status"] = "modified"
                            }

                        }
                    }
                    user.scenes = SceneList
                    user.userId = userId
                    dynamoDBMapper!!.save<RulesDO>(user)
                }
            }
            Log.i(LOG_TAG, "User Device Updated")
        } catch (e: Exception) {
            Log.i(LOG_TAG, "Error : $e")
        }
    }


    fun updateUserScene(userId :String, home:String,name:String,scene:MutableList<RoomModel>,icon:Int ,type:String,oldSceneName:String) {
        try {
            var gson = Gson()
            var user = dynamoDBMapper!!.load(RulesDO::class.java , userId)
            var newUser = RulesDO()
            var SceneList : MutableList<MutableMap<String,MutableList<MutableMap<String,String>>>> = ArrayList()
            var Scene = HashMap<String,MutableList<MutableMap<String,String>>>()

            var deviceList : MutableList<MutableMap<String,String>> = ArrayList()
            var array = arrayOf(0,0,0,0)
            for(room in scene){
                for(device in room.deviceList){
                    var map = HashMap<String,String>()
                    map["type"] = "device"
                    map["deviceName"] = device.deviceName
                    map["level"] = device.dimVal.toString()
                    map["index"] = device.index.toString()
                    map["state"] = device.isTurnOn.toString()
                    deviceList.add(map)
                }
            }
            var map = HashMap<String,String>()
            map["type"] = "scene"
            map["name"] = name
            map["icon"] = icon.toString()
            map["home"] = Constant.HOME!!
            deviceList.add(map)
            Scene[name] = deviceList

            if(user == null){
                SceneList.add(Scene)
                newUser.scenes = SceneList
                newUser.userId = userId
                dynamoDBMapper!!.save<RulesDO>(newUser)
            }else{
                if(user!!.scenes == null){
                    SceneList.add(Scene)
                    user.scenes = SceneList
                    dynamoDBMapper!!.save<RulesDO>(user)
                }
                else{
                    if(type == "edit"){
                        for(s in  user.scenes!!){
                            var keys = s.keys
                            var flag = false
                            for(k in keys){
                                if(k == oldSceneName){
                                    flag = true
                                }
                            }
                            if(!flag){
                                SceneList.add(s)
                            }
                        }

                    }else{
                        SceneList = user.scenes!!
                    }
                    if(user!!.master != null){
                        for(modifiedCheck in user.master!!){
                            if(modifiedCheck["Home"] == home){
                                modifiedCheck["Status"] = "modified"
                            }
                        }
                    }
                    SceneList.add(Scene)
                    user.scenes = SceneList
                    dynamoDBMapper!!.save<RulesDO>(user)
                }
            }
            Log.i(LOG_TAG, "User Device Updated")
        } catch (e: Exception) {
            Log.i(LOG_TAG, "Error : $e")
        }
    }

    fun deleteUserDevice(home:String,devices: MutableList<String>) {
        try {

            val user = dynamoDBMapper!!.load(RulesDO::class.java, Constant.IDENTITY_ID)

            var newDeviceList : MutableList<MutableMap<String,String>> = ArrayList()
            if(user != null){
                if(user.devices != null){
                    for(d in user.devices!!){
                        var flag = false
                        for(delDevice in devices){
                            if(delDevice == d["id"]){
                                flag = true
                            }
                        }
                        if(!flag){
                            newDeviceList.add(d)
                        }
                    }
                    user.devices = newDeviceList
                }
                if(user!!.master != null){
                    for(modifiedCheck in user.master!!){
                        if(modifiedCheck["Home"] == home){
                            modifiedCheck["Status"] = "modified"
                        }
                    }
                }

            }

            dynamoDBMapper!!.save<RulesDO>(user)
            return
        } catch (e: Exception) {
            Log.i(LOG_TAG, "Error : $e")
        }

    }

    fun updateHomeData(data: MutableList<RoomModelJson>, userId: String){
        var roomList : MutableList<MutableMap<String,String>> = ArrayList()
        var rulesDO = RulesDO()

        try{
            var user = dynamoDBMapper!!.load(RulesDO::class.java , userId)
            for (homes in data) {
                var map = HashMap<String,String>()
                map["bg"] = homes.bgUrl
                map["name"] = homes.name
                map["type"] = homes.type
                map["access"] = homes.sharedHome
                map["icon"] = homes.roomIcon.toString()
                roomList.add(map)
            }
            if(user == null){
                rulesDO.userId = userId
                rulesDO.homes = roomList
                dynamoDBMapper!!.save<RulesDO>(rulesDO)
            }else{
                if(user.homes == null){
                    user.homes = roomList
                    dynamoDBMapper!!.save<RulesDO>(user)
                }else{
                    user.homes = roomList
                    dynamoDBMapper!!.save<RulesDO>(user)
                }
            }
            if(user.guest != null){
                for(share in user.guest!!){
                    share[""]
                }
            }
            Log.i(LOG_TAG,"Home Inserted InTo DynamoDb")
        }catch (e:Exception){
            Log.i(LOG_TAG, "Error : $e")
        }
    }


    fun homeSQLtoDYNAMO(data:MutableList<RoomModelJson>, userId: String) {
        var roomList : MutableList<MutableMap<String,String>> = ArrayList()
        var rulesDO = RulesDO()
        var homeName : String ?= null

        try{
            var user = dynamoDBMapper!!.load(RulesDO::class.java , userId)
            for (homes in data) {
                var map = HashMap<String,String>()
                map["bg"] = homes.bgUrl
                map["name"] = homes.name
                map["type"] = homes.type
                map["access"] = homes.sharedHome
                map["icon"] = homes.roomIcon.toString()
                homeName  =homes.name
                roomList.add(map)
            }
            if(user == null){
                rulesDO.userId = userId
                rulesDO.homes = roomList
                dynamoDBMapper!!.save<RulesDO>(rulesDO)
            }else{
                if(user.homes == null){
                    user.homes = roomList
                    dynamoDBMapper!!.save<RulesDO>(user)
                }else{
                    user.homes = roomList

                    if(user!!.master != null){
                        for(modifiedCheck in user.master!!){
                            if(modifiedCheck["Home"] == Constant.HOME){
                                modifiedCheck["Status"] = "modified"
                            }

                        }
                    }

                    dynamoDBMapper!!.save<RulesDO>(user)
                }
            }

            Log.i(LOG_TAG,"Home Inserted InTo DynamoDb")
        }catch (e:Exception){
            Log.i(LOG_TAG, "Error : $e")
        }

    }

    fun shareDevices(sharemail: String, home: String): Boolean {
        try {
            thread {
                val owner =  getUser()
                if(owner == null){
                    returnError()
                }else{
                    val sharedMasterData = HashMap<String, String>()
                    sharedMasterData["Access"] = "invite"
                    sharedMasterData["Home"] = home
                    sharedMasterData["Email"] = sharemail
                    sharedMasterData["Name"] = sharemail
                    sharedMasterData["Status"] = "modified"
                    if (owner!!.master != null) {
                        var sharedFlagMasterAdd = true
                        for (share in owner!!.master!!) {
                            if((share["Email"] == sharemail) and (share["Home"] == home)){
                                share["Status"] = "modified"
                                sharedFlagMasterAdd = false
                            }
                        }
                        if(sharedFlagMasterAdd){
                            val sharedAccessForMaster = owner!!.master
                            sharedAccessForMaster?.add(sharedMasterData)
                            owner!!.master = (sharedAccessForMaster)
                        }
                    }else{
                        val newGuestList: MutableList<MutableMap<String, String>> = ArrayList()
                        newGuestList.add(sharedMasterData)
                        owner!!.master = (newGuestList)
                    }
                    dynamoDBMapper!!.save<RulesDO>(owner)
                }

            }
            return true
        } catch (e: Exception) {
            Log.i(LOG_TAG, "Error : $e")
            return false
        }
    }

    fun returnError():Boolean?{
        return false
    }

    fun deleteGuestAccessmaster(home: String, userId: String, email: String?) {
        var owner = dynamoDBMapper!!.load(RulesDO::class.java , userId)
        try{
            if(owner != null){
                if(owner!!.master != null){
                    for (sharing in owner!!.master!!) {
                        if ((sharing["Home"] == home) and (sharing["Email"] == email)) {
                            sharing["Status"] = "deleted"
                        }
                        dynamoDBMapper!!.save<RulesDO>(owner)
                    }
                    dynamoDBMapper!!.save<RulesDO>(owner)
                }
            }
        }catch (e:Exception){
            Log.i("Error","MASTER_DELETE_ERROR"+e)

        }

    }

    fun deleteGuestAccessFromMaster(home:String,userId:String) {
        var owner = dynamoDBMapper!!.load(RulesDO::class.java , userId)
        try{
            if(owner != null){
                if(owner!!.guest != null){
                    for (sharing in owner!!.guest!!) {
                        if (sharing["Home"] == home) {
                            sharing["Status"] = "deleted"
                        }
                    }
                    dynamoDBMapper!!.save<RulesDO>(owner)
                }
            }
        }catch (e:Exception){
            Log.i("Error","GUEST_DELETE_ERROR"+e)
        }


    }




    fun homeDYNAMOtoSQL(list :MutableList<MutableMap<String, String>>):MutableList<RoomModelJson>{
        var roomDetailsHome = ArrayList<RoomModelJson>()
        for(d in list){
            var item = RoomModelJson(d["name"].toString(),d["type"].toString(),d["access"].toString(),d["bg"].toString(),d["icon"]!!.toInt())
            roomDetailsHome.add(item)
        }
        return roomDetailsHome
    }

    fun deleteHomeDynamo(data:MutableList<RoomModelJson>,userId: String,home:String) {
        try {
            var roomList: MutableList<MutableMap<String, String>> = ArrayList()
            var SceneList : MutableList<MutableMap<String,MutableList<MutableMap<String,String>>>> = ArrayList()
            var ScheduleList : MutableList<MutableMap<String,MutableList<MutableMap<String,String>>>> = ArrayList()
            var devices :MutableList<MutableMap<String,String>> = ArrayList()
            var allLoads : MutableList<MutableList<MutableMap<String,String>>> = ArrayList()
            var rulesDO = RulesDO()

            var user = dynamoDBMapper!!.load(RulesDO::class.java, userId)
            for (homes in data) {
                var map = HashMap<String,String>()
                map["bg"] = homes.bgUrl
                map["name"] = homes.name
                map["type"] = homes.type
                map["access"] = homes.sharedHome
                map["icon"] = homes.roomIcon.toString()
                roomList.add(map)
            }
            if(user == null){
                rulesDO.userId = userId
                rulesDO.homes = roomList
                dynamoDBMapper!!.save<RulesDO>(user)
            }else{
                user.homes = roomList

                if(user.devices != null){
                    for(d in user.devices!!){
                        if(d["home"] != home){
                            devices.add(d)
                            if(user.loads != null){
                                for(l in user.loads!!){
                                    var flag = false
                                    for(load in l!!){
                                        if((load["type"] == "device") and (load["name"] == d["id"])){
                                            flag = true
                                            break
                                        }
                                    }
                                    if(!flag){
                                        allLoads.add(l)
                                        break
                                    }
                                }

                            }
                        }
                    }
                    user.devices = devices
                    user.loads = allLoads


                }
                if(user.scenes != null){
                    for(s in  user.scenes!!){
                        var keys = s.keys
                        var flag = false
                        var header:String?=null
                        for(k in keys){
                            header = k
                        }
                        var scene = s[header]
                        for(s in scene!!){
                            if((s["type"] == "scene") and (s["home"] == home)){
                                flag = true
                                break
                            }
                        }
                        if(!flag){
                            SceneList.add(s)
                        }
                    }
                    user.scenes = SceneList
                }
                if(user.schedules == null){
                    for(s in  user.schedules!!){
                        var keys = s.keys
                        var flag = false
                        var header:String?=null
                        for(k in keys){
                            header = k
                        }
                        var schedule = s[header]
                        for(s in schedule!!){
                            if((s["type"] == "schedule") and (s["home"] == home)){
                                flag = true
                                break
                            }
                        }
                        if(!flag){
                            ScheduleList.add(s)
                        }
                    }
                    user.schedules = ScheduleList
                }
                if(user!!.master != null){
                    for(modifiedCheck in user.master!!){
                        if(modifiedCheck["Home"] == Constant.HOME){
                            modifiedCheck["Status"] = "modified"
                        }

                    }
                }
                dynamoDBMapper!!.save<RulesDO>(user)

            }
        }catch (e:Exception){
            Log.i("Error","GUEST_DELETE_ERROR"+e)
        }
    }
    fun getRulesTable(): RulesDO? {
        try{
            var rules = dynamoDBMapper!!.load(RulesDO::class.java , Constant.IDENTITY_ID)
            if(rules == null){
                var Rules = RulesDO()
                Rules.userId = "ERROR"
                return Rules
            }else{
                return rules
            }
        } catch (e: Exception) {
            var Rules = RulesDO()
            Rules.userId = "ERROR"
            return Rules
        }

    }
    fun saveRulesTable(table:RulesDO): Boolean? {
        try{
            dynamoDBMapper!!.save<RulesDO>(table)
            return true
        } catch (e: Exception) {
            return false
        }
    }
    fun insertDeviceLoads(device: AuraSwitch) {
        try {
            var gson = Gson()
            var user = dynamoDBMapper!!.load(RulesDO::class.java , Constant.IDENTITY_ID)
            var newUser = RulesDO()
            var map = HashMap<String,String>()
            var deviceId = device.uiud.substring(0, Math.min(device.uiud.length, 12))
            map["id"] =  deviceId
            map["name"] = device.name
            map["thing"] = device.thing!!
            map["uiud"] = device.uiud
            map["room"] = device.room!!
            map["home"] = Constant.HOME!!
            var devices :MutableList<MutableMap<String,String>> = ArrayList()
            devices.add(map)
            var allLoads : MutableList<MutableList<MutableMap<String,String>>> = ArrayList()
            var loadList : MutableList<MutableMap<String,String>> = ArrayList()
            var m = HashMap<String,String>()
            m["device"] = deviceId
            m["type"] = "device"
            loadList.add(m)
            for(load in device.loads){
                var m = HashMap<String,String>()
                m["name"] = load.name!!
                m["dimmable"] = load.dimmable.toString()
                m["favourite"] = load.favourite.toString()
                m["icon"] = load.icon.toString()
                m["index"] = load.index.toString()
                m["device"] = deviceId
                m["type"] = "load"
                loadList.add(m)
            }
            allLoads.add(loadList)

            if(user == null){
                newUser.userId = Constant.IDENTITY_ID
                newUser.devices = devices
                newUser.loads = allLoads
            }else{
                if(user.devices == null){
                    user.devices = devices
                }else{
                    for(d in user.devices!!){
                         if(d["id"] != deviceId){
                             devices.add(d)
                         }
                    }
                    user.devices = devices
                }

                if(user.loads == null){
                    user.loads = allLoads
                }else{
                    for(l in user.loads!!){
                        var flag = false
                        for(load in l!!){
                            if((load["type"] == "device") and (load["device"] == deviceId)){
                                flag = true
                                break
                            }
                        }
                        if(!flag){
                            allLoads.add(l)
                        }
                    }

                    user.loads = allLoads
                }

            }
            if(user!!.master != null){
                for(modifiedCheck in user.master!!){
                    if(modifiedCheck["Home"] == Constant.HOME){
                        modifiedCheck["Status"] = "modified"

                    }

                }
            }

            dynamoDBMapper!!.save<RulesDO>(user)

            Log.i(LOG_TAG, "User Device Updated")
        } catch (e: Exception) {
            Log.i(LOG_TAG, "Error : $e")

        }
    }

    fun deleteDevice(device: AuraSwitch): String {
        try {
           // var gson = Gson()
            var user = dynamoDBMapper!!.load(RulesDO::class.java , Constant.IDENTITY_ID)
            var deviceId = device.uiud.substring(0, Math.min(device.uiud.length, 12))
            var devices :MutableList<MutableMap<String,String>> = ArrayList()
            var allLoads : MutableList<MutableList<MutableMap<String,String>>> = ArrayList()
            if(user != null){
                if(user.devices != null){
                    for(d in user.devices!!){
                        if(d["id"] != deviceId){
                            devices.add(d)
                        }
                    }
                    user.devices = devices
                }

                if(user.loads != null){
                    for(l in user.loads!!){
                        var flag = false
                        for(load in l!!){
                            if((load["type"] == "device") and (load["name"] == deviceId)){
                                flag = true
                            }
                        }
                        if(!flag){
                            allLoads.add(l)
                        }
                    }

                    user.loads = allLoads
                }
                if(user!!.master != null){
                    for(modifiedCheck in user.master!!){
                        if(modifiedCheck["Home"] == Constant.HOME!!){
                            modifiedCheck["Status"] = "modified"
                        }

                    }
                }
                dynamoDBMapper!!.save<RulesDO>(user)
            }


            Log.i(LOG_TAG, "User Device Updated")
            return "SUCCESS"
        } catch (e: Exception) {
            return "ERROR"
            Log.i(LOG_TAG, "Error : $e")
        }
    }
}