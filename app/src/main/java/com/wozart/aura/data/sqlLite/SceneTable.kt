package com.wozart.aura.data.sqlLite

import android.content.ContentValues
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.wozart.aura.utilities.Constant.Companion.CHECK_SCENE_QUERY
import com.wozart.aura.utilities.Constant.Companion.DELETE_SCENE
import com.wozart.aura.utilities.Constant.Companion.GET_SCENE_QUERY
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wozart.aura.entity.model.scene.Scene
import com.wozart.aura.entity.sql.scenes.SceneContract
import com.wozart.aura.ui.createautomation.RoomModel
import com.wozart.aura.ui.dashboard.Scenes
import com.wozart.aura.utilities.Constant.Companion.DELETE_HOME_SCENE_QUERY
import com.wozart.aura.utilities.Constant.Companion.DELETE_SCENE_DEVICE
import com.wozart.aura.utilities.Constant.Companion.GET_ALL_SCENES
import com.wozart.aura.utilities.Constant.Companion.GET_SCENE_FOR_ROOM_QUERY
import com.wozart.aura.utilities.Constant.Companion.GET_SCENE_QUERY_ROOM
import com.wozart.aura.utilities.Constant.Companion.GET_SELECTED_SCENE
import com.wozart.aura.utilities.Constant.Companion.UPDATE_ROOM_DETAILS_SCENE
import com.wozart.aura.utilities.Constant.Companion.UPDATE_SCENE_PARAMS
import org.jetbrains.anko.db.update
import org.json.JSONArray

/***************************************************************************
 * File Name :
 * Author : Aarth Tandel
 * Date of Creation : 08/06/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class SceneTable {

    fun insertScene(db: SQLiteDatabase, name: String, loads: String, rooms: String, home: String, icon : Int,oldScene: String,sceneType:String): Boolean {


        if(sceneType == "create"){

            var sceneExists = false
            val paramsCheck = arrayOf<String>(name)
            val cursorCheck = db.rawQuery(GET_SELECTED_SCENE, paramsCheck)
            while (cursorCheck.moveToNext()) {
                sceneExists = true

            }
            if(sceneExists){
                cursorCheck.close()
                return false
            }

            val cv = ContentValues()
            cv.put(SceneContract.SceneEntry.HOME, home)
            cv.put(SceneContract.SceneEntry.LOAD, loads)
            cv.put(SceneContract.SceneEntry.SCENE_ID, name)
            cv.put(SceneContract.SceneEntry.ROOM, rooms)
            cv.put(SceneContract.SceneEntry.ICON, icon)
            try {
                db.beginTransaction()
                db.insert(SceneContract.SceneEntry.TABLE_NAME, null, cv)
                db.setTransactionSuccessful()
            } catch (e: SQLException) {
                error("Error : $e")
            } finally {
                db.endTransaction()
            }
            return true
        }else {
            val params = arrayOf<String>(oldScene)
            val cursor = db.rawQuery(CHECK_SCENE_QUERY, params)
            val gson = Gson()
            if (cursor.count != 0) {
                val cnv = ContentValues()
                cnv.put(SceneContract.SceneEntry.ICON, icon)
                cnv.put(SceneContract.SceneEntry.LOAD, loads)
                cnv.put(SceneContract.SceneEntry.ROOM, rooms)
                cnv.put(SceneContract.SceneEntry.SCENE_ID, name)
                db.update(SceneContract.SceneEntry.TABLE_NAME, cnv, UPDATE_SCENE_PARAMS, arrayOf(oldScene))
                cursor.close()
                return true
            }
        }
       return true

    }
    fun updateScene(db: SQLiteDatabase, name: String, loads: String, rooms: String, home: String, icon : Int,oldScene: String): Boolean {
            var sceneExists = false
            val paramsCheck = arrayOf<String>(oldScene,home)
            val cursorCheck = db.rawQuery(GET_SELECTED_SCENE, paramsCheck)
            if (cursorCheck.count != 0) {
                val params = arrayOf(name,home)
                db.delete(SceneContract.SceneEntry.TABLE_NAME, DELETE_SCENE, params)
            }
            cursorCheck.close()

            val cv = ContentValues()
            cv.put(SceneContract.SceneEntry.HOME, home)
            cv.put(SceneContract.SceneEntry.LOAD, loads)
            cv.put(SceneContract.SceneEntry.SCENE_ID, name)
            cv.put(SceneContract.SceneEntry.ROOM, rooms)
            cv.put(SceneContract.SceneEntry.ICON, icon)
            try {
                db.beginTransaction()
                db.insert(SceneContract.SceneEntry.TABLE_NAME, null, cv)
                db.setTransactionSuccessful()
            } catch (e: SQLException) {
                //error("Error : $e")
                return false
            } finally {
                db.endTransaction()
            }
            return true


    }

    fun getAllScenes(db: SQLiteDatabase, home: String): ArrayList<Scene> {
        val params = arrayOf<String>(home)
        val cursor = db.rawQuery(GET_SCENE_QUERY, params)
        val Scenes: ArrayList<Scene> = ArrayList()
        val gson = Gson()
        while (cursor.moveToNext()) {
            val dummyScene = Scene()
            dummyScene.icon = Integer.valueOf(cursor.getString(2))
            dummyScene.name = cursor.getString(0)

            var room: ArrayList<RoomModel>
            val type = object : TypeToken<List<RoomModel>>() {}.type
            room = gson.fromJson(cursor.getString(1), type)

            dummyScene.room = room
            Scenes.add(dummyScene)
        }
        cursor.close()
        return Scenes
    }

    fun getSceneTable(db: SQLiteDatabase): ArrayList<Scene> {
        val cursor = db.rawQuery(GET_ALL_SCENES, null)
        val Scenes: ArrayList<Scene> = ArrayList()
        val gson = Gson()
        while (cursor.moveToNext()) {
            val dummyScene = Scene()
            dummyScene.home = cursor.getString(0)

            var room: ArrayList<RoomModel>
            val type = object : TypeToken<List<RoomModel>>() {}.type
            room = gson.fromJson(cursor.getString(1), type)
            dummyScene.room = room

            dummyScene.name = cursor.getString(2)
            dummyScene.roomName = cursor.getString(3)

            dummyScene.icon = Integer.valueOf(cursor.getString(4))
            Scenes.add(dummyScene)
        }
        cursor.close()
        return Scenes
    }

    //GET_SELECTED_SCENE
    fun getSceneByName(db: SQLiteDatabase, sceneName: String,home:String): Scene {
        val params = arrayOf<String>(sceneName,home)
        val cursor = db.rawQuery(GET_SELECTED_SCENE, params)
        val gson = Gson()
        val dummyScene = Scene()
        while (cursor.moveToNext()) {
            dummyScene.icon = Integer.valueOf(cursor.getString(2))
            dummyScene.name = cursor.getString(0)

            var room: ArrayList<RoomModel>
            val type = object : TypeToken<List<RoomModel>>() {}.type
            room = gson.fromJson(cursor.getString(1), type)

            dummyScene.room = room
        }
        cursor.close()
        return dummyScene
    }


    fun getSceneForRoom(db: SQLiteDatabase, home:String,roomName: String): ArrayList<Scene> {

        val params = arrayOf(home,roomName)
        val cursor = db.rawQuery(GET_SCENE_FOR_ROOM_QUERY, params)
        val Scenes: ArrayList<Scene> = ArrayList()
        val gson = Gson()
        while (cursor.moveToNext()) {
            val dummyScene = Scene()
            var room: ArrayList<RoomModel>
            val type = object : TypeToken<List<RoomModel>>() {}.type
            room = gson.fromJson(cursor.getString(1), type)
            dummyScene.name = cursor.getString(0)
            dummyScene.roomName = cursor.getString(2)
            dummyScene.icon = Integer.valueOf(cursor.getString(3))
            dummyScene.room = room
            Scenes.add(dummyScene)
//            var sceneExistFlag = false
//            for(scene in dummyScene.room){
//                if(scene.name == roomName){
//                    sceneExistFlag = true
//                }
//            }
//            if(sceneExistFlag){
//                Scenes.add(dummyScene)
//            }

        }
        cursor.close()
        return Scenes
    }


    fun renameRoomForScene(db: SQLiteDatabase, scene:String,roomNameOld: String,roomNameNew:String): ArrayList<Scene> {


        val params = arrayOf<String>(scene)
        val cursor = db.rawQuery(GET_SCENE_QUERY, params)
        val Scenes: ArrayList<Scene> = ArrayList()
        val gson = Gson()
        while (cursor.moveToNext()) {
            val dummyScene = Scene()
            dummyScene.name = cursor.getString(0)

            var room: ArrayList<RoomModel>
            val type = object : TypeToken<List<RoomModel>>() {}.type
            room = gson.fromJson(cursor.getString(1), type)

            dummyScene.icon = Integer.valueOf(cursor.getString(2))
            dummyScene.room = room
            var sceneExistFlag = false
            for(scene in dummyScene.room){
               // if(scene.name == roomName){
                    sceneExistFlag = true
                //}
            }
            if(sceneExistFlag){
                Scenes.add(dummyScene)
            }

        }
        cursor.close()
        return Scenes
    }

    fun updateRoom(db: SQLiteDatabase,roomOldName: String, roomNewName: String) {
        Log.d("ROOM_UPDATE","updateRoom")
        val cv = ContentValues()
        cv.put(SceneContract.SceneEntry.ROOM, roomNewName)
        db.update(SceneContract.SceneEntry.TABLE_NAME, cv, UPDATE_ROOM_DETAILS_SCENE, arrayOf(roomOldName))
    }


    fun deleteScene(db: SQLiteDatabase, sceneName: String,home:String) {
        val params = arrayOf(sceneName,home)
        db.delete(SceneContract.SceneEntry.TABLE_NAME, DELETE_SCENE, params)
    }
    fun deleteHomeScenes(db: SQLiteDatabase, homeName: String) {
        val params = arrayOf(homeName)
        db.delete(SceneContract.SceneEntry.TABLE_NAME, DELETE_HOME_SCENE_QUERY, params)
    }
    fun deleteTable(db: SQLiteDatabase){
        db.delete("scene", null, null)
    }

    fun deleteSceneDevice(db: SQLiteDatabase,sceneName: String,device:String){
        val params = arrayOf(sceneName,device)
        db.delete(SceneContract.SceneEntry.TABLE_NAME,DELETE_SCENE_DEVICE,params)

    }
}