package com.wozart.aura.entity.model.aura

import android.text.Editable
import java.io.Serializable

/***************************************************************************
 * File Name :
 * Author : Aarth Tandel
 * Date of Creation : 15/05/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class AuraSwitchLoad : Serializable {
    var name : String? = null
    var favourite : Boolean? = false
    var dimmable : Boolean? = false
    var icon : Int? = 0
    var index : Int ?= 0


    fun defaultLoadList() : MutableList<AuraSwitchLoad>{
        val loadList: MutableList<AuraSwitchLoad> = ArrayList()
        val load = AuraSwitchLoad()
        val load1 = AuraSwitchLoad()
        val load2 = AuraSwitchLoad()
        val load3 = AuraSwitchLoad()
        load1.name = "Light 1"
        load1.dimmable = true
        load1.favourite = true
        load1.icon = 0
        load1.index  = 0
        loadList.add(load1)

        load2.name = "Light 2"
        load2.dimmable = true
        load2.favourite = true
        load2.icon = 1
        load2.index = 1
        loadList.add(load2)

        load3.name = "Light 3"
        load3.dimmable = true
        load3.favourite = true
        load3.icon = 2
        load3.index = 2
        loadList.add(load3)

        load.name = "Switch"
        load.dimmable = false
        load.favourite = true
        load.icon = 4
        load.index = 3
        loadList.add(load)
        return loadList
    }
}