package com.wozart.aura.entity.service

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.preference.PreferenceManager
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.wozart.aura.utilities.Constant
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread
import com.amazonaws.mobileconnectors.iot.AWSIotMqttClientStatusCallback
import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos
import com.amazonaws.regions.Regions
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.util.*

/***************************************************************************
 * File Name :
 * Author : Aarth Tandel
 * Date of Creation : 30/04/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/

class AwsPubSub : Service() {
    internal var mBinder: IBinder = LocalAwsBinder()


    private var mqttManager: AWSIotMqttManager? = null
    private var clientId: String? = null

    private var credentialsProvider: CognitoCachingCredentialsProvider? = null

    inner class LocalAwsBinder : Binder() {
        fun getServerInstance(): AwsPubSub {
            return this@AwsPubSub
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        return mBinder
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val id = prefs.getString("ID", "NO_USER")
        val client_id = prefs.getString("CLIENT_ID", "NULL")

        if (client_id == "NULL") {
            val prefEditor = PreferenceManager.getDefaultSharedPreferences(this).edit()
            prefEditor.putString("CLIENT_ID", UUID.randomUUID().toString())
            prefEditor.apply()
        }

        clientId = client_id

        // Initialize the AWS Cognito credentials provider
        credentialsProvider = CognitoCachingCredentialsProvider(
                applicationContext, // context
                COGNITO_POOL_ID, // Identity Pool ID
                MY_REGION // Region
        )

        Constant.IDENTITY_ID = id
        mqttManager = AWSIotMqttManager(clientId!!, CUSTOMER_SPECIFIC_ENDPOINT)


        Thread(Runnable { Connect() }).start()
        return Service.START_NOT_STICKY
    }


    private fun Connect() {
        Log.d(LOG_TAG, "clientId = " + clientId!!)

        try {
            mqttManager!!.connect(credentialsProvider!!) { status, throwable ->
                Log.d(LOG_TAG, "Status = " + (status).toString())

                runOnUiThread {
                    if (status == AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.Connecting) {

                    } else if (status == AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.Connected) {
                        sendDataToActivity("Connected")
                    } else if (status == AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.Reconnecting) {
                        if (throwable != null) {
                            Log.d(LOG_TAG, "Connection error.", throwable)
                        }
                    } else if (status == AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.ConnectionLost) {
                        if (throwable != null) {
                            Log.d(LOG_TAG, "Connection error.", throwable)
                            throwable.printStackTrace()
                        }
                    }
                }
            }
        } catch (e: Exception) {
            Log.d(LOG_TAG, "Connection error: ", e)
        }

    }

    fun AwsSubscribe(device: String) {
        val topic = String.format(Constant.AWS_UPDATE_ACCEPTED, device)
        Log.d(LOG_TAG, "topic = $topic")

        try {
            mqttManager!!.subscribeToTopic(topic, AWSIotMqttQos.QOS0
            ) { topic, data ->
                runOnUiThread {
                    try {
                        val message = String(data, Charset.forName("UTF-8"))
                        Log.d(LOG_TAG, "Message arrived:")
                        Log.d(LOG_TAG, "   Topic: $topic")
                        Log.d(LOG_TAG, " Message: $message")
                        val segments = topic.split(("/").toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        sendDataToActivity(message + "/" + segments[2] + "/" + segments[5])
                    } catch (e: UnsupportedEncodingException) {
                        Log.d(LOG_TAG, "Message encoding error.", e)
                    }
                }
            }
        } catch (e: Exception) {
            Log.d(LOG_TAG, "Subscription error.", e)
        }

    }

    fun AWSPublish(device: String, data: String): Boolean {

        val topic = String.format(Constant.AWS_UPDATE, device)
        Log.d(LOG_TAG, "Data to Publish: $data")
        try {
            //if (mqttManager == null) {
                //sendDataToActivity("Reconnect")
           // } else {
                mqttManager!!.publishString(data, topic, AWSIotMqttQos.QOS0)
            return true
            //}
        }catch (e: Exception) {
            Log.d(LOG_TAG, "Publish error.", e)
            return false
        }

    }

    fun AwsGet(device: String) {
        val topic = String.format(Constant.AWS_GET_ACCEPTED, device)
        Log.d(LOG_TAG, "topic = $topic")

        try {
            mqttManager!!.subscribeToTopic(topic, AWSIotMqttQos.QOS0
            ) { topic, data ->
                runOnUiThread {
                    try {
                        val message = String(data, Charset.forName("UTF-8"))
                        Log.d(LOG_TAG, "Message arrived:")
                        Log.d(LOG_TAG, "   Topic: $topic")
                        Log.d(LOG_TAG, " Message: $message")
                        val segments = topic.split(("/").toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        sendDataToActivity(message + "/" + segments[2] + "/" + segments[5])
                    } catch (e: UnsupportedEncodingException) {
                        Log.d(LOG_TAG, "Message encoding error.", e)
                    }
                }
            }
        } catch (e: Exception) {
            Log.d(LOG_TAG, "Subscription error.", e)
        }

    }

    fun AwsGetPublish(device: String) {
        val topic = String.format(Constant.AWS_GET, device)
        val led = (System.currentTimeMillis() / 1000)
        val msg = "{\"state\":{\"desired\": {\"led\": $led}}}"

        try {
            mqttManager!!.publishString(msg, topic, AWSIotMqttQos.QOS0)
        } catch (e: Exception) {
            Log.d(LOG_TAG, "Publish error.", e)
        }

    }

    private fun sendDataToActivity(message: String) {
        val intent = Intent("AwsShadow")
        intent.putExtra("data", message)
        LocalBroadcastManager.getInstance(this@AwsPubSub).sendBroadcast(intent)
    }

    companion object {

        private val LOG_TAG = "AWS IoT PubSub"
        // --- Constants to modify per your configuration ---

        // Customer specific IoT endpoint
        private val CUSTOMER_SPECIFIC_ENDPOINT = "a15bui8ebaqvjn.iot.us-east-1.amazonaws.com"
        private val COGNITO_POOL_ID = "us-east-1:52da6706-7a78-41f4-950c-9d940b890788"

        // Region of AWS IoT
        private val MY_REGION = Regions.US_EAST_1
    }
}
