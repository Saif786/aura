package com.wozart.aura.entity.network

import android.util.Log
import com.wozart.aura.utilities.Constant
import com.wozart.aura.utilities.Encryption
import java.io.*
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.Socket

/*****************************************************************************
 * File Name : TcpClient
 * Author : Aarth Tandel
 * Date of Creation : 29/12/17
 * Description : TCP client to exchange JSON dat with Aura device
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 ******************************************************************************/

class TcpClient
/**
 * Constructor of the class. OnMessagedReceived listens for the messages received from server
 */
(listener: OnMessageReceived) {
    // message to send to the server
    private  var mServerMessage: String ?= null
    // sends message received notifications
    private var mMessageListener: OnMessageReceived?= null
    // while this is true, the server will continue running
    private var mRun = false
    // used to send messages
    private var mBufferOut: PrintWriter ?= null
    // used to read messages from the server
    private var mBufferIn: BufferedReader?= null

    private val TAG:String = "TCP_CLIENT"

    init {
        mMessageListener = listener
    }

    /**
     * Sends the message entered by client to the server
     *
     * @param message text entered by client
     */
    fun sendMessage(message: String) {
        Log.d(TAG,"  fun sendMessage(message: String) ")
        if (mBufferOut != null) {
            mBufferOut!!.println(message)
            mBufferOut!!.flush()
        }
    }

    /**
     * Close the connection and release the members
     */
    fun stopClient() {

        // send message that we are closing the connection
        Log.d(TAG,"fun stopClient() >> "+ Constant.CLOSED_CONNECTION)
        sendMessage(Constant.CLOSED_CONNECTION)

        mRun = false

        if (mBufferOut != null) {
            Log.d(TAG,"if (mBufferOut != null)")
            mBufferOut?.flush()
            mBufferOut?.close()
        }

        mMessageListener = null
        mBufferIn = null
        mBufferOut = null
        mServerMessage = null
    }

    fun run(data: String, ip: String) {

        mRun = true
        Log.d(TAG,"fun run(data: String, ip: String) > " + data + "  " + ip)
        val encryptedData = Encryption.encryptMessage(data)
        try {
            //here you must put your computer's IP address.
            val serverAddr = InetAddress.getByName(ip)

            //create a socket to make the connection with the server
            val socket = Socket()
            try {
                Log.d(TAG,"YEAS NEXT socket.connect(InetSocketAddress(serverAddr, SERVER_PORT), 5000)")
                socket.connect(InetSocketAddress(serverAddr, SERVER_PORT), 5000)
            } catch (e: Exception) {

                Log.d(TAG, "Error NO NEXT: $e")
            }

            try {
                Log.d(TAG,"NEXT CHECK : PrintWriter")
                //sends the message to the server
                mBufferOut = PrintWriter(BufferedWriter(OutputStreamWriter(socket.getOutputStream())), true)

                //receives the message which the server sends back
                mBufferIn = BufferedReader(InputStreamReader(socket.getInputStream()))
                // send login name
                Log.d(TAG," sendMessage(encryptedData)")
                sendMessage(encryptedData)
                Log.i("TCP Cleint", "TCP TX Data : $data")
                //in this while the client listens for the messages sent by the server
                var count = 0
                var messageFlag = false
                while (mRun && count < 1000 && !messageFlag) {

                    mServerMessage = mBufferIn?.readLine()
                    if (mServerMessage != null && mMessageListener != null) {
                        //call the method messageReceived from MyActivity class
                        Log.d(TAG,"  if (mServerMessage != null && mMessageListener != null)")
                        mMessageListener?.messageReceived(mServerMessage!!)
                        messageFlag = true
                    }
                    count++
                }
                if (!messageFlag) {
                    Log.d(TAG,"messageReceived(\"Server Not Reachable\")")
                    mMessageListener?.messageReceived("Server Not Reachable")
                }

            } catch (e: Exception) {
                Log.d(TAG, "S: Error", e)

            } finally {
                //the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                Log.d(TAG,"socket.close()    stopClient()")
                socket.close()
                stopClient()
            }

        } catch (e: Exception) {
            mMessageListener?.messageReceived(Constant.SERVER_NOT_REACHABLE)
            Log.d(TAG, "mMessageListener?.messageReceived(Constant.SERVER_NOT_REACHABLE)", e)
        }
    }

    //Declare the interface. The method messageReceived(String message) will must be implemented in the MyActivity
    //class at on asynckTask doInBackground
    interface OnMessageReceived {
        fun messageReceived(message: String)
    }

    companion object {
        val SERVER_PORT = 2345
    }
}
