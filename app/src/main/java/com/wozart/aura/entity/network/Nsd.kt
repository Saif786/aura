package aura.wozart.com.aura.entity.network

import android.content.Context
import android.content.Intent
import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import android.os.Handler
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.Toast
import java.util.ArrayList

/***************************************************************************
 * File Name : NsdClient
 * Author : Aarth Tandel
 * Date of Creation : 29/12/17
 * Description : Discovers local Aura Devices in network
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/

class Nsd(private val mContext: Context,type : String) {

    private val mNsdManager: NsdManager = mContext.getSystemService(Context.NSD_SERVICE) as NsdManager
    internal lateinit var mDiscoveryListener: NsdManager.DiscoveryListener
    private var discoveryStarted : Boolean = false
    private var globalCount:Boolean=true
    private var locationBroadcast:String=type

    fun initializeNsd() {
        initializeDiscoveryListener()
        discoveryStarted = true
        ReportServices!!.clear()
        arraydata!!.clear()
        ServicesAvailable!!.clear()
    }

    fun initializeDiscoveryListener() {
        mDiscoveryListener = object : NsdManager.DiscoveryListener {

            override fun onDiscoveryStarted(regType: String) {
                Log.d(TAG, "Service discovery started $regType")
                ServicesAvailable!!.clear()
            }

            override fun onServiceFound(service: NsdServiceInfo) {
                Log.d(TAG, "Service discovery success $service")

                if (service.serviceType != SERVICE_TYPE) {
                    Log.d(TAG, "Unknown Service Type: " + service.serviceType)
                } else if (service.serviceName == mServiceName) {
                    Log.d(TAG, "Same Machine: $mServiceName")
                } else if (service.serviceName.contains(mServiceName)) {
                    Log.d(TAG, "Resolving Services: $service")
                    arraydata!!.add(service)
                    if(globalCount){
                        getResoverResult()
                        globalCount = false
                    }
                    //mNsdManager.resolveService(service, initializeResolveListener())
                }
            }

            override fun onServiceLost(service: NsdServiceInfo) {
                Log.d(TAG, "service lost$service")
                for (x in ServicesAvailable!!){
                    if (x == service) {
                        ServicesAvailable = null
                    }
                }

            }

            override fun onDiscoveryStopped(serviceType: String) {
                Log.i(TAG, "Discovery stopped: $serviceType")
            }

            override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
               Log.d(TAG, "Discovery failed: Error code:$errorCode")
               // Toast.makeText(mContext,"Discovery Failed",Toast.LENGTH_SHORT).show()
                if(errorCode >= 4){

                }else{
                    mNsdManager.stopServiceDiscovery(this)
                }

            }

            override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
                Log.d(TAG, "Discovery failed: Error code:$errorCode")
                //Toast.makeText(mContext,"Discovery Failed",Toast.LENGTH_SHORT).show()
                if(errorCode >= 4){

                }else{
                    mNsdManager.stopServiceDiscovery(this)
                }

            }
        }
    }

    inner class initializeResolveListener : NsdManager.ResolveListener {

        override fun onResolveFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
           // Log.e(TAG, "Resolve failed $errorCode")
            //Toast.makeText(mContext,"Resolve failed",Toast.LENGTH_SHORT).show()
            when (errorCode) {
                NsdManager.FAILURE_ALREADY_ACTIVE -> {
                   Log.d(TAG, "FAILURE ALREADY ACTIVE")
                   // Toast.makeText(mContext,"Fail to Resolve",Toast.LENGTH_SHORT).show()
                   // mNsdManager.resolveService(serviceInfo, initializeResolveListener())
                }
                NsdManager.FAILURE_INTERNAL_ERROR -> Log.e(TAG, "FAILURE_INTERNAL_ERROR")
                NsdManager.FAILURE_MAX_LIMIT -> Log.e(TAG, "FAILURE_MAX_LIMIT")
            }
            if(arraydata!!.size != 0) {
                arraydata!!.removeAt(0)
            }
            setReolveFunction()
        }

        override fun onServiceResolved(serviceInfo: NsdServiceInfo) {
           Log.d(TAG, "Resolve Succeeded. $serviceInfo")
            //Toast.makeText(mContext,"Resolve Succeeded.",Toast.LENGTH_SHORT).show()

            if(arraydata!!.size != 0) {
                arraydata!!.removeAt(0)
            }

            try {
                ServicesAvailable!!.add(serviceInfo)
                formatAllServices()
            } catch (e: Exception) {
                //Log.e("NSD Client", "Error: $e")
                Toast.makeText(mContext,"Error in Discovery.",Toast.LENGTH_SHORT).show()
            }
            sendMessageToActivity(serviceInfo.serviceName,serviceInfo.host.hostAddress)
            setReolveFunction()

        }
    }
    fun formatAllServices(){
        ReportServices!!.clear()
        for(x in ServicesAvailable!!){
            var flagSameConditon = false
            for(y in ReportServices!!){
                  if(x.serviceName == y.serviceName){
                      flagSameConditon = true
                  }
            }
            if(!flagSameConditon){
                ReportServices!!.add(x)
            }
        }

    }
    fun setReolveFunction(){
        val nsdDiscoveryHandler = Handler()
        nsdDiscoveryHandler.postDelayed({
            getResoverResult()
        },50)
    }

    fun stopDiscovery() {
        mNsdManager.stopServiceDiscovery(mDiscoveryListener)
        discoveryStarted = false
    }

    fun getAllServices(): List<NsdServiceInfo>? {
        return ReportServices
    }


    fun discoverServices() {
        mNsdManager.discoverServices(
                SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, mDiscoveryListener)
    }

    fun isDeviceInLocalNetwork(device: String) : Boolean{
        for(x in ReportServices!!){
            if(x.serviceName.contains(device)){
                return true
            }
        }
        return false
    }

    fun getIP(device: String): String? {
        for (x in ReportServices!!) {
            if (x.serviceName.contains(device)) {
                return x.host.hostAddress
            }
        }
        return null
    }

    fun getServiceInfo(): List<NsdServiceInfo>? {
        return ReportServices
    }
    fun getResoverResult(){
        var dummy : NsdServiceInfo
        if(arraydata!!.size != 0) {
            dummy = arraydata!![0]
            mNsdManager.resolveService(dummy, initializeResolveListener())
        }else{
            globalCount = true
        }

    }
    fun getDiscoveryStatus() : Boolean{
        return discoveryStarted
    }
    fun setBroadcastType(type:String){
        locationBroadcast = type
    }

    private fun sendMessageToActivity(name :String,ip:String) {

        if(locationBroadcast == "HOME"){
            val intent = Intent("nsdDiscoverHome")
            intent.putExtra("name", name)
            intent.putExtra("ip", ip)
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent)
        }else if(locationBroadcast == "ROOM"){
            val intent = Intent("nsdDiscoverRoom")
            intent.putExtra("name", name)
            intent.putExtra("ip", ip)
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent)
        }else if(locationBroadcast == "WIFI"){
            val intent = Intent("nsdDiscoverWifi")
            intent.putExtra("name", name)
            intent.putExtra("ip", ip)
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent)
        }else if(locationBroadcast == "CUSTOMISE") {
            val intent = Intent("nsdDiscoverDeviceConnect")
            intent.putExtra("name", name)
            intent.putExtra("ip", ip)
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent)
        }
        else if(locationBroadcast == "ALL"){

//            val intent1 = Intent("nsdDiscoverHome")
//            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent1)
//            val intent2 = Intent("nsdDiscoverRoom")
//            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent2)
//            val intent3 = Intent("nsdDiscoverWifi")
//            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent3)
        }

    }

    companion object {

        //To find all the available networks SERVICE_TYPE = "_services._dns-sd._udp"
        //Aura devices use _hap._tcp.
        val SERVICE_TYPE = "_hap._tcp."
        val TAG = "NsdClient"
        private val mServiceName = "Aura"

        private var arraydata : MutableList<NsdServiceInfo>? = ArrayList()
        private var ServicesAvailable: MutableList<NsdServiceInfo>? = ArrayList()
        private var ReportServices: MutableList<NsdServiceInfo>? = ArrayList()

    }
}
