package com.wozart.aura.entity.model.aura

import java.io.Serializable

/***************************************************************************
 * File Name :
 * Author : Aarth Tandel
 * Date of Creation : 22/08/18
 * Description :
 * Revision History :
 * ____________________________________________________________________________
 * 29/12/17  Aarth Tandel - Initial Commit
 * ____________________________________________________________________________
 * 29/12/17 Version 1.0
 * ____________________________________________________________________________
 *
 *****************************************************************************/
class AuraLoad : Serializable {
    var name : String? = null
    var favourite : Boolean? = false
    var dimmable : Boolean? = false
    var icon : Int? = 0
    var index : Int ?= 0
    var status : Boolean? = false

    fun defaultLoadList() : MutableList<AuraLoad>{
        val loadList: MutableList<AuraLoad> = ArrayList()
        val load = AuraLoad()
        val load1 = AuraLoad()
        val load2 = AuraLoad()
        val load3 = AuraLoad()
        load1.name = "LED"
        load1.dimmable = true
        load1.favourite = true
        load1.icon = 0
        load1.index  = 0
        load1.status = false
        loadList.add(load1)

        load2.name = "Table Light"
        load2.dimmable = true
        load2.favourite = true
        load2.icon = 1
        load2.index = 1
        load1.status = false
        loadList.add(load2)

        load3.name = "Lamp"
        load3.dimmable = true
        load3.favourite = true
        load3.icon = 2
        load3.index = 2
        load1.status = false
        loadList.add(load3)

        load.name = "Switch"
        load.dimmable = false
        load.favourite = true
        load.icon = 4
        load.index = 3
        load1.status = false
        loadList.add(load)
        return loadList
    }
}