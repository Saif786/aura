package com.wozart.aura.entity.amazonaws.models.nosql


import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBAttribute
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBHashKey
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBTable

@DynamoDBTable(tableName = "wozartaura-mobilehub-1863763842-Rules")
class RulesDO {
    @get:DynamoDBHashKey(attributeName = "userId")
    @get:DynamoDBAttribute(attributeName = "userId")
    var userId: String? = null
    @get:DynamoDBAttribute(attributeName = "Devices")
    var devices:MutableList<MutableMap<String, String>>? = null
    @get:DynamoDBAttribute(attributeName = "Email")
    var email: String? = null
    @get:DynamoDBAttribute(attributeName = "Guest")
    var guest: MutableList<MutableMap<String, String>>? = null
    @get:DynamoDBAttribute(attributeName = "Homes")
    var homes: MutableList<MutableMap<String, String>>? = null
    @get:DynamoDBAttribute(attributeName = "List1")
    var list1: List<String>? = null
    @get:DynamoDBAttribute(attributeName = "List2")
    var list2: List<String>? = null
    @get:DynamoDBAttribute(attributeName = "Loads")
    var loads: MutableList<MutableList<MutableMap<String,String>>>? = null
    @get:DynamoDBAttribute(attributeName = "Master")
    var master: MutableList<MutableMap<String, String>>? = null
    @get:DynamoDBAttribute(attributeName = "Name")
    var name: String? = null
    @get:DynamoDBAttribute(attributeName = "Notifications")
    var notifications: List<String>? = null
    @get:DynamoDBAttribute(attributeName = "Scenes")
    var scenes: MutableList<MutableMap<String,MutableList<MutableMap<String,String>>>>? = null
    @get:DynamoDBAttribute(attributeName = "Schedules")
    var schedules: MutableList<MutableMap<String,MutableList<MutableMap<String,String>>>> ? = null
    @get:DynamoDBAttribute(attributeName = "String1")
    var string1: String? = null
    @get:DynamoDBAttribute(attributeName = "String2")
    var string2: String? = null
    @get:DynamoDBAttribute(attributeName = "UserThing")
    var userThing: String? = null
    @get:DynamoDBAttribute(attributeName = "MasterDevices")
    var masterDevices: MutableList<MutableMap<String, String>>? = null
    @get:DynamoDBAttribute(attributeName = "MasterHomes")
    var masterHomes: MutableList<MutableMap<String, String>>? = null
    @get:DynamoDBAttribute(attributeName = "MasterLoads")
    var masterLoads: MutableList<MutableList<MutableMap<String,String>>>? = null
    @get:DynamoDBAttribute(attributeName = "MasterScenes")
    var masterScenes:  MutableList<MutableMap<String,MutableList<MutableMap<String,String>>>>?  = null
    @get:DynamoDBAttribute(attributeName = "MasterSchedules")
    var masterSchedules: MutableList<MutableMap<String,MutableList<MutableMap<String,String>>>>? = null
    @get:DynamoDBAttribute(attributeName = "Verified")
    var verified: String? = null
}